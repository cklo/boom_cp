# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections, filter_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.extern.tabulate import tabulate
from boom.utils import hist_sum, yield_tuple, hist_max, rqcd_calc
from boom.plotting import derive_fake_shape_comp

# import logging
# logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(no_fake=True,no_signal=True)

cat = 'boost_loose_low'

categories = (cat+'_sr',cat+'_loose_loose_sr',)
channels   = ('1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','3p0n_1p1n','1p1n_3p0n',)

### define your selection objects 
sels = get_selections(
    channels= channels,
    years=('15', '16', '17', '18'),
    categories=categories,
    regions='same_sign')


# define your list of variables
variables = [ 
    VARIABLES['phi_star_5bins'],
    VARIABLES['met'],
    VARIABLES['tau_0_pt'],
    VARIABLES['tau_1_pt'],
    VARIABLES['mmc_mlm_m'],
    VARIABLES['jet_0_pt'],
    VARIABLES['ditau_dr'],
    VARIABLES['ditau_deta'],
    VARIABLES['higgs_pt'],

    ]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

cat_comp = [
     (cat+'_loose_loose_sr',cat+'_sr'),
]

for variable in variables:
    for channel in channels:
        lines = []
        data_minus_mc = {}
        for category in categories:
            sels_r = filter_selections(sels, categories=category, channels=channel)
            hdata = processor.get_hist_physics_process('Data', sels_r, variable)
            data = hdata.Integral()

            h_mcs = []
            mc = 0
            for p in processor.mc_backgrounds:
                h = processor.get_hist_physics_process(p.name, sels_r, variable)
                mc += h.Integral()
                h_mcs.append(h)

            h_mc = hist_sum(h_mcs)
            h_data_minus_mc = hdata.Clone()
            h_data_minus_mc.Add(h_mc, -1)
            #print '%s %f' %(category, h_data_minus_mc.Integral())
            data_minus_mc[category] = h_data_minus_mc

        for (sr, cr) in cat_comp:
            derive_fake_shape_comp( data_minus_mc[sr], data_minus_mc[cr] , variable, channel, cat, 'Loose-Loose', 'Medium-Medium')

print('closing stores...')
close_stores(physicsProcesses)
print('done')
