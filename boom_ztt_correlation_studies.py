# python imports
import os
import ROOT
import uuid
from multiprocessing import cpu_count

# HAPPy imports
from happy.plot import Plot
#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes()
physicsProcesses = [p for p in physicsProcesses if 'Ztt' in p.name]

channels=('1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','1p1n_3p0n','3p0n_1p1n')

category = 'preselection'

### define your selection objects 
sels = get_selections(
    channels=channels,
    regions='SR',
    categories=category)

### define your list of variables
var_x = VARIABLES['mmc_mlm_m']
var_y = VARIABLES['phi_star_3bins']

variables_x = [ var_x ]
variables_y = [ var_y ]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables_x, variables_y)
processor.book_2D()
processor.run(n_cores=cpu_count() - 1)

### plot making
hist = processor.get_hist_physics_process_2D('Ztt', sels, var_x, var_y)
final_hist = hist.Clone("final_hist")

#hist.Scale(1/hist.Integral())
for x in range(1,hist.GetNbinsX()+1):
    # get the integral for each column
    tot_x = 0
    for y in range(1,hist.GetNbinsY()+1):
        tot_x += hist.GetBinContent(x,y)
   
    for z in range(1,hist.GetNbinsY()+1):   
        if(tot_x != 0) : final_hist.SetBinContent(x,z,hist.GetBinContent(x,z)/tot_x)
        else : final_hist.SetBinContent(x,z,0)

plot = Plot(
        uuid.uuid4().hex,
        var_x,
        var_y)
plot.addHistogram(final_hist, 'COLZ')
plot.draw()
plot.saveAs('./plots/plot_ztt_correlation_studies_{}_{}_{}.pdf'.format(var_x.name,var_y.name, category))

print('closing stores...')
close_stores(physicsProcesses)
print('done')
