import os
from multiprocessing import cpu_count

def _systematic_list(args):
    from boom.batch import KINEMATICS, WEIGHT_VARIATIONS
    from boom.systematics.summary import _is_valid_systematic

    if args.var_group_name in list(KINEMATICS.keys()):
        _full_list = KINEMATICS[args.var_group_name]
    elif args.var_group_name in list(WEIGHT_VARIATIONS.keys()):
        _full_list = WEIGHT_VARIATIONS[args.var_group_name]
    else:
        raise ValueError(args.var_group_name)

    _final_list = []
    for _syst in _full_list:
        if _is_valid_systematic(
            _syst, 
            args.channel, 
            args.year,
            args.process_group_name,
            args.zcr):
            _final_list.append(_syst)
 
    return _final_list


def _wsi_file_name(args):
    from datetime import date

    _year = args.year
    if isinstance(_year, (list, tuple)):
        _year = ''.join(_year)

    _channel = args.channel
    if isinstance(_channel, (list, tuple)):
        _channel = ''.join(_channel)

    _region = 'sigreg'
    if args.zcr:
        _region = 'zllreg'
   
    _cpangles = args.cpangles 
    _var = args.var_group_name
    
    if args.process_group_name == None:
        _processes = 'all'
    else:
        _processes = args.process_group_name


    _date = '{}.{}.{}'.format(
        date.today().year,        
        date.today().month,
        date.today().day)

    identifier = 'zcr' if '_zcr' in args.json else 'sr'

    workspace_file = 'wsi_{identifier}_{year}_{cpangles}_{chan}decays_{reg}_{cpangles}_{var}_{processes}_{date}.root'.format(
        year=_year,
        identifier=identifier,
        chan=_channel,
        reg=_region,
        cpangles=_cpangles,
        var=_var,
        processes=_processes,
        date=_date)
    return workspace_file

def _hist_name(var_name, var_type):
    if var_name == 'nominal':
        return 'nominal'
    else:
        if var_type == 'up':
            return var_name + '_high'
        elif var_type == 'down':
            return var_name + '_low'
        else:
            raise ValueError
        
            

def _get_channels(channel_dict, args):
    from boom.selection_utils import get_selections
    _channel_sels = {}
    for key, _chan in list(channel_dict.items()):
        if args.channel != 'all':
            _channel_names = args.channel
            if not isinstance(_channel_names, (list, tuple)):
                _channel_names = [_channel_names]
            _include_channel = False
            for _name in _channel_names:
                if _name in key:
                    _include_channel = True
            if not _include_channel:
                continue

        if args.zcr:
            if'_ztt' not in key:
                continue
        else:
            if'_ztt' in key:
                continue

        _channel_sels[key] = get_selections(
            channels=_chan['channels'],
            categories=_chan['categories'],
            years=args.year,
            regions=_chan['regions'])
    return _channel_sels
    
def _get_processes(args):
    from boom.database import get_processes
       
    if 'cpangles' in args.process_group_name:
        _cpangles = True
    else:
        _cpangles = False 

    if args.process_group_name != None:
        from boom.batch import PROCESSES
        _specified_processes = PROCESSES[args.process_group_name]
    
    _split_z = False
    if 'Ztt' in args.process_group_name or 'Zll' in args.process_group_name:
        if len(_specified_processes) != 1:
            print('BOOM: activate z splitting')
            _split_z = True
   
    if 'nom' in args.ntuples_block:
        _add_he7_samples = True
        _add_mad_ztt = True 
        _add_ggh_fxfx = True
        _add_vbf_vh_fxfx = True
    else:
        _add_he7_samples = False
        _add_mad_ztt = False
        _add_ggh_fxfx = False
        _add_vbf_vh_fxfx = False

    if 'afii' in args.ntuples_block:
        _add_afii_samples = True
    else:
        _add_afii_samples = False

    if 'geo1' in args.ntuples_block:
        _add_geo1_samples = True
    else:
        _add_geo1_samples = False
 
    if 'geo2' in args.ntuples_block:
        _add_geo2_samples = True
    else:
        _add_geo2_samples = False   

    if 'geo3' in args.ntuples_block:
        _add_geo3_samples = True
    else:
        _add_geo3_samples = False 

    if 'physlist' in args.ntuples_block:
        _add_physlist_samples = True
    else:
        _add_physlist_samples = False
 
    _processes = get_processes(
        cpangles=_cpangles,
        split_ztt=_split_z,
        split_zll=_split_z,
        no_fake=args.zcr,
        ntuples_block=args.ntuples_block,
        add_he7_samples=_add_he7_samples,
        add_afii_samples=_add_afii_samples,
        add_mad_ztt=_add_mad_ztt,
        add_ggh_fxfx=_add_ggh_fxfx,
        add_vbf_vh_fxfx=_add_vbf_vh_fxfx,
        add_geo1_samples=_add_geo1_samples,
        add_geo2_samples=_add_geo2_samples,
        add_geo3_samples=_add_geo3_samples,
        add_physlist_samples=_add_physlist_samples)

    if args.process_group_name == None:
        return _processes
    else:
        _processes = [p for p in _processes if p.name in _specified_processes]
        return _processes

if __name__ == '__main__':

    from argparse import ArgumentParser
    parser = ArgumentParser(usage='%(prog)s [options]')
    parser.add_argument(
        '--year', default=['15', '16', '17', '18'], nargs='*', 
        choices=['15', '16', '17', '18'], help = "year (default: %(default)s)")
    parser.add_argument('--channel', default='all', nargs='*', choices=['hh'], help = "(default: %(default)s)")
    parser.add_argument('--zcr', default=False, action='store_true', help = "(default: %(default)s)")
    parser.add_argument('--cpangles', default='even', choices=['even', 'cpangles'], help = "(default: %(default)s)")
    parser.add_argument('--process-group-name', default=None, help = "(default: %(default)s)")
    parser.add_argument('--var-group-name', default='nominal')
    parser.add_argument('--json', default='data/wsi_sr.json', help = "(default: %(default)s)")
    parser.add_argument('--variable', default='phi_star_fit', help = "(default: %(default)s)")
    parser.add_argument(
        '--ntuples-block', 
        default='nom', 
        choices=['nom', 'sys_j1', 'sys_j2', 'sys_j4', 'sys_j5', 'sys_j6', 'sys_l', 'sys_trk', 'sys_afii', 'sys_geo1', 'sys_geo2', 'sys_geo3', 'sys_physlist'], 
        help="(default: %(default)s)")
    parser.add_argument('--queue', default='tomorrow')
    args = parser.parse_args()

    import json
    with open(args.json) as f:
        channel_dict = json.load(f)

        
    _channels_dict = _get_channels(channel_dict, args)
    _processes = _get_processes(args)

    _selections = []
    for key, _sels in list(_channels_dict.items()):
        _selections += _sels

    from boom.variables import VARIABLES
    _variable = VARIABLES[args.variable]

    if args.var_group_name == 'nominal':
        _syst = None
    else:
        _syst = _systematic_list(args)

    from boom.core import boom_processor
    _mc_processes = [p for p in _processes if p.isData == False and (p.isSignal == False or 'HWW' in p.name)]
    processor = boom_processor(
        _processes, 
        _selections,
        _variable,
        [], 
        _syst,
        force_fake_mc_subtraction=len(_mc_processes) != 0)
    processor.book()
    processor.run(n_cores=cpu_count() - 1)

    _wsi_input = _wsi_file_name(args)
    
    from boom.workspace import channel, make_skeleton, fill_wsi
    # assume for now that all channels have the same list of samples
    sample_names = [p.name for p in _processes]

    if not args.zcr:
        if len(processor.mc_backgrounds) != 0:
            if not 'Data' in sample_names:
                if not 'Fake' in sample_names:
                    sample_names += ['Fake_subtraction']

    write_lumi = True
    if args.var_group_name != 'nominal':
        write_lumi = False
    if not 'Data' in sample_names:
        write_lumi = False

    _channel_list = []
    for k in sorted(_channels_dict.keys()):
        chan = channel(k)
        chan.samples = sample_names
        chan.selections = _channels_dict[k]
        _channel_list.append(chan)

    make_skeleton(os.getcwd(), _wsi_input, _channel_list)
    if _syst == None:
        fill_wsi(
            os.getcwd(), 
            _wsi_input, 
            _channel_list, 
            _variable, 
            processor, 
            hist_name='nominal', 
            write_lumi=write_lumi,
            syst_name=_syst,
            syst_type='up',
            multi_processing=True)
    else:
        if not isinstance(_syst, (list, tuple)):
            _syst = [_syst]
        for _s in _syst:
            for _var_type in ('up', 'down'):
                fill_wsi(
                    os.getcwd(), 
                    _wsi_input, 
                    _channel_list, 
                    _variable, 
                    processor, 
                    hist_name=_hist_name(_s, _var_type), 
                    write_lumi=write_lumi,
                    syst_name=_s,
                    syst_type=_var_type,
                    multi_processing=True)
                    
    print('closing stores...')
    from boom.core import close_stores
    close_stores(_processes)
    print('done')

