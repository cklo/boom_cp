.. boom documentation master file, created by
   sphinx-quickstart on Fri Feb  8 14:42:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BOOM!
=====

Welcome to the BOOM analysis package.
Download it, run it and boom you get plots.

.. toctree::
   :hidden:

   install
   dependencies
   example

.. toctree::
   :hidden:
   :maxdepth: 1

   source/modules

.. include:: install.md


API
===

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Contributing
============

If you want to contribute to the project, we advise you start implementing your work into a steering script.

**To get started:**

* example script: `boom_hadhad_sr.py <https://gitlab.cern.ch/ATauLeptonAnalysiS/boom/blob/master/boom_hadhad_sr.py>`_
* template: :ref:`steering_example`

You can then submit a merge request through the GitLab interface of the project (https://gitlab.cern.ch/ATauLeptonAnalysiS/boom).


Authors
=======

* Antonio De Maria
* Pier-Olivier Deviveiros
* Quentin Buat


