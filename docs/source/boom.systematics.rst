boom.systematics package
========================

Submodules
----------

boom.systematics.base module
----------------------------

.. automodule:: boom.systematics.base
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.electron module
--------------------------------

.. automodule:: boom.systematics.electron
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.event module
-----------------------------

.. automodule:: boom.systematics.event
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.jet module
---------------------------

.. automodule:: boom.systematics.jet
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.kinematics module
----------------------------------

.. automodule:: boom.systematics.kinematics
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.muon module
----------------------------

.. automodule:: boom.systematics.muon
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.object\_weight\_dictionary module
--------------------------------------------------

.. automodule:: boom.systematics.object_weight_dictionary
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.summary module
-------------------------------

.. automodule:: boom.systematics.summary
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.systbook module
--------------------------------

.. automodule:: boom.systematics.systbook
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.tau module
---------------------------

.. automodule:: boom.systematics.tau
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.theory module
------------------------------

.. automodule:: boom.systematics.theory
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.trigger module
-------------------------------

.. automodule:: boom.systematics.trigger
    :members:
    :undoc-members:
    :show-inheritance:

boom.systematics.trigger\_weight\_dictionary module
---------------------------------------------------

.. automodule:: boom.systematics.trigger_weight_dictionary
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: boom.systematics
    :members:
    :undoc-members:
    :show-inheritance:
