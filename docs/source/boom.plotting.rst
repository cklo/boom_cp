boom.plotting package
=====================

Submodules
----------

boom.plotting.data\_mc module
-----------------------------

.. automodule:: boom.plotting.data_mc
    :members:
    :undoc-members:
    :show-inheritance:

boom.plotting.fake module
-------------------------

.. automodule:: boom.plotting.fake
    :members:
    :undoc-members:
    :show-inheritance:

boom.plotting.ff module
-----------------------

.. automodule:: boom.plotting.ff
    :members:
    :undoc-members:
    :show-inheritance:

boom.plotting.roc module
------------------------

.. automodule:: boom.plotting.roc
    :members:
    :undoc-members:
    :show-inheritance:

boom.plotting.rqcd module
-------------------------

.. automodule:: boom.plotting.rqcd
    :members:
    :undoc-members:
    :show-inheritance:

boom.plotting.stxs module
-------------------------

.. automodule:: boom.plotting.stxs
    :members:
    :undoc-members:
    :show-inheritance:

boom.plotting.zll module
------------------------

.. automodule:: boom.plotting.zll
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: boom.plotting
    :members:
    :undoc-members:
    :show-inheritance:
