boom package
============

Subpackages
-----------

.. toctree::

    boom.cuts
    boom.extern
    boom.plotting
    boom.systematics

Submodules
----------

boom.batch module
-----------------

.. automodule:: boom.batch
    :members:
    :undoc-members:
    :show-inheritance:

boom.core module
----------------

.. automodule:: boom.core
    :members:
    :undoc-members:
    :show-inheritance:

boom.database module
--------------------

.. automodule:: boom.database
    :members:
    :undoc-members:
    :show-inheritance:

boom.fake module
----------------

.. automodule:: boom.fake
    :members:
    :undoc-members:
    :show-inheritance:

boom.fakefactors module
-----------------------

.. automodule:: boom.fakefactors
    :members:
    :undoc-members:
    :show-inheritance:

boom.lumi module
----------------

.. automodule:: boom.lumi
    :members:
    :undoc-members:
    :show-inheritance:

boom.mva module
---------------

.. automodule:: boom.mva
    :members:
    :undoc-members:
    :show-inheritance:

boom.norm module
----------------

.. automodule:: boom.norm
    :members:
    :undoc-members:
    :show-inheritance:

boom.process\_generator module
------------------------------

.. automodule:: boom.process_generator
    :members:
    :undoc-members:
    :show-inheritance:

boom.selection module
---------------------

.. automodule:: boom.selection
    :members:
    :undoc-members:
    :show-inheritance:

boom.selection\_utils module
----------------------------

.. automodule:: boom.selection_utils
    :members:
    :undoc-members:
    :show-inheritance:

boom.stxs module
----------------

.. automodule:: boom.stxs
    :members:
    :undoc-members:
    :show-inheritance:

boom.utils module
-----------------

.. automodule:: boom.utils
    :members:
    :undoc-members:
    :show-inheritance:

boom.variables module
---------------------

.. automodule:: boom.variables
    :members:
    :undoc-members:
    :show-inheritance:

boom.workspace module
---------------------

.. automodule:: boom.workspace
    :members:
    :undoc-members:
    :show-inheritance:

boom.xsec\_sumofweights module
------------------------------

.. automodule:: boom.xsec_sumofweights
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: boom
    :members:
    :undoc-members:
    :show-inheritance:
