Getting Started
===============

Installation on lxplus
______________________

.. code-block:: bash

	setupATLAS
	lsetup "root 6.24.06-x86_64-centos7-gcc8-opt"
	git clone ssh://git@gitlab.cern.ch:7999/ATauLeptonAnalysiS/HAPPy.git
	git clone ssh://git@gitlab.cern.ch:7999/ATauLeptonAnalysiS/boom.git
	source HAPPy/setup.sh
	cd boom/

Ntuples
_______

We are currently working with the V01 ntuples, they are stored in eos at the following location.
location on lxplus

.. code-block:: bash	

   	/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/SM_Htautau_R21/V02/

Not on lxplus?
______________

You can run boom on any computer. To teach boom how to read the data, simply setup the `BOOM_NTUPLE_PATH` variable.

.. code-block:: bash	

    export BOOM_NTUPLE_PATH=/my/favorite/path/for/Htautau/ntuples/
    
The variable is meant to be a replacement for the EOS path mentionned above.
