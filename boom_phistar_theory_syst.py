# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_phistar_theory_syst_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

from boom.cuts.objects import CutTauIDMedium, Cut_1p0n_1p0n, Cut_1p0n_1p1n, Cut_1p1n_1p0n, Cut_1p1n_1p1n, Cut_1p1n_1pXn, Cut_1pXn_1p1n, Cut_1p0n_1pXn, Cut_1pXn_1p0n, Cut_3p0n_1p1n, Cut_1p1n_3p0n

Cut_1p0n_1p0n.cut.cut = 'tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 0'
Cut_1p0n_1p1n.cut.cut = 'tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 1'
Cut_1p1n_1p0n.cut.cut = 'tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 0'
Cut_1p1n_1p1n.cut.cut = 'tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 1'
Cut_1p1n_1pXn.cut.cut = 'tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 2'
Cut_1pXn_1p1n.cut.cut = 'tau_0_matched_decay_mode == 2 && tau_1_matched_decay_mode == 1'
Cut_1p0n_1pXn.cut.cut = 'tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 2'
Cut_1pXn_1p0n.cut.cut = 'tau_0_matched_decay_mode == 2 && tau_1_matched_decay_mode == 0'
Cut_3p0n_1p1n.cut.cut = 'tau_0_matched_decay_mode == 3 && tau_1_matched_decay_mode == 1'
Cut_1p1n_3p0n.cut.cut = 'tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 3'

CutTauIDMedium.cut.cut = '1'

# retrieve physics processes
physicsProcesses = get_processes(cpangles=True)

do_ggH_only    = False
do_VBFH_only   = False
do_WH_only     = False 
do_ZH_only     = False
do_ttH_only    = True

if do_ggH_only :
    physicsProcesses = [p for p in physicsProcesses if 'ggH' in p.name]
elif do_VBFH_only :
    physicsProcesses = [p for p in physicsProcesses if 'VBFH' in p.name]
elif do_WH_only :
    physicsProcesses = [p for p in physicsProcesses if 'WH' in p.name]
elif do_ZH_only :
    physicsProcesses = [p for p in physicsProcesses if 'ZH' in p.name]
elif do_ttH_only :
    physicsProcesses = [p for p in physicsProcesses if 'ttH' in p.name]
else:
    physicsProcesses = [p for p in physicsProcesses if 'ggH' in p.name or 'VBFH' in p.name or 'WH' in p.name or 'ZH' in p.name or 'ttH' in p.name]



_channel = ( '1p0n_1p0n',
             #'1p1n_1p0n',
             #'1p0n_1p1n',
             #'1p1n_1p1n',
             #'1p1n_1pXn',
             #'1pXn_1p1n'
             #'1p0n_1pXn',
             #'1pXn_1p0n',
             #'3p0n_1p1n',
             #'1p1n_3p0n',
             
           )

### define your selection objects 
sels = get_selections(
    channels=_channel,
    regions='SR',
    categories=('preselection'))

### define your list of variables
variables = [
    VARIABLES['phi_star_truth_3bins'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

title = 'Preselection'

### plot making
for var in variables:
    make_phistar_theory_syst_plot(processor, sels, _channel, var, title=title, do_ggH_only=do_ggH_only, do_VBFH_only=do_VBFH_only, do_WH_only=do_WH_only, do_ZH_only=do_ZH_only, do_ttH_only=do_ttH_only)

print('closing stores...')
close_stores(physicsProcesses)
print('done')
