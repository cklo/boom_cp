# python imports
import os
import ROOT

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_channel_comparison_plot
from boom import scheduler
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.lumi import LUMI 
from boom.database import get_processes, sum_of_weight_bin

# disable scheduler for cutflow
scheduler.isActive = False

import logging
logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes()
physicsProcesses = [p for p in physicsProcesses if p.name == 'ggH']

process = physicsProcesses[0]

## switch to the proper bin in the h_metadata histogram
sum_of_weight_bin(process)

do_raw_events = False

lumi = LUMI['16'] 

### define your selection objects 
sels = get_selections(
    channels=('1p3p', '3p1p', '3p3p'),
    years='16',
    regions='SR',
    categories='boost_loose')


cutlists = []
cutflows = []

for sel in sels:
    cuts = []
    for cut in sel._cut_list_mc:
        cuts.append(cut.cut)
    cutflow_list = []
    for dataset in process.datasets:
        if sel.is_valid(dataset):
            ### get the weights
            systset = sel.get_weight(process.name, dataset.isData, process.isSignal)
            if do_raw_events : cutflow = dataset.getCutFlowYields(cuts, ignoreWeights=True)
            else : cutflow = dataset.getCutFlowYields(cuts, systematicsSet=systset, luminosity=lumi )
            cutflow_list.append(cutflow)

    cutflow_process = {}
    for cut in list(cutflow_list[0].keys()):
        cutflow_process[cut] = sum([cutflow[cut][0] for cutflow in cutflow_list])
    cutflows.append(cutflow_process)
    cutlists.append(cuts)



print(len(cutlists))
print([len(cutlist) for cutlist in cutlists])
print(len(cutflows))
print(len(sels))
from boom.extern.tabulate import tabulate
all_cuts = []
yield_dict = {}
for sel, cutlist, cutflow in zip(sels, cutlists, cutflows):
    for c in cutlist:
        if not c in all_cuts:
            all_cuts.append(c)
            yield_dict[c] = {}
            yield_dict[c][sel] = cutflow[c]
        else:
            yield_dict[c][sel] = cutflow[c]

headers = ['cut'] + [sel.name for sel in sels] + ['sum']
lines = []
for c in all_cuts:
    line = [c.name]
    summed_yields = 0
    for sel in sels:
        if sel in list(yield_dict[c].keys()):
            line.append(yield_dict[c][sel])
            summed_yields+=yield_dict[c][sel]
        else:
            line.append('N/A')
    line.append(summed_yields)
    lines.append(line)

print(tabulate(lines, headers=headers))


