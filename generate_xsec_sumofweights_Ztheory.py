#local imports

def generate_weight(physics_processes, campaign='mc16a', ntuple_extension='vr', sumweight_sys =''):
    """
    """
    from boom.database import sum_of_weight_bin
    dsid_weight = []
    for process in physics_processes:
        if process.isData:
            continue
        sum_of_weight_bin(process, sumweight_sys)
        for dataset in process.datasets:
            if not campaign in dataset.name:
                continue
            if not ntuple_extension in dataset.name:
                continue
            dsid_weight.append([
                    dataset.dsid, 
                    dataset.effectiveCrossSection, float(dataset.sumOfWeights)])
    return dsid_weight




if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--ntuples-block', default='nom')
    parser.add_argument('--z-pdfsys-block', default=False)
    args = parser.parse_args()

    from boom.database import get_processes, ZttQCD
    physicsProcesses = get_processes(squash=False, ntuples_block=args.ntuples_block, xsec_weights_loading=False)
    campaigns = [
        'mc16a', 
        'mc16d', 
        'mc16e'
        ]

    streams = [
        'hh', 
        ]

    sys = [""]
    if args.z_pdfsys_block != '':
      sys= [      'theory_z_pdf_0'   ,
                  'theory_z_pdf_1'   ,
                  'theory_z_pdf_2'   ,
                  'theory_z_pdf_3'   ,
                  'theory_z_pdf_4'   ,
                  'theory_z_pdf_5'   ,
                  'theory_z_pdf_6'   ,
                  'theory_z_pdf_7'   ,
                  'theory_z_pdf_8'   ,
                  'theory_z_pdf_9'   ,

                  'theory_z_pdf_10'  ,
                  'theory_z_pdf_11'  ,
                  'theory_z_pdf_12'  ,
                  'theory_z_pdf_13'  ,
                  'theory_z_pdf_14'  ,
                  'theory_z_pdf_15'  ,
                  'theory_z_pdf_16'  ,
                  'theory_z_pdf_17'  ,
                  'theory_z_pdf_18'  ,
                  'theory_z_pdf_19'  ,

                  'theory_z_pdf_20'  ,
                  'theory_z_pdf_21'  ,
                  'theory_z_pdf_22'  ,
                  'theory_z_pdf_23'  ,
                  'theory_z_pdf_24'  ,
                  'theory_z_pdf_25'  ,
                  'theory_z_pdf_26'  ,
                  'theory_z_pdf_27'  ,
                  'theory_z_pdf_28'  ,
                  'theory_z_pdf_29'  ,

                  'theory_z_pdf_30'  ,
                  'theory_z_pdf_31'  ,
                  'theory_z_pdf_32'  ,
                  'theory_z_pdf_33'  ,
                  'theory_z_pdf_34'  ,
                  'theory_z_pdf_35'  ,
                  'theory_z_pdf_36'  ,
                  'theory_z_pdf_37'  ,
                  'theory_z_pdf_38'  ,
                  'theory_z_pdf_39'  ,

                  'theory_z_pdf_40'  ,
                  'theory_z_pdf_41'  ,
                  'theory_z_pdf_42'  ,
                  'theory_z_pdf_43'  ,
                  'theory_z_pdf_44'  ,
                  'theory_z_pdf_45'  ,
                  'theory_z_pdf_46'  ,
                  'theory_z_pdf_47'  ,
                  'theory_z_pdf_48'  ,
                  'theory_z_pdf_49'  ,

                  'theory_z_pdf_50'  ,
                  'theory_z_pdf_51'  ,
                  'theory_z_pdf_52'  ,
                  'theory_z_pdf_53'  ,
                  'theory_z_pdf_54'  ,
                  'theory_z_pdf_55'  ,
                  'theory_z_pdf_56'  ,
                  'theory_z_pdf_57'  ,
                  'theory_z_pdf_58'  ,
                  'theory_z_pdf_59'  ,

                  'theory_z_pdf_60'  ,
                  'theory_z_pdf_61'  ,
                  'theory_z_pdf_62'  ,
                  'theory_z_pdf_63'  ,
                  'theory_z_pdf_64'  ,
                  'theory_z_pdf_65'  ,
                  'theory_z_pdf_66'  ,
                  'theory_z_pdf_67'  ,
                  'theory_z_pdf_68'  ,
                  'theory_z_pdf_69'  ,

                  'theory_z_pdf_70'  ,
                  'theory_z_pdf_71'  ,
                  'theory_z_pdf_72'  ,
                  'theory_z_pdf_73'  ,
                  'theory_z_pdf_74'  ,
                  'theory_z_pdf_75'  ,
                  'theory_z_pdf_76'  ,
                  'theory_z_pdf_77'  ,
                  'theory_z_pdf_78'  ,
                  'theory_z_pdf_79'  ,

                  'theory_z_pdf_80'  ,
                  'theory_z_pdf_81'  ,
                  'theory_z_pdf_82'  ,
                  'theory_z_pdf_83'  ,
                  'theory_z_pdf_84'  ,
                  'theory_z_pdf_85'  ,
                  'theory_z_pdf_86'  ,
                  'theory_z_pdf_87'  ,
                  'theory_z_pdf_88'  ,
                  'theory_z_pdf_89'  ,

                  'theory_z_pdf_90'  ,
                  'theory_z_pdf_91'  ,
                  'theory_z_pdf_92'  ,
                  'theory_z_pdf_93'  ,
                  'theory_z_pdf_94'  ,
                  'theory_z_pdf_95'  ,
                  'theory_z_pdf_96'  ,
                  'theory_z_pdf_97'  ,
                  'theory_z_pdf_98'  ,
                  'theory_z_pdf_99'  ,

                  'theory_z_lhe3weight_mur05_muf05_pdf261000' ,
                  'theory_z_lhe3weight_mur05_muf1_pdf261000'  ,
                  'theory_z_lhe3weight_mur1_muf05_pdf261000'  ,
                  'theory_z_lhe3weight_mur1_muf2_pdf261000'   ,
                  'theory_z_lhe3weight_mur2_muf1_pdf261000'   ,
                  'theory_z_lhe3weight_mur2_muf2_pdf261000'   ,

                  'theory_z_CT14_pdfset'   ,
                  'theory_z_MMHT_pdfset'   ,
                  'theory_z_alphaS_up'   ,
                  'theory_z_alphaS_down'   ,]

      if sys == 'empty':
        print(args.z_pdfsys_block + " NOT FOUND")
        exit(0)

    print (sys)
    xsec_weights_dict = {}
    for _camp in campaigns:
      print(_camp)
      #if _camp == 'mc16a': 
      xsec_weights_dict[_camp] = {}
      print(xsec_weights_dict)

      first = True
      for _sys in sys :
        if _sys in ('theory_z_qsf', 'theory_z_ckk',):
          continue 
        print(_sys)
        for _stream in streams:
           dsid_xsec_sumofweights = generate_weight(physicsProcesses, campaign=_camp, ntuple_extension='_' + _stream , sumweight_sys =_sys) 
           dsid_xsec_sumofweights_nominal = generate_weight(physicsProcesses, campaign=_camp, ntuple_extension='_' + _stream , sumweight_sys ='nominal')

           for v in dsid_xsec_sumofweights: 

	     if (v[0] in ZttQCD):
               if first == True: 
 	         xsec_weights_dict[_camp][v[0]] = [] 
	       tmp = [x for x in dsid_xsec_sumofweights_nominal if x[0] == v[0]]
               if v[2] == 0 :  xsec_weights_dict[_camp][v[0]].append(1)
	       else : xsec_weights_dict[_camp][v[0]].append( tmp[0][2]/ v[2]) #dsid_xsec_sumofweights
               #print ( v[0], tmp[0][2], v[2], tmp[0][2]/ v[2] )

	first=False

    print(xsec_weights_dict)
    import json
    _filename = 'xsec_sumofweights_theoryZ.json' 
#      else: _filename = 'xsec_sumofweights_{}.json'.format(args.ntuples_block) 
    with open(_filename, 'w') as fout:
        json.dump(xsec_weights_dict, fout, indent=2)
