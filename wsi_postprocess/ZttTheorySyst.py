try:
  # command line agrument parser
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('-ws', '--wsInputFile', help='Name of the combined WS input root file')
  parser.add_argument('-o', '--overallSysFileName', default = "overallsystematics13TevZtt.txt", help='Name of the overall sys file for global yield variations')
  args = parser.parse_args()

  # have to import root after the parser
  from ROOT import TFile, TH1D, TCanvas, Double, kBlack, kRed, kGreen, kBlue, gROOT
  from wsPostProcess import *
  from math import sqrt
  import os, sys

  # open the WS inpt file
  f    = TFile.Open(args.wsInputFile)
  
  print "Loading WS input folder tree..."
  tree = readFolderTree(f)
  print "...done"
  
  # names of the systematic variation histograms to be processed (without _high/low suffix) and the corresponding samples
  variations  = [
                 'theory_z_ckk', 
                 'theory_z_qsf', 
                 'theory_z_mur_muf_envelope', 
                 'theory_z_alphaS', 
                 'theory_z_alt_pdf_envelope', 
                 'theory_z_pdf_envelope',
                 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P0N_RECO_1P0N_TOTAL',
                 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1P0N_TOTAL',
                 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1P1N_TOTAL',
                 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1PXN_TOTAL',
                 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1PXN_RECO_1P1N_TOTAL',
                 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1PXN_RECO_1PXN_TOTAL',
                 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_3P0N_RECO_3P0N_TOTAL',
                 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_3PXN_RECO_3P0N_TOTAL',
                ]

  sample = 'ZttQCD'

  # names of the categories (cba and mva)
  #categories = ['boost', 'vbf', 'boost_0', 'vbf_0']
  categories = ['boost'] #, 'vbf_0']  

  # define indices for easier access of the lists
  chNames = ['hh']
  hh = 0
  
 
  # define CBA and MVA regions
  regionsDict = { 'boost' :  [
                              ('HH SR BOOST Loose A1 rho high',      hh,  ['hh_cba_boost_loose_signal_a1rho_high']),
                              ('HH SR BOOST Loose A1 rho low',       hh,  ['hh_cba_boost_loose_signal_a1rho_low']),
                              ('HH SR BOOST Loose ipip high',        hh,  ['hh_cba_boost_loose_signal_ipip_d0sig_high']),
                              ('HH SR BOOST Loose ipip low',         hh,  ['hh_cba_boost_loose_signal_ipip_d0sig_low']),
                              ('HH SR BOOST Loose iprho high',       hh,  ['hh_cba_boost_loose_signal_iprho_d0sigy_high']),
                              ('HH SR BOOST Loose iprho low',        hh,  ['hh_cba_boost_loose_signal_iprho_d0sigy_low']),
                              ('HH SR BOOST Loose rhorho 1pXn high', hh,  ['hh_cba_boost_loose_signal_rhorho_1pXn_high']),
                              ('HH SR BOOST Loose rhorho 1pXn low',  hh,  ['hh_cba_boost_loose_signal_rhorho_1pXn_low']),
                              ('HH SR BOOST Loose rhorho high',      hh,  ['hh_cba_boost_loose_signal_rhorho_y0y1_high']),
                              ('HH SR BOOST Loose rhorho low',       hh,  ['hh_cba_boost_loose_signal_rhorho_y0y1_low']),
                              ('HH SR BOOST Loose iprho 1pXn high',  hh,  ['hh_cba_boost_loose_signal_iprho_1pXn_high']),
                              ('HH SR BOOST Loose iprho 1pXn high',  hh,  ['hh_cba_boost_loose_signal_iprho_1pXn_low']),
                              ('HH SR BOOST Tight A1 rho high',      hh,  ['hh_cba_boost_tight_signal_a1rho_high']),
                              ('HH SR BOOST Tight A1 rho low',       hh,  ['hh_cba_boost_tight_signal_a1rho_low']),
                              ('HH SR BOOST Tight ipip high',        hh,  ['hh_cba_boost_tight_signal_ipip_d0sig_high']),
                              ('HH SR BOOST Tight ipip low',         hh,  ['hh_cba_boost_tight_signal_ipip_d0sig_low']),
                              ('HH SR BOOST Tight iprho high',       hh,  ['hh_cba_boost_tight_signal_iprho_d0sigy_high']),
                              ('HH SR BOOST Tight iprho low',        hh,  ['hh_cba_boost_tight_signal_iprho_d0sigy_low']),
                              ('HH SR BOOST Tight rhorho 1pXn high', hh,  ['hh_cba_boost_tight_signal_rhorho_1pXn_high']),
                              ('HH SR BOOST Tight rhorho 1pXn low',  hh,  ['hh_cba_boost_tight_signal_rhorho_1pXn_low']),
                              ('HH SR BOOST Tight rhorho high',      hh,  ['hh_cba_boost_tight_signal_rhorho_y0y1_high']),
                              ('HH SR BOOST Tight rhorho low',       hh,  ['hh_cba_boost_tight_signal_rhorho_y0y1_low']),
                              ('HH SR BOOST Tight iprho 1pXn high',  hh,  ['hh_cba_boost_tight_signal_iprho_1pXn_high']),
                              ('HH SR BOOST Tight iprho 1pXn high',  hh,  ['hh_cba_boost_tight_signal_iprho_1pXn_low']),
                              ('HH SR BOOST 0',                      hh,  ['hh_cba_boost_zcr_0'])
                              
                              ],
 
                   #'boost_0' : [ ('HH SR BOOST 0',      hh,  ['hh_cba_boost_zcr_0'])],
  
                   'vbf' :    [
                               ('HH SR VBF 0 A1 rho high',      hh,  ['hh_cba_vbf_0_signal_a1rho_high']),
                               ('HH SR VBF 0 A1 rho low',       hh,  ['hh_cba_vbf_0_signal_a1rho_low']),
                               ('HH SR VBF 0 ipip high',        hh,  ['hh_cba_vbf_0_signal_ipip_d0sig_high']),
                               ('HH SR VBF 0 ipip low',         hh,  ['hh_cba_vbf_0_signal_ipip_d0sig_low']),
                               ('HH SR VBF 0 iprho high',       hh,  ['hh_cba_vbf_0_signal_iprho_d0sigy_high']),
                               ('HH SR VBF 0 iprho low',        hh,  ['hh_cba_vbf_0_signal_iprho_d0sigy_low']),
                               ('HH SR VBF 0 rhorho 1pXn high', hh,  ['hh_cba_vbf_0_signal_rhorho_1pXn_high']),
                               ('HH SR VBF 0 rhorho 1pXn low',  hh,  ['hh_cba_vbf_0_signal_rhorho_1pXn_low']),
                               ('HH SR VBF 0 rhorho high',      hh,  ['hh_cba_vbf_0_signal_rhorho_y0y1_high']),
                               ('HH SR VBF 0 rhorho low',       hh,  ['hh_cba_vbf_0_signal_rhorho_y0y1_low']),
                               ('HH SR VBF 0 iprho 1pXn high',  hh,  ['hh_cba_vbf_0_signal_iprho_1pXn_high']),
                               ('HH SR VBF 0 iprho 1pXn low',   hh,  ['hh_cba_vbf_0_signal_iprho_1pXn_low']),
            
                               ('HH SR VBF 1 A1 rho high',      hh,  ['hh_cba_vbf_1_signal_a1rho_high']),
                               ('HH SR VBF 1 A1 rho low',       hh,  ['hh_cba_vbf_1_signal_a1rho_low']),
                               ('HH SR VBF 1 ipip high',        hh,  ['hh_cba_vbf_1_signal_ipip_d0sig_high']),
                               ('HH SR VBF 1 ipip low',         hh,  ['hh_cba_vbf_1_signal_ipip_d0sig_low']),
                               ('HH SR VBF 1 iprho high',       hh,  ['hh_cba_vbf_1_signal_iprho_d0sigy_high']),
                               ('HH SR VBF 1 iprho low',        hh,  ['hh_cba_vbf_1_signal_iprho_d0sigy_low']),
                               ('HH SR VBF 1 rhorho 1pXn high', hh,  ['hh_cba_vbf_1_signal_rhorho_1pXn_high']),
                               ('HH SR VBF 1 rhorho 1pXn low',  hh,  ['hh_cba_vbf_1_signal_rhorho_1pXn_low']),
                               ('HH SR VBF 1 rhorho high',      hh,  ['hh_cba_vbf_1_signal_rhorho_y0y1_high']),
                               ('HH SR VBF 1 rhorho low',       hh,  ['hh_cba_vbf_1_signal_rhorho_y0y1_low']),
                               ('HH SR VBF 1 iprho 1pXn high',  hh,  ['hh_cba_vbf_1_signal_iprho_1pXn_high']),
                               ('HH SR VBF 1 iprho 1pXn low',   hh,  ['hh_cba_vbf_1_signal_iprho_1pXn_low']),
                               ('HH VBF 0',      hh,  ['hh_cba_vbf_zcr_0'])           

                              ],


                                
                   #'vbf_0' : [ ('HH VBF 0',      hh,  ['hh_cba_vbf_zcr_0'])],
                                
                              }

  # overall syst file content
  lines = []
  
  # loop over categories
  for category in categories:
    
    # define which regions are used in the fit
    regions = regionsDict[category]
    
    # loop over variation (CKKW, QSF, etc)
    for variation in variations:
      # initialize the global normalization factors
      totalYield     = 0
      totalYieldHigh = 0
      totalYieldLow  = 0
      
      #initialize the per-channel normalization factors
      channelYield     = [0]
      channelYieldHigh = [0]
      channelYieldLow  = [0]
      
  
      # loop over regions
      allRegionsAvailable = True
      for name,ch,subRegions in regions:
        #initialize variables to hold yields of the sub-categories
        nominal = 0  # nominal yield in the region
        varHigh = 0   # up-variation yield in the region
        varLow  = 0   # down-variation yield in the region
        
        # loop over sub-regions and sum up the yields
        for region in subRegions:
          #retrieve plots
          if region in tree:
            plotNominal = tree[region][sample]['nominal']
            if '{}_high'.format(variation) in tree[region][sample]:
              plotHigh    = tree[region][sample]['{}_high'.format(variation)]
            if '{}_low'.format(variation) in tree[region][sample]:
              plotLow     = tree[region][sample]['{}_low'.format(variation)]
            
	        # calculate integral yields
            nominal += plotNominal.Integral()
            if '{}_high'.format(variation) in tree[region][sample]:
              varHigh += plotHigh.Integral()
            if '{}_low'.format(variation) in tree[region][sample]:
              varLow  += plotLow.Integral()
          
          else:
            allRegionsAvailable = False
          
        # add the yields up to get the total yield accross all the regions
        totalYield     += nominal
        totalYieldHigh += varHigh
        totalYieldLow  += varLow
        
        # add the yields up to get the "per-channel" yields
        # N.B. we are only using both signal and control regions to get the totalYield
        channelYield    [ch] += nominal
        channelYieldHigh[ch] += varHigh
        channelYieldLow [ch] += varLow
      
      # calculate relative variations between channels
      if allRegionsAvailable:
        print "Global yield variations of {} in {} category".format(variation, category)
        for ch in [hh]:
          print "  {}_{}_{}_high {}".format(chNames[ch], category, variation, (channelYieldHigh[ch] / channelYield[ch] * totalYield / totalYieldHigh))
          if totalYieldLow!= 0:
            print "  {}_{}_{}_low  {}".format(chNames[ch], category, variation, (channelYieldLow[ch]  / channelYield[ch] * totalYield / totalYieldLow))
          else:
            print "totalYieldLow = 0 is expected for MGvsSH (is onesided)"
            print "  {}_{}_{}_low  {}".format(chNames[ch], category, variation, 2-(channelYieldHigh[ch] / channelYield[ch] * totalYield / totalYieldHigh))
            
          #also store in the txt file
          lines += [ "{}_{}_{}_high {}\n".format(chNames[ch], category, variation, (channelYieldHigh[ch] / channelYield[ch] * totalYield / totalYieldHigh)) ]
          if totalYieldLow!= 0:
            lines += [ "{}_{}_{}_low  {}\n".format(chNames[ch], category, variation, (channelYieldLow[ch]  / channelYield[ch] * totalYield / totalYieldLow)) ]
          else:
            lines += [ "{}_{}_{}_low  {}\n".format(chNames[ch], category, variation, 2-(channelYieldHigh[ch] / channelYield[ch] * totalYield / totalYieldHigh)) ]
        
      # Rescale the original variation hostograms in the WS inputs so that they do not change the total yield in each channel
      for name,ch,subRegions in regions:
        for region in subRegions:
          if region in tree:
            # clone the original histograms
            if '{}_high'.format(variation) in tree[region][sample]:
              newPlotHigh = tree[region][sample]['{}_high'.format(variation)].Clone('{}_full_constrained_high'.format(variation))
            if '{}_low'.format(variation) in tree[region][sample]:
              newPlotLow  = tree[region][sample]['{}_low' .format(variation)].Clone('{}_full_constrained_low' .format(variation))
            
            # now do the actual rescaling (N.B. different scale used for different channels)
            if '{}_high'.format(variation) in tree[region][sample]:
              newPlotHigh.Scale(  channelYield[ch]/channelYieldHigh[ch] )
            if '{}_low'.format(variation) in tree[region][sample]:
              newPlotLow .Scale(  channelYield[ch]/channelYieldLow [ch] )
                  
            # store the new plots in the WS input folder tree
            if '{}_high'.format(variation) in tree[region][sample]:
              tree[region][sample][newPlotHigh.GetName()] = newPlotHigh
            if '{}_low'.format(variation) in tree[region][sample]:
              tree[region][sample][newPlotLow .GetName()] = newPlotLow
  
  # perform sanity checks: check if the channel yield after the constrain match the nominal
  print "Sanity check for the rescaled variation histograms: (N.B. constrained variation yields in each channel should be the same as nominal)"
  for category in categories:
    regions = regionsDict[category]
    for variation in variations:
      channelYield                = [0]
      channelYieldConstrainedHigh = [0]
      channelYieldConstrainedLow  = [0]
      for name,ch,subRegions in regions:
        nominal = 0; varHigh = 0; varLow  = 0
        for region in subRegions:
          if region in tree:
            nominal += tree[region][sample]['nominal'].Integral()
            if '{}_full_constrained_high'.format(variation) in tree[region][sample]:
              varHigh += tree[region][sample]['{}_full_constrained_high'.format(variation)].Integral()
            if '{}_full_constrained_low'.format(variation) in tree[region][sample]:
              varLow  += tree[region][sample]['{}_full_constrained_low'.format(variation)].Integral()
        channelYield               [ch] += nominal
        channelYieldConstrainedHigh[ch] += varHigh
        channelYieldConstrainedLow [ch] += varLow
      for ch in [hh]:
        if channelYield[ch]!=0:
          if variation=="theory_ztt_MGvsSh":
            print "{} {}: nominal {}, {}_full_constrained_high {}".format(category, chNames[ch], channelYield[ch], variation, channelYieldConstrainedHigh[ch])
          else:
            print "{} {}: nominal {}, {}_full_constrained_high {}, {}_full_constrained_low {}".format(category, chNames[ch], channelYield[ch], variation, channelYieldConstrainedHigh[ch], variation, channelYieldConstrainedLow [ch])
      
  
  # finally, save the modified WS input tree and overallsyst file:
  if '.root' in args.wsInputFile:
    newFileName = args.wsInputFile.replace('.root','_zttTheoryFullConstrained_zttDecayModeFullConstrained.root')
  else:
    newFileName = args.wsInputFile + '_zttTheoryFullConstrained_zttDecayModeFullConstrained'

  print "Creating the processed WS input file {} ...".format(newFileName)
  fOut = TFile.Open(newFileName, 'recreate')
  save(tree, fOut)
  gROOT.GetListOfFiles().Remove(fOut)
  fOut.Close()
  print "...done"
  
  if len(lines)!=0:
    print "Creating the overall sys file {} ...".format(args.overallSysFileName)
    with open(args.overallSysFileName,"w") as ff:
      for line in lines:
        ff.write(line)
       
    print "...done"

except SystemExit as e:
  pass
except Exception:
  raise


# the end
