Welcome to the BOOM_CP analysis package.
Download it, run it and boom you get plots.

# [Documentation](http://cern.ch/boom-doc)

The documentation of the project lives here: http://cern.ch/boom-doc

## Installation on lxplus (to be adapted for other computer systems)
```bash
setupATLAS
lsetup "root 6.28.00-x86_64-centos7-gcc11-opt"
git clone ssh://git@gitlab.cern.ch:7999/ATauLeptonAnalysiS/HAPPy.git
git clone ssh://git@gitlab.cern.ch:7999/ATauLeptonAnalysiS/boom_CP.git
cd HAPPy
source setup.sh
cd ../boom_CP/
g++ -g MergeWorkspaceOutputs.cxx -o MergeWorkspaceOutputs `root-config --cflags --libs` -L $ROOTSYS/lib -Wall -pedantic -Ofast
```
# [Important analysis reference]
```bash
theory paper (https://arxiv.org/abs/1510.03850)

HLepton status report (https://indico.cern.ch/event/901642/contributions/3797763/attachments/2008852/3355761/status_update_HttDecayCP_HLeptons_200325.pdf)

rel 21 INT note (https://gitlab.cern.ch/atlas-physics-office/HIGG/ANA-HIGG-2019-10/ANA-HIGG-2019-10-INT1)
```


## Project Maintainers
*  ...


## Original BOOM Authors
* Antonio De Maria
* Quentin Buat
* Pier-Olivier Deviveiros (no more in ATLAS)

