#!/usr/bin/python
from ROOT import TFile, TH1F, TCanvas, TObject, gROOT, gDirectory, TIter, TKey
from math import sqrt
from array import *
import argparse

def getRegions(rootfile):
    rootfile.cd()
    nextkey = TIter(gDirectory.GetListOfKeys())
    key = nextkey.Next()
    regions={}
    while key and key.GetTitle():
        if key.IsFolder() == 1:
            label=key.GetName()
            if label in regions:
                regions[label].append(key.GetName())
            else:
                regions[label]=[key.GetName()]
        key=nextkey.Next()
    return regions

def rebin(histogram, binborders,directory):
    xarray = array('d',binborders)
    hist=TH1F(histogram.GetName(), histogram.GetTitle(),len(binborders)-1,xarray)
    hist.SetDirectory(directory)
    if histogram.GetTitle()=="nominal":
        print(directory.GetName(), directory.GetMotherDir().GetName(), " -> ", histogram.GetEntries(), histogram.GetName(), histogram.GetTitle())
    sumw=0.
    sumw2=0.
    newbin=0
    for bin in range(0,histogram.GetNbinsX()+2):
        if abs(histogram.GetBinLowEdge(bin)-binborders[newbin])<0.0001:
            hist.SetBinContent(newbin,sumw)
            hist.SetBinError(newbin,sqrt(sumw2))
            newbin=newbin+1
            sumw=0.
            sumw2=0.
        sumw=sumw+histogram.GetBinContent(bin)
        sumw2=sumw2+pow(histogram.GetBinError(bin),2.)
    hist.SetName(histogram.GetName())
    hist.SetEntries(histogram.GetEntries())
    return hist

def writeNewFile(infile,outfile,regions):
    infile.cd()
    allregions=[]
    nextregionkey = TIter(gDirectory.GetListOfKeys())
    regionkey = nextregionkey.Next()
    histograms={}
    while regionkey and regionkey.GetTitle():
        print(regionkey)
        if regionkey.IsFolder() == 1:
            regionname=regionkey.GetName()
            histograms[regionname]={}
            allregions.append(regionname)
            inregiondir=infile.Get(regionname)
            inregiondir.cd()
            nextsamplekey = TIter(gDirectory.GetListOfKeys())
            samplekey = nextsamplekey.Next()
            while samplekey:
                if samplekey.IsFolder() == 1:
                    samplename=samplekey.GetName()
                    insampledir=inregiondir.Get(samplename)                    
                    insampledir.cd()
                    hist=insampledir.Get("nominal")
                    if samplename in histograms[regionname]:
                        histograms[regionname][samplename].Add(hist)
                    else:
                        histograms[regionname][samplename]=hist
                samplekey = nextsamplekey.Next()
        regionkey = nextregionkey.Next()

    #get the binning
    binning={}
    for newregion in list(histograms.keys()):
        #SRs
        binning["hh_cba_boost_loose_signal_ipip_d0sig_low"] = [0,6.3]
        binning["hh_cba_boost_loose_signal_ipip_d0sig_high"] = [0,6.3]
        binning["hh_cba_boost_loose_signal_iprho_d0sigy_low"] = [0,6.3]
        binning["hh_cba_boost_loose_signal_iprho_d0sigy_high"] = [0,6.3]
        binning["hh_cba_boost_loose_signal_rhorho_y0y1_low"] = [0,6.3]
        binning["hh_cba_boost_loose_signal_rhorho_y0y1_high"] = [0,6.3]
        binning["hh_cba_boost_tight_signal_ipip_d0sig_low"] = [0,6.3]
        binning["hh_cba_boost_tight_signal_ipip_d0sig_high"] = [0,6.3]
        binning["hh_cba_boost_tight_signal_iprho_d0sigy_low"] = [0,6.3]
        binning["hh_cba_boost_tight_signal_iprho_d0sigy_high"] = [0,6.3]
        binning["hh_cba_boost_tight_signal_rhorho_y0y1_low"] = [0,6.3]
        binning["hh_cba_boost_tight_signal_rhorho_y0y1_high"] = [0,6.3]
        binning["hh_cba_vbf_signal_ipip_d0sig_low"] = [0,6.3]
        binning["hh_cba_vbf_signal_ipip_d0sig_high"] = [0,6.3]
        binning["hh_cba_vbf_signal_iprho_d0sigy_low"] = [0,6.3]
        binning["hh_cba_vbf_signal_iprho_d0sigy_high"] = [0,6.3]
        binning["hh_cba_vbf_signal_rhorho_y0y1_high"] = [0,6.3]
        binning["hh_cba_vbf_signal_rhorho_y0y1_low"] = [0,6.3]

        #ZCRs
        binning["hh_cba_boost_loose_zcr_ipip_d0sig_low"] = [0,6.3]
        binning["hh_cba_boost_loose_zcr_ipip_d0sig_high"] = [0,6.3]
        binning["hh_cba_boost_loose_zcr_iprho_d0sigy_low"] = [0,6.3]
        binning["hh_cba_boost_loose_zcr_iprho_d0sigy_high"] = [0,6.3]
        binning["hh_cba_boost_loose_zcr_rhorho_y0y1_low"] = [0,6.3]
        binning["hh_cba_boost_loose_zcr_rhorho_y0y1_high"] = [0,6.3]
        binning["hh_cba_boost_tight_zcr_ipip_d0sig_low"] = [0,6.3]
        binning["hh_cba_boost_tight_zcr_ipip_d0sig_high"] = [0,6.3]
        binning["hh_cba_boost_tight_zcr_iprho_d0sigy_low"] = [0,6.3]
        binning["hh_cba_boost_tight_zcr_iprho_d0sigy_high"] = [0,6.3]
        binning["hh_cba_boost_tight_zcr_rhorho_y0y1_low"] = [0,6.3]
        binning["hh_cba_boost_tight_zcr_rhorho_y0y1_high"] = [0,6.3]
        binning["hh_cba_vbf_zcr_ipip_d0sig_low"] = [0,6.3]
        binning["hh_cba_vbf_zcr_ipip_d0sig_high"] = [0,6.3]
        binning["hh_cba_vbf_zcr_iprho_d0sigy_low"] = [0,6.3]
        binning["hh_cba_vbf_zcr_iprho_d0sigy_high"] = [0,6.3]
        binning["hh_cba_vbf_zcr_rhorho_y0y1_low"] = [0,6.3]
        binning["hh_cba_vbf_zcr_rhorho_y0y1_high"] = [0,6.3]

        print("Binning for region ", newregion, " -> ", binning[newregion])

    #now write output file
    infile.cd()
    nextregionkey = TIter(gDirectory.GetListOfKeys())
    regionkey = nextregionkey.Next()
    while regionkey and regionkey.GetTitle():
        print(regionkey)
        if regionkey.IsFolder() == 1:
            regionname=regionkey.GetName()
            outfile.cd()
            outfile.mkdir(regionname)
            outregiondir=outfile.Get(regionname)
            infile.cd()
            inregiondir=infile.Get(regionname)
            inregiondir.cd()
            nextsamplekey = TIter(gDirectory.GetListOfKeys())
            samplekey = nextsamplekey.Next()
            while samplekey:
                if samplekey.IsFolder() == 1:
                    samplename=samplekey.GetName()
                    outregiondir.cd()
                    outregiondir.mkdir(samplename)
                    outsampledir=outregiondir.Get(samplename)
                    inregiondir.cd()
                    insampledir=inregiondir.Get(samplename)
                    insampledir.cd()
                    nextsystkey = TIter(gDirectory.GetListOfKeys())
                    systkey = nextsystkey.Next()
                    while systkey:
                        obj = systkey.ReadObj()
                        if obj.IsA().InheritsFrom("TH1"):
                            systname=systkey.GetName()
                            outsampledir.cd()
                            outhist=rebin(obj,binning[regionname],outsampledir)
                            outhist.Write()
                        systkey = nextsystkey.Next()
                else: #take care of lumi histogram
                    obj = samplekey.ReadObj()
                    if obj.IsA().InheritsFrom("TH1"):
                        newobj=obj.Clone()
                        outregiondir.cd()
                        newobj.SetDirectory(outregiondir)
                        newobj.Write()
                samplekey = nextsamplekey.Next()
        regionkey = nextregionkey.Next()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Description of your program')
    parser.add_argument('-i','--infile', help='Input file', required=True)
    parser.add_argument('-o','--outfile', help='Output file', required=True)
    args = vars(parser.parse_args())
    
    infilename=args['infile'] 
    outfilename=args['outfile'] 
    
    rootfile=TFile.Open(infilename,"READ")
    outfile=TFile.Open(outfilename,"RECREATE")
    regions=getRegions(rootfile)
    writeNewFile(rootfile,outfile,regions)
