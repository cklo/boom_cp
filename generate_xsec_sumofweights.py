#local imports

def generate_weight(physics_processes, campaign='mc16a', ntuple_extension='vr'):
    """
    """
    from boom.database import sum_of_weight_bin
    dsid_weight = []
    for process in physics_processes:
        if process.isData:
            continue
        sum_of_weight_bin(process)
        for dataset in process.datasets:
            if not campaign in dataset.name:
                continue
            if not ntuple_extension in dataset.name:
                continue

            dsid_weight.append([
                    dataset.dsid, 
                    dataset.effectiveCrossSection, float(dataset.sumOfWeights)])
    return dsid_weight


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--ntuples-block', default='nom')
    args = parser.parse_args()

    if 'nom' in args.ntuples_block:
        _add_he7_samples = True
        _add_mad_ztt = True
        _add_ggh_fxfx= True
        _add_vbf_vh_fxfx = True
        _add_pow_ztt = True
    else:
        _add_he7_samples = False
        _add_mad_ztt = False
        _add_ggh_fxfx = False
        _add_vbf_vh_fxfx= False
        _add_pow_ztt = False

    if 'afii' in args.ntuples_block:
        _add_afii_samples = True
    else:
        _add_afii_samples = False

    if 'geo1' in args.ntuples_block:
        _add_geo1_samples = True
    else:
        _add_geo1_samples = False

    if 'geo2' in args.ntuples_block:
        _add_geo2_samples = True
    else:
        _add_geo2_samples = False

    if 'geo3' in args.ntuples_block:
        _add_geo3_samples = True
    else:
        _add_geo3_samples = False

    if 'physlist' in args.ntuples_block:
        _add_physlist_samples = True
    else:
        _add_physlist_samples = False

    from boom.database import get_processes
    physicsProcesses = get_processes(squash=False, ntuples_block=args.ntuples_block, xsec_weights_loading=False, cpangles=False, add_he7_samples=_add_he7_samples, add_afii_samples=_add_afii_samples, add_mad_ztt=_add_mad_ztt, add_ggh_fxfx=_add_ggh_fxfx, add_vbf_vh_fxfx=_add_vbf_vh_fxfx, add_pow_ztt=_add_pow_ztt, add_geo1_samples=_add_geo1_samples, add_geo2_samples=_add_geo2_samples, add_geo3_samples=_add_geo3_samples, add_physlist_samples=_add_physlist_samples)

    campaigns = [
        'mc16a', 
        'mc16d', 
        'mc16e'
        ]

    streams = [
        'hh',
        ]

    xsec_weights_dict = {}
    for _camp in campaigns:
        xsec_weights_dict[_camp] = {}
        for _stream in streams:
            dsid_xsec_sumofweights = generate_weight(
                physicsProcesses, campaign=_camp, ntuple_extension='_' + _stream) 
            xsec_weights_dict[_camp][_stream] = dsid_xsec_sumofweights

    import json
    _filename = 'xsec_sumofweights_{}.json'.format(args.ntuples_block) 
    with open(_filename, 'w') as fout:
        json.dump(xsec_weights_dict, fout, indent=2)
