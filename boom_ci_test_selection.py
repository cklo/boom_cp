import json
import sys
import subprocess
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('action', default='build', choices=['build', 'compare'])

    args = parser.parse_args()

    if args.action == 'build':

        with open('data/wsi_sr.json') as f:
            channel_dict = json.load(f)


        output = open('ci_output.txt', 'w')


        from boom.selection_utils import get_selections, print_selection

        _default = sys.stdout
        sys.stdout = output
        for _, v in list(channel_dict.items()):
            _selections = get_selections(
                channels=v['channels'],
                regions=v['regions'],
                categories=v['categories'])
            sorted_sels = sorted(_selections, key=lambda x: x.name)
            for _sel in sorted_sels:
                print_selection(_sel)
                
        sys.stdout = _default

    else:
    
        subprocess.call('diff ci_output.txt data/ci_output_reference.txt > diff.txt', shell=True)
        with open('diff.txt') as f:
            _lines = f.readlines()
            if len(_lines) !=0:
                for _l in _lines:
                    print(_l)
                raise RuntimeError('The selections are different than the baseline!')