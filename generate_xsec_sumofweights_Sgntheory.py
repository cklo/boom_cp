#local imports

def generate_weight(physics_processes, campaign='mc16a', ntuple_extension='vr', sumweight_sys =''):
    """
    """
    from boom.database import sum_of_weight_bin
    dsid_weight = []
    for process in physics_processes:
        if process.isData:
            continue
        sum_of_weight_bin(process, sumweight_sys)
        for dataset in process.datasets:
            if not campaign in dataset.name:
                continue
            if not ntuple_extension in dataset.name:
                continue
            dsid_weight.append([
                    dataset.dsid, 
                    dataset.effectiveCrossSection, float(dataset.sumOfWeights)])
    return dsid_weight




if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--ntuples-block', default='nom')
    parser.add_argument('--sgn-pdfsys-block', default=False)
    args = parser.parse_args()

    from boom.database import get_processes, ggH
    physicsProcesses = get_processes(squash=False, ntuples_block=args.ntuples_block, xsec_weights_loading=False, cpangles=False)
    physicsProcesses = [p for p in physicsProcesses if p.isSignal] 
    campaigns = [
        'mc16a', 
        'mc16d', 
        'mc16e'
        ]

    streams = [
        'hh', 
        ]

    sys = [""]
    if args.sgn_pdfsys_block != '':
      sys= [ 
             'theory_sig_pdf_0',
             'theory_sig_pdf_1',
             'theory_sig_pdf_2',
             'theory_sig_pdf_3',
             'theory_sig_pdf_4',
             'theory_sig_pdf_5',
             'theory_sig_pdf_6',
             'theory_sig_pdf_7',
             'theory_sig_pdf_8',
             'theory_sig_pdf_9',
             'theory_sig_pdf_10',
             'theory_sig_pdf_11',
             'theory_sig_pdf_12',
             'theory_sig_pdf_13',
             'theory_sig_pdf_14',
             'theory_sig_pdf_15',
             'theory_sig_pdf_16',
             'theory_sig_pdf_17',
             'theory_sig_pdf_18',
             'theory_sig_pdf_19',
             'theory_sig_pdf_20',
             'theory_sig_pdf_21',
             'theory_sig_pdf_22',
             'theory_sig_pdf_23',
             'theory_sig_pdf_24',
             'theory_sig_pdf_25',
             'theory_sig_pdf_26',
             'theory_sig_pdf_27',
             'theory_sig_pdf_28',
             'theory_sig_pdf_29',
             'theory_sig_alphaS_low',
             'theory_sig_alphaS_high', 
             # not for ggH
             'theory_sig_mur_muf_0',
             'theory_sig_mur_muf_1',
             'theory_sig_mur_muf_2',
             'theory_sig_mur_muf_3',
             'theory_sig_mur_muf_4',
             'theory_sig_mur_muf_5',
             'theory_sig_mur_muf_6',
             'theory_sig_mur_muf_7',
      ]

      if sys == 'empty':
        print(args.sgn_pdfsys_block + " NOT FOUND")
        exit(0)

    print (sys)
    xsec_weights_dict = {}
    for _camp in campaigns:
      print(_camp)
      #if _camp == 'mc16a': 
      xsec_weights_dict[_camp] = {}
      print(xsec_weights_dict)

      first = True
      for _sys in sys :
        #if _sys in ('theory_z_qsf', 'theory_z_ckk',):
        #  continue 
        print(_sys)
        for _stream in streams:
           dsid_xsec_sumofweights = generate_weight(physicsProcesses, campaign=_camp, ntuple_extension='_' + _stream , sumweight_sys =_sys) 
           dsid_xsec_sumofweights_nominal = generate_weight(physicsProcesses, campaign=_camp, ntuple_extension='_' + _stream , sumweight_sys ='nominal')

           for v in dsid_xsec_sumofweights: 
	     #if (v[0] in ggH):
             if first == True: 
 	       xsec_weights_dict[_camp][v[0]] = [] 
	     tmp = [x for x in dsid_xsec_sumofweights_nominal if x[0] == v[0]]
             if v[2] == 0 :  xsec_weights_dict[_camp][v[0]].append(1)
	     else : xsec_weights_dict[_camp][v[0]].append( tmp[0][2]/ v[2]) #dsid_xsec_sumofweights
             print( v[0], tmp[0][2], v[2], tmp[0][2]/ v[2] )
            

	first=False

    print(xsec_weights_dict)
    import json
    _filename = 'xsec_sumofweights_theorySgn.json' 
#      else: _filename = 'xsec_sumofweights_{}.json'.format(args.ntuples_block) 
    with open(_filename, 'w') as fout:
        json.dump(xsec_weights_dict, fout, indent=2)
