# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# Switch on debug output
#import logging
#logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
process_name =  'ggH_theta_130'
physicsProcesses = get_processes(squash=True, ntuples_block='nom')
ztt_processes = [p for p in physicsProcesses if process_name == p.name]

syst_name = 'theory_sig_qcd_8'

_categories = (
    'preselection',
     )


### define your selection objects
sels = get_selections(
    channels=('1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n', '1p1n_1pXn', '1pXn_1p1n','1p1n_3p0n', '3p0n_1p1n'),
    #channels=channels,
    categories=_categories,
    regions='SR')


# define your list of variables
variables = [
    # VARIABLES['phi_star'],
    # VARIABLES['mmc_mlm_m'],
    VARIABLES['phi_star_5bins'],
]

# #### processor declaration, booking and running
processor = boom_processor(ztt_processes, sels, variables, systematic_names=syst_name)
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
import ROOT
stop_watch = ROOT.TStopwatch()

from boom.plotting import make_sys_nom_plot
for _cat in _categories:
    for var in variables:
        make_sys_nom_plot(processor, sels, var, process_name, syst_name, categories=_cat)

stop_watch.Stop()
stop_watch.Print()

print('closing stores...')
close_stores(ztt_processes)
print('done')
