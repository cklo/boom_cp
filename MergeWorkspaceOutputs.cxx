// Author: Nathan Readioff
// November 2020
// Program functions like hadd, merging multiple root files into a single output.
// Unlike hadd, it assumes that each object appears once, and once only, in all
// the input files. If an object with same path and name appears in another file,
// the new histogram is cached and added to the histogram in the output file at 
// the very end of execution. Use of the cache reduces disc read/writes and 
// reduces runtime significantly.

// WARNING: only valid for root files containing Directories and TH1F histograms
// Other objects will not be properly merged (unless specific rules added!)
// There is no limit on the size of the cache, it may prove necessry to periodically
// "flush" the cache by writing it to the output file. Not an issue in current tests.

// Usage:
// g++ -g MergeWorkspaceOutputs.cxx -o MergeWorkspaceOutputs `root-config --cflags --libs` -L $ROOTSYS/lib -Wall -pedantic -Ofast
// ./MergeWorkspaceOutputs [Output file] [file1 file2 file3 ...]

#include "TROOT.h"
#include "TSystem.h"
#include "TList.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TKey.h"
#include "TH1F.h"
#include "TString.h"
#include "TStopwatch.h"

#include <vector>
#include <iostream>


// Helper class holding cache of TH1F objects
// Class can be extended with maps of other types if other objects need to be stored
class ObjectCache{
private:
  std::map<TString, TList*> cacheTH1F;

public:
  ObjectCache() {}
  ~ObjectCache(){
    for (auto it=cacheTH1F.begin(); it!=cacheTH1F.end(); ++it) {
      TIter next(it->second);
      it->second->Delete(); // delete the histograms
      delete it->second; // delete the TList;
    }
  }

  // Put a TH1F in the cache for later writing
  void InsertTH1F(TString path, TH1F* hist) {
    hist->SetDirectory(0);
    if ( cacheTH1F.find(path) == cacheTH1F.end() )  cacheTH1F[path] = new TList();
    cacheTH1F[path]->Add(hist);
  }

  // Merge objects in cacche with those already written to file
  void MergeAndWrite(TFile* outfile) {
    for (auto cacheIter =cacheTH1F.begin(); cacheIter!=cacheTH1F.end(); ++cacheIter) {
      TString fullPath = cacheIter->first;
      size_t splitPos = fullPath.Last('/');    // find split between path and object name
      TString dirPath = fullPath(0, splitPos); // directory only
      TString objName = fullPath(splitPos+1, fullPath.Length() - dirPath.Length());

      // try to enter directory in output file
      if (!outfile->cd(dirPath)) {
	std::cout << "ERROR! Path does not exist! Skipping TH1F: " << fullPath << std::endl;
	continue;
      }
     
      // Try to get the histogram that was already written to disc
      TH1F *discHist = (TH1F*) gDirectory->Get(objName);
      if (!discHist) {
	std::cout << "ERROR! In Directory, but TH1F not found! Skipping TH1F: " << fullPath << std::endl;
	continue;
      }
      
      std::cout << "Merging from cache: " << fullPath << std::endl;
      
      // Merge list TH1F objects with the one on the disc
      discHist->Merge(cacheIter->second);
     
      // Write final TH1F to disc
      discHist->Write(discHist->GetName(), TObject::kOverwrite);
    }
  }
};






// Recursiuvely load a directory's contents and copy it from input to output
// There should only be one instance of ObjectCache
void copyContent(TDirectory* inputDir, TDirectory* outputDir, ObjectCache* cache) {
  inputDir->ReadAll(); 
  TIter next(inputDir->GetList());
  TObject *obj;
  
  while (( obj = next() )) {
    if (obj->InheritsFrom("TDirectory")) {
      TString dirName = obj->GetName();
      if (!outputDir->GetDirectory(dirName)) outputDir->mkdir(dirName);
      
      // Get directories
      outputDir->cd(dirName);
      TDirectory* outputSubDir = gDirectory;
      inputDir->cd(dirName);
      TDirectory* inputSubDir = gDirectory;

      // Recursive copy
      copyContent(inputSubDir, outputSubDir, cache);
      
      // Delete the directory just copied (all objects should have already been cleared)
      delete obj;
    } 
    else if (obj->InheritsFrom("TH1F")) {
      // Load TH1F and detach from directory (so we can delete it)
      outputDir->cd();
      TH1F *hist = (TH1F*)obj;
      hist->SetDirectory(0);

      // If hist already exists on disc, then the new one needs adding to it.
      // Save the new on in the cache and do the addition later

      if (outputDir->GetListOfKeys()->Contains(obj->GetName())) {
	cache->InsertTH1F(TString(outputDir->GetPath()) + "/" + obj->GetName(), hist);
	// do not delete hist in this case! It must remain alive in the cache!
      } else {
	hist->Write();
	delete hist;
      }
    }
    else {
      std::cout << "Unrecognised object!" << obj->GetName() << std::endl;
    }
  }
}







int main(int argc, char **argv) 
{
  if (argc < 3) {
    std::cout << "ERROR: Please provide output file and input files" << std::endl;
    return 1;
  }
    
  TString outputFileName = argv[1];
  if (!gSystem->AccessPathName(outputFileName)) {
    std::cout << "ERROR! Output file already exists! Aborting!!!" << std::endl;
    return 1;
  }

  std::vector<TString> inputFileList;
  for (auto i=2; i<argc; i++) inputFileList.push_back(argv[i]);

  std::cout << "Program will process" << std::endl;
  for (auto f : inputFileList) std::cout << f << std::endl;
  std::cout << "=========================" << std::endl;
  std::cout << "Processing commences now:" << std::endl;
  std::cout << "=========================" << std::endl;

  TStopwatch timer;
  timer.Start();

  TFile *outputFile = new TFile(outputFileName, "RECREATE");
  
  // Temp storage for all histograms that need merging
  ObjectCache* globalCache = new ObjectCache();

  for (auto inputFileName : inputFileList) {
    // Load input file
    std::cout << inputFileName << std::endl;
    TFile *inputFile = new TFile(inputFileName);
    if (!inputFile ||inputFile->IsZombie()) {
      std::cout << "ERROR! Could not open " << inputFileName << std::endl;
    }

    // Tell Root that we will be repsonsible for deletion/cleanup
    gROOT->GetListOfFiles()->Remove(inputFile);

    // Get input/output base dirs
    inputFile->cd();
    TDirectory *inputBaseDir = gDirectory;    
    outputFile->cd();
    TDirectory *outputBaseDir = gDirectory;

    // Start recursive copy of content
    copyContent(inputBaseDir, outputBaseDir, globalCache);

    inputFile->Close();
    delete inputFile;
  }

  // Process all cached objects and write to file.
  globalCache->MergeAndWrite(outputFile);
  delete globalCache;

  outputFile->Close();
  delete outputFile;
  timer.Stop();
  timer.Print();
}



/* Alternative, much shorter main method:
  TStopwatch timer;
  timer.Start();
  
  // Load input files
  std::vector<TFile*> inputFiles;
  for (auto filename : inputFileList) {
    TFile *f = new TFile(filename);
    inputFiles.push_back(f);
  } 

  TFileMerger *FM = new TFileMerger();
  for (auto f : inputFiles) FM->AddFile(f);
  FM->SetNotrees();
  FM->SetMaxOpenedFiles(5);
  FM->OutputFile("./NPRTest.root");
  FM->Merge();

  timer.Stop();
  timer.Print();
  
  This has comparable speed and is more general-purpose.
  Part of speed boost comes from copying all files to tmp dir
  Not very user firendly, as it displays no progress updates 
  during execution.
*/











