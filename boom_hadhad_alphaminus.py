# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# Switch on debug output
#import logging
#logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(squash=True)

categories =( 'preselection',
              ## signal regions
              #'boost_sr',
              #'boost_tight_sr',
              #'boost_loose_sr',
              #'vbf_sr',
              # 'vbf_0_sr',
              # 'vbf_1_sr',
              ## control regions
              'boost_zcr',
              'vbf_zcr',
              # 'boost_zcr_0',
              # 'vbf_zcr_0',
              # 'boost_zcr_1',
              # 'vbf_zcr_1',
              ## optimised signal regions
             # 'boost_loose_high_sr',
             # 'boost_loose_low_sr',
             # 'boost_loose_high_zcr',
             # 'boost_loose_low_zcr',
             # 'boost_tight_high_sr',
             # 'boost_tight_low_sr',
             # 'boost_tight_high_zcr',
             # 'boost_tight_low_zcr', 
             # 'vbf_0_high_sr',
             # 'vbf_0_low_sr',
             # 'vbf_1_high_sr',
             # 'vbf_1_low_sr',
             # 'vbf_high_sr',
             # 'vbf_low_sr',
             # 'vbf_high_zcr',
             # 'vbf_low_zcr',
             ## alphaminus cuts
            # 'vbf_zcr_aminus_s',
            # 'vbf_zcr_aminus_l',
            # 'boost_zcr_aminus_s',
            # 'boost_zcr_aminus_l',
            # 'aminus_s',
            # 'aminus_l',


            )

### define your selection objects 
sels = get_selections(
    channels=('1p0n_1p0n', '1p0n_1p1n', '1p1n_1p0n', '1p1n_1p1n', '1p1n_1pXn', '1p1n_3p0n', '1pXn_1p1n', '3p0n_1p1n', '1p0n_1pXn', '1pXn_1p0n'), 
    categories= categories,
    regions='SR')


# define your list of variables
variables = [
    # VARIABLES['phi_star_7bins'],
    # VARIABLES['vbf_tagger'],
     # VARIABLES['norm'],
     # VARIABLES['vbf_tagger'],
     ## kinematics for categorisation 
     # VARIABLES['tau_0_pt'],
     # VARIABLES['jet_0_pt'],
     # VARIABLES['ditau_dr'],
     # VARIABLES['ditau_deta'],
     # VARIABLES['higgs_pt'],
     # VARIABLES['tau_1_pt'],
     # VARIABLES['met'],
     # VARIABLES['mmc_mlm_m'],
     # VARIABLES['mjj'],
     # VARIABLES['jets_deta'],
     ## kinematics in decay products (for phistar construction)
#     VARIABLES['tau_0_leadTrk_d0'],
#     VARIABLES['tau_1_leadTrk_d0'],
#     VARIABLES['tau_0_leadTrk_z0'],
#     VARIABLES['tau_1_leadTrk_z0'],
     # VARIABLES['tau_0_leadTrk_eta'],
     # VARIABLES['tau_1_leadTrk_eta'],
     # VARIABLES['tau_0_leadTrk_phi'],
     # VARIABLES['tau_1_leadTrk_phi'],
     # VARIABLES['tau_0_leadTrk_pt'],
     # VARIABLES['tau_1_leadTrk_pt'],
     # VARIABLES['primary_vertex_x'],
     # VARIABLES['primary_vertex_y'],
     # VARIABLES['primary_vertex_z'],
#     VARIABLES['tau_0_leadTrk_d0_sig'],
#     VARIABLES['tau_1_leadTrk_d0_sig'],
     # VARIABLES['tau_0_decay_neutral_pt'],
     # VARIABLES['tau_1_decay_neutral_pt'],
     # VARIABLES['tau_0_decay_neutral_eta'],
     # VARIABLES['tau_1_decay_neutral_eta'],
     # VARIABLES['tau_0_decay_neutral_phi'],
     # VARIABLES['tau_1_decay_neutral_phi'],
     # VARIABLES['tau_0_decay_neutral_m'],
     # VARIABLES['tau_1_decay_neutral_m'],
     # VARIABLES['tau_0_track0_m'],
     # VARIABLES['tau_1_track0_m'],
#     VARIABLES['tau_0_decay_neutral_E'],
#     VARIABLES['tau_1_decay_neutral_E'],
     # VARIABLES['ditau_leadTrk_deta'],
     # VARIABLES['ditau_leadTrk_dphi'],
     # VARIABLES['ditau_decay_neutral_deta'],
     # VARIABLES['ditau_decay_neutral_dphi'],
     # VARIABLES['tau_0_decay_charged_neutral_deta'],
     # VARIABLES['tau_1_decay_charged_neutral_deta'],
     # VARIABLES['tau_0_decay_charged_neutral_dphi'],
     # VARIABLES['tau_1_decay_charged_neutral_dphi'],
    # VARIABLES['tau_0_decay_charged_pt'],
    # VARIABLES['tau_1_decay_charged_pt'],
    # VARIABLES['tau_0_decay_charged_eta'],
    # VARIABLES['tau_1_decay_charged_eta'],
    # VARIABLES['tau_0_decay_charged_phi'],
    # VARIABLES['tau_1_decay_charged_phi'],
    # VARIABLES['tau_0_decay_charged_E'],
    # VARIABLES['tau_1_decay_charged_E'],
     # VARIABLES['tau_0_track0_pt'],
     # VARIABLES['tau_1_track0_pt'],
     # VARIABLES['tau_0_track1_pt'],
     # VARIABLES['tau_1_track1_pt'],
     # VARIABLES['tau_0_track2_pt'],
     # VARIABLES['tau_1_track2_pt'],
     # VARIABLES['tau_0_track0_eta'],
     # VARIABLES['tau_1_track0_eta'],
     # VARIABLES['tau_0_track1_eta'],
     # VARIABLES['tau_1_track1_eta'],
     # VARIABLES['tau_0_track2_eta'],
     # VARIABLES['tau_1_track2_eta'],
     # VARIABLES['tau_0_track0_phi'],
     # VARIABLES['tau_1_track0_phi'],
     # VARIABLES['tau_0_track1_phi'],
     # VARIABLES['tau_1_track1_phi'],
     # VARIABLES['tau_0_track2_phi'],
     # VARIABLES['tau_1_track2_phi'],
#     VARIABLES['tau_0_upsilon'],
#     VARIABLES['tau_1_upsilon'],
     #VARIABLES['tau_0_1_upsilon_product'],
#     VARIABLES['a1_upsilon'],
     VARIABLES['alphaminus_IP'],
     # VARIABLES['alphaminus_IPrho']
     # VARIABLES['alphaminus_rhorho']
]

# #### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)


# plot making
import ROOT
stop_watch = ROOT.TStopwatch()
for var in variables:  
     # single category plots
     for category in categories:
         if 'preselection' in category: unblind = True
         elif '_zcr' in category: unblind = True
         else:  unblind = False

         if 'mmc' in var.name or 'phi_star' in var.name : chisquare = False
         else: chisquare = False

         # make_data_mc_plot(processor, sels, var, categories=category, force_unblind=unblind, print_chisquare=chisquare)
         make_data_mc_plot(processor, sels, var, categories=category, channels=('1p0n_1p0n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p0n_1p1n','1p1n_1p0n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p0n_1p1n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_1p0n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p0n_1pXn','1pXn_1p0n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p0n_1pXn'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1pXn_1p0n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_1p1n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_1pXn','1pXn_1p1n'), force_unblind=unblind) 
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_1pXn'), force_unblind=unblind) 
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1pXn_1p1n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_3p0n','3p0n_1p1n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('3p0n_1p1n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_3p0n'), force_unblind=unblind)

     # multiple categories plots
     continue

     # make_data_mc_plot(processor, sels, var, categories=('boost_tight_high_zcr','boost_loose_high_zcr','boost_tight_low_zcr','boost_loose_low_zcr', 'boost_tight_high_sr','boost_loose_high_sr','boost_tight_low_sr','boost_loose_low_sr'))
     # make_data_mc_plot(processor, sels, var, categories=('boost_tight_high_zcr','boost_tight_low_zcr', 'boost_tight_high_sr','boost_tight_low_sr'))
     # make_data_mc_plot(processor, sels, var, categories=('boost_loose_high_zcr','boost_loose_low_zcr', 'boost_loose_high_sr','boost_loose_low_sr'))
     # make_data_mc_plot(processor, sels, var, categories=('vbf_high_zcr','vbf_low_zcr','vbf_high_sr','vbf_low_sr'))

     # make_data_mc_plot(processor, sels, var, categories=('boost_tight_high_zcr','boost_tight_high_sr'))
     # make_data_mc_plot(processor, sels, var, categories=('boost_tight_low_zcr','boost_tight_low_sr'))
     # make_data_mc_plot(processor, sels, var, categories=('boost_loose_high_zcr','boost_loose_high_sr'))
     # make_data_mc_plot(processor, sels, var, categories=('boost_loose_low_zcr','boost_loose_low_sr'))
     # make_data_mc_plot(processor, sels, var, categories=('boost_tight_high_zcr','boost_loose_high_zcr', 'boost_tight_high_sr','boost_loose_high_sr' ))
     # make_data_mc_plot(processor, sels, var, categories=('boost_tight_low_zcr','boost_loose_low_zcr', 'boost_tight_low_sr','boost_loose_low_sr' ))
     
     # make_data_mc_plot(processor, sels, var, categories=('vbf_high_zcr','vbf_high_sr'))
     # make_data_mc_plot(processor, sels, var, categories=('vbf_low_zcr','vbf_low_sr'))


stop_watch.Stop()
stop_watch.Print()

print('closing stores...')
close_stores(physicsProcesses)
print('done')
