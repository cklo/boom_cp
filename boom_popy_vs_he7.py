# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes(squash=True, add_he7_samples=True)
ztt_processes = [p for p in physicsProcesses if p.isSignal and 'ttH' not in p.name and 'theta' not in p.name]


_categories = (
     'preselection',
    )

### define your selection objects 
sels = get_selections(
    #channels=('1p0n_1p0n', '1p0n_1p1n', '1p1n_1p0n', '1p1n_1p1n', '1p1n_1pXn', '1p1n_3p0n', '1pXn_1p1n', '3p0n_1p1n', '1p0n_1pXn', '1pXn_1p0n'),
    channels=('1p1n_1p1n'),
    years=('15','16', '17', '18'), 
    categories=_categories,
    regions='SR')

# define your list of variables
variables = [
    #VARIABLES['tau_0_upsilon'], 
    #VARIABLES['tau_1_upsilon'],
    #VARIABLES['tau_0_decay_neutral_pt'],
    #VARIABLES['tau_1_decay_neutral_pt'],
    #VARIABLES['tau_0_decay_neutral_eta'],
    #VARIABLES['tau_1_decay_neutral_eta'],
    VARIABLES['phi_star_9bins'],
    #VARIABLES['tau_0_decay_neutral_phi'],
    #VARIABLES['tau_1_decay_neutral_phi'],
    #VARIABLES['ditau_decay_neutral_deta'],
    #VARIABLES['ditau_decay_neutral_dphi'],
    #VARIABLES['tau_0_decay_charged_neutral_deta'],
    #VARIABLES['tau_0_decay_charged_neutral_dphi'],
    #VARIABLES['tau_1_decay_charged_neutral_deta'],
    #VARIABLES['tau_1_decay_charged_neutral_dphi'],
    #VARIABLES['tau_0_track0_pt'],
    #VARIABLES['tau_1_track0_pt'],
    #VARIABLES['tau_0_track0_eta'],
    #VARIABLES['tau_1_track0_eta'],
    #VARIABLES['tau_0_track0_phi'],
    #VARIABLES['tau_1_track0_phi'], 
    #VARIABLES['ditau_leadTrk_deta'],
    #VARIABLES['ditau_leadTrk_dphi'],
    #VARIABLES['alphaminus_rhorho'],
    #VARIABLES['tau_0_1_upsilon_product']
    VARIABLES['fabs_tau_0_1_upsilon_product']
]

# #### processor declaration, booking and running
processor = boom_processor(ztt_processes, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

# plot making
import ROOT
stop_watch = ROOT.TStopwatch()

from boom.plotting import make_popy_vs_he7_plot
for _cat in _categories:
    for var in variables:
        make_popy_vs_he7_plot(processor, sels, var, categories=_cat)

stop_watch.Stop()
stop_watch.Print()

print('closing stores...')
close_stores(ztt_processes)
print('done')



