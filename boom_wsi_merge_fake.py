import ROOT
import os

        

if __name__ == '__main__':
    stopwatch = ROOT.TStopwatch()
    stopwatch.Start()

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('input')
    parser.add_argument('--outdir', default='./')
    parser.add_argument('--verbose', default=False, action='store_true')
    args = parser.parse_args()
    
    print('BOOM: reading {}'.format(args.input))
    rfile_input = ROOT.TFile(args.input)

    from boom.wsi_tools import workspace_input
    _wsi = workspace_input(rfile_input, verbose=args.verbose)

    # loop over sthe channels
    for _chan in _wsi.channels:
        if not 'Fake' in _chan.sample_names:
            continue

        print('BOOM: {} - summing data and MC for Fake...'.format(_chan.name))
        _fake = _chan.sample('Fake') + _chan.sample('Fake_subtraction')
        _chan.remove_sample('Fake', verbose=args.verbose)
        _chan.remove_sample('Fake_subtraction', verbose=args.verbose)
        _chan.add_sample(_fake, name='Fake', verbose=args.verbose)
        
    output_name = os.path.basename(args.input).replace('.root', '_fakesummed.root')
    output_name = os.path.join(args.outdir, output_name)
    print('BOOM: writing {}'.format(output_name))
    rfile_out = ROOT.TFile(output_name, 'recreate')    
    _wsi.dump(rfile_out, verbose=args.verbose)

    # Tell ROOT not to check the input file or its contents
    # Not necessarily safe, but at this point the output file
    # is written to disk, so it doesn't matter. It also makes 
    # cleanup faster and stops execution from appearing to freeze.
    ROOT.gROOT.GetListOfFiles().Remove(rfile_input)
    
    # Manually close output
    rfile_out.Close()

    stopwatch.Stop
    stopwatch.Print()
#     rfile_out.Close()
#     rfile_input.Close()
