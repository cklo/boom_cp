# python imports
import os
import ROOT
import math
import array
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.selection_utils import get_selections, filter_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.utils import hist_sum

########################### helper functions
def constructNumDenom(varlead, varsublead):
    if not isinstance(varsublead, (list, tuple)):
        varsublead = [varsublead]
    if not isinstance(varlead, (list, tuple)):
        varlead = [varlead]
    assert len(varlead) == len(varsublead)

    data_minus_mc = {}
    for prong in _prongness:
        for region in _regions:
            for category in _categories:
                sels_r = filter_selections(sels, channels=(prong), regions=(region), categories=(category))
                h_mcs = []
                if dim == 1:
                    var = varsublead[0] if "sublead" in region else varlead[0]
                    data_minus_mc[region + '_' + prong +  '_' + category] = dataMinusMCsum(sels_r, var)
                elif dim == 2:
                    xvar = varsublead[0] if "sublead" in region else varlead[0]
                    yvar = varsublead[1] if "sublead" in region else varlead[1]
                    data_minus_mc[region + '_' + prong +  '_' + category] = dataMinusMCsum(sels_r, xvar, yvar)
                else:
                    print("Only implemented 1d and 2d ffs currently")
                    raise NotImplementedError

                if region in ["same_sign", "high_deta"]: # also get sublead hists in this region
                    if dim == 1: # 1d ffs
                        data_minus_mc[region + '_' + prong +  '_' + category + "_sub"] = dataMinusMCsum(sels_r, varsublead[0])
                    if dim == 2:
                        data_minus_mc[region + '_' + prong +  '_' + category + "_sub"] = dataMinusMCsum(sels_r, varsublead[0], varsublead[1])

    return data_minus_mc
 

def dataMinusMCsum(selection, var, var_2 = None):
    h_mcs = []
    if var_2:
        hdata = processor_2d.get_hist_physics_process_2D('Data', selection, var, var_2)
        for p in processor_2d.mc_backgrounds:
            h = processor_2d.get_hist_physics_process_2D(p.name, selection, var, var_2)
            h_mcs.append(h)
    else:
        hdata = processor.get_hist_physics_process('Data', selection, var)
        for p in processor.mc_backgrounds:
            h = processor.get_hist_physics_process(p.name, selection, var)
            h_mcs.append(h)
    h_mc = hist_sum(h_mcs)
    h_data_minus_mc = hdata.Clone()
    h_data_minus_mc.Add(h_mc, -1)

    return h_data_minus_mc


def constructFFs(histDict, outfile_name):
    outfile = ROOT.TFile(outfile_name, "recreate")
    for category in _categories:
        combHists = {"1p0n" : {"num" : None, "den" : None}, "1p0n_loose" : {"num" : None, "den" : None},
                     "1p1n" : {"num" : None, "den" : None}, "1p1n_loose" : {"num" : None, "den" : None},
                     "1pXn" : {"num" : None, "den" : None}, "1pXn_loose" : {"num" : None, "den" : None},
                     "3p0n" : {"num" : None, "den" : None}, "3p0n_loose" : {"num" : None, "den" : None},
                    }
        for region in _regions[1:]:
            if "sublead" in region:
                listOfProngCombs = ["sublead1p0n","sublead1p1n","sublead1pXn","sublead3p0n"]
            else:
                listOfProngCombs = ["lead1p0n","lead1p1n","lead1pXn","lead3p0n"]
            for prongSetup in listOfProngCombs:
                fake_factor = None
                denom = None
                title = 'FF_' + category + "_" + prongSetup + "_" + region
                for prong in prongDict[prongSetup]:
                    altNum = "_sub" if "sublead" in region else ""
                    numName = _regions[0] + '_' + prong +  '_' + category + altNum
                    denName = region + '_' + prong +  '_' + category

                    if not fake_factor:
                        fake_factor = histDict[numName].Clone()
                        denom = histDict[denName].Clone()
                    else:
                        fake_factor.Add(histDict[numName])
                        denom.Add(histDict[denName])
    
                # for combining lead and sublead ffs
                nprongs = prongSetup[-4:]
                if region.endswith("loose"): nprongs += "_loose"
                if not combHists[nprongs]["num"]:
                    combHists[nprongs]["num"] = fake_factor.Clone()
                    combHists[nprongs]["den"] = denom.Clone()
                else:
                    addCorrectBins(combHists[nprongs]["num"], fake_factor)
                    addCorrectBins(combHists[nprongs]["den"], denom)

                # back to calculation of separate FFs
                fake_factor.Divide(denom)
                fake_factor.SetName(title)
                fake_factor.SetTitle(title)
    
                fake_factor_stat_up   = fake_factor.Clone(title+'_stat_up')
                fake_factor_stat_down = fake_factor.Clone(title+'_stat_down')
                for i in range(fake_factor.GetNbinsX()+1):
                    fake_factor_stat_up.SetBinContent(i, fake_factor.GetBinContent(i) + fake_factor.GetBinError(i))
                    fake_factor_stat_down.SetBinContent(i, fake_factor.GetBinContent(i) - fake_factor.GetBinError(i))
    
                outfile.cd()
                fake_factor.Write()
                fake_factor_stat_up.Write()
                fake_factor_stat_down.Write()

        # finalize sublead + lead ff calculation
        for nprongs in ["1p0n", "1p0n_loose", "1p1n", "1p1n_loose", "1pXn", "1pXn_loose", "3p0n", "3p0n_loose"]:
            comb_title = "FF_" + category + "_comb" + nprongs
            ffcomb = combHists[nprongs]["num"]
            denomcomb = combHists[nprongs]["den"]
            # calculate final ffs
            ffcomb.Divide(denomcomb)
            ffcomb.SetName(comb_title)
            ffcomb.SetTitle(comb_title)

            comb_fake_factor_stat_up = ffcomb.Clone(comb_title + "_stat_up")
            comb_fake_factor_stat_down = ffcomb.Clone(comb_title + "_stat_down")
            for i in range(combHists[nprongs]["num"].GetNbinsX() + 1):
                comb_fake_factor_stat_up.SetBinContent(i, ffcomb.GetBinContent(i) + ffcomb.GetBinError(i))
                comb_fake_factor_stat_down.SetBinContent(i, ffcomb.GetBinContent(i) - ffcomb.GetBinError(i))

            outfile.cd()
            ffcomb.Write()
            comb_fake_factor_stat_up.Write()
            comb_fake_factor_stat_down.Write()
    outfile.Close()
    print("Saved FFs in", outfile.GetName())

def addCorrectBins(hist, histToAdd):
    if isinstance(hist, ROOT.TH2):
        for xbins in range(histToAdd.GetNbinsX() + 2):
            for ybins in range(histToAdd.GetNbinsY() + 2):
                targetBin = hist.FindBin(histToAdd.GetXaxis().GetBinLowEdge(xbins) + 0.02, histToAdd.GetYaxis().GetBinLowEdge(ybins) + 0.02)
                hist.SetBinContent(targetBin, hist.GetBinContent(targetBin) + histToAdd.GetBinContent(xbins, ybins))
                hist.SetBinError(targetBin, math.sqrt(hist.GetBinError(targetBin)**2 + histToAdd.GetBinError(xbins, ybins)**2))
    elif isinstance(hist, ROOT.TH1):
        for ibins in range(histToAdd.GetNbinsX() + 2):
            targetBin = hist.FindBin(histToAdd.GetBinLowEdge(ibins) + 0.02)# assuming we have bins wider than 0.02, and both hists have same bin edges (just one of them more than the other in case of pt)
            hist.SetBinContent(targetBin, hist.GetBinContent(targetBin) + histToAdd.GetBinContent(ibins))
            hist.SetBinError(targetBin, math.sqrt(hist.GetBinError(targetBin)**2 + histToAdd.GetBinError(ibins)**2))
    else:
        print("4D chess?")
        raise NotImplementedError

################################################

# retrieve physics processes
physicsProcesses = get_processes(no_fake=True)
physicsProcesses = [p for p in physicsProcesses if not p.isSignal] 

_categories = (
   'preselection',
   #'boost',
   #'vbf',
   #'vh',
   )

#################### choose what you want to calculate ########################
sourceRegion = "same_sign"
#sourceRegion = "high_deta"
dim = 1 # 1- or 2-dim FFs? Trying to do both in one go causes errors
###############################################################################

if sourceRegion == "same_sign":
    _regions = (
        'same_sign',
        'fake_factors_hh_same_sign_anti_sublead_tau',
        'fake_factors_hh_same_sign_anti_lead_tau',
        'fake_factors_hh_same_sign_anti_sublead_tau_loose',
        'fake_factors_hh_same_sign_anti_lead_tau_loose',
        )

    """
    variableListList = [
        [
            VARIABLES['tau_0_pt_hhff'],
            VARIABLES['tau_1_pt_hhff'],
        ],
        [
            VARIABLES['tau_0_eta_hhff'],
            VARIABLES['tau_1_eta_hhff'],
        ],
    ]
    """

    variableListList = [ # for deriving parametrization uncertainty
        [
            VARIABLES['tau_0_pt_hhff_samesign'],
            VARIABLES['tau_1_pt_hhff_samesign'],
        ],
        [
            VARIABLES['tau_0_eta_hhff_samesign'],
            VARIABLES['tau_1_eta_hhff_samesign'],
        ],
    ]
elif sourceRegion == "high_deta":
    _regions = (
        'high_deta',
        'high_deta_anti_sublead_tau',
        'high_deta_anti_lead_tau',
        'high_deta_anti_sublead_tau_loose',
        'high_deta_anti_lead_tau_loose',
        )

    variableListList = [
        [
            VARIABLES['tau_0_pt_hhff_high_deta'],
            VARIABLES['tau_1_pt_hhff_high_deta'],
        ],
        [
            VARIABLES['tau_0_eta_hhff_high_deta'],
            VARIABLES['tau_1_eta_hhff_high_deta'],
        ],
    ]
else:
    raise NotImplementedError


### define your selection objects 
sels = get_selections(
    channels=('1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','1p0n_1pXn','1pXn_1p0n','1p1n_3p0n','3p0n_1p1n'), 
    years=('15','16','17','18'),
    regions=_regions,
    categories=_categories)

listOfVariablesToBeLoaded = []
for vvv in variableListList:
    for var in vvv:
        if not var in listOfVariablesToBeLoaded:
            listOfVariablesToBeLoaded.append(var)
#### processor declaration, booking and running

if dim == 1:
    processor = boom_processor(physicsProcesses, sels, listOfVariablesToBeLoaded)
    processor.book()
    processor.run(n_cores=cpu_count() - 1)
elif dim == 2:
    processor_2d = boom_processor(physicsProcesses, sels, variableListList[0], variableListList[1])
    processor_2d.book_2D()
    processor_2d.run(n_cores=cpu_count() - 1)

_prongness = (
    '1p0n_1p0n',
    '1p0n_1p1n',
    '1p1n_1p0n',
    '1p1n_1p1n',
    '1p1n_1pXn',
    '1pXn_1p1n',
    '1p0n_1pXn',
    '1pXn_1p0n',
    '1p1n_3p0n',
    '3p0n_1p1n',
    )

# plot making
prongDict = {"sublead1p0n" : ["1p0n_1p0n","1p1n_1p0n","1pXn_1p0n"], 
             "sublead1p1n" : ["1p0n_1p1n","1p1n_1p1n","1pXn_1p1n","3p0n_1p1n"],
             "sublead1pXn" : ["1p1n_1pXn","1p0n_1pXn"],
             "sublead3p0n" : ["1p1n_3p0n"],
             "lead1p0n"    : ["1p0n_1p0n","1p0n_1p1n","1p0n_1pXn"],
             "lead1p1n"    : ["1p1n_1p0n","1p1n_1p1n","1p1n_1pXn","1p1n_3p0n"],
             "lead1pXn"    : ["1pXn_1p1n","1pXn_1p0n"],
             "lead3p0n"    : ["3p0n_1p1n"],
           }

if __name__ == "__main__":
    if dim == 2:
        data_minus_mc_2d = constructNumDenom([variableListList[0][0], variableListList[1][0]], [variableListList[0][1], variableListList[1][1]])
        constructFFs(data_minus_mc_2d, "hhffsfromhh_obj_2d_" + variableListList[0][0].name + "_" + variableListList[1][0].name + ".root")
    elif dim == 1:
        for vvv in variableListList:
            data_minus_mc = constructNumDenom([vvv[0]], [vvv[1]])
            constructFFs(data_minus_mc, "hhffsfromhh_obj_" + vvv[0].name + ".root")

print('closing stores...')
close_stores(physicsProcesses)
print('done')
