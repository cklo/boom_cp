# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_channel_comparison_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes()
physicsProcesses = [p for p in physicsProcesses if 'Ztt' in p.name]

### define your selection objects 
sels = get_selections(
    channels=('1p0n_1p0n','1p1n_1p0n','1p0n_1p1n','1p1n_1p1n'),
    regions='SR',
    categories=('boost_loose_low_sr'))

### define your list of variables
variables = [
    VARIABLES['mmc_mlm_m'],
    VARIABLES['phi_star'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

title = 'Boost Loose Low'

### plot making
for var in variables:
    make_channel_comparison_plot(processor, sels, var, process_name='Ztt', title=title)

print('closing stores...')
close_stores(physicsProcesses)
print('done')
