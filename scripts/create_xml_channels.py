# This is a script which will open an xml file,
# and duplicate the lephad channel into a leplep
# and hadhad channel - also adds dataset suffixes

infile  = open("../data/allchannels-dataset-R21-MC16a.xml", "r")
outfile = open("../data/allchannels-dataset-R21-MC16a-mod.xml", "w")

# first, replace all instances of paths with the variable
myLines = infile.readlines()

# output file lines
outputLines = []

# Let's look for datasets!!
toSkip=0

for index in range(len(myLines)):
    
    if toSkip > 0:
        toSkip -= 1
        continue

    # (if possible) Store the following 2 lines
    currentLine = myLines[index]
    if index < len(myLines)-1:
        nLine = myLines[index+1]
    if index < len(myLines)-2:
        nlLine = myLines[index+2]

    # Look for a dataset tag
    if currentLine.find("Dataset") != -1:
        # We've found a new dataset
        datasetLines = []
        
        # Find the dataset name
        indexName = currentLine.find("name") 

        if currentLine.find("isData") != -1 :  
            endofName = currentLine.find('"', indexName+7)
            theName = currentLine[indexName+6:endofName+1]       
        else:
            endofName = currentLine.find('"', indexName+6)
            theName = currentLine[indexName+5:endofName+1]
        theName = theName[:-1]+'_lh"'
        
        # Frankenstein a new line
        if currentLine.find("isData") != -1 : newLine = currentLine[0:indexName+6]+theName+currentLine[endofName+1:]
        else : newLine = currentLine[0:indexName+5]+theName+currentLine[endofName+1:]

        # Frankenstein a new set of lines: leplep
        outputLines += [ newLine.replace('_lh', '_ll') ]
        outputLines += [ nLine.replace('lephad', 'leplep') ]
        outputLines += [ nlLine ]

        # Frankenstein a new set of lines: lephad
        outputLines += [ newLine ]
        outputLines += [ nLine ]
        outputLines += [ nlLine ]

        # Frankenstein a new set of lines: hadhad
        outputLines += [ newLine.replace('_lh', '_hh') ]
        outputLines += [ nLine.replace('lephad', 'hadhad')]
        outputLines += [ nlLine ]

        # Skip the next 2 lines
        toSkip = 2
        
    else:    
        # otherwise just dump the line and carry on
        outputLines += [myLines[index]]


for outLine in outputLines:
    outfile.write(outLine)


# Write output to a new file
#    else: 
#        outfile.write(line)



# Sample file format to look at
#      <Dataset name="363355" crossSection="15.564" kFactor="0.27976" weightExpression="weight_total">
#          <File> &lh_path;/mc/lephad/mc16a/nom/*363355*/*.root*</File>
#      </Dataset>
