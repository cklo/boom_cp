import ROOT
import uuid

def quadratic_sum(vals):
    squared_vals = []
    for v in vals:
        squared_vals.append(v * v)
    return ROOT.TMath.Sqrt(sum(squared_vals))



cut_values = []

significances_new = {
    'hadhad': {
        'low': {},
        'high': {},
        'comb': {},
        },
    'lephad': {
        'low': {},
        'high': {},
        'comb': {},
        },
    'leplep': {
        'low': {},
        'high': {},
        'comb': {},
        },
    }

with open('./vbf_optimisation.txt') as f:
    for l in f.readlines():
        line = l.strip('\n').split(' ')
        line = [a for a in line if a != '']
        if len(line) == 0:
            continue
        if not line[0] in ('hadhad', 'lephad', 'leplep'):
            continue
        _channel, _cut, _low, _high, _comb = line
        _cut = float(_cut)
        _low = float(_low)
        _high = float(_high)
        _comb = float(_comb)
        if _cut not in cut_values:
            cut_values.append(_cut)
        print(_channel, _cut, _low, _high, _comb)
        significances_new[_channel]['low'][_cut] = _low
        significances_new[_channel]['high'][_cut] = _high
        significances_new[_channel]['comb'][_cut] = _comb

cut_values = sorted(cut_values)

significances_old = {
    'hadhad': {
        'loose': 0.142971432414,
        'tight': 0.702202644424,
        'lowdr': 1.41729643955,
        },
    'lephad': {
        'loose': 0.367369449851,
        'tight': 1.67005909394,
        },
    'leplep': {
        'loose': 0.217664816708,
        'tight': 0.660777570083,
        },
    }

for chan, cat_dict in list(significances_old.items()):
    cat_signis = []
    for _, signi in list(cat_dict.items()):
        cat_signis.append(signi)
    significances_old[chan]['comb'] = quadratic_sum(cat_signis)


graphs = {}

for _chan in ('leplep', 'lephad', 'hadhad'):
    graphs[_chan] = {}
    for _cat in ('low', 'high', 'comb'):
        graphs[_chan][_cat] = ROOT.TGraph(len(cut_values))
        graphs[_chan][_cat].SetTitle(_cat)

for _chan in ('leplep', 'lephad', 'hadhad'):
    for _cat in ('low', 'high', 'comb'):
        for ip, _val in enumerate(cut_values):
            graphs[_chan][_cat].SetPoint(
                ip, _val, significances_new[_chan][_cat][_val])

graphs_old = {}
for _chan in list(significances_old.keys()):
    graphs_old[_chan] = {}
    for _cat in list(significances_old[_chan].keys()):
        graphs_old[_chan][_cat] = ROOT.TGraph(1)
        graphs_old[_chan][_cat].SetPoint(0, 0, significances_old[_chan][_cat])


color = {
    'low': ROOT.kBlue,
    'high': ROOT.kRed,
    'loose': ROOT.kBlue,
    'tight': ROOT.kRed,
    'lowdr': ROOT.kRed,
    'comb': ROOT.kBlack,
}

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.SetStyle('ATLAS')

def _plot():
    canv = ROOT.TCanvas(uuid.uuid4().hex, "", 400, 600)
    canv.Divide(1, 3, 0.00)
    canv.SetBottomMargin(0.1)
    pads = [canv.GetPad(ipad) for ipad in range(1, 4)]

    labels = []
    h_refs = []
    _y_lims = {
        'leplep': 1.15,
        'lephad': 2.7,
        'hadhad': 2.35
        }
    x_labels = [0.95, 0.64, 0.33]
    for ichan, _chan in enumerate(('leplep', 'lephad', 'hadhad')):
        label = ROOT.TText(0.8, 0.95 - 0.31 * ichan, _chan)
        label.SetNDC(True)
        label.SetTextSize(0.8 * label.GetTextSize())
        labels.append(label)

        h = ROOT.TH1F(uuid.uuid4().hex, '', 1, cut_values[0], 1.09 * cut_values[-1])
        h.SetBinContent(1, -99999)
        h.GetXaxis().SetTitle('')
        h.GetYaxis().SetTitle('')
        h.GetYaxis().SetRangeUser(-0.05, _y_lims[_chan]) 
        h_refs.append(h)



    for ip, _chan in enumerate(('leplep', 'lephad', 'hadhad')):
        pad = canv.cd(ip + 1)
        h_refs[ip].Draw("HIST")
        for _cat in list(graphs_old[_chan].keys()):
            graphs_old[_chan][_cat].SetMarkerStyle(22)
            graphs_old[_chan][_cat].SetMarkerColor(color[_cat])
            graphs_old[_chan][_cat].Draw('sameP')

        for _cat in ('low', 'high', 'comb'):
            graphs[_chan][_cat].SetLineColor(color[_cat])
            graphs[_chan][_cat].SetMarkerColor(color[_cat])
            graphs[_chan][_cat].SetLineWidth(3)
            graphs[_chan][_cat].SetMarkerStyle(ROOT.kFullCircle)
            graphs[_chan][_cat].SetMarkerSize(0.5)
            graphs[_chan][_cat].Draw('sameLP')

        pad.RedrawAxis()
        pad.Update()

    legend = ROOT.TLegend(canv.GetLeftMargin(), 0.7, 0.6, 0.95)
    legend.AddEntry(graphs['hadhad']['comb'], '#sqrt{low^{2} + high^{2}}', 'lp')
    legend.AddEntry(graphs['hadhad']['high'], 'high cat', 'lp')
    legend.AddEntry(graphs['hadhad']['low'], 'low cat.', 'lp')
    legend.AddEntry(graphs_old['hadhad']['comb'], 'cba', 'p')
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
    canv.cd(1)
    legend.Draw()
    canv.cd(0)
    x_title = ROOT.TText(0.3, 0.01, "Categorisation Cut Value")
    x_title.SetNDC(True)
    x_title.SetTextSize(x_title.GetTextSize() * 1.)
    x_title.Draw()
    y_title = ROOT.TText(0.09, 0.3, "VBF Stat-only Significance (counting exp.)")
    y_title.SetNDC(True)
    y_title.SetTextAngle(90)
    y_title.SetTextSize(y_title.GetTextSize() * 1.)
    y_title.Draw()
    for label in labels:
        label.Draw()
    canv.Update()
    canv.SaveAs('significance_optimisation.pdf')

_plot()
