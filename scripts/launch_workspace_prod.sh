# python condor_boom boom_workspace_prod.py --year 15 16 --channel ll  --no-signal
# python condor_boom boom_workspace_prod.py --year 15 16 --channel lh  --no-signal
# python condor_boom boom_workspace_prod.py --year 15 16 --channel hh  --no-signal
python condor_boom boom_workspace_prod.py --year 15 16 --channel all --no-signal --zcr
# python condor_boom boom_workspace_prod.py --year 15 16 --channel ll  --signal-only
# python condor_boom boom_workspace_prod.py --year 15 16 --channel lh  --signal-only
# python condor_boom boom_workspace_prod.py --year 15 16 --channel hh  --signal-only
# python condor_boom boom_workspace_prod.py --year 15 16 --channel all --signal-only --zcr
# python condor_boom boom_workspace_prod.py --year 15 16 --channel ll  --signal-only --stxs stxs1 
# python condor_boom boom_workspace_prod.py --year 15 16 --channel lh  --signal-only --stxs stxs1 
# python condor_boom boom_workspace_prod.py --year 15 16 --channel hh  --signal-only --stxs stxs1 
# python condor_boom boom_workspace_prod.py --year 15 16 --channel all --signal-only --stxs stxs1 --zcr

# python condor_boom boom_workspace_prod.py --year 17 --channel ll  --no-signal
# python condor_boom boom_workspace_prod.py --year 17 --channel lh  --no-signal
# python condor_boom boom_workspace_prod.py --year 17 --channel hh  --no-signal
python condor_boom boom_workspace_prod.py --year 17 --channel all --no-signal --zcr
# python condor_boom boom_workspace_prod.py --year 17 --channel ll  --signal-only
# python condor_boom boom_workspace_prod.py --year 17 --channel lh  --signal-only
# python condor_boom boom_workspace_prod.py --year 17 --channel hh  --signal-only
# python condor_boom boom_workspace_prod.py --year 17 --channel all --signal-only --zcr
# python condor_boom boom_workspace_prod.py --year 17 --channel ll  --signal-only --stxs stxs1 
# python condor_boom boom_workspace_prod.py --year 17 --channel lh  --signal-only --stxs stxs1 
# python condor_boom boom_workspace_prod.py --year 17 --channel hh  --signal-only --stxs stxs1 
# python condor_boom boom_workspace_prod.py --year 17 --channel all --signal-only --stxs stxs1 --zcr

# python condor_boom boom_workspace_prod.py --year 18 --channel ll  --no-signal
# python condor_boom boom_workspace_prod.py --year 18 --channel lh  --no-signal
# python condor_boom boom_workspace_prod.py --year 18 --channel hh  --no-signal
python condor_boom boom_workspace_prod.py --year 18 --channel all --no-signal --zcr
# python condor_boom boom_workspace_prod.py --year 18 --channel ll  --signal-only
# python condor_boom boom_workspace_prod.py --year 18 --channel lh  --signal-only
# python condor_boom boom_workspace_prod.py --year 18 --channel hh  --signal-only
# python condor_boom boom_workspace_prod.py --year 18 --channel all --signal-only --zcr
# python condor_boom boom_workspace_prod.py --year 18 --channel ll  --signal-only --stxs stxs1 
# python condor_boom boom_workspace_prod.py --year 18 --channel lh  --signal-only --stxs stxs1 
# python condor_boom boom_workspace_prod.py --year 18 --channel hh  --signal-only --stxs stxs1 
# python condor_boom boom_workspace_prod.py --year 18 --channel all --signal-only --stxs stxs1 --zcr
