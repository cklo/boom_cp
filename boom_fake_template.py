# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# Switch on debug output
# import logging
# logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(squash=True, no_fake=True)

categories =( 'preselection',
              'boost_sr',
              'vbf_sr',
              'boost_loose_high_sr',
              'boost_loose_low_sr',
              'boost_tight_high_sr',
              'boost_tight_low_sr', 
              'vbf_high_sr',
              'vbf_low_sr',
            )

### define your selection objects 
sels = get_selections(
    channels=('1p0n_1p0n', '1p0n_1p1n', '1p1n_1p0n', '1p1n_1p1n', '1p1n_1pXn', '1p1n_3p0n', '1pXn_1p1n', '3p0n_1p1n'),
    categories= categories,
    regions='same_sign')


# define your list of variables
variables = [
     VARIABLES['phi_star_5bins'],
     #VARIABLES['norm'],
     VARIABLES['tau_0_pt'],
     VARIABLES['mmc_mlm_m'],
     VARIABLES['jet_0_pt'],
     VARIABLES['ditau_dr'],
     VARIABLES['ditau_deta'],
]

# #### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)


# plot making
import ROOT
stop_watch = ROOT.TStopwatch()
for var in variables:
     # single category plots
     for category in categories:
         make_data_mc_plot(processor, sels, var, categories=category, force_unblind=True, print_chisquare=False)
     

stop_watch.Stop()
stop_watch.Print()

print('closing stores...')
close_stores(physicsProcesses)
print('done')
