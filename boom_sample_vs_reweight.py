# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_sample_vs_reweighted_comparison_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes(cpangles=True)

do_ggH_only    = False
do_VBFH_only   = False

if do_ggH_only :
    physicsProcesses = [p for p in physicsProcesses if 'ggH' in p.name]
elif do_VBFH_only : 
    physicsProcesses = [p for p in physicsProcesses if 'VBFH' in p.name]
else:
    physicsProcesses = [p for p in physicsProcesses if 'ggH' in p.name or 'VBFH' in p.name]

### define your selection objects 
sels = get_selections(
    channels=('1p0n_1p0n', '1p0n_1p1n', '1p1n_1p0n', '1p1n_1p1n', '1p1n_1pXn', '1p1n_3p0n', '1pXn_1p1n', '3p0n_1p1n'),
    regions='SR',
    categories=('preselection'))

### define your list of variables
variables = [
    #VARIABLES['norm'],
    #VARIABLES['mmc_mlm_m'],
    VARIABLES['phi_star_5bins'],
    #VARIABLES['tau_0_pt'],
    #VARIABLES['tau_0_eta'],
    #VARIABLES['tau_1_pt'],
    #VARIABLES['tau_1_eta'],
    #VARIABLES['jet_0_pt'],
    #VARIABLES['ditau_dr'],
    #VARIABLES['ditau_deta'],
    #VARIABLES['higgs_pt'],
  
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

title = 'Preselection'

### plot making
for var in variables:
    make_sample_vs_reweighted_comparison_plot(processor, sels, var, title=title, do_ggH_only=do_ggH_only, do_VBFH_only=do_VBFH_only)

print('closing stores...')
close_stores(physicsProcesses)
print('done')
