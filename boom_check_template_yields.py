# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_yield_comparison_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes(cpangles=True)

do_ggH_only    = False
do_VBFH_only   = False
do_WH_only     = True 
do_ZH_only     = False

if do_ggH_only :
    physicsProcesses = [p for p in physicsProcesses if 'ggH' in p.name]
    reference = 'ggH' 
elif do_VBFH_only :
    physicsProcesses = [p for p in physicsProcesses if 'VBFH' in p.name]
    reference = 'VBFH'
elif do_WH_only :
    physicsProcesses = [p for p in physicsProcesses if 'WH' in p.name]
    reference = 'WH'
elif do_ZH_only :
    physicsProcesses = [p for p in physicsProcesses if 'ZH' in p.name]
    reference = 'ZH'
else :
    physicsProcesses = [p for p in physicsProcesses if 'ttH' in p.name]
    reference = 'ttH'

_channel = ( 
             '1p0n_1p0n', '1p0n_1p1n', '1p1n_1p0n', '1p1n_1p1n', '1p1n_1pXn', '1p1n_3p0n', '1pXn_1p1n', '3p0n_1p1n', '1p0n_1pXn', '1pXn_1p0n'
           )

_categories = ( 
               'preselection',
              )

### define your selection objects 
sels = get_selections(
    channels=_channel,
    regions='SR',
    categories=_categories,
    )


### define your list of variables
variables = [
    VARIABLES['norm'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

### plot making
for cat in _categories:
    for var in variables:
        make_yield_comparison_plot(processor, sels, cat, var, reference) 

print('closing stores...')
close_stores(physicsProcesses)
print('done')
