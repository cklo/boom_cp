# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections, filter_selections
from boom.variables import VARIABLES
from boom.database import get_processes
from boom.extern.tabulate import tabulate
from boom.utils import hist_sum, yield_tuple, hist_max, rqcd_calc
#from boom.plotting import derive_syst_unc

# import logging
# logging.root.setLevel( logging.DEBUG )
exclude_processes = (
        'ggH_theta_10',
        'ggH_theta_20',
        'ggH_theta_30',
        'ggH_theta_40',
        'ggH_theta_50',
        'ggH_theta_60',
        'ggH_theta_70',
        'ggH_theta_80',
        'ggH_theta_100',
        'ggH_theta_110',
        'ggH_theta_120',
        'ggH_theta_130',
        'ggH_theta_140',
        'ggH_theta_150',
        'ggH_theta_160',
        'ggH_theta_170',
        'VBFH_theta_10',
        'VBFH_theta_20',
        'VBFH_theta_30',
        'VBFH_theta_40',
        'VBFH_theta_50',
        'VBFH_theta_60',
        'VBFH_theta_70',
        'VBFH_theta_80',
        'VBFH_theta_100',
        'VBFH_theta_110',
        'VBFH_theta_120',
        'VBFH_theta_130',
        'VBFH_theta_140',
        'VBFH_theta_150',
        'VBFH_theta_160',
        'VBFH_theta_170',
        )

# retrieve physics processes
physicsProcesses = get_processes(squash=True)
physicsProcesses = [p for p in physicsProcesses if p.name not in exclude_processes]

_categories = ('preselection'),

### define your selection objects 
sels = get_selections(
    channels=('1p0n_1p0n', '1p0n_1p1n', '1p1n_1p0n', '1p1n_1p1n', '1p1n_1pXn', '1p1n_3p0n', '1pXn_1p1n', '3p0n_1p1n', '1p0n_1pXn', '1pXn_1p0n'),
    years=('15', '16', '17', '18'),
    categories=_categories,
    regions='same_sign')


# define your list of variables
variable = VARIABLES['mmc_mlm_m_closure']

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variable)
processor.book()
processor.run(n_cores=cpu_count() - 1)

for category in _categories:

    sels_r = filter_selections(sels, categories=category)
    h_data = processor.get_hist_physics_process('Data', sels_r, variable)

    bkgs = []
    for name in processor.merged_physics_process_names[::-1]:
        p = processor.get_physics_process(name)[0]
        if (p.isSignal and 'HWW' not in p.name):
            continue
        if p.name == 'Data':
            continue
        h = processor.get_hist_physics_process(name, sels_r, variable)
        bkgs.append(h)
    hBkg = hist_sum(bkgs)
    h_data.Divide(hBkg)

    #decay_mode = '1p0n' if do_1p0n else '1p1n' if do_1p1n else '1pXn' if do_1pXn else '3p0n' if do_3p0n else '3pXn'
    cat = 'Presel' #if 'preselection' in category else 'VBF' if 'vbf' in category else 'Boosted'
    filename = 'hh_fake_param_uncert'
    outfile = ROOT.TFile(filename+'.root','recreate')
    h_data.SetName(filename)
    h_data.Write()
    outfile.Close()
         
print('closing stores...')
close_stores(physicsProcesses)
print('done')


