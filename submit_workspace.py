import os
import subprocess
import itertools
import json

#_QUEUE_DICT = {}
#with open('data/condor_queue.json') as f:
#    _QUEUE_DICT = json.load(f)

def _build_command(
        variable,
        channel, 
        year, 
        process_group_name,
        var_group_name,
        executable='python condor_boom', 
        steering_script='boom_workspace_prod.py',
        json_file="data/wsi_sr.json",
        z_cr=False,
        forced_queue=None):
    """
    """

    # manually skip Fake for zcr for now
    if z_cr and process_group_name == 'Fake':
        return None

    _expected_jobs = expected_jobs(channel, year, process_group_name, z_cr=z_cr)
    # select the job corresponding to the specific group
    _expected_jobs = [_j for _j in _expected_jobs if _j[3] == var_group_name]

    if len(_expected_jobs) == 0:
        return None

    _kinematic_variation = False
    if var_group_name in list(KINEMATICS.keys()):
        _kinematic_variation = True

    # selec the variable to put in the wsi input
    if '_sr' in json_file:
       variable = variable #"phi_star_fit"
    else:
       if args.usemmc:
           variable = "mmc_mlm_m_zcr"
       elif args.usevismass:
           variable = "visible_mass_zcr"
       elif args.usetaumass:
           variable = "tau_vis_mass"
       elif args.usephistar:
           variable = "phi_star_fit"
       else:
           variable = variable #"norm"


    # skip systematics for He7 processes
    if 'He7' in process_group_name and 'nominal' not in var_group_name:
        return None
   
    # skip systematics for afii processes
    if 'afii' in process_group_name and 'nominal' not in var_group_name:
        return None

    # skip systematics for alternative samples
    if ('geo1' in process_group_name or 'geo2' in  process_group_name or 'geo3' in  process_group_name or 'physlist' in process_group_name) and  'nominal' not in var_group_name:
        return None

    _cmd_args = [
        executable,
        steering_script,
        '--year ' + year,
        '--channel ' + channel,
        '--var-group-name ' + var_group_name,
        '--process-group-name ' + process_group_name,
        '--json ' + json_file,
        '--variable ' + variable
        ]

    # hack for afii
    if 'afii' in process_group_name:
        _cmd_args += ['--ntuples-block ' + 'sys_afii']

    if 'geo1' in process_group_name:
        _cmd_args += ['--ntuples-block ' + 'sys_geo1']

    if 'geo2' in process_group_name:
        _cmd_args += ['--ntuples-block ' + 'sys_geo2']

    if 'geo3' in process_group_name:
        _cmd_args += ['--ntuples-block ' + 'sys_geo3']

    if 'physlist' in process_group_name:
        _cmd_args += ['--ntuples-block ' + 'sys_physlist']

    if _kinematic_variation:
        _cmd_args += ['--ntuples-block ' + var_group_name]

    if z_cr:
        _cmd_args += ['--zcr']


    if forced_queue != None:
        _cmd_args += ['--queue {}'.format(forced_queue)]
    else:
        _queue_key = '_'.join([
            year,
            channel,
            'zcr' if z_cr else 'sr',
            var_group_name,
            process_group_name])
        #if not _queue_key in _QUEUE_DICT.keys():
        #    print 'BOOM: {} is missing from the queue dict'.format(_queue_key)
        #    _cmd_args += ['--queue workday']
        #else:
        #    _cmd_args += ['--queue {}'.format(_QUEUE_DICT[_queue_key])]
        _cmd_args += ['--queue tomorrow']

    out_dir = 'condor_{}'.format(variable)
    if 'zcr' in json_file:
        out_dir += '_zcr_{}'.format(channel)
    else:
        out_dir += '_{}'.format(channel)

    if 'boost' in json_file:
        out_dir += '_boost_{}'.format(channel)
    else:
        out_dir += '_vbf_{}'.format(channel)

    if not args.local:
        _cmd_args += ['--outdir {}'.format(out_dir)]

    _command = ' '.join(_cmd_args)
    return _command


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(usage='%(prog)s [options]')
    parser.add_argument('--year', default=None, nargs='*', choices=['15', '16', '17', '18'])
    parser.add_argument('--channel', default=None, nargs='*', choices=['hh'])
    parser.add_argument('--process-group-name', default=None, nargs='*')
    parser.add_argument('--var-group-name', default=None, nargs='*')
    parser.add_argument('--steering-script', default='boom_workspace_prod.py')
    parser.add_argument('--variable', default='mmc_mlm_m_fine_binning', help = "(default: %(default)s)")
    parser.add_argument('--zcr', default=False, action='store_true')
    parser.add_argument('--json', default='data/wsi_sr.json', help = "(default: %(default)s)")
    parser.add_argument('--usemmc', default=False, action='store_true')
    parser.add_argument('--usevismass', default=False, action='store_true')
    parser.add_argument('--usetaumass', default=False, action='store_true')
    parser.add_argument('--usephistar', default=False, action='store_true')
    # skip args
    parser.add_argument('--skip-nominal', default=False, action='store_true')
    parser.add_argument('--skip-variations', default=False, action='store_true')
    parser.add_argument('--skip-weights', default=False, action='store_true')
    parser.add_argument('--skip-kinematics', default=False, action='store_true')
    # submit args
    parser.add_argument('--local', default=False, action='store_true')
    parser.add_argument('--submit', default=False, action='store_true')
    parser.add_argument('--forced-queue', default=None)
    args = parser.parse_args()

    from boom.batch import KINEMATICS, WEIGHT_VARIATIONS, PROCESSES
    from boom.batch import expected_jobs

    if args.channel == None:
        _channels = [
            'hh',  
        ]
    else:
        _channels = args.channel

    if args.year == None:
        _years = [
            '15 16', 
            '17', 
            '18',
        ]
    else:
        if '15' in args.year and '16' in args.year:
            _years = args.year
            _years.remove('15')
            _years.remove('16')
            _years.append('15 16')
        else:
            _years = args.year

    _kinematic_variations = sorted(KINEMATICS.keys())
    _weight_variations = sorted(WEIGHT_VARIATIONS.keys()) 
    _variations = _kinematic_variations + _weight_variations
    _process_groups = sorted(PROCESSES.keys())

    _specific_systematic_block = args.var_group_name
    _specific_process_group = args.process_group_name

    if args.local:
        _executable = 'python'
    else:
        _executable = 'python condor_boom'
    _steering_script = args.steering_script

    commands = []

    # nominal
    if not args.skip_nominal:
        for _chan, _year, _process_gr in itertools.product(
                _channels, _years, _process_groups):

            # skip if process is different than specified
            if _specific_process_group != None:
                if _process_gr not in  _specific_process_group:
                    continue

            _cmd = _build_command(
                args.variable,
                _chan, 
                _year,
                _process_gr, 
                'nominal', 
                executable=_executable, 
                steering_script=_steering_script,
                json_file = args.json,
                z_cr=args.zcr,
                forced_queue=args.forced_queue)
            if _cmd != None:
                commands.append(_cmd)

    #  systematics
    if not args.skip_variations:
        for _var, _chan, _year, _process_gr in itertools.product(
            _variations, _channels, _years, _process_groups):

            # skip if process is different than specified
            if _specific_process_group != None:
                if _process_gr not in _specific_process_group:
                    continue
                    
            # skip if block is different than specified
            if _specific_systematic_block != None:
                if _var not in _specific_systematic_block:
                    continue

            # skip if skip_weights is specified and _var is a weight var
            if args.skip_weights and _var in list(WEIGHT_VARIATIONS.keys()):
                continue

            # skip if skip_kinematics and _var is a kinematic variation
            if args.skip_kinematics and _var in list(KINEMATICS.keys()):
                continue

            _command = _build_command(
                args.variable,
                _chan, 
                _year, 
                _process_gr,
                _var, 
                executable=_executable, 
                steering_script=_steering_script,
                json_file = args.json,
                z_cr=args.zcr,
                forced_queue=args.forced_queue)

            if _command != None:
                commands.append( _command)


    for i_cmd, _cmd in enumerate(commands):
        print('{}/{}'.format(i_cmd + 1, len(commands)), _cmd)
        if args.submit:
            subprocess.call(_cmd, shell=True)
