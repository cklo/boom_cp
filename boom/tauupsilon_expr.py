"""
"""
import ROOT

lead_tau_upsilon_expr = 'TauUpsilonCalc::calc_tau_upsilon({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})'.format(
    'tau_0_decay_mode', 
    'tau_0_decay_charged_p4.Pt()',
    'tau_0_decay_charged_p4.Eta()',
    'tau_0_decay_charged_p4.Phi()',
    'tau_0_decay_charged_p4.M()',
    'tau_0_decay_neutral_p4.Pt()',
    'tau_0_decay_neutral_p4.Eta()',
    'tau_0_decay_neutral_p4.Phi()',
    'tau_0_decay_neutral_p4.M()',
    'event_number',
    '0',
    '0',
    )
"""lead_tau_upsilon_expr"""

sublead_tau_upsilon_expr = 'TauUpsilonCalc::calc_tau_upsilon({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})'.format(
    'tau_1_decay_mode',
    'tau_1_decay_charged_p4.Pt()',
    'tau_1_decay_charged_p4.Eta()',
    'tau_1_decay_charged_p4.Phi()',
    'tau_1_decay_charged_p4.M()',
    'tau_1_decay_neutral_p4.Pt()',
    'tau_1_decay_neutral_p4.Eta()',
    'tau_1_decay_neutral_p4.Phi()',
    'tau_1_decay_neutral_p4.M()',
    'event_number',
    '0',
    '0',
    )
"""sublead_tau_upsilon_expr"""

lead_tau_upsilon_energy_scale_up_expr = 'TauUpsilonCalc::calc_tau_upsilon({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})'.format(
    'tau_0_decay_mode',
    'tau_0_decay_charged_p4.Pt()',
    'tau_0_decay_charged_p4.Eta()',
    'tau_0_decay_charged_p4.Phi()',
    'tau_0_decay_charged_p4.M()',
    'tau_0_decay_neutral_p4.Pt()',
    'tau_0_decay_neutral_p4.Eta()',
    'tau_0_decay_neutral_p4.Phi()',
    'tau_0_decay_neutral_p4.M()',
    'event_number',
    '1',
    '0',
    )
"""lead_tau_upsilon_energy_scale_up_expr"""

sublead_tau_upsilon_energy_scale_up_expr = 'TauUpsilonCalc::calc_tau_upsilon({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})'.format(
    'tau_1_decay_mode',
    'tau_1_decay_charged_p4.Pt()',
    'tau_1_decay_charged_p4.Eta()',
    'tau_1_decay_charged_p4.Phi()',
    'tau_1_decay_charged_p4.M()',
    'tau_1_decay_neutral_p4.Pt()',
    'tau_1_decay_neutral_p4.Eta()',
    'tau_1_decay_neutral_p4.Phi()',
    'tau_1_decay_neutral_p4.M()',
    'event_number',
    '1',
    '0',
    )
"""sublead_tau_upsilon_energy_scale_up_expr"""

lead_tau_upsilon_energy_scale_down_expr = 'TauUpsilonCalc::calc_tau_upsilon({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})'.format(
    'tau_0_decay_mode',
    'tau_0_decay_charged_p4.Pt()',
    'tau_0_decay_charged_p4.Eta()',
    'tau_0_decay_charged_p4.Phi()',
    'tau_0_decay_charged_p4.M()',
    'tau_0_decay_neutral_p4.Pt()',
    'tau_0_decay_neutral_p4.Eta()',
    'tau_0_decay_neutral_p4.Phi()',
    'tau_0_decay_neutral_p4.M()',
    'event_number',
    '0',
    '1',
    )
"""lead_tau_upsilon_energy_scale_down_expr"""

sublead_tau_upsilon_energy_scale_down_expr = 'TauUpsilonCalc::calc_tau_upsilon({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})'.format(
    'tau_1_decay_mode',
    'tau_1_decay_charged_p4.Pt()',
    'tau_1_decay_charged_p4.Eta()',
    'tau_1_decay_charged_p4.Phi()',
    'tau_1_decay_charged_p4.M()',
    'tau_1_decay_neutral_p4.Pt()',
    'tau_1_decay_neutral_p4.Eta()',
    'tau_1_decay_neutral_p4.Phi()',
    'tau_1_decay_neutral_p4.M()',
    'event_number',
    '0',
    '1',
    )
"""sublead_tau_upsilon_energy_scale_down_expr"""



