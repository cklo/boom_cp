"""
Implementation of the Alternative samples uncertainty parameterisation
"""
from happy.systematics import Systematics, SystematicsSet

def get_altsamp_weights():
    """Retrieve the Signal theory shape uncertainty

    Returns
    _______
    sys_set : HAPPy SystematicsSet
  
    """
   
    lead_tau_pt = 'tau_0_p4.Pt()'
    lead_tau_decay_mode    = 'tau_0_decay_mode'

    sublead_tau_pt = 'tau_1_p4.Pt()'
    sublead_tau_decay_mode    = 'tau_1_decay_mode'

    # schedule all the uncertainty types
    from .altsampsyst import altsample_param     

    geo1_syst = Systematics.weightSystematics('altsamp_geo1','altsamp_geo1',
                                              '('+altsample_param(lead_tau_pt,lead_tau_decay_mode,0)+')*('+altsample_param(sublead_tau_pt,sublead_tau_decay_mode,0)+')',
                                              '(1)',
                                              '(1)')
    
    geo2_syst = Systematics.weightSystematics('altsamp_geo2','altsamp_geo2',
                                              '('+altsample_param(lead_tau_pt,lead_tau_decay_mode,1)+')*('+altsample_param(sublead_tau_pt,sublead_tau_decay_mode,1)+')',
                                              '(1)',
                                              '(1)')

    geo3_syst = Systematics.weightSystematics('altsamp_geo3','altsamp_geo3',
                                              '('+altsample_param(lead_tau_pt,lead_tau_decay_mode,2)+')*('+altsample_param(sublead_tau_pt,sublead_tau_decay_mode,2)+')',
                                              '(1)',
                                              '(1)')

    physlist_syst = Systematics.weightSystematics('altsamp_physlist','altsamp_physlist',
                                              '('+altsample_param(lead_tau_pt,lead_tau_decay_mode,3)+')*('+altsample_param(sublead_tau_pt,sublead_tau_decay_mode,3)+')',
                                              '(1)',
                                              '(1)')
 
    sys_set  = SystematicsSet(set([geo1_syst,geo2_syst,geo3_syst,physlist_syst]))
    return sys_set
 
