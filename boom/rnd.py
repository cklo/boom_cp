import ROOT
import os

try:
    ROOT.gSystem.CompileMacro('boom/cpp/random.cpp', 'k-', '', 'cache')
    print('BOOM:\t Randomiser tool loaded!')
except:
    raise RuntimeError

# this is needed because of
# https://sft.its.cern.ch/jira/browse/ROOT-7216

if not hasattr(ROOT, 'Rnd'):
    print('Ransomiser tool not compiled!')

# try:
#     ROOT.Rnd()
# except:
#     print('Randomiser tool not compiled')
