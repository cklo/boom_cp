"""
"""
_all__ = [
    'get_mc_norm',
]

# small helper to append the mc normalisation
from happy.systematics import Systematics, SystematicsSet
def get_mc_norm(channel, region):#, year, is_data):
    """Expression to compute the MC normalisation per tree entry

    Parameters
    __________
    channel: str
       see list in boom/cuts/__init__.py

    Returns
    _______
    weight: HAPPY Systematics.weightSystematics
       Expression to compute the MC normalisation per tree entry
    """

    if channel in ('1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','1p0n_1pXn','1pXn_1p0n','1p1n_3p0n','3p0n_1p1n'):
        _function = 'TotalWeight_hadhad'

    else:
        raise NotImplementedError

    expression = 'MC_Norm::{function}(mc_channel_number, NOMINAL_pileup_random_run_number)'.format(
        function=_function)

    sys_set = SystematicsSet()
    weight = Systematics.weightSystematics(
        'mc_norm', 
        'mc_norm',
        expression,
        expression,
        expression)
    sys_set.add(weight)
    return sys_set
