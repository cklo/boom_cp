from .base import DecoratedCut, CUTBOOK


Cut_npv = DecoratedCut(
    'npv', 
    'n_pvx >= 1',
    'n_pvx >= 1')
CUTBOOK.append(Cut_npv)

Cut_bad_batman = DecoratedCut(
   'event_is_not_bad_batman', 
   'event_is_not_bad_batman',
   'event_is_bad_batman==0') 
CUTBOOK.append(Cut_bad_batman)

Cut_truth_passedVBFFilter  = DecoratedCut(
    'truth_passedVBFFilter ', 'truth_passedVBFFilter ',
    'truth_passedVBFFilter == 0',
    mc_only=True)
CUTBOOK.append(Cut_truth_passedVBFFilter)

