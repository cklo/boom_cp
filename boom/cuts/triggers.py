"""
Trigger decisions and trigger matching
"""

from .base import DecoratedCut, CUTBOOK


###
# Trigger
###

# had-had
Cut_trig_hh_15 = DecoratedCut(
    'trig_hh_15', 'trigger hadhad 15',
    'tau_0_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM && tau_1_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM',
    years=['15'])
CUTBOOK.append(Cut_trig_hh_15)

Cut_trig_hh_16 = DecoratedCut(
    'trig_hh_16', 'trigger hadhad 16',
    'tau_0_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo && tau_1_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo',
    years=['16'])
CUTBOOK.append(Cut_trig_hh_16)

Cut_trig_hh_17 = DecoratedCut(
    'trig_hh_17', 'trigger hadhad 17',
    'tau_0_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_03dR30_L1DR_TAU20ITAU12I_J25 && tau_1_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_03dR30_L1DR_TAU20ITAU12I_J25',
    years=['17'])
CUTBOOK.append(Cut_trig_hh_17)

Cut_trig_hh_18 = DecoratedCut(
    'trig_hh_18', 'trigger hadhad 18',
    'tau_0_trig_HLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_03dR30_L1DR_TAU20ITAU12I_J25 && tau_1_trig_HLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_03dR30_L1DR_TAU20ITAU12I_J25',
    years=['18'])
CUTBOOK.append(Cut_trig_hh_18)

