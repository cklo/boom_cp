"""
cuts on the charge products of the two leptons
"""

from . import CHANNELS, YEARS, CATEGORIES, REGIONS, SYSTEMATICS
from .base import DecoratedCut, CUTBOOK

CutOS = DecoratedCut(
    'OS', 'OS', 
    'ditau_qxq == -1',
    regions=[t for t in REGIONS if not t in ( 'same_sign', 
                                        'same_sign_anti_tau',
                                        'fake_factors_hh_same_sign_anti_lead_tau',
                                        'fake_factors_hh_same_sign_anti_sublead_tau',
                                        'fake_factors_hh_same_sign_anti_lead_tau_loose',
                                        'fake_factors_hh_same_sign_anti_sublead_tau_loose',
                                        'high_deta_same_sign',
                                        'high_deta_same_sign_anti_tau', 
                                      )])
CUTBOOK.append(CutOS)

CutSS = DecoratedCut(
    'SS', 'SS', 
    'ditau_qxq == 1', 
    regions=['same_sign', 
             'same_sign_anti_tau',
             'fake_factors_hh_same_sign',
             'fake_factors_hh_same_sign_anti_tau',
             'fake_factors_hh_same_sign_anti_lead_tau',
             'fake_factors_hh_same_sign_anti_sublead_tau',
             'fake_factors_hh_same_sign_anti_lead_tau_loose',
             'fake_factors_hh_same_sign_anti_sublead_tau_loose',
             'high_deta_same_sign',
             'high_deta_same_sign_anti_tau',])
CUTBOOK.append(CutSS)

