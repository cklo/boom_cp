"""
Module to define all the cuts needed 
in the Htautau couplings analysis
"""

CHANNELS = [
    '1p0n_1p0n',
    '1p1n_1p0n',
    '1p0n_1p1n',
    '1p1n_1p1n',
    '1p1n_1pXn',
    '1pXn_1p1n',
    '1p0n_1pXn',
    '1pXn_1p0n',
    '1p1n_3p0n',
    '3p0n_1p1n',
    ]
"""
list(str): channels used for boom internal computation.
need to be combined according to plotting needs.
"""


YEARS = [
    '15',
    '16',
    '17',
    '18',
]
"""
list(str): years used for boom internal computation.
need to be combined according to plotting needs.
"""

TRIGGERS = [
    'di-had',
]
"""
list(str): trigger split used for boom internal computation.
need to be combined according to plotting needs.
"""

CATEGORIES = [
    'vbf',
    'vbf_zcr_0',
    'vbf_zcr_1',
    'boost_zcr_0',
    'boost_zcr_1',
    'vbf_sr',
    'vbf_high_sr',
    'vbf_low_sr',
    'vbf_zcr',
    'vbf_high_zcr',
    'vbf_high_zcr_0',
    'vbf_low_zcr',
    'vbf_low_zcr_0',
    'vbf_zpeak',
    'vbf_high_zpeak',
    'vbf_low_zpeak',
    'vbf_zcr_1_zpeak',
    'vbf_high_zcr_1_zpeak',
    'vbf_low_zcr_1_zpeak',
    'vbf_0_sr',
    'vbf_0_high_sr',
    'vbf_0_low_sr',
    'vbf_0_zcr',
    'vbf_0_high_zcr',
    'vbf_0_high_zcr_0',
    'vbf_0_low_zcr',
    'vbf_0_low_zcr_0',
    'vbf_0_zpeak',
    'vbf_0_high_zpeak',
    'vbf_0_low_zpeak',
    'vbf_0_zcr_1_zpeak',
    'vbf_0_high_zcr_1_zpeak',
    'vbf_0_low_zcr_1_zpeak',
    'vbf_1_sr',
    'vbf_1_high_sr',
    'vbf_1_low_sr',
    'vbf_1_zcr',
    'vbf_1_high_zcr',
    'vbf_1_high_zcr_0',
    'vbf_1_low_zcr',
    'vbf_1_low_zcr_0',
    'vbf_1_zpeak',
    'vbf_1_high_zpeak',
    'vbf_1_low_zpeak',
    'vbf_1_zcr_1_zpeak',
    'vbf_1_high_zcr_1_zpeak',
    'vbf_1_low_zcr_1_zpeak',
    'boost',
    'boost_sr',
    'boost_high_sr',
    'boost_low_sr', 
    'boost_zcr',
    'boost_high_zcr',
    'boost_low_zcr',
    'boost_zpeak',
    'boost_high_zpeak',
    'boost_low_zpeak',
    'boost_zcr_1_zpeak',
    'boost_high_zcr_1_zpeak',
    'boost_low_zcr_1_zpeak', 
    'boost_loose_sr',
    'boost_loose_high_sr',
    'boost_loose_low_sr',
    'boost_loose_zcr',
    'boost_loose_high_zcr',
    'boost_loose_high_zcr_0',
    'boost_loose_low_zcr',
    'boost_loose_low_zcr_0',
    'boost_loose_zpeak',
    'boost_loose_high_zpeak',
    'boost_loose_low_zpeak', 
    'boost_loose_zcr_1_zpeak',
    'boost_loose_high_zcr_1_zpeak',
    'boost_loose_low_zcr_1_zpeak',
    'boost_tight_sr',
    'boost_tight_high_sr',
    'boost_tight_low_sr',
    'boost_tight_zcr',
    'boost_tight_high_zcr',
    'boost_tight_high_zcr_0',
    'boost_tight_low_zcr',
    'boost_tight_low_zcr_0',
    'boost_tight_zpeak',
    'boost_tight_high_zpeak',
    'boost_tight_low_zpeak',
    'boost_tight_zcr_1_zpeak',
    'boost_tight_high_zcr_1_zpeak',
    'boost_tight_low_zcr_1_zpeak',
    'preselection',
    'preselection_high',
    'preselection_low',
    'qcd_fit',
    ## alphaminus cut
    'vbf_zcr_aminus_s',
    'vbf_zcr_aminus_l',
    'boost_zcr_aminus_s',
    'boost_zcr_aminus_l',
    # 'aminus_s',
    # 'aminus_l',
    ## pileup 
    'preselection_mu_0_20',
    'preselection_mu_20_30',
    'preselection_mu_30_40',
    'preselection_mu_40_50',
    'preselection_mu_50_60',
    'preselection_mu_50_80',
    'boost_tight_high_zcr_1',
    'boost_loose_high_zcr_1',
    'boost_tight_low_zcr_1',
    'boost_loose_low_zcr_1',
    'vbf_0_high_zcr_1',
    'vbf_1_high_zcr_1',
    'vbf_0_low_zcr_1',
    'vbf_1_low_zcr_1',
    # other regions
    'vbf_0_hscale_up_sr',
    'vbf_0_lscale_up_sr',
    'vbf_0_hscale_down_sr',
    'vbf_0_lscale_down_sr',
    'vbf_1_hscale_up_sr',
    'vbf_1_lscale_up_sr',
    'vbf_1_hscale_down_sr',
    'vbf_1_lscale_down_sr',
    'vbf_0_hscale_up_zcr',
    'vbf_0_lscale_up_zcr',
    'vbf_0_hscale_down_zcr',
    'vbf_0_lscale_down_zcr',
    'vbf_1_hscale_up_zcr',
    'vbf_1_lscale_up_zcr',
    'vbf_1_hscale_down_zcr',
    'vbf_1_lscale_down_zcr',
    'vbf_0_hscale_up_zcr_0',
    'vbf_0_lscale_up_zcr_0',
    'vbf_0_hscale_down_zcr_0',
    'vbf_0_lscale_down_zcr_0',
    'vbf_1_hscale_up_zcr_0',
    'vbf_1_lscale_up_zcr_0',
    'vbf_1_hscale_down_zcr_0',
    'vbf_1_lscale_down_zcr_0',
    'vbf_0_hscale_up_zcr_1',
    'vbf_0_lscale_up_zcr_1',
    'vbf_0_hscale_down_zcr_1',
    'vbf_0_lscale_down_zcr_1',
    'vbf_1_hscale_up_zcr_1',
    'vbf_1_lscale_up_zcr_1',
    'vbf_1_hscale_down_zcr_1',
    'vbf_1_lscale_down_zcr_1',
    'boost_loose_hscale_up_sr',
    'boost_loose_lscale_up_sr',
    'boost_loose_hscale_down_sr',
    'boost_loose_lscale_down_sr',
    'boost_tight_hscale_up_sr',
    'boost_tight_lscale_up_sr',
    'boost_tight_hscale_down_sr',
    'boost_tight_lscale_down_sr',
    'boost_loose_hscale_up_zcr',
    'boost_loose_lscale_up_zcr',
    'boost_loose_hscale_down_zcr',
    'boost_loose_lscale_down_zcr',
    'boost_tight_hscale_up_zcr',
    'boost_tight_lscale_up_zcr',
    'boost_tight_hscale_down_zcr',
    'boost_tight_lscale_down_zcr',
    'boost_loose_hscale_up_zcr_0',
    'boost_loose_lscale_up_zcr_0',
    'boost_loose_hscale_down_zcr_0',
    'boost_loose_lscale_down_zcr_0',
    'boost_tight_hscale_up_zcr_0',
    'boost_tight_lscale_up_zcr_0',
    'boost_tight_hscale_down_zcr_0',
    'boost_tight_lscale_down_zcr_0',
    'boost_loose_hscale_up_zcr_1',
    'boost_loose_lscale_up_zcr_1',
    'boost_loose_hscale_down_zcr_1',
    'boost_loose_lscale_down_zcr_1',
    'boost_tight_hscale_up_zcr_1',
    'boost_tight_lscale_up_zcr_1',
    'boost_tight_hscale_down_zcr_1',
    'boost_tight_lscale_down_zcr_1',

]
"""
list(str): categories used for boom internal computation.
need to be combined according to plotting needs.
"""

REGIONS = [
    'SR',
    'anti_tau',
    'same_sign',
    'same_sign_anti_tau',
    # per-tau hadhad FF regions
    'fake_factors_hh_same_sign_anti_lead_tau',
    'fake_factors_hh_same_sign_anti_sublead_tau',
    'fake_factors_hh_same_sign_anti_lead_tau_loose',
    'fake_factors_hh_same_sign_anti_sublead_tau_loose',
    # hadhad high-dEta(tau, tau) regions with single taus being anti-IDd
    'high_deta',
    'high_deta_anti_tau',
    'high_deta_same_sign',
    'high_deta_same_sign_anti_tau',
    'high_deta_anti_lead_tau',
    'high_deta_anti_sublead_tau',
    'high_deta_anti_lead_tau_loose',
    'high_deta_anti_sublead_tau_loose',
]
"""
list(str): regions (sr and various crs).
need to be combined according to plotting needs.
"""
 
SYSTEMATICS = [
    'NOMINAL',
]


from . import cutbook
from .base import CUTBOOK
