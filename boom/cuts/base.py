from . import CHANNELS, TRIGGERS, YEARS, CATEGORIES, REGIONS, SYSTEMATICS
from happy.cut import Cut

# building the cut book
CUTBOOK = []

class DecoratedCut(object):
    def __init__(
        self,
        name,
        title,
        cut_string,
        mc_only=False,
        data_only=False,
        channels=CHANNELS,
        triggers=TRIGGERS,
        years=YEARS,
        categories=CATEGORIES,
        regions=REGIONS):
        """object to hold a HAPPy Cut, 
        
        Attributes are defined to filter on them
        in the selection constructor
        
        Attributes
        __________
        name : str
        title : str
        cut : HAPPy.Cut object
        mc_only : bool
        data_only : bool
        channels : list(str)
        triggers : list(str)
        years : list(str)
        categories : list(str)
        regions : list(str)

        """

        self.name = name
        self.title = title
        self.cut = Cut(self.name, self.title, cut_string)
        self.channels = channels
        self.triggers = triggers
        self.years = years
        self.categories = categories
        self.regions = regions
        self.data_only = data_only
        self.mc_only = mc_only
        


    def __str__(self):
        return self.name

    def __repr__(self):
        return self.cut.cut
