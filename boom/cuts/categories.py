"""
Implementation of the inclusive categories
"""

from .base import DecoratedCut, CUTBOOK
from . import REGIONS, CATEGORIES
from ..tauupsilon_expr import *
import math

_vbf_incl_title = 'pt(j1) > 30, mjj > 400, |Deltaeta(j, j)| > 3, eta(j0) x eta(j1) < 0, dijet_centrality'
_vbf_incl_expr = 'jet_1_p4.Pt() > 30 && dijet_p4.M() > 400 && fabs(jet_0_p4.Eta()-jet_1_p4.Eta()) > 3 && jet_0_p4.Eta() * jet_1_p4.Eta() < 0 && is_dijet_centrality == 1'


#### VBF Category cuts
CutVBFInclusive = DecoratedCut(
    'vbf inclusive', 
    _vbf_incl_title,
    _vbf_incl_expr,
    categories=[c for c in CATEGORIES if 'vbf' in c])
CUTBOOK.append(CutVBFInclusive)

### BOOST category cuts
CutNotVBFInclusive = DecoratedCut(
    'Not VBF inclusive', 
    'NOT vbf inclusive',
    CutVBFInclusive.cut.NOT().cut,
    categories=[c for c in CATEGORIES if 'boost' in c])
CUTBOOK.append(CutNotVBFInclusive)

CutBoostInclusive = DecoratedCut(
    'boost inclusive', 
    'pt(h) > 100',
    'ditau_higgspt > 100',
    categories=[c for c in CATEGORIES if 'boost' in c])
CUTBOOK.append(CutBoostInclusive)

### Split Boost in Loose/Tight
CutBoostTight = DecoratedCut(
    'boost_tight',
    'pt(h) > 140 and dr(tau, l) < 1.5',
    'ditau_higgspt > 140 && ditau_dr < 1.5',
    categories=[c for c in CATEGORIES if 'tight' in c and 'boost' in c])
CUTBOOK.append(CutBoostTight)

CutBoostLoose = DecoratedCut(
    'boost_loose',
    'NOT boost_tight',
    CutBoostTight.cut.NOT().cut,
    categories=[c for c in CATEGORIES if 'loose' in c and 'boost' in c])
CUTBOOK.append(CutBoostLoose)

### Split in High/Low 

#### 1p0n_1p0n #### 
CutHigh_1p0n_1p0n = DecoratedCut(
    'high_1p0n_1p0n',
    'tau0 d0 sgn > 1.5 and tau1 d0sgn > 1.5',
    'fabs(tau_0_leadTrk_d0_sig) > 1.5 && fabs(tau_1_leadTrk_d0_sig) > 1.5',
    channels=('1p0n_1p0n'),
    categories=[c for c in CATEGORIES if 'high' in c])
CUTBOOK.append(CutHigh_1p0n_1p0n)

CutLow_1p0n_1p0n = DecoratedCut(
    'low_1p0n_1p0n',
    'tau0 d0 sgn < 1.5 or tau1 d0sgn < 1.5',
    CutHigh_1p0n_1p0n.cut.NOT().cut,
    channels=('1p0n_1p0n'),
    categories=[c for c in CATEGORIES if 'low' in c])
CUTBOOK.append(CutLow_1p0n_1p0n)

#### 1p0n_1p1n, 1p0n_1pXn ####
CutHigh_1p0n_1p1n = DecoratedCut(
    'high_1p0n_1p1n',
    'tau0 d0 sgn > 1.5 and  tau1 y > 0.1',
    'fabs(tau_0_leadTrk_d0_sig) > 1.5 && fabs(ditau_CP_tau1_upsilon) > 0.1',
    channels=('1p0n_1p1n','1p0n_1pXn'),
    categories=[c for c in CATEGORIES if 'high' in c])
CUTBOOK.append(CutHigh_1p0n_1p1n)

CutLow_1p0n_1p1n = DecoratedCut(
    'low_1p0n_1p1n',
    'tau0 d0 sgn < 1.5 or tau1 y < 0.1',
    CutHigh_1p0n_1p1n.cut.NOT().cut,
    channels=('1p0n_1p1n','1p0n_1pXn'),
    categories=[c for c in CATEGORIES if 'low' in c])
CUTBOOK.append(CutLow_1p0n_1p1n)

####
Cut_hscale_up_1p0n_1p1n = DecoratedCut(
    'high_1p0n_1p1n',
    'tau0 d0 sgn > 1.5 and  tau1 y > 0.1',
    'fabs(tau_0_leadTrk_d0_sig) > 1.5 && fabs('+sublead_tau_upsilon_energy_scale_up_expr+') > 0.1',
    channels=('1p0n_1p1n','1p0n_1pXn'),
    categories=[c for c in CATEGORIES if 'hscale_up' in c])
CUTBOOK.append(Cut_hscale_up_1p0n_1p1n)

Cut_lscale_up_1p0n_1p1n = DecoratedCut(
    'low_1p0n_1p1n',
    'tau0 d0 sgn < 1.5 or tau1 y < 0.1',
    Cut_hscale_up_1p0n_1p1n.cut.NOT().cut,
    channels=('1p0n_1p1n','1p0n_1pXn'),
    categories=[c for c in CATEGORIES if 'lscale_up' in c])
CUTBOOK.append(Cut_lscale_up_1p0n_1p1n)

####
Cut_hscale_down_1p0n_1p1n = DecoratedCut(
    'high_1p0n_1p1n',
    'tau0 d0 sgn > 1.5 and  tau1 y > 0.1',
    'fabs(tau_0_leadTrk_d0_sig) > 1.5 && fabs('+sublead_tau_upsilon_energy_scale_down_expr+') > 0.1',
    channels=('1p0n_1p1n','1p0n_1pXn'),
    categories=[c for c in CATEGORIES if 'hscale_down' in c])
CUTBOOK.append(Cut_hscale_down_1p0n_1p1n)

Cut_lscale_down_1p0n_1p1n = DecoratedCut(
    'low_1p0n_1p1n',
    'tau0 d0 sgn < 1.5 or tau1 y < 0.1',
    Cut_hscale_down_1p0n_1p1n.cut.NOT().cut,
    channels=('1p0n_1p1n','1p0n_1pXn'),
    categories=[c for c in CATEGORIES if 'lscale_down' in c])
CUTBOOK.append(Cut_lscale_down_1p0n_1p1n)

#### 1p1n_1p0n, 1pXn 1p0n ####
CutHigh_1p1n_1p0n = DecoratedCut(
    'high_1p1n_1p0n',
    'tau1 d0 sgn > 1.5 and tau0 y > 0.1',
    'fabs(tau_1_leadTrk_d0_sig) > 1.5 && fabs(ditau_CP_tau0_upsilon) > 0.1',
    channels=('1p1n_1p0n','1pXn_1p0n'),
    categories=[c for c in CATEGORIES if 'high' in c])
CUTBOOK.append(CutHigh_1p1n_1p0n)

CutLow_1p1n_1p0n = DecoratedCut(
    'low_1p1n_1p0n',
    'tau1 d0 sgn < 1.5 or tau0 y < 0.1',
    CutHigh_1p1n_1p0n.cut.NOT().cut,
    channels=('1p1n_1p0n','1pXn_1p0n'),
    categories=[c for c in CATEGORIES if 'low' in c])
CUTBOOK.append(CutLow_1p1n_1p0n)

#####
Cut_hscale_up_1p1n_1p0n = DecoratedCut(
    'high_1p1n_1p0n',
    'tau0 d1 sgn > 1.5 and  tau0 y > 0.1',
    'fabs(tau_1_leadTrk_d0_sig) > 1.5 && fabs('+lead_tau_upsilon_energy_scale_up_expr+') > 0.1',
    channels=('1p1n_1p0n','1pXn_1p0n'),
    categories=[c for c in CATEGORIES if 'hscale_up' in c])
CUTBOOK.append(Cut_hscale_up_1p1n_1p0n)

Cut_lscale_up_1p1n_1p0n = DecoratedCut(
    'low_1p1n_1p0n',
    'tau1 d0 sgn < 1.5 or tau0 y < 0.1',
    Cut_hscale_up_1p1n_1p0n.cut.NOT().cut,
    channels=('1p1n_1p0n','1pXn_1p0n'),
    categories=[c for c in CATEGORIES if 'lscale_up' in c])
CUTBOOK.append(Cut_lscale_up_1p1n_1p0n)

#####
Cut_hscale_down_1p1n_1p0n = DecoratedCut(
    'high_1p1n_1p0n',
    'tau0 d1 sgn > 1.5 and  tau0 y > 0.1',
    'fabs(tau_1_leadTrk_d0_sig) > 1.5 && fabs('+lead_tau_upsilon_energy_scale_down_expr+') > 0.1',
    channels=('1p1n_1p0n','1pXn_1p0n'),
    categories=[c for c in CATEGORIES if 'hscale_down' in c])
CUTBOOK.append(Cut_hscale_down_1p1n_1p0n)
    
Cut_lscale_down_1p1n_1p0n = DecoratedCut(
    'low_1p1n_1p0n',
    'tau1 d0 sgn < 1.5 or tau0 y < 0.1',
    Cut_hscale_down_1p1n_1p0n.cut.NOT().cut,
    channels=('1p1n_1p0n','1pXn_1p0n'),
    categories=[c for c in CATEGORIES if 'lscale_down' in c])
CUTBOOK.append(Cut_lscale_down_1p1n_1p0n)

#### 1p1n_1p1n, 1pXn_1p1n, 1p1n_1pXn ####
CutHigh_1p1n_1p1n = DecoratedCut(
    'high_1p1n_1p1n',
    '|tau 0 y *  tau 1 y| > 0.2',
    'fabs(ditau_CP_tau0_upsilon * ditau_CP_tau1_upsilon) > 0.2',
    channels=('1p1n_1p1n','1p1n_1pXn','1pXn_1p1n'),
    categories=[c for c in CATEGORIES if 'high' in c])
CUTBOOK.append(CutHigh_1p1n_1p1n)

CutLow_1p1n_1p1n = DecoratedCut(
    'low_1p1n_1p1n',
    '|tau 0 y < 0.2 * tau 1 y| < 0.2',
    CutHigh_1p1n_1p1n.cut.NOT().cut,
    channels=('1p1n_1p1n','1p1n_1pXn','1pXn_1p1n'),
    categories=[c for c in CATEGORIES if 'low' in c])
CUTBOOK.append(CutLow_1p1n_1p1n)

####
Cut_hscale_up_1p1n_1p1n = DecoratedCut(
    'high_1p1n_1p1n',
    '|tau 0 y *  tau 1 y| > 0.2',
    'fabs('+lead_tau_upsilon_energy_scale_up_expr+'*'+ sublead_tau_upsilon_energy_scale_up_expr+') > 0.2',
    channels=('1p1n_1p1n','1p1n_1pXn','1pXn_1p1n'),
    categories=[c for c in CATEGORIES if 'hscale_up' in c])
CUTBOOK.append(Cut_hscale_up_1p1n_1p1n)

Cut_lscale_up_1p1n_1p1n = DecoratedCut(
    'low_1p1n_1p1n',
    '|tau 0 y < 0.2 * tau 1 y| < 0.2',
    Cut_hscale_up_1p1n_1p1n.cut.NOT().cut,
    channels=('1p1n_1p1n','1p1n_1pXn','1pXn_1p1n'),
    categories=[c for c in CATEGORIES if 'lscale_up' in c])
CUTBOOK.append(Cut_lscale_up_1p1n_1p1n)

#####
Cut_hscale_down_1p1n_1p1n = DecoratedCut(
    'high_1p1n_1p1n',
    '|tau 0 y *  tau 1 y| > 0.2',
    'fabs('+lead_tau_upsilon_energy_scale_down_expr+'*'+ sublead_tau_upsilon_energy_scale_down_expr+') > 0.2',
    channels=('1p1n_1p1n','1p1n_1pXn','1pXn_1p1n'),
    categories=[c for c in CATEGORIES if 'hscale_down' in c])
CUTBOOK.append(Cut_hscale_down_1p1n_1p1n)

Cut_lscale_down_1p1n_1p1n = DecoratedCut(
    'low_1p1n_1p1n',
    '|tau 0 y < 0.2 * tau 1 y| < 0.2',
    Cut_hscale_down_1p1n_1p1n.cut.NOT().cut,
    channels=('1p1n_1p1n','1p1n_1pXn','1pXn_1p1n'),
    categories=[c for c in CATEGORIES if 'lscale_down' in c])
CUTBOOK.append(Cut_lscale_down_1p1n_1p1n)

##### 3p0n_1p1n #####
CutHigh_3p0n_1p1n = DecoratedCut(
    'high_3p0n_1p1n',
    '|tau 0 y(a1)| > 0.6 and |tau 1 y| > 0.1',
    'fabs(ditau_CP_upsilon_a1)> 0.6 && fabs(ditau_CP_tau1_upsilon) > 0.1',
    channels=('3p0n_1p1n'),
    categories=[c for c in CATEGORIES if 'high' in c])
CUTBOOK.append(CutHigh_3p0n_1p1n)

CutLow_3p0n_1p1n = DecoratedCut(
    'low_3p0n_1p1n',
    '|tau 0 y(a1)| < 0.6 or |tau 1 y| < 0.1',
    CutHigh_3p0n_1p1n.cut.NOT().cut,
    channels=('3p0n_1p1n'),
    categories=[c for c in CATEGORIES if 'low' in c])
CUTBOOK.append(CutLow_3p0n_1p1n)

#####
Cut_hscale_up_3p0n_1p1n = DecoratedCut(
    'high_3p0n_1p1n',
    '|tau 0 y(a1)| > 0.6 and |tau 1 y| > 0.1',
    'fabs(ditau_CP_upsilon_a1)> 0.6 && fabs('+sublead_tau_upsilon_energy_scale_up_expr+') > 0.1',
    channels=('3p0n_1p1n'),
    categories=[c for c in CATEGORIES if 'hscale_up' in c])
CUTBOOK.append(Cut_hscale_up_3p0n_1p1n)

Cut_lscale_up_3p0n_1p1n = DecoratedCut(
    'low_3p0n_1p1n',
    '|tau 0 y(a1)| < 0.6 or |tau 1 y| < 0.1',
    Cut_hscale_up_3p0n_1p1n.cut.NOT().cut,
    channels=('3p0n_1p1n'),
    categories=[c for c in CATEGORIES if 'lscale_up' in c])
CUTBOOK.append(Cut_lscale_up_3p0n_1p1n)

#####
Cut_hscale_down_3p0n_1p1n = DecoratedCut(
    'high_3p0n_1p1n',
    '|tau 0 y(a1)| > 0.6 and |tau 1 y| > 0.1',
    'fabs(ditau_CP_upsilon_a1)> 0.6 && fabs('+sublead_tau_upsilon_energy_scale_down_expr+') > 0.1',
    channels=('3p0n_1p1n'),
    categories=[c for c in CATEGORIES if 'hscale_down' in c])
CUTBOOK.append(Cut_hscale_down_3p0n_1p1n)

Cut_lscale_down_3p0n_1p1n = DecoratedCut(
    'low_3p0n_1p1n',
    '|tau 0 y(a1)| < 0.6 or |tau 1 y| < 0.1',
    Cut_hscale_down_3p0n_1p1n.cut.NOT().cut,
    channels=('3p0n_1p1n'),
    categories=[c for c in CATEGORIES if 'lscale_down' in c])
CUTBOOK.append(Cut_lscale_down_3p0n_1p1n)

##### 1p1n_3p0n #####
CutHigh_1p1n_3p0n = DecoratedCut(
    'high_1p1n_3p0n',
    '|tau 1 y(a1)| > 0.6 and |tau 0 y| > 0.1',
    'fabs(ditau_CP_upsilon_a1)> 0.6 && fabs(ditau_CP_tau0_upsilon) > 0.1',
    channels=('1p1n_3p0n'),
    categories=[c for c in CATEGORIES if 'high' in c])
CUTBOOK.append(CutHigh_1p1n_3p0n)

CutLow_1p1n_3p0n = DecoratedCut(
    'low_1p1n_3p0n',
    '|tau 1 y(a1)| < 0.6 or |tau 0 y| < 0.1',
    CutHigh_1p1n_3p0n.cut.NOT().cut,
    channels=('1p1n_3p0n'),
    categories=[c for c in CATEGORIES if 'low' in c])
CUTBOOK.append(CutLow_1p1n_3p0n)

#####
Cut_hscale_up_1p1n_3p0n = DecoratedCut(
    'high_1p1n_3p0n',
    '|tau 1 y(a1)| > 0.6 and |tau 0 y| > 0.1',
    'fabs(ditau_CP_upsilon_a1)> 0.6 && fabs('+lead_tau_upsilon_energy_scale_up_expr+') > 0.1',
    channels=('1p1n_3p0n'),
    categories=[c for c in CATEGORIES if 'hscale_up' in c])
CUTBOOK.append(Cut_hscale_up_1p1n_3p0n)

Cut_lscale_up_1p1n_3p0n = DecoratedCut(
    'low_1p1n_3p0n',
    '|tau 1 y(a1)| < 0.6 or |tau 0 y| < 0.1',
    Cut_hscale_up_1p1n_3p0n.cut.NOT().cut,
    channels=('1p1n_3p0n'),
    categories=[c for c in CATEGORIES if 'lscale_up' in c])
CUTBOOK.append(Cut_lscale_up_1p1n_3p0n)

#####
Cut_hscale_down_1p1n_3p0n = DecoratedCut(
    'high_1p1n_3p0n',
    '|tau 1 y(a1)| > 0.6 and |tau 0 y| > 0.1',
    'fabs(ditau_CP_upsilon_a1)> 0.6 && fabs('+lead_tau_upsilon_energy_scale_down_expr+') > 0.1',
    channels=('1p1n_3p0n'),
    categories=[c for c in CATEGORIES if 'hscale_down' in c])
CUTBOOK.append(Cut_hscale_down_1p1n_3p0n)

Cut_lscale_down_1p1n_3p0n = DecoratedCut(
    'low_1p1n_3p0n',
    '|tau 1 y(a1)| < 0.6 or |tau 0 y| < 0.1',
    Cut_hscale_down_1p1n_3p0n.cut.NOT().cut,
    channels=('1p1n_3p0n'),
    categories=[c for c in CATEGORIES if 'lscale_down' in c])
CUTBOOK.append(Cut_lscale_down_1p1n_3p0n)

##### SR definition (MMC mass window) #####
Cut_MMC_SR = DecoratedCut(
    'mmc_sr_window',
    '110 <  ditau_mmc_mlm_m < 150',
    'ditau_mmc_mlm_m > 110 && ditau_mmc_mlm_m < 150',
    categories=[c for c in CATEGORIES if '_sr' in c])
CUTBOOK.append(Cut_MMC_SR)

Cut_MMC_ZCR = DecoratedCut(
    'mmc_zcr_region',
    '60 < ditau_mmc_mlm_m < 110',
    'ditau_mmc_mlm_m > 60 && ditau_mmc_mlm_m < 110',
    categories=[c for c in CATEGORIES if '_zcr' in c])
CUTBOOK.append(Cut_MMC_ZCR)

Cut_MMC_ZPEAK = DecoratedCut(
    'mmc_zpeak_region',
    '70 < ditau_mmc_mlm_m < 110',
    'ditau_mmc_mlm_m > 70 && ditau_mmc_mlm_m < 110',
    categories=[c for c in CATEGORIES if '_zpeak' in c])
CUTBOOK.append(Cut_MMC_ZPEAK)

###### Split ZCR Boost region in 0/1 ###############
Cut_zcr_0 = DecoratedCut(
    'split_events_zcr_0',
    'split_events_zcr_0',
    'Rnd::zcr_split_cut(event_number, 0, 0.5) == 1',
    categories=[c for c in CATEGORIES if '_zcr_0' in c])
CUTBOOK.append(Cut_zcr_0)

Cut_zcr_1 = DecoratedCut(
    'split_events_zcr_1',
    'split_events_zcr_1',
    'Rnd::zcr_split_cut(event_number, 0.5, 1) == 1',
    categories=[c for c in CATEGORIES if '_zcr_1' in c])
CUTBOOK.append(Cut_zcr_1)

### Pileup dependence check ###
Cut_mu_0_20 = DecoratedCut(
    'mu_0_20',
    '0 < n_avg_int_cor < 20',
    'n_avg_int_cor > 0 && n_avg_int_cor < 20',
    categories=[c for c in CATEGORIES if 'mu_0_20' in c])
CUTBOOK.append(Cut_mu_0_20)

Cut_mu_20_30 = DecoratedCut(
    'mu_20_30',
    '20 < n_avg_int_cor < 30',
    'n_avg_int_cor > 20 && n_avg_int_cor < 30',
    categories=[c for c in CATEGORIES if 'mu_20_30' in c])
CUTBOOK.append(Cut_mu_20_30)

Cut_mu_30_40 = DecoratedCut(
    'mu_30_40',
    '30 < n_avg_int_cor < 40',
    'n_avg_int_cor > 30 && n_avg_int_cor < 40',
    categories=[c for c in CATEGORIES if 'mu_30_40' in c])
CUTBOOK.append(Cut_mu_30_40)

Cut_mu_40_50 = DecoratedCut(
    'mu_40_50',
    '40 < n_avg_int_cor < 50',
    'n_avg_int_cor > 40 && n_avg_int_cor < 50',
    categories=[c for c in CATEGORIES if 'mu_40_50' in c])
CUTBOOK.append(Cut_mu_40_50)

Cut_mu_50_60 = DecoratedCut(
    'mu_50_60',
    '50 < n_avg_int_cor < 60',
    'n_avg_int_cor > 50 && n_avg_int_cor < 60',
    categories=[c for c in CATEGORIES if 'mu_50_60' in c])
CUTBOOK.append(Cut_mu_50_60)

Cut_mu_50_80 = DecoratedCut(
    'mu_50_80',
    '50 < n_avg_int_cor < 80',
    'n_avg_int_cor > 50 && n_avg_int_cor < 80',
    categories=[c for c in CATEGORIES if 'mu_50_80' in c])
CUTBOOK.append(Cut_mu_50_80)


## Merging all decaymodes to get more statistics

Cut_alphaminus_ip_s = DecoratedCut(
    'alphaminus_ip_s',
    'alpha_(IP) < Pi/4',
    'ditau_CP_alphaminus_ip < 3.1416/4',
    channels=('1p0n_1p0n'),
    categories=[c for c in CATEGORIES if 'aminus_s' in c])
CUTBOOK.append(Cut_alphaminus_ip_s)

Cut_alphaminus_ip_l = DecoratedCut(
    'alphaminus_ip_l',
    'alpha_(IP) > Pi/4',
    'ditau_CP_alphaminus_ip > 3.1416*3/8',
    channels=('1p0n_1p0n'),
    categories=[c for c in CATEGORIES if 'aminus_l' in c])
CUTBOOK.append(Cut_alphaminus_ip_l)

Cut_alphaminus_ip_rho_s = DecoratedCut(
    'alphaminus_ip_rho_s',
    'alpha_(IPrho) < Pi/4',
    'ditau_CP_alphaminus_ip_rho < 3.1416/8',
    channels=('1p0n_1p1n','1p1n_1p0n','1p0n_1pXn','1pXn_1p0n'),
    categories=[c for c in CATEGORIES if 'aminus_s' in c])
CUTBOOK.append(Cut_alphaminus_ip_rho_s)

Cut_alphaminus_ip_rho_l = DecoratedCut(
    'alphaminus_ip_rho_l',
    'alpha_(IPrho) > Pi/4',
    'ditau_CP_alphaminus_ip_rho > 3.1416*3/8',
    channels=('1p0n_1p1n', '1p1n_1p0n', '1p0n_1pXn', '1pXn_1p0n'),
    categories=[c for c in CATEGORIES if 'aminus_l' in c])
CUTBOOK.append(Cut_alphaminus_ip_rho_l)

Cut_alphaminus_rho_rho_s = DecoratedCut(
    'alphaminus_rho_rho_s',
    'alpha_(rhorho) < Pi/4',
    'ditau_CP_alphaminus_rho_rho < 3.1416/8',
    channels=('1p1n_1p1n','1p1n_1pXn','1pXn_1p1n'),
    categories=[c for c in CATEGORIES if 'aminus_s' in c])
CUTBOOK.append(Cut_alphaminus_rho_rho_s)

Cut_alphaminus_rho_rho_l = DecoratedCut(
    'alphaminus_rhorho_l',
    'alpha_(rhorho) > Pi/4',
    'ditau_CP_alphaminus_rho_rho > 3.1416*3/8',
    channels=('1p1n_1p1n','1p1n_1pXn','1pXn_1p1n'),
    categories=[c for c in CATEGORIES if 'aminus_l' in c])
CUTBOOK.append(Cut_alphaminus_rho_rho_l)
