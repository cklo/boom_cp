"""
Implementation of the VBF categories
"""

from .base import DecoratedCut, CUTBOOK
from . import REGIONS, CATEGORIES
from ..mva_expr import vbf_tagger_expr

Cut_VBF_1_hh = DecoratedCut(
    'vbf_1',
    'vbf_1',
    '{tagger} > 0.0'.format(tagger=vbf_tagger_expr),
    categories=[c for c in CATEGORIES if 'vbf_1' in c])
    # categories=['vbf_1_sr'])
CUTBOOK.append(Cut_VBF_1_hh)

Cut_VBF_0_hh = DecoratedCut(
    'vbf_0',
    'vbf_0',
    Cut_VBF_1_hh.cut.NOT().cut,
    categories=[c for c in CATEGORIES if 'vbf_0' in c])
    # categories=['vbf_0_sr'])
CUTBOOK.append(Cut_VBF_0_hh)

# Cut_VBF_1_lh = DecoratedCut(
#     'vbf_1_lh',
#     'vbf_1_lh',
#     '{tagger} > 0.15'.format(tagger=vbf_tagger_expr),
#     channels=['e1p', 'e3p', 'mu1p', 'mu3p'],
#     categories=['vbf_1'])
# CUTBOOK.append(Cut_VBF_1_lh)

# Cut_VBF_0_lh = DecoratedCut(
#     'vbf_0_lh',
#     'vbf_0_lh',
#     Cut_VBF_1_lh.cut.NOT().cut,
#     channels=['e1p', 'e3p', 'mu1p', 'mu3p'],
#     categories=['vbf_0'])
# CUTBOOK.append(Cut_VBF_0_lh)

# Cut_VBF_1_ll = DecoratedCut(
#     'vbf_1_ll',
#     'vbf_1_ll',
#     '{tagger} > 0.15'.format(tagger=vbf_tagger_expr),
#     channels=['emu', 'mue', 'ee', 'mumu'],
#     categories=['vbf_1'])
# CUTBOOK.append(Cut_VBF_1_ll)

# Cut_VBF_0_ll = DecoratedCut(
#     'vbf_0_ll',
#     'vbf_0_ll',
#     Cut_VBF_1_ll.cut.NOT().cut,
#     channels=['emu', 'mue', 'ee', 'mumu'],
#     categories=['vbf_0'])
# CUTBOOK.append(Cut_VBF_0_ll)

