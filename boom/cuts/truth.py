"""
Truth-level cuts for the MC samples
"""

from . import CHANNELS, YEARS, CATEGORIES, REGIONS, SYSTEMATICS
from .base import DecoratedCut, CUTBOOK


# Truth matching cuts
CutLeadOrSubLeadTau_NotAJet = DecoratedCut(
    'LeadSubLeadTau_NotAJet',
    'lead tau or sublead tau is not truth matched to a quark or a gluon',
    '((abs(tau_0_matched_pdgId) < 7 || tau_0_matched_pdgId == 21) == 0) || ((abs(tau_1_matched_pdgId) < 7 || tau_1_matched_pdgId == 21) == 0)',
    mc_only=True)
#CUTBOOK.append(CutLeadOrSubLeadTau_NotAJet)

CutLeadAndSubLeadTau_NotAJet = DecoratedCut(
    'LeadSubLeadTau_NotAJet',
    'lead tau and sublead tau are not truth matched to a quark or a gluon',
    '((abs(tau_0_matched_pdgId) < 7 || tau_0_matched_pdgId == 21) == 0) && ((abs(tau_1_matched_pdgId) < 7 || tau_1_matched_pdgId == 21) == 0)',
    mc_only=True)
CUTBOOK.append(CutLeadAndSubLeadTau_NotAJet)

CutHadTau_NotAJet = DecoratedCut(
    'HadTau_NotAJet',
    'had tau not truth matched to a quark or a gluon',
    '((abs(tau_0_matched_pdgId) < 7 || tau_0_matched_pdgId == 21) == 0)',
    regions=[t for t in REGIONS if t.endswith('hhff')],
    mc_only=True)
CUTBOOK.append(CutLeadAndSubLeadTau_NotAJet)

