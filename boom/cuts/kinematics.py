"""
Event kinematic cuts
"""

from . import CHANNELS, YEARS, CATEGORIES, REGIONS, SYSTEMATICS
from .base import DecoratedCut, CUTBOOK


### 
# Lead jet pt cut
CutJetPt_hh = DecoratedCut(
    'lead_jet', 'lead jet',
    'jet_0_p4.Pt() > 70 && fabs(jet_0_p4.Eta()) < 3.2')
CUTBOOK.append(CutJetPt_hh)

# MET Cuts
CutMET = DecoratedCut(
    'met_cut', 'met > 20',
    'met_p4.Pt() > 20')
CUTBOOK.append(CutMET)

# X0. X1 Cuts
CutX0X1_low = DecoratedCut(
    'x0x1_low', 
    'lower cut on Collinear approx of the two visible leptons',
    'ditau_coll_approx_x0 > 0.1 && ditau_coll_approx_x1 > 0.1')
CUTBOOK.append(CutX0X1_low)

CutX0X1_high_hh = DecoratedCut(
    'x0x1_ll', 
    'higher cut of Collinear approx of the two visible leptons',
    'ditau_coll_approx_x0 < 1.4 && ditau_coll_approx_x1 < 1.4')
CUTBOOK.append(CutX0X1_high_hh)

# Angular Cuts
CutDiTauAngle_hh = DecoratedCut(
    'ditau_dr_hh', '0.6 < DR(t_l, t_l) < 2.5 and |deta(t_h, t_h)| < 1.5',
    'ditau_dr > 0.6 && ditau_dr < 2.5 && fabs(ditau_deta) < 1.5',
    regions=[t for t in REGIONS if not t.startswith("high_deta")],
    categories=[t for t in CATEGORIES if t not in ('qcd_fit')])
CUTBOOK.append(CutDiTauAngle_hh)

CutDiTauAngle_hh_qcdfit = DecoratedCut(
    'ditau_dr_hh_fake', '0.6 < DR(t_l, t_l) < 2.5 and |deta(t_h, t_h)| < 2.0',
    'ditau_dr > 0.6 && ditau_dr < 2.5 && fabs(ditau_deta) < 2.0',
    categories=[t for t in CATEGORIES if t in ('qcd_fit')])
CUTBOOK.append(CutDiTauAngle_hh_qcdfit)

CutDiTauAngle_hh_high_deta = DecoratedCut(
    'ditau_dr_hh_fake',
    '0.6 < DR(t_l, t_l) < 2.5 and |deta(t_h, t_h)| < 2.0',
    'ditau_dr > 0.6 && ditau_dr < 2.5 && fabs(ditau_deta) > 1.5 && fabs(ditau_deta) < 2.0',
    regions=[t for t in REGIONS if t.startswith("high_deta")])
CUTBOOK.append(CutDiTauAngle_hh_high_deta)

# MMC status
Cut_MMCStatus = DecoratedCut(
    'mmc_status',
    'mmc_status',
    'ditau_mmc_mlm_fit_status == 1')
CUTBOOK.append(Cut_MMCStatus)
