"""
Object-level cuts (counting, pt, eta, ID, isolation)
"""
from . import CHANNELS, YEARS, CATEGORIES, REGIONS, SYSTEMATICS
from .base import DecoratedCut, CUTBOOK

# Object counting
CutCountTAUTAU = DecoratedCut(
    'count_tautau', '0 el, 0 muon, 2 taus',
    'n_electrons == 0 && n_muons == 0 && n_taus == 2')
CUTBOOK.append(CutCountTAUTAU)


####
# Object level cuts
####

# had-had pt cuts
CutTauPt_hh = DecoratedCut(
    'tau_pt_hh', 
    'pt(tau_0) > 40 and pt(tau_1) > 30',
    'tau_0_p4.Pt() > 40 && tau_1_p4.Pt() > 30')
CUTBOOK.append(CutTauPt_hh)

CutTauCharge_HH = DecoratedCut(
    'tau-charge', 
    '|q(tau_0)| == 1 and |q(tau_1)| == 1',
    'abs(tau_0_q) == 1 && abs(tau_1_q) == 1')
CUTBOOK.append(CutTauCharge_HH)

CutTauIDMedium = DecoratedCut(
    'tau-id-medium', 'medium-medium',
    '((tau_0_n_charged_tracks == 1 && tau_0_jet_rnn_score_trans > 0.25 ) || (tau_0_n_charged_tracks == 3 && tau_0_jet_rnn_score_trans > 0.4)) && ((tau_1_n_charged_tracks == 1 && tau_1_jet_rnn_score_trans > 0.25 ) || (tau_1_n_charged_tracks == 3 && tau_1_jet_rnn_score_trans > 0.4))',
    regions=[t for t in REGIONS if not t in ('anti_tau', 'same_sign_anti_tau','fake_factors_hh_same_sign_anti_lead_tau', 'fake_factors_hh_same_sign_anti_sublead_tau', 'fake_factors_hh_same_sign_anti_lead_tau_loose', 'fake_factors_hh_same_sign_anti_sublead_tau_loose', 'high_deta_anti_lead_tau', 'high_deta_anti_sublead_tau', 'high_deta_anti_lead_tau_loose', 'high_deta_anti_sublead_tau_loose', 'high_deta_anti_tau', 'high_deta_same_sign_anti_tau')])
CUTBOOK.append(CutTauIDMedium)

CutTauIDAntiMedium = DecoratedCut(
    'tau-id-anti-medium', 'not medium - not medium',
    '(!(((tau_0_n_charged_tracks == 1 && tau_0_jet_rnn_score_trans > 0.25 ) || (tau_0_n_charged_tracks == 3 && tau_0_jet_rnn_score_trans > 0.4)) && ((tau_1_n_charged_tracks == 1 && tau_1_jet_rnn_score_trans > 0.25 ) || (tau_1_n_charged_tracks == 3 && tau_1_jet_rnn_score_trans > 0.4))))',
    regions=[t for t in REGIONS if t in ('anti_tau', 'same_sign_anti_tau')])
#CUTBOOK.append(CutTauIDAntiMedium)

CutTauIDAntiMediumFull = DecoratedCut(
    'tau-id-anti-medium', 'not medium - not medium',
    '(tau_0_jet_rnn_medium != 1 || tau_1_jet_rnn_medium != 1) && (tau_0_jet_rnn_loose == 1 || tau_1_jet_rnn_loose == 1)',
    regions=['anti_tau', 'same_sign_anti_tau', 'nos_anti_tau', 'high_deta_anti_tau', 'high_deta_same_sign_anti_tau'])
CUTBOOK.append(CutTauIDAntiMediumFull)

CutTauIDMinScore = DecoratedCut(
    'minimu-score', 'minimum-score',
    'tau_0_jet_rnn_score_trans > 0.01 && tau_1_jet_rnn_score_trans > 0.01')
CUTBOOK.append(CutTauIDMinScore)    

########## added for per-tau ff derivation
CutTauIDAntiMediumLead = DecoratedCut(
    'tau-0-id-anti-medium', 'not medium - medium',
    'tau_0_jet_rnn_medium != 1 && tau_1_jet_rnn_medium == 1',
    regions=['fake_factors_hh_same_sign_anti_lead_tau', 'high_deta_anti_lead_tau'])
CUTBOOK.append(CutTauIDAntiMediumLead)

CutTauIDAntiMediumSublead = DecoratedCut(
    'tau-1-id-anti-medium', 'medium - not medium',
    'tau_0_jet_rnn_medium == 1 && tau_1_jet_rnn_medium != 1',
    regions=['fake_factors_hh_same_sign_anti_sublead_tau', 'high_deta_anti_sublead_tau'])
CUTBOOK.append(CutTauIDAntiMediumSublead)

CutTauIDAntiMediumLeadLoose = DecoratedCut(
    'tau-0-id-loose-anti-medium', 'loose not medium - medium',
    'tau_0_jet_rnn_medium != 1 && tau_1_jet_rnn_medium == 1 && tau_0_jet_rnn_loose == 1',
    regions=['fake_factors_hh_same_sign_anti_lead_tau_loose', 'high_deta_anti_lead_tau_loose'])
CUTBOOK.append(CutTauIDAntiMediumLeadLoose)

CutTauIDAntiMediumSubleadLoose = DecoratedCut(
    'tau-1-id-loose-anti-medium', 'medium - loose not medium',
    'tau_0_jet_rnn_medium == 1 && tau_1_jet_rnn_medium != 1 && tau_1_jet_rnn_loose == 1',
    regions=['fake_factors_hh_same_sign_anti_sublead_tau_loose', 'high_deta_anti_sublead_tau_loose'])
CUTBOOK.append(CutTauIDAntiMediumSubleadLoose)

Cut_1p0n_1p0n = DecoratedCut(
    '1p0n_1p0n', '1p0n_1p0n',
    'tau_0_decay_mode == 0 && tau_1_decay_mode == 0',
    channels=['1p0n_1p0n'])
CUTBOOK.append(Cut_1p0n_1p0n)

Cut_1p0n_1p1n = DecoratedCut(
    '1p0n_1p1n', '1p0n_1p1n',
    'tau_0_decay_mode == 0 && tau_1_decay_mode == 1',
    channels=['1p0n_1p1n'])
CUTBOOK.append(Cut_1p0n_1p1n)

Cut_1p1n_1p0n = DecoratedCut(
    '1p1n_1p0n', '1p1n_1p0n',
    'tau_0_decay_mode == 1 && tau_1_decay_mode == 0',
    channels=['1p1n_1p0n'])
CUTBOOK.append(Cut_1p1n_1p0n)

Cut_1p1n_1p1n = DecoratedCut(
    '1p1n_1p1n', '1p1n_1p1n',
    'tau_0_decay_mode == 1 && tau_1_decay_mode == 1',
    channels=['1p1n_1p1n'])
CUTBOOK.append(Cut_1p1n_1p1n)

Cut_1p1n_1pXn = DecoratedCut(
    '1p1n_1pXn', '1p1n_1pXn',
    'tau_0_decay_mode == 1 && tau_1_decay_mode == 2',
    channels=['1p1n_1pXn'])
CUTBOOK.append(Cut_1p1n_1pXn)

Cut_1pXn_1p1n = DecoratedCut(
    '1pXn_1p1n', '1pXn_1p1n',
    'tau_0_decay_mode == 2 && tau_1_decay_mode == 1',
    channels=['1pXn_1p1n'])
CUTBOOK.append(Cut_1pXn_1p1n)

Cut_1p0n_1pXn = DecoratedCut(
    '1p0n_1pXn', '1p0n_1pXn',
    'tau_0_decay_mode == 0 && tau_1_decay_mode == 2',
    channels=['1p0n_1pXn'])
CUTBOOK.append(Cut_1p0n_1pXn)

Cut_1pXn_1p0n = DecoratedCut(
    '1pXn_1p0n', '1pXn_1p0n',
    'tau_0_decay_mode == 2 && tau_1_decay_mode == 0',
    channels=['1pXn_1p0n'])
CUTBOOK.append(Cut_1pXn_1p0n)

Cut_1p1n_3p0n = DecoratedCut(
    '1p1n_3p0n', '1p1n_3p0n',
    'tau_0_decay_mode == 1 && tau_1_decay_mode == 3',
    channels=['1p1n_3p0n'])
CUTBOOK.append(Cut_1p1n_3p0n)

Cut_3p0n_1p1n = DecoratedCut(
    '3p0n_1p1n', '3p0n_1p1n',
    'tau_0_decay_mode == 3 && tau_1_decay_mode == 1',
    channels=['3p0n_1p1n'])
CUTBOOK.append(Cut_3p0n_1p1n)

