import ROOT
from .extern.tabulate import tabulate
from .selection_utils import filter_selections
from .lumi import LUMI


def rqcd_calc(h1, h2):
    """
    Helper function to compute the ratio (and its error)
    of the integral of two histograms
    """

    h_ratio = h1.Clone()
    h_ratio = h_ratio.Rebin(h_ratio.GetNbinsX())
    h_den = h2.Clone()
    h_den = h_den.Rebin(h2.GetNbinsX())
    h_ratio.Divide(h_den)
    rqcd_val, rqcd_err = yield_tuple(h_ratio)
    return rqcd_val, rqcd_err


def hist_max(hists):
    """
    Helper function to determine
    max of list of histogram
    """
    if not isinstance(hists, (list, tuple)):
        raise TypeError('type(hists) = {} is not a list or a tuple'.format(type(hists)))

    if len(hists) == 0:
        return 0.
    maximum = max([h.GetBinContent(h.GetMaximumBin()) for h in hists])
    return maximum

def hist_sum(hists):
    """
    Helper function to sum 
    a list of histograms
    """
    if not isinstance(hists, (list, tuple)):
        raise TypeError('type(hists) = {} is not a list or a tuple'.format(type(hists)))
    #hist = hists[0].Clone()
    #for h in hists[1:]:
    #    hist.Add(h)

    # Cast list of hists to TList so Merge can be used
    # (Slightly faster than repeatedly caqlling "Add()")
    hist = hists[0].Clone()
    hist_collection = ROOT.TList()
    for h in hists[1:]:
        hist_collection.Add(h)
    hist.Merge(hist_collection)
    return hist


def blinded_hist(h, var_name='mmc', signal=None, signal_frac=0.5, rtol_blind_range=True):
    """
    Helper function to blind 
    histogram range
    """

    if not isinstance(h, ROOT.TH1):
        raise NotImplementedError

    blind_range = {
        'mmc': (110, 150),
        'visible': (70, 110),
        'collinear': (100, 150),
        'phi_star' : (0.1, 6.3),
        'phi_star_3bins': (0.1, 6.3),
        'phi_star_5bins': (0.1, 6.3),
        'phi_star_7bins': (0.1, 6.3),
        'phi_star_9bins': (0.1, 6.3),
        }


    if signal is not None:
        if not isinstance(signal, ROOT.TH1):
            raise NotImplementedError

        _bin_min = 0
        if rtol_blind_range:
            for ibin in reversed(range(signal.GetNbinsX() + 1)):
               _frac = float(signal.Integral(ibin, signal.GetNbinsX() + 1))
               _frac /= float(signal.Integral(0,signal.GetNbinsX() + 1))
               if _frac > signal_frac:
                   break
               else:
                   _bin_max = ibin
            bin_min = 0
            bin_max = _bin_max 
            
        else:
            for ibin in range(signal.GetNbinsX() + 1):
               _frac = float(signal.Integral(0, ibin))
               _frac /= float(signal.Integral(0, signal.GetNbinsX() + 1))
               if _frac > signal_frac:
                   break
               else:
                   _bin_min = ibin
            bin_min = _bin_min 
            bin_max = signal.GetNbinsX() + 1
            
    else:
        x_min, x_max = blind_range[var_name]
        bin_min = h.FindBin(x_min)
        bin_max = h.FindBin(x_max)

    h_blind = h.Clone(h.GetName() + '_blinded')
    h_blind.SetTitle(h.GetTitle())

    for ibin in range(bin_min, bin_max):
        h_blind.SetBinContent(ibin, 0)
        h_blind.SetBinError(ibin, 0)
    
    return h_blind


def significance_counting(signal, bkg, x_range=(100, 150), return_pval=False):
    """
    Helper function to perform stat-only counting-experiment
    """
    if not isinstance(signal, ROOT.TH1):
        raise NotImplementedError

    if not isinstance(bkg, ROOT.TH1):
        raise NotImplementedError
    
    if not isinstance(x_range, (list, tuple)):
        raise TypeError('x_range should be a list or a tuple')
    
    if len(x_range) != 2:
        raise ValueError('x_range is expected to be of size 2')

    x_min, x_max = x_range[0], x_range[1]
    bin_min = signal.FindBin(x_min)
    bin_max = signal.FindBin(x_max)
    
    _n_sig = signal.Integral(bin_min, bin_max)
    bkg_err = ROOT.Double(0.)
    _n_bkg = bkg.IntegralAndError(bin_min, bin_max, bkg_err)
    _rel_bkg_err = bkg_err / _n_bkg
    
    if return_pval:
        return ROOT.RooStats.NumberCountingUtils.BinomialExpP(
            _n_sig, _n_bkg, _rel_bkg_err)
    else:
        return ROOT.RooStats.NumberCountingUtils.BinomialExpZ(
            _n_sig, _n_bkg, _rel_bkg_err)
        



def yield_tuple(hist):
    """
    Helper function to retrieve total yields and error from 
    a ROOT.TH1F (including over/underflow bins)
    """
    from array import array
    error = array('d', [0])
    integral = hist.IntegralAndError(0, hist.GetNbinsX() + 1, error)
    return integral, error[0]

def yield_table(data, signal, bkgs, bkg_names=None):
    """
    Pretty print-out of the yields
    """

    if not isinstance(data, ROOT.TH1):
        raise TypeError

    if not isinstance(signal, ROOT.TH1):
        raise TypeError

    if not isinstance(bkgs, (list, tuple)):
        bkgs = [bkgs]

    for b in bkgs:
        if not isinstance(b, ROOT.TH1):
            raise TypeError
        

    table = []

    if bkg_names is None:
        bkg_names = [b.GetName() for b in bkgs]
    
    if len(bkg_names) != len(bkgs):
        raise ValueError


    for name, b in zip(bkg_names, bkgs):
        integral, error = yield_tuple(b)
        line = [name, integral, error]
        table.append(line)

    bkg = hist_sum(bkgs)
    tot_bkg, tot_bkg_err = yield_tuple(bkg)
    tot_sig, tot_sig_err = yield_tuple(signal)
    tot_data, _ = yield_tuple(data)

    table.append(['Signal', tot_sig, tot_sig_err])
    table.append(['Tot. Bkg', tot_bkg, tot_bkg_err])
    table.append(['Data', tot_data,])
              
    headers = ['Sample', 'Yields', 'Stat. Uncert']
    nice_table = tabulate(table, headers=headers, tablefmt='orgtbl', floatfmt='.2f')
    return nice_table


def lumi_calc(selections):
    """
    infer the integrated luminosity
    based on the list of selections
    """
    int_lumi = 0
    for year in ('15', '16', '17', '18'):
        _sels = filter_selections(selections, years=year)
        if len(_sels) != 0:
            int_lumi += LUMI[year]

    return int_lumi

def plot_title_extension(    
    selections,
    channels=None,
    categories=None,
    years=None,
    regions=None,
    triggers=None):
    """Function to generate plot title and file extension

    Arguments
    _________
    selections : list(selection)
       selection objects to filter on
    channels: str or list(str) 
       see CHANNELS in cuts/__init__.py
    categories: str or list(str) 
       see CATEGORIES in cuts/__init__.py
    years: str or list(str) 
       see YEARS in cuts/__init__.py
    regions: str or list(str) 
       see REGIONS in cuts/__init__.py
    triggers: str or list(str) 
       see TRIGGERS in cuts/__init__.py

    Returns
    _______
    title, extension: (str, str)
       plot title, file extension
    """

    _channels = []
    if channels is None:
        for sel in selections:
            if not sel.channel in _channels:
                _channels.append(sel.channel)
    else:
        if isinstance(channels, (list, tuple)):
            _channels = channels[:]
        else:
            _channels = [channels]
    _channels = sorted(_channels)

    _categories = []
    if categories is None:
        for sel in selections:
            if not sel.category in _categories:
                _categories.append(sel.category)
    else:
        if isinstance(categories, (list, tuple)):
            _categories = categories[:]
        else:
            _categories = [categories]
    _categories = sorted(_categories)

    _years = []
    if years is None:
        for sel in selections:
            if not sel.year in _years:
                _years.append(sel.year)
    else:
        if isinstance(years, (list, tuple)):
            _years = years[:]
        else:
            _years = [years]
    _years = sorted(_years)

    _regions = []
    if regions is None:
        for sel in selections:
            if not sel.region in _regions:
                _regions.append(sel.region)
    else:
        if isinstance(regions, (list, tuple)):
            _regions = regions[:]
        else:
            _regions = [regions]
    _regions = sorted(_regions)
    _triggers = []
    if triggers is None:
        for sel in selections:
            if not sel.trigger in _triggers:
                _triggers.append(sel.trigger)
    else:
        if isinstance(triggers, (list, tuple)):
            _triggers = triggers[:]
        else:
            _triggers = [triggers]
    _triggers = sorted(_triggers)

    # channels
    if sorted(_channels) == ['1p0n_1p0n', '1p0n_1p1n', '1p0n_1pXn', '1p1n_1p0n', '1p1n_1p1n', '1p1n_1pXn', '1p1n_3p0n', '1pXn_1p0n', '1pXn_1p1n', '3p0n_1p1n']:
        _chan_str = 'hh'
    else:
        _chan_str = ', '.join(_channels)

    # categories
    if _categories == sorted(['boost_tight_high_sr','boost_loose_high_sr','boost_tight_low_sr','boost_loose_low_sr']):
        _cat_str = 'Boost Incl SR'
    elif _categories == sorted(['boost_tight_sr','boost_loose_sr']):
        _cat_str = 'Boost Incl SR'
    elif _categories == sorted(['boost_tight_high_sr','boost_tight_low_sr']):
        _cat_str = 'Boost Tight SR'
    elif _categories == sorted(['boost_tight_sr']):
        _cat_str = 'Boost Tight SR'
    elif _categories == sorted(['boost_loose_high_sr','boost_loose_low_sr']):
        _cat_str = 'Boost Loose SR'
    elif _categories == sorted(['boost_loose_sr']):
        _cat_str = 'Boost Loose SR'
    elif _categories == sorted(['boost_high_sr','boost_low_sr']):
        _cat_str = 'Boost Incl SR'
    elif _categories == sorted(['boost_sr']):
        _cat_str = 'Boost Incl SR'
    elif _categories == sorted(['boost_tight_high_zcr','boost_loose_high_zcr','boost_tight_low_zcr','boost_loose_low_zcr']):
        _cat_str = 'Boost Incl ZCR'
    elif _categories == sorted(['boost_tight_high_zpeak','boost_loose_high_zpeak','boost_tight_low_zpeak','boost_loose_low_zpeak']):
        _cat_str = 'Boost Incl Z-Peak'
    elif _categories == sorted(['boost_tight_zcr','boost_loose_zcr']):
        _cat_str = 'Boost Incl ZCR'
    elif _categories == sorted(['boost_tight_zpeak','boost_loose_zpeak']):
        _cat_str = 'Boost Incl Z-Peak'
    elif _categories == sorted(['boost_high_zcr','boost_low_zcr']):
        _cat_str = 'Boost Incl ZCR'
    elif _categories == sorted(['boost_high_zpeak','boost_low_zpeak']):
        _cat_str = 'Boost Incl Z-Peak'
    elif _categories == sorted(['boost_zcr']):
        _cat_str = 'Boost Incl ZCR'
    elif _categories == sorted(['boost_zpeak']):
        _cat_str = 'Boost Incl Z-Peak'
    elif _categories == sorted(['boost_tight_high_zcr','boost_tight_low_zcr']):
        _cat_str = 'Boost Tight ZCR'
    elif _categories == sorted(['boost_tight_high_zpeak','boost_tight_low_zpeak']):
        _cat_str = 'Boost Tight Z-Peak'
    elif _categories == sorted(['boost_tight_zcr']):
        _cat_str = 'Boost Tight ZCR'
    elif _categories == sorted(['boost_tight_zpeak']):
        _cat_str = 'Boost Tight Z-Peak'
    elif _categories == sorted(['boost_loose_high_zcr','boost_loose_low_zcr']):
        _cat_str = 'Boost Loose ZCR'
    elif _categories == sorted(['boost_loose_high_zpeak','boost_loose_low_zpeak']):
        _cat_str = 'Boost Loose Z-Peak'
    elif _categories == sorted(['boost_loose_zcr']):
        _cat_str = 'Boost Loose ZCR'
    elif _categories == sorted(['boost_loose_zpeak']):
        _cat_str = 'Boost Loose Z-Peak'
    elif _categories == sorted(['vbf_high_sr','vbf_low_sr']):
        _cat_str = 'VBF SR'
    elif _categories == sorted(['vbf_sr']):
        _cat_str = 'VBF SR'
    elif _categories == sorted(['vbf_high_zcr','vbf_low_zcr']):
        _cat_str = 'VBF ZCR'
    elif _categories == sorted(['vbf_high_zpeak','vbf_low_zpeak']):
        _cat_str = 'VBF Z-Peak'
    elif _categories == sorted(['vbf_zcr']):
        _cat_str = 'VBF ZCR'
    elif _categories == sorted(['vbf_zpeak']):
        _cat_str = 'VBF Z-Peak'
    elif _categories == sorted(['boost']):
        _cat_str = 'Boost Incl SR and ZCR' 
    elif _categories == sorted(['boost_tight_high_zcr','boost_loose_high_zcr','boost_tight_low_zcr','boost_loose_low_zcr','boost_tight_high_sr','boost_loose_high_sr','boost_tight_low_sr','boost_loose_low_sr']):
        _cat_str = 'Boost Incl SR and ZCR'
    elif _categories == sorted(['boost_tight_zcr','boost_loose_zcr','boost_tight_low_zcr','boost_tight_sr','boost_loose_sr','boost_tight_low_sr']):
        _cat_str = 'Boost Incl SR and ZCR'
    elif _categories == sorted(['boost_tight_zcr','boost_loose_zcr','boost_tight_sr','boost_loose_sr']):
        _cat_str = 'Boost Incl SR and ZCR'
    elif _categories == sorted(['boost_zcr','boost_sr']):
        _cat_str = 'Boost Incl SR and ZCR'
    elif _categories == sorted(['boost_tight_high_zcr','boost_tight_low_zcr','boost_tight_high_sr','boost_tight_low_sr']):
        _cat_str = 'Boost Tight SR and ZCR'
    elif _categories == sorted(['boost_tight_zcr','boost_tight_sr']):
        _cat_str = 'Boost Tight SR and ZCR'
    elif _categories == sorted(['boost_tight_high_zcr','boost_tight_high_sr']):
        _cat_str = 'Boost Tight High SR and ZCR'
    elif _categories == sorted(['boost_tight_low_zcr','boost_tight_low_sr']):
        _cat_str = 'Boost Tight Low SR and ZCR'
    elif _categories == sorted(['boost_loose_high_zcr','boost_loose_high_sr']):
        _cat_str = 'Boost Loose High SR and ZCR'
    elif _categories == sorted(['boost_loose_low_zcr','boost_loose_low_sr']):
        _cat_str = 'Boost Loose Low SR and ZCR'
    elif _categories == sorted(['boost_loose_high_zcr','boost_loose_low_zcr','boost_loose_high_sr','boost_loose_low_sr']):
        _cat_str = 'Boost Loose SR and ZCR'
    elif _categories == sorted(['boost_loose_zcr','boost_loose_sr']):
        _cat_str = 'Boost Loose SR and ZCR'
    elif _categories == sorted(['boost_tight_high_zcr','boost_loose_high_zcr', 'boost_tight_high_sr','boost_loose_high_sr']):
        _cat_str = 'Boost High SR and ZCR'
    elif _categories == sorted(['boost_tight_low_zcr','boost_loose_low_zcr', 'boost_tight_low_sr','boost_loose_low_sr']):
        _cat_str = 'Boost Low SR and ZCR'
    elif _categories == sorted(['vbf_high_zcr','vbf_low_zcr','vbf_high_sr','vbf_low_sr']):
        _cat_str = 'VBF SR and ZCR'
    elif _categories == sorted(['vbf_zcr','vbf_sr']):
        _cat_str = 'VBF SR and ZCR'
    elif _categories == sorted(['vbf']):
        _cat_str = 'VBF SR and ZCR'
    elif _categories == sorted(['vbf_loose_sr','vbf_loose_zcr']):
        _cat_str = 'VBF Loose SR and ZCR'
    elif _categories == sorted(['vbf_tight_sr','vbf_tight_zcr']):
        _cat_str = 'VBF Tight SR and ZCR'
    elif _categories == sorted(['vbf_downdr_sr','vbf_downdr_zcr']):
        _cat_str = 'VBF LowDR SR and ZCR'
    elif _categories == sorted(['vbf_high_zcr','vbf_high_sr']):
        _cat_str = 'VBF High SR and ZCR'
    elif _categories == sorted(['vbf_low_zcr','vbf_low_sr']):
        _cat_str = 'VBF Low SR and ZCR'
    else:
        _cat_str = ', '.join(_categories)

    # years
    if _years == ['15', '16', '17', '18']:
        _year_str = ''
    else:
        _year_str = ', '.join(_years)

    # regions
    if len(_regions) > 1:
        _region_str = ''
    else:
        _region_str = _regions[0]

    if _region_str == 'SR':
        _region_str = ''
        
    # triggers
    _trigger_str = ''

    _title = [_chan_str, _cat_str, _region_str, _trigger_str]
    _title = [s for s in _title if s != '']
    _title_str = ', '.join(_title)

    _extension = [_chan_str, _cat_str, _year_str, _region_str, _trigger_str]
    _extension = [s for s in _extension if s != '']
    _extension_str = '_'.join(_extension)
    _extension_str = _extension_str.replace(',', '').replace(' ', '_')

    return _title_str, _extension_str
