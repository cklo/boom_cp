"""
Implementation of the Fake weights
"""
from happy.systematics import Systematics, SystematicsSet

hh_fake_systs = ["hh_fake_ff_stat_1p0n_nm",
                 "hh_fake_ff_mcsubtr_1p0n_nm",
                 "hh_fake_ff_stat_1p1n_nm",
                 "hh_fake_ff_mcsubtr_1p1n_nm",
                 "hh_fake_ff_stat_1pXn_nm",
                 "hh_fake_ff_mcsubtr_1pXn_nm",
                 "hh_fake_ff_stat_3p0n_nm",
                 "hh_fake_ff_mcsubtr_3p0n_nm",
                 "hh_fake_ff_stat_1p0n_lnm",
                 "hh_fake_ff_mcsubtr_1p0n_lnm",
                 "hh_fake_ff_stat_1p1n_lnm",
                 "hh_fake_ff_mcsubtr_1p1n_lnm",
                 "hh_fake_ff_stat_1pXn_lnm",
                 "hh_fake_ff_mcsubtr_1pXn_lnm",
                 "hh_fake_ff_stat_3p0n_lnm",
                 "hh_fake_ff_mcsubtr_3p0n_lnm",
                 "hh_fake_ff_param",
                 "hh_fake_ff_composition_ss",
                 "hh_fake_ff_composition_highdeta",
                ]

def get_fake_weights(channel, category, year, region, trigger, is_data):
    """Retrieve the Fake weights

    Parameters
    __________
    channel : str
       see CHANNELS in cuts/__init__.py
    trigger : str
       see TRIGGERS in cuts/__init__.py
    year: str
       see YEARS in cuts/__init__.py
    category : str
       see CATEGORIES in cuts/__init__.py
    region: str or list(str) 
       see REGIONS in cuts/__init__.py
    is_data : bool
       switch between data and MC

    Returns
    _______
    sys_set : HAPPy SystematicsSet
       set of weights to normalise the Fake template

    Raises
    ______
    NotImplementedError
       If there is no weight for a given combination of input parameters
    """

    sys_set = SystematicsSet()

    from .hhfake import hh_2dobj_ff_expr

    if 'preselection' in category:
        cat = 'preselection'
    elif 'boost' in category:
        cat = 'boost'
    elif 'vh' in category:
        cat = 'vh'
    else :
        cat = 'vbf'

    _ff_expr_nom = hh_2dobj_ff_expr(category, 'nominal')
    for syst_name in hh_fake_systs:
        _ff_expr_syst = hh_2dobj_ff_expr(category, syst_name)
        sysw = Systematics.weightSystematics(
                syst_name,
                syst_name,
                _ff_expr_syst,
                _ff_expr_syst,
                _ff_expr_nom
        )
        sys_set.add(sysw)


    if not is_data:
        substraction = Systematics.weightSystematics(
            '-1', '-1', '-1', '-1', '-1')
        sys_set.add(substraction)

    return sys_set
        
