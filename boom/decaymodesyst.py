"""
Implementation of the DecayMode scale factors/systematics on top of the TauID
"""
import ROOT
import os
from happy.systematics import Systematics, SystematicsSet

try:
    ROOT.gSystem.CompileMacro('boom/cpp/decaymode_helper.cpp', 'k-', '', 'cache')
    print('BOOM:\t DecayMode syst helper tool loaded!')
except:
    raise RuntimeError

# this is needed because of
# https://sft.its.cern.ch/jira/browse/ROOT-7216
try:
    ROOT.DecayModeSystHelper()
except:
    print('DecayMode Syst tool not compiled')

__all__ = [
    # np per truth-reco decay mode combination
    'decaymode_syst_singletau_true_1p0n_reco_1p0n_expr',
    'decaymode_syst_singletau_true_1p1n_reco_1p0n_expr',
    'decaymode_syst_singletau_true_1p1n_reco_1p1n_expr',
    'decaymode_syst_singletau_true_1p1n_reco_1pXn_expr',
    'decaymode_syst_singletau_true_1pXn_reco_1p1n_expr',
    'decaymode_syst_singletau_true_1pXn_reco_1pXn_expr',
    'decaymode_syst_singletau_true_3p0n_reco_3p0n_expr',
    'decaymode_syst_singletau_true_3pXn_reco_3p0n_expr',
  ]



_HH_DECAYMODE_SYST_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/DecayModeSubstructure_TrueHadTau_2019-summer.root")

_root_file = ROOT.TFile(_HH_DECAYMODE_SYST_FILE, "read") 

ROOT.decaymode_histDict.h_sf_decaymode_1p0n[0]    = _root_file.Get("sf_jetbdtsigmedium_r1p0n")
ROOT.decaymode_histDict.h_sf_decaymode_1p1n[0]    = _root_file.Get("sf_jetbdtsigmedium_r1p1n")
ROOT.decaymode_histDict.h_sf_decaymode_1pXn[0]    = _root_file.Get("sf_jetbdtsigmedium_r1pXn")
ROOT.decaymode_histDict.h_sf_decaymode_3p0n[0]    = _root_file.Get("sf_jetbdtsigmedium_r3p0n")
ROOT.decaymode_histDict.h_sf_decaymode_3pXn[0]    = _root_file.Get("sf_jetbdtsigmedium_r3pXn")
ROOT.decaymode_histDict.h_total_decaymode_1p0n[0] = _root_file.Get("TOTAL_jetbdtsigmedium_r1p0n")
ROOT.decaymode_histDict.h_total_decaymode_1p1n[0] = _root_file.Get("TOTAL_jetbdtsigmedium_r1p1n")
ROOT.decaymode_histDict.h_total_decaymode_1pXn[0] = _root_file.Get("TOTAL_jetbdtsigmedium_r1pXn")
ROOT.decaymode_histDict.h_total_decaymode_3p0n[0] = _root_file.Get("TOTAL_jetbdtsigmedium_r3p0n")
ROOT.decaymode_histDict.h_total_decaymode_3pXn[0] = _root_file.Get("TOTAL_jetbdtsigmedium_r3pXn")

def decaymode_syst_singletau_true_1p0n_reco_1p0n_expr(reco_decay_mode,truth_decay_mode,reco_tau_eta):

    expr = 'DecayModeSystHelper::decaymode_singletau_weight_true_1p0n_reco_1p0n({reco_decay_mode},{truth_decay_mode},{reco_tau_eta},0)'.format(
        reco_decay_mode=reco_decay_mode,
        truth_decay_mode=truth_decay_mode,
        reco_tau_eta=reco_tau_eta,
        )
    return expr

def decaymode_syst_singletau_true_1p1n_reco_1p0n_expr(reco_decay_mode,truth_decay_mode,reco_tau_eta):

    expr = 'DecayModeSystHelper::decaymode_singletau_weight_true_1p1n_reco_1p0n({reco_decay_mode},{truth_decay_mode},{reco_tau_eta},0)'.format(
        reco_decay_mode=reco_decay_mode,
        truth_decay_mode=truth_decay_mode,
        reco_tau_eta=reco_tau_eta,
        )
    return expr

def decaymode_syst_singletau_true_1p1n_reco_1p1n_expr(reco_decay_mode,truth_decay_mode,reco_tau_eta):

    expr = 'DecayModeSystHelper::decaymode_singletau_weight_true_1p1n_reco_1p1n({reco_decay_mode},{truth_decay_mode},{reco_tau_eta},0)'.format(
        reco_decay_mode=reco_decay_mode,
        truth_decay_mode=truth_decay_mode,
        reco_tau_eta=reco_tau_eta,
        )
    return expr

def decaymode_syst_singletau_true_1p1n_reco_1pXn_expr(reco_decay_mode,truth_decay_mode,reco_tau_eta):

    expr = 'DecayModeSystHelper::decaymode_singletau_weight_true_1p1n_reco_1pXn({reco_decay_mode},{truth_decay_mode},{reco_tau_eta},0)'.format(
        reco_decay_mode=reco_decay_mode,
        truth_decay_mode=truth_decay_mode,
        reco_tau_eta=reco_tau_eta,
        )
    return expr

def decaymode_syst_singletau_true_1pXn_reco_1p1n_expr(reco_decay_mode,truth_decay_mode,reco_tau_eta):

    expr = 'DecayModeSystHelper::decaymode_singletau_weight_true_1pXn_reco_1p1n({reco_decay_mode},{truth_decay_mode},{reco_tau_eta},0)'.format(
        reco_decay_mode=reco_decay_mode,
        truth_decay_mode=truth_decay_mode,
        reco_tau_eta=reco_tau_eta,
        )
    return expr

def decaymode_syst_singletau_true_1pXn_reco_1pXn_expr(reco_decay_mode,truth_decay_mode,reco_tau_eta):

    expr = 'DecayModeSystHelper::decaymode_singletau_weight_true_1pXn_reco_1pXn({reco_decay_mode},{truth_decay_mode},{reco_tau_eta},0)'.format(
        reco_decay_mode=reco_decay_mode,
        truth_decay_mode=truth_decay_mode,
        reco_tau_eta=reco_tau_eta,
        )
    return expr

def decaymode_syst_singletau_true_3p0n_reco_3p0n_expr(reco_decay_mode,truth_decay_mode,reco_tau_eta):

    expr = 'DecayModeSystHelper::decaymode_singletau_weight_true_3p0n_reco_3p0n({reco_decay_mode},{truth_decay_mode},{reco_tau_eta},0)'.format(
        reco_decay_mode=reco_decay_mode,
        truth_decay_mode=truth_decay_mode,
        reco_tau_eta=reco_tau_eta,
        )
    return expr

def decaymode_syst_singletau_true_3pXn_reco_3p0n_expr(reco_decay_mode,truth_decay_mode,reco_tau_eta):

    expr = 'DecayModeSystHelper::decaymode_singletau_weight_true_3pXn_reco_3p0n({reco_decay_mode},{truth_decay_mode},{reco_tau_eta},0)'.format(
        reco_decay_mode=reco_decay_mode,
        truth_decay_mode=truth_decay_mode,
        reco_tau_eta=reco_tau_eta,
        )
    return expr

 
