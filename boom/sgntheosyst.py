"""
Implementation of the hh signal theory systematics
"""
import ROOT
import os
from happy.systematics import Systematics, SystematicsSet

try:
    ROOT.gSystem.CompileMacro('boom/cpp/sgnsyst_helper.cpp', 'k-', '', 'cache')
    print('BOOM:\t Signal syst helper tool loaded!')
except:
    raise RuntimeError

# this is needed because of
# https://sft.its.cern.ch/jira/browse/ROOT-7216
try:
    ROOT.SgnSystHelper()
except:
    print('Signal Syst tool not compiled')

__all__ = [
    'sgntheosyst_expr',
]


_HH_SGN_SYST_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/hh_phistar_theory_uncert_v3.root")

_root_file = ROOT.TFile(_HH_SGN_SYST_FILE, "read") 

ROOT.sgn_histDic.h_sgn_syst_1p0n_1p0n_ggH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1p0n_ggH")
ROOT.sgn_histDic.h_sgn_syst_1p0n_1p1n_ggH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1p1n_ggH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1p0n_ggH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1p0n_ggH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1p1n_ggH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1p1n_ggH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1pXn_ggH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1pXn_ggH")
ROOT.sgn_histDic.h_sgn_syst_1pXn_1p1n_ggH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1pXn_1p1n_ggH")
ROOT.sgn_histDic.h_sgn_syst_1p0n_1pXn_ggH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1pXn_ggH")
ROOT.sgn_histDic.h_sgn_syst_1pXn_1p0n_ggH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1pXn_1p0n_ggH")
ROOT.sgn_histDic.h_sgn_syst_3p0n_1p1n_ggH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_3p0n_1p1n_ggH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_3p0n_ggH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_3p0n_ggH")

ROOT.sgn_histDic.h_sgn_syst_1p0n_1p0n_VBFH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1p0n_VBFH")
ROOT.sgn_histDic.h_sgn_syst_1p0n_1p1n_VBFH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1p1n_VBFH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1p0n_VBFH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1p0n_VBFH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1p1n_VBFH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1p1n_VBFH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1pXn_VBFH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1pXn_VBFH")
ROOT.sgn_histDic.h_sgn_syst_1pXn_1p1n_VBFH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1pXn_1p1n_VBFH")
ROOT.sgn_histDic.h_sgn_syst_1p0n_1pXn_VBFH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1pXn_VBFH")
ROOT.sgn_histDic.h_sgn_syst_1pXn_1p0n_VBFH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1pXn_1p0n_VBFH")
ROOT.sgn_histDic.h_sgn_syst_3p0n_1p1n_VBFH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_3p0n_1p1n_VBFH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_3p0n_VBFH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_3p0n_VBFH")

ROOT.sgn_histDic.h_sgn_syst_1p0n_1p0n_WH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1p0n_WH")
ROOT.sgn_histDic.h_sgn_syst_1p0n_1p1n_WH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1p1n_WH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1p0n_WH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1p0n_WH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1p1n_WH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1p1n_WH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1pXn_WH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1pXn_WH")
ROOT.sgn_histDic.h_sgn_syst_1pXn_1p1n_WH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1pXn_1p1n_WH")
ROOT.sgn_histDic.h_sgn_syst_1p0n_1pXn_WH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1pXn_WH")
ROOT.sgn_histDic.h_sgn_syst_1pXn_1p0n_WH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1pXn_1p0n_WH")
ROOT.sgn_histDic.h_sgn_syst_3p0n_1p1n_WH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_3p0n_1p1n_WH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_3p0n_WH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_3p0n_WH")

ROOT.sgn_histDic.h_sgn_syst_1p0n_1p0n_ZH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1p0n_ZH")
ROOT.sgn_histDic.h_sgn_syst_1p0n_1p1n_ZH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1p1n_ZH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1p0n_ZH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1p0n_ZH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1p1n_ZH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1p1n_ZH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1pXn_ZH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1pXn_ZH")
ROOT.sgn_histDic.h_sgn_syst_1pXn_1p1n_ZH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1pXn_1p1n_ZH")
ROOT.sgn_histDic.h_sgn_syst_1p0n_1pXn_ZH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1pXn_ZH")
ROOT.sgn_histDic.h_sgn_syst_1pXn_1p0n_ZH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1pXn_1p0n_ZH")
ROOT.sgn_histDic.h_sgn_syst_3p0n_1p1n_ZH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_3p0n_1p1n_ZH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_3p0n_ZH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_3p0n_ZH")

ROOT.sgn_histDic.h_sgn_syst_1p0n_1p0n_ttH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1p0n_ttH")
ROOT.sgn_histDic.h_sgn_syst_1p0n_1p1n_ttH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1p1n_ttH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1p0n_ttH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1p0n_ttH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1p1n_ttH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1p1n_ttH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_1pXn_ttH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_1pXn_ttH")
ROOT.sgn_histDic.h_sgn_syst_1pXn_1p1n_ttH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1pXn_1p1n_ttH")
ROOT.sgn_histDic.h_sgn_syst_1p0n_1pXn_ttH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p0n_1pXn_ttH")
ROOT.sgn_histDic.h_sgn_syst_1pXn_1p0n_ttH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1pXn_1p0n_ttH")
ROOT.sgn_histDic.h_sgn_syst_3p0n_1p1n_ttH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_3p0n_1p1n_ttH")
ROOT.sgn_histDic.h_sgn_syst_1p1n_3p0n_ttH[0]    = _root_file.Get("hh_theory_uncert_phi_star_truth_3bins_1p1n_3p0n_ttH")

def sgntheosyst_expr(variable,tau_0_truth_decay_mode,tau_1_truth_decay_mode, process):
    expr = 'SgnSystHelper::sgnsyst_weight({variable},{tau_0_truth_decay_mode},{tau_1_truth_decay_mode},{process},0)'.format(
        variable = variable,
        tau_0_truth_decay_mode = tau_0_truth_decay_mode,
        tau_1_truth_decay_mode = tau_1_truth_decay_mode,
        process = int(process),
        )
    return expr
        
