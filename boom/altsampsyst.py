"""
Implementation of the hh signal theory systematics
"""
import ROOT
import os
from happy.systematics import Systematics, SystematicsSet

try:
    ROOT.gSystem.CompileMacro('boom/cpp/altsamp_helper.cpp', 'k-', '', 'cache')
    print('BOOM:\t Alternative sample helper tool loaded!')
except:
    raise RuntimeError

# this is needed because of
# https://sft.its.cern.ch/jira/browse/ROOT-7216
try:
    ROOT.AltSampSystHelper()
except:
    print('Alternative sample helper tool not compiled')

__all__ = [
    'altsample_param',
]


_ALTSAMP_SYST_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/alt_samples_param.root")

_root_file = ROOT.TFile(_ALTSAMP_SYST_FILE, "read") 

ROOT.altsamp_histDic.h_altsampl_geo1_syst_1p0n[0]       = _root_file.Get("geo1_syst_1p0n")
ROOT.altsamp_histDic.h_altsampl_geo2_syst_1p0n[0]       = _root_file.Get("geo2_syst_1p0n")
ROOT.altsamp_histDic.h_altsampl_geo3_syst_1p0n[0]       = _root_file.Get("geo3_syst_1p0n")
ROOT.altsamp_histDic.h_altsampl_physlist_syst_1p0n[0]   = _root_file.Get("physlist_syst_1p0n")

ROOT.altsamp_histDic.h_altsampl_geo1_syst_1p1n[0]       = _root_file.Get("geo1_syst_1p1n")
ROOT.altsamp_histDic.h_altsampl_geo2_syst_1p1n[0]       = _root_file.Get("geo2_syst_1p1n")
ROOT.altsamp_histDic.h_altsampl_geo3_syst_1p1n[0]       = _root_file.Get("geo3_syst_1p1n")
ROOT.altsamp_histDic.h_altsampl_physlist_syst_1p1n[0]   = _root_file.Get("physlist_syst_1p1n")    

ROOT.altsamp_histDic.h_altsampl_geo1_syst_1pXn[0]       = _root_file.Get("geo1_syst_1pXn")
ROOT.altsamp_histDic.h_altsampl_geo2_syst_1pXn[0]       = _root_file.Get("geo2_syst_1pXn")
ROOT.altsamp_histDic.h_altsampl_geo3_syst_1pXn[0]       = _root_file.Get("geo3_syst_1pXn")
ROOT.altsamp_histDic.h_altsampl_physlist_syst_1pXn[0]   = _root_file.Get("physlist_syst_1pXn")

ROOT.altsamp_histDic.h_altsampl_geo1_syst_3p0n[0]       = _root_file.Get("geo1_syst_3p0n")
ROOT.altsamp_histDic.h_altsampl_geo2_syst_3p0n[0]       = _root_file.Get("geo2_syst_3p0n")
ROOT.altsamp_histDic.h_altsampl_geo3_syst_3p0n[0]       = _root_file.Get("geo3_syst_3p0n")
ROOT.altsamp_histDic.h_altsampl_physlist_syst_3p0n[0]   = _root_file.Get("physlist_syst_3p0n")


def altsample_param(variable, tau_decaymode, syst_type):
    expr = 'AltSampSystHelper::altsample_param({variable},{tau_decay_mode},{syst_type},0)'.format(
        variable = variable,
        tau_decay_mode = tau_decaymode,
        syst_type = syst_type,
        )
    return expr

    
