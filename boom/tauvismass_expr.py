"""
"""
import ROOT

tauvismass_expr = 'TauVisMassCalc::calc_tau_vismass({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14})'.format(
    'tau_0_decay_mode', 
    'tau_0_decay_charged_p4.Pt()',
    'tau_0_decay_charged_p4.Eta()',
    'tau_0_decay_charged_p4.Phi()',
    'tau_0_decay_charged_p4.M()',
    'tau_0_decay_neutral_p4.Pt()',
    'tau_0_decay_neutral_p4.Eta()',
    'tau_0_decay_neutral_p4.Phi()',
    'tau_0_decay_neutral_p4.M()',
    'event_number',
    '0',
    '0',
    '0',
    '0',
    '0',
    )
"""tauvismass_expr expression"""

tauvismass_eta_smear_expr = 'TauVisMassCalc::calc_tau_vismass({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14})'.format(
    'tau_0_decay_mode',
    'tau_0_decay_charged_p4.Pt()',
    'tau_0_decay_charged_p4.Eta()',
    'tau_0_decay_charged_p4.Phi()',
    'tau_0_decay_charged_p4.M()',
    'tau_0_decay_neutral_p4.Pt()',
    'tau_0_decay_neutral_p4.Eta()',
    'tau_0_decay_neutral_p4.Phi()',
    'tau_0_decay_neutral_p4.M()',
    'event_number',
    '0',
    '0',
    '1',
    '0',
    '0',
    )
"""tauvismass_expr expression"""

tauvismass_phi_smear_expr = 'TauVisMassCalc::calc_tau_vismass({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14})'.format(
    'tau_0_decay_mode',
    'tau_0_decay_charged_p4.Pt()',
    'tau_0_decay_charged_p4.Eta()',
    'tau_0_decay_charged_p4.Phi()',
    'tau_0_decay_charged_p4.M()',
    'tau_0_decay_neutral_p4.Pt()',
    'tau_0_decay_neutral_p4.Eta()',
    'tau_0_decay_neutral_p4.Phi()',
    'tau_0_decay_neutral_p4.M()',
    'event_number',
    '0',
    '0',
    '0',
    '1',
    '0',
    )
"""tauvismass_expr expression"""

tauvismass_etaphi_smear_expr = 'TauVisMassCalc::calc_tau_vismass({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14})'.format(
    'tau_0_decay_mode',
    'tau_0_decay_charged_p4.Pt()',
    'tau_0_decay_charged_p4.Eta()',
    'tau_0_decay_charged_p4.Phi()',
    'tau_0_decay_charged_p4.M()',
    'tau_0_decay_neutral_p4.Pt()',
    'tau_0_decay_neutral_p4.Eta()',
    'tau_0_decay_neutral_p4.Phi()',
    'tau_0_decay_neutral_p4.M()',
    'event_number',
    '0',
    '0',
    '0',
    '0',
    '1',
    )
"""tauvismass_expr expression"""

tauvismass_energy_scale_up_expr = 'TauVisMassCalc::calc_tau_vismass({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14})'.format(
    'tau_0_decay_mode',
    'tau_0_decay_charged_p4.Pt()',
    'tau_0_decay_charged_p4.Eta()',
    'tau_0_decay_charged_p4.Phi()',
    'tau_0_decay_charged_p4.M()',
    'tau_0_decay_neutral_p4.Pt()',
    'tau_0_decay_neutral_p4.Eta()',
    'tau_0_decay_neutral_p4.Phi()',
    'tau_0_decay_neutral_p4.M()',
    'event_number',
    '1',
    '0',
    '0',
    '0',
    '0',
    )
"""tauvismass_expr expression"""

tauvismass_energy_scale_down_expr = 'TauVisMassCalc::calc_tau_vismass({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14})'.format(
    'tau_0_decay_mode',
    'tau_0_decay_charged_p4.Pt()',
    'tau_0_decay_charged_p4.Eta()',
    'tau_0_decay_charged_p4.Phi()',
    'tau_0_decay_charged_p4.M()',
    'tau_0_decay_neutral_p4.Pt()',
    'tau_0_decay_neutral_p4.Eta()',
    'tau_0_decay_neutral_p4.Phi()',
    'tau_0_decay_neutral_p4.M()',
    'event_number',
    '0',
    '1',
    '0',
    '0',
    '0',
    )
"""tauvismass_expr expression"""


