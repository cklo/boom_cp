"""
Implementation of the Signal theory shape  weights
"""
from happy.systematics import Systematics, SystematicsSet

def get_sgn_weights(process_name):
    """Retrieve the Signal theory shape uncertainty

    Returns
    _______
    sys_set : HAPPy SystematicsSet
  
    """

    var = '(ditau_matched_CP_phi_star_cp_ip_ip *(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 0)) + (ditau_matched_CP_phi_star_cp_ip_rho*(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_rho_ip*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 0)) + (ditau_matched_CP_phi_star_cp_rho_rho *(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_rho_rho*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 2)) + (ditau_matched_CP_phi_star_cp_rho_rho*(tau_0_matched_decay_mode == 2 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_a1_rho*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 3)) + (ditau_matched_CP_phi_star_cp_a1_rho*(tau_0_matched_decay_mode == 3 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_ip_rho*(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 2)) + (ditau_matched_CP_phi_star_cp_rho_ip*(tau_0_matched_decay_mode == 2 && tau_1_matched_decay_mode == 0))' 

    lead_tau_decay_mode    = 'tau_0_matched_decay_mode'
    sublead_tau_decay_mode = 'tau_1_matched_decay_mode'

    process = 0 
    if 'ggH' in process_name:
        process = 1
    elif 'VBFH' in process_name:
        process = 2
    elif 'WH' in process_name:
        process = 3 
    elif 'ZH' in process_name:
        process = 4     
    elif 'ttH' in process_name:
        process = 5

    from .sgntheosyst import sgntheosyst_expr
    sgnsyst = Systematics.weightSystematics('phistar_theory_shape', 'phistar_theory_shape',
                                            '('+sgntheosyst_expr(var,lead_tau_decay_mode,sublead_tau_decay_mode,process)+')',
                                            '(2-'+sgntheosyst_expr(var,lead_tau_decay_mode,sublead_tau_decay_mode,process)+')',
                                            '(1)')

    sys_set  = SystematicsSet(set([sgnsyst]))
    return sys_set
        
