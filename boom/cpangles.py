"""
Module defining the different mixed sample names in WS input
"""

theta_0   = '(tauspinner_HCP_Theta_0)'
theta_10  = '(tauspinner_HCP_Theta_10)'
theta_20  = '(tauspinner_HCP_Theta_20)'
theta_30  = '(tauspinner_HCP_Theta_30)'
theta_40  = '(tauspinner_HCP_Theta_40)'
theta_50  = '(tauspinner_HCP_Theta_50)'
theta_60  = '(tauspinner_HCP_Theta_60)'
theta_70  = '(tauspinner_HCP_Theta_70)'
theta_80  = '(tauspinner_HCP_Theta_80)'
theta_90  = '(tauspinner_HCP_Theta_90)'
theta_100 = '(tauspinner_HCP_Theta_100)'
theta_110 = '(tauspinner_HCP_Theta_110)'
theta_120 = '(tauspinner_HCP_Theta_120)'
theta_130 = '(tauspinner_HCP_Theta_130)'
theta_140 = '(tauspinner_HCP_Theta_140)'
theta_150 = '(tauspinner_HCP_Theta_150)'
theta_160 = '(tauspinner_HCP_Theta_160)'
theta_170 = '(tauspinner_HCP_Theta_170)'

CPANGLES = {

    'ggH': {
        '_theta_0'  :  theta_0,
        '_theta_10' :  theta_10,
        '_theta_20' :  theta_20, 
        '_theta_30' :  theta_30,
        '_theta_40' :  theta_40,
        '_theta_50' :  theta_50,
        '_theta_60' :  theta_60,
        '_theta_70' :  theta_70,
        '_theta_80' :  theta_80,
        '_theta_90' :  theta_90,
        '_theta_100' :  theta_100,
        '_theta_110' :  theta_110,
        '_theta_120' :  theta_120,
        '_theta_130' :  theta_130,
        '_theta_140' :  theta_140,
        '_theta_150' :  theta_150,
        '_theta_160' :  theta_160,
        '_theta_170' :  theta_170,
        },

    'ggH_geo1' : {
        '_theta_0'  :  theta_0,
        },
  
    'ggH_geo2' : {
        '_theta_0'  :  theta_0,
        },
   
    'ggH_geo3' : {
        '_theta_0'  :  theta_0,
        },

    'ggH_physlist' : {
        '_theta_0'  :  theta_0,
        },
    
    'VBFH': {
        '_theta_0'  :  theta_0,
        '_theta_10' :  theta_10,
        '_theta_20' :  theta_20,
        '_theta_30' :  theta_30,
        '_theta_40' :  theta_40,
        '_theta_50' :  theta_50,
        '_theta_60' :  theta_60,
        '_theta_70' :  theta_70,
        '_theta_80' :  theta_80,
        '_theta_90' :  theta_90,
        '_theta_100' :  theta_100,
        '_theta_110' :  theta_110,
        '_theta_120' :  theta_120,
        '_theta_130' :  theta_130,
        '_theta_140' :  theta_140,
        '_theta_150' :  theta_150,
        '_theta_160' :  theta_160,
        '_theta_170' :  theta_170,
        },

    'VBFH_geo1': {
        '_theta_0'  :  theta_0,
        },

    'VBFH_geo2': {
        '_theta_0'  :  theta_0,
        },

    'VBFH_geo3': {
        '_theta_0'  :  theta_0,
        },

    'VBFH_physlist': {
        '_theta_0'  :  theta_0,
        },

    'WH': {
        '_theta_0'  :  theta_0,
        '_theta_10' :  theta_10,
        '_theta_20' :  theta_20,
        '_theta_30' :  theta_30,
        '_theta_40' :  theta_40,
        '_theta_50' :  theta_50,
        '_theta_60' :  theta_60,
        '_theta_70' :  theta_70,
        '_theta_80' :  theta_80,
        '_theta_90' :  theta_90,
        '_theta_100' :  theta_100,
        '_theta_110' :  theta_110,
        '_theta_120' :  theta_120,
        '_theta_130' :  theta_130,
        '_theta_140' :  theta_140,
        '_theta_150' :  theta_150,
        '_theta_160' :  theta_160,
        '_theta_170' :  theta_170,
        },

    'WH_geo1': {
        '_theta_0'  :  theta_0,
        },

    'WH_geo2': {
        '_theta_0'  :  theta_0,
        },
 
    'WH_geo3': {
        '_theta_0'  :  theta_0,
        },

    'WH_physlist': {
        '_theta_0'  :  theta_0,
        },

    'ZH' : {
        '_theta_0'  :  theta_0,
        '_theta_10' :  theta_10,
        '_theta_20' :  theta_20,
        '_theta_30' :  theta_30,
        '_theta_40' :  theta_40,
        '_theta_50' :  theta_50,
        '_theta_60' :  theta_60,
        '_theta_70' :  theta_70,
        '_theta_80' :  theta_80,
        '_theta_90' :  theta_90,
        '_theta_100' :  theta_100,
        '_theta_110' :  theta_110,
        '_theta_120' :  theta_120,
        '_theta_130' :  theta_130,
        '_theta_140' :  theta_140,
        '_theta_150' :  theta_150,
        '_theta_160' :  theta_160,
        '_theta_170' :  theta_170,
        },

    'ZH_geo1' : {
        '_theta_0'  :  theta_0,
        },

    'ZH_geo2' : {
        '_theta_0'  :  theta_0,
        },

    'ZH_geo3' : {
        '_theta_0'  :  theta_0,
        },
   
    'ZH_physlist' : {
        '_theta_0'  :  theta_0,
        },

    'ttH' : {
        '_theta_0'  :  theta_0,
        '_theta_10' :  theta_10,
        '_theta_20' :  theta_20,
        '_theta_30' :  theta_30,
        '_theta_40' :  theta_40,
        '_theta_50' :  theta_50,
        '_theta_60' :  theta_60,
        '_theta_70' :  theta_70,
        '_theta_80' :  theta_80,
        '_theta_90' :  theta_90,
        '_theta_100' :  theta_100,
        '_theta_110' :  theta_110,
        '_theta_120' :  theta_120,
        '_theta_130' :  theta_130,
        '_theta_140' :  theta_140,
        '_theta_150' :  theta_150,
        '_theta_160' :  theta_160,
        '_theta_170' :  theta_170,
        },

}
