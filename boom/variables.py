"""Definition of the variables to plot (VARIABLES dict)

We use HAPPy Variables to define the variable to plot.
The third argument in the constructor is the actual expression 
being evaluated (from the ntuples branches).
To add a variable, slot it in at the bottom of the dictionnary following:

     'key': Variable(
        'name', # user-defined

        'expr', # ntuple branch name or any TTreeFormula-able expression

        'title', # x-axis title on the plot (w/o units)

        '', # units - empty string '' if dimensionless

        Binning(nbins, xmin, xmax) # or any supported use of HAPPy.Binning

        ), 
"""
import ROOT
from happy.variable import Binning, Variable, VariableBinning
from .mva_expr import pt_jj_expr, vbf_tagger_expr, dr_jj_expr, dr_dijetditau_expr
from .phistar_expr import * 
from .tauvismass_expr import *
from .tauupsilon_expr  import *

binningPi   = Binning(16, 0, ROOT.TMath.Pi())
binningPhi  = Binning(32, -ROOT.TMath.Pi(), ROOT.TMath.Pi())
binningPhiCoarse  = Binning(10, -ROOT.TMath.Pi(), ROOT.TMath.Pi())
binning2Pi  = Binning(45, 0, ROOT.TMath.TwoPi())

VARIABLES = {
    'norm': Variable(
        'norm', 
        '1', 
        'norm', 
        '', 
        Binning(1, 0, 2)),

    'mva_random': Variable(
        'mva_random',
        'Rnd::mva_random_number(event_number)',
        'MVA Random Number',
        '',
        Binning(10, 0, 1)),
    
    'ditau_decay_mode_combination': Variable(
        'ditau_decay_mode_combination', 
        '1.5*(tau_0_decay_mode==0&&tau_1_decay_mode==0) + 2.5*(tau_0_decay_mode==1&&tau_1_decay_mode==0)+2.5*(tau_0_decay_mode==0&&tau_1_decay_mode==1) + 3.5*(tau_0_decay_mode==1&&tau_1_decay_mode==1) + 4.5*(tau_0_decay_mode==2&&tau_1_decay_mode==0)+4.5*(tau_0_decay_mode==0&&tau_1_decay_mode==2) + 5.5*(tau_0_decay_mode==2&&tau_1_decay_mode==1)+5.5*(tau_0_decay_mode==1&&tau_1_decay_mode==2) + 6.5*(tau_0_decay_mode==1&&tau_1_decay_mode==3) + 6.5*(tau_0_decay_mode==3&&tau_1_decay_mode==1)', 
        'ditau decay mode combination', 
        '', 
        Binning( 6, 1, 7, ['1p0n-1p0n','1p0n-1p1n','1p1n-1p1n','1p0n-1pXn','1p1n-1pXn','1p1n-3p0n'])),

    'tau_0_decay_mode': Variable(
        'tau_0_decay_mode', 
        'tau_0_decay_mode', 
        'leading tau decay mode', 
        '', 
        Binning( 5, -0.5, 4.5, ['1p0n','1p1n','1pXn','3p0n','3pXn'])),
#        Binning(6, 0, 6, ['1p0n','1p1n','1pXn','3p0n','3pXn','other'])),

    'tau_1_decay_mode': Variable(
        'tau_1_decay_mode', 
        'tau_1_decay_mode', 
        'subleading tau decay mode', 
        '', 
        Binning( 5, -0.5, 4.5, ['1p0n','1p1n','1pXn','3p0n','3pXn'])),
#        Binning(6, 0, 6, ['1p0n','1p1n','1pXn','3p0n','3pXn','other'])),

    'tau_0_matched_decay_mode': Variable(
        'tau_0_matched_decay_mode', 
        'tau_0_matched_decay_mode', 
        'truth leading tau decay mode', 
        '', 
        Binning( 5, -0.5, 4.5, ['1p0n','1p1n','1pXn','3p0n','3pXn'])),
#        Binning(6, 0, 6, ['1p0n','1p1n','1pXn','3p0n','3pXn','other'])),

    'tau_1_matched_decay_mode': Variable(
        'tau_1_matched_decay_mode', 
        'tau_1_matched_decay_mode', 
        'truth subleading tau decay mode', 
        '', 
        Binning( 5, -0.5, 4.5, ['1p0n','1p1n','1pXn','3p0n','3pXn'])),
#        Binning(6, 0, 6, ['1p0n','1p1n','1pXn','3p0n','3pXn','other'])),

    'tau_0_eta': Variable(
        'tau_0_eta', 
        'tau_0_p4.Eta()', 
        'leading #eta(#tau)', 
        '', 
        Binning(50, -2.5, 2.5)),

    'tau_0_pt': Variable(
        'tau_0_pt', 
        'tau_0_p4.Pt()', 
        'leading p_{T}', 
        'GeV', 
        Binning(20, 20, 220)),

    'tau_1_eta': Variable(
        'tau_1_eta',
        'tau_1_p4.Eta()',
        'subleading #eta(#tau)',
        '',
        Binning(50, -2.5, 2.5)),

    'tau_1_pt': Variable(
        'tau_1_pt',
        'tau_1_p4.Pt()',
        'subleading p_{T}',
        'GeV',
        Binning(20, 20, 220)),

    'visible_mass_zcr': Variable(
        'visible_mass',
        'ditau_p4.M()',
        'Visible Mass',
        'GeV',
        Binning(18, 20, 110)),

    'mmc_mlm_m': Variable(
        'ditau_mmc_mlm_m', 
        'ditau_mmc_mlm_m', 
        'MMC (mlm) Mass Estimator', 
        'GeV', 
        Binning(20, 0, 200)),

    'mmc_mlm_m_zcr': Variable(
        'ditau_mmc_mlm_m',
        'ditau_mmc_mlm_m',
        'MMC (mlm) Mass Estimator',
        'GeV',
        Binning(10, 60, 110)),

    'jet_0_pt': Variable(
        'jet_0_pt', 
        'jet_0_p4.Pt()', 
        'Leading jet p_{T}', 
        'GeV', 
        Binning(18, 70, 250)),

    'jet_1_pt': Variable(
        'jet_1_pt', 
        'jet_1_p4.Pt()', 
        'Subleading jet p_{T}', 
        'GeV', 
        Binning(19, 10, 200)),

    'jets_deta': Variable(
        'dijet_deta', 
        'fabs(jet_0_p4.Eta()-jet_1_p4.Eta())', 
        '|#Delta#eta(jet_{0}, jet_{1})|', 
        '', 
        Binning(20, 3, 7)),

    'mjj': Variable(
        'dijet_m', 
        'dijet_p4.M()', 
        'm_{jj}', 
        'GeV', 
        Binning(20, 300, 2300)),

    'n_jets': Variable( 
        'n_jets', 
        'n_jets', 
        'N_{jets}', 
        '',   
        Binning(11, -0.5, 10.5, [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'])),

    'n_jets_30': Variable( 
        'n_jets_30', 
        'n_jets_30', 
        'N_{jets} (p_{T} > 30 GeV)', 
        '',   
        Binning(6, -0.5, 5.5, [ '0', '1', '2', '3', '4', '5' ])),

    'tau_0_bdt_score':  Variable(
        'tau_0_jet_bdt_score_trans', 
        'tau_0_jet_bdt_score_trans', 
        '#tau BDT score', 
        '',  
        Binning(50, 0, 1)),

    'met': Variable(
        'met_reco_et', 
        'met_p4.Pt()', 
        '#slash{E}_{T}',
        'GeV', 
        Binning(20, 0, 200)),

    'higgs_pt': Variable(
        'ditau_higgspt', 
        'ditau_higgspt',                               
        'p_{T}(H)', 
        'GeV', 
        Binning(30, 90, 390)),

    'ditau_dr': Variable(
        'ditau_dr',
        'ditau_dr',
        '#DeltaR(#tau, #tau)',
        '',
        Binning(25, 0, 2.5)),
 
    'ditau_dr_reweighting': Variable(
        'ditau_dr_reweighting',
        'ditau_dr',
        '#DeltaR(#tau, #tau)',
        '',
        Binning(2, 0, 2.6)),
 
    'ditau_deta': Variable(
        'ditau_deta', 
        'fabs(ditau_deta)',                               
        '#Delta#eta(#tau, #tau)', 
        '',
        Binning(20, 0, 2)),

    'ditau_dphi': Variable(
        'ditau_dphi',
        'ditau_dphi',
        '#Delta#Phi(#tau, #tau)',
        '',
        Binning(13, 0, 2.6)),

    'ditau_dphi_reweighting': Variable(
        'ditau_dphi_reweighting',
        'ditau_dphi',
        '#Delta#Phi(#tau, #tau)',
        '',
        Binning(2, 0, 2.8)),

    'vbf_tagger': Variable(
        'vbf_tagger',
        vbf_tagger_expr,
        'VBF Tagger Score', 
        '',
        Binning(24, -0.6, 0.6)),

    'vbf_tagger_pr': Variable(
        'vbf_tagger',
        vbf_tagger_expr,
        'VBF Tagger Score', 
        '',
        Binning(24, -0.6, 0.6)),

    'vbf_tagger_medium_fine_binning': Variable(
        'vbf_tagger',
        vbf_tagger_expr,
        'VBF Tagger Score', 
        '',
        Binning(20, -1, 1)),

    'vbf_tagger_fine_binning': Variable(
        'vbf_tagger',
        vbf_tagger_expr,
        'VBF Tagger Score', 
        '',
        Binning(100, -1, 1)),

    'phi_star_21bins': Variable(
        'phi_star_21bins',
        '(ditau_CP_phi_star_cp_ip_ip*(tau_0_decay_mode == 0 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_ip_rho*(tau_0_decay_mode == 0 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_ip*(tau_0_decay_mode == 1 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 2)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 2 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 3)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 3 && tau_1_decay_mode == 1)) + (((ditau_CP_phi_star_cp_ip_rho < 3.1416)*(ditau_CP_phi_star_cp_ip_rho + 3.1416) + (ditau_CP_phi_star_cp_ip_rho > 3.1416)*(ditau_CP_phi_star_cp_ip_rho -3.1416))*(tau_0_decay_mode == 0 && tau_1_decay_mode == 2)) + (((ditau_CP_phi_star_cp_rho_ip < 3.1416)*(ditau_CP_phi_star_cp_rho_ip + 3.1416) + (ditau_CP_phi_star_cp_rho_ip > 3.1416)*(ditau_CP_phi_star_cp_rho_ip -3.1416))*(tau_0_decay_mode == 2 && tau_1_decay_mode == 0))',
        '#varphi*_{CP}',
        '',
        Binning(21, 0, 6.3)),

    'phi_star_12bins': Variable(
        'phi_star_12bins',
        '(ditau_CP_phi_star_cp_ip_ip*(tau_0_decay_mode == 0 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_ip_rho*(tau_0_decay_mode == 0 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_ip*(tau_0_decay_mode == 1 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 2)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 2 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 3)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 3 && tau_1_decay_mode == 1)) + (((ditau_CP_phi_star_cp_ip_rho < 3.1416)*(ditau_CP_phi_star_cp_ip_rho + 3.1416) + (ditau_CP_phi_star_cp_ip_rho > 3.1416)*(ditau_CP_phi_star_cp_ip_rho -3.1416))*(tau_0_decay_mode == 0 && tau_1_decay_mode == 2)) + (((ditau_CP_phi_star_cp_rho_ip < 3.1416)*(ditau_CP_phi_star_cp_rho_ip + 3.1416) + (ditau_CP_phi_star_cp_rho_ip > 3.1416)*(ditau_CP_phi_star_cp_rho_ip -3.1416))*(tau_0_decay_mode == 2 && tau_1_decay_mode == 0))',
        '#varphi*_{CP}',
        '',
        Binning(12, 0, 6.3)),

    'phi_star_9bins' : Variable(
        'phi_star_9bins',
        '(ditau_CP_phi_star_cp_ip_ip*(tau_0_decay_mode == 0 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_ip_rho*(tau_0_decay_mode == 0 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_ip*(tau_0_decay_mode == 1 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 2)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 2 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 3)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 3 && tau_1_decay_mode == 1)) + (((ditau_CP_phi_star_cp_ip_rho < 3.1416)*(ditau_CP_phi_star_cp_ip_rho + 3.1416) + (ditau_CP_phi_star_cp_ip_rho > 3.1416)*(ditau_CP_phi_star_cp_ip_rho -3.1416))*(tau_0_decay_mode == 0 && tau_1_decay_mode == 2)) + (((ditau_CP_phi_star_cp_rho_ip < 3.1416)*(ditau_CP_phi_star_cp_rho_ip + 3.1416) + (ditau_CP_phi_star_cp_rho_ip > 3.1416)*(ditau_CP_phi_star_cp_rho_ip -3.1416))*(tau_0_decay_mode == 2 && tau_1_decay_mode == 0))',
        '#varphi*_{CP}',
        '',
        Binning( 9, 0, 6.3)),

     #'phi_star_9bins_no_inv' : Variable(
     #   'phi_star_9bins_no_inv',
     #   '(ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 3 && tau_1_decay_mode == 1) + (ditau_CP_phi_star_cp_ip_rho*(tau_0_decay_mode == 0 && tau_1_decay_mode == 2)) + (ditau_CP_phi_star_cp_rho_ip*(tau_0_decay_mode == 2 && tau_1_decay_mode == 0)))',
     #   '#varphi*_{CP}',
     #   '',
     #   Binning( 9, 0, 6.3)),

    #'phi_star_9bins' : Variable(
    #    'phi_star_9bins',
    #    '(((ditau_CP_phi_star_cp_ip_rho < 3.1416)*(ditau_CP_phi_star_cp_ip_rho + 3.1416) + (ditau_CP_phi_star_cp_ip_rho > 3.1416)*(ditau_CP_phi_star_cp_ip_rho -3.1416))*(tau_0_decay_mode == 0 && tau_1_decay_mode == 2)) + (((ditau_CP_phi_star_cp_rho_ip < 3.1416)*(ditau_CP_phi_star_cp_rho_ip + 3.1416) + (ditau_CP_phi_star_cp_rho_ip > 3.1416)*(ditau_CP_phi_star_cp_rho_ip -3.1416))*(tau_0_decay_mode == 2 && tau_1_decay_mode == 0))',
    #    '#varphi*_{CP}',
    #    '',
    #    Binning( 9, 0, 6.3)),
 
    'phi_star_truth_9bins' : Variable(
        'phi_star_truth_9bins',
        '(ditau_matched_CP_phi_star_cp_ip_ip *(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 0)) + (ditau_matched_CP_phi_star_cp_ip_rho*(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_rho_ip*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 0)) + (ditau_matched_CP_phi_star_cp_rho_rho *(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_rho_rho*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 2)) + (ditau_matched_CP_phi_star_cp_rho_rho*(tau_0_matched_decay_mode == 2 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_a1_rho*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 3)) + (ditau_matched_CP_phi_star_cp_a1_rho*(tau_0_matched_decay_mode == 3 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_ip_rho*(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 2)) + (ditau_matched_CP_phi_star_cp_rho_ip*(tau_0_matched_decay_mode == 2 && tau_1_matched_decay_mode == 0))',
        'truth #varphi*_{CP}',
        '',
        Binning( 9, 0, 6.3)),  

    'phi_star_7bins' : Variable(
        'phi_star_7bins',
        '(ditau_CP_phi_star_cp_ip_ip*(tau_0_decay_mode == 0 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_ip_rho*(tau_0_decay_mode == 0 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_ip*(tau_0_decay_mode == 1 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 2)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 2 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 3)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 3 && tau_1_decay_mode == 1)) + (((ditau_CP_phi_star_cp_ip_rho < 3.1416)*(ditau_CP_phi_star_cp_ip_rho + 3.1416) + (ditau_CP_phi_star_cp_ip_rho > 3.1416)*(ditau_CP_phi_star_cp_ip_rho -3.1416))*(tau_0_decay_mode == 0 && tau_1_decay_mode == 2)) + (((ditau_CP_phi_star_cp_rho_ip < 3.1416)*(ditau_CP_phi_star_cp_rho_ip + 3.1416) + (ditau_CP_phi_star_cp_rho_ip > 3.1416)*(ditau_CP_phi_star_cp_rho_ip -3.1416))*(tau_0_decay_mode == 2 && tau_1_decay_mode == 0))',
        '#varphi*_{CP}',
        '',
        Binning( 7, 0, 6.3)),

    'phi_star_5bins' : Variable(
        'phi_star_5bins',
        '(ditau_CP_phi_star_cp_ip_ip*(tau_0_decay_mode == 0 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_ip_rho*(tau_0_decay_mode == 0 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_ip*(tau_0_decay_mode == 1 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 2)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 2 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 3)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 3 && tau_1_decay_mode == 1)) + (((ditau_CP_phi_star_cp_ip_rho < 3.1416)*(ditau_CP_phi_star_cp_ip_rho + 3.1416) + (ditau_CP_phi_star_cp_ip_rho > 3.1416)*(ditau_CP_phi_star_cp_ip_rho -3.1416))*(tau_0_decay_mode == 0 && tau_1_decay_mode == 2)) + (((ditau_CP_phi_star_cp_rho_ip < 3.1416)*(ditau_CP_phi_star_cp_rho_ip + 3.1416) + (ditau_CP_phi_star_cp_rho_ip > 3.1416)*(ditau_CP_phi_star_cp_rho_ip -3.1416))*(tau_0_decay_mode == 2 && tau_1_decay_mode == 0))',
        '#varphi*_{CP}',
        '',
        Binning( 5, 0, 6.3)),
 
    'phi_star_truth_5bins' : Variable(
        'phi_star_truth_5bins',
        '(ditau_matched_CP_phi_star_cp_ip_ip *(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 0)) + (ditau_matched_CP_phi_star_cp_ip_rho*(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_rho_ip*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 0)) + (ditau_matched_CP_phi_star_cp_rho_rho *(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_rho_rho*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 2)) + (ditau_matched_CP_phi_star_cp_rho_rho*(tau_0_matched_decay_mode == 2 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_a1_rho*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 3)) + (ditau_matched_CP_phi_star_cp_a1_rho*(tau_0_matched_decay_mode == 3 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_ip_rho*(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 2)) + (ditau_matched_CP_phi_star_cp_rho_ip*(tau_0_matched_decay_mode == 2 && tau_1_matched_decay_mode == 0))',
        'truth #varphi*_{CP}',
        '',
        Binning( 5, 0, 6.3)),
    
    'phi_star_2bins' : Variable(
        'phi_star_2bins',
        '(ditau_CP_phi_star_cp_ip_ip*(tau_0_decay_mode == 0 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_ip_rho*(tau_0_decay_mode == 0 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_ip*(tau_0_decay_mode == 1 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 2)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 2 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 3)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 3 && tau_1_decay_mode == 1)) + (((ditau_CP_phi_star_cp_ip_rho < 3.1416)*(ditau_CP_phi_star_cp_ip_rho + 3.1416) + (ditau_CP_phi_star_cp_ip_rho > 3.1416)*(ditau_CP_phi_star_cp_ip_rho -3.1416))*(tau_0_decay_mode == 0 && tau_1_decay_mode == 2)) + (((ditau_CP_phi_star_cp_rho_ip < 3.1416)*(ditau_CP_phi_star_cp_rho_ip + 3.1416) + (ditau_CP_phi_star_cp_rho_ip > 3.1416)*(ditau_CP_phi_star_cp_rho_ip -3.1416))*(tau_0_decay_mode == 2 && tau_1_decay_mode == 0))',
        '#varphi*_{CP}',
        '',
        Binning( 2, 0, 6.3)),
     
    'phi_star_3bins' : Variable(
        'phi_star_3bins',
        '(ditau_CP_phi_star_cp_ip_ip*(tau_0_decay_mode == 0 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_ip_rho*(tau_0_decay_mode == 0 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_ip*(tau_0_decay_mode == 1 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 2)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 2 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 3)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 3 && tau_1_decay_mode == 1)) + (((ditau_CP_phi_star_cp_ip_rho < 3.1416)*(ditau_CP_phi_star_cp_ip_rho + 3.1416) + (ditau_CP_phi_star_cp_ip_rho > 3.1416)*(ditau_CP_phi_star_cp_ip_rho -3.1416))*(tau_0_decay_mode == 0 && tau_1_decay_mode == 2)) + (((ditau_CP_phi_star_cp_rho_ip < 3.1416)*(ditau_CP_phi_star_cp_rho_ip + 3.1416) + (ditau_CP_phi_star_cp_rho_ip > 3.1416)*(ditau_CP_phi_star_cp_rho_ip -3.1416))*(tau_0_decay_mode == 2 && tau_1_decay_mode == 0))',
        '#varphi*_{CP}',
        '',
        Binning( 3, 0, 6.3)),

    'phi_star_truth_3bins' : Variable(
        'phi_star_truth_3bins',
        '(ditau_matched_CP_phi_star_cp_ip_ip *(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 0)) + (ditau_matched_CP_phi_star_cp_ip_rho*(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_rho_ip*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 0)) + (ditau_matched_CP_phi_star_cp_rho_rho *(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_rho_rho*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 2)) + (ditau_matched_CP_phi_star_cp_rho_rho*(tau_0_matched_decay_mode == 2 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_a1_rho*(tau_0_matched_decay_mode == 1 && tau_1_matched_decay_mode == 3)) + (ditau_matched_CP_phi_star_cp_a1_rho*(tau_0_matched_decay_mode == 3 && tau_1_matched_decay_mode == 1)) + (ditau_matched_CP_phi_star_cp_ip_rho*(tau_0_matched_decay_mode == 0 && tau_1_matched_decay_mode == 2)) + (ditau_matched_CP_phi_star_cp_rho_ip*(tau_0_matched_decay_mode == 2 && tau_1_matched_decay_mode == 0))',  
        'truth #varphi*_{CP}',
        '',
        Binning( 3, 0, 6.3)),
     
    'phi_star_fit' : Variable(
        'phi_star',
        '(ditau_CP_phi_star_cp_ip_ip*(tau_0_decay_mode == 0 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_ip_rho*(tau_0_decay_mode == 0 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_ip*(tau_0_decay_mode == 1 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 2)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 2 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 3)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 3 && tau_1_decay_mode == 1)) + (((ditau_CP_phi_star_cp_ip_rho < 3.1416)*(ditau_CP_phi_star_cp_ip_rho + 3.1416) + (ditau_CP_phi_star_cp_ip_rho > 3.1416)*(ditau_CP_phi_star_cp_ip_rho -3.1416))*(tau_0_decay_mode == 0 && tau_1_decay_mode == 2)) + (((ditau_CP_phi_star_cp_rho_ip < 3.1416)*(ditau_CP_phi_star_cp_rho_ip + 3.1416) + (ditau_CP_phi_star_cp_rho_ip > 3.1416)*(ditau_CP_phi_star_cp_rho_ip -3.1416))*(tau_0_decay_mode == 2 && tau_1_decay_mode == 0))',
        '#varphi*_{CP}',
        '',
        Binning( 63, 0, 6.3)),
  
    'phi_star_uncert' : Variable(
        'phi_star',
        '(ditau_CP_phi_star_cp_ip_ip*(tau_0_decay_mode == 0 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_ip_rho*(tau_0_decay_mode == 0 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_ip*(tau_0_decay_mode == 1 && tau_1_decay_mode == 0)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 2)) + (ditau_CP_phi_star_cp_rho_rho*(tau_0_decay_mode == 2 && tau_1_decay_mode == 1)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 1 && tau_1_decay_mode == 3)) + (ditau_CP_phi_star_cp_a1_rho*(tau_0_decay_mode == 3 && tau_1_decay_mode == 1)) + (((ditau_CP_phi_star_cp_ip_rho < 3.1416)*(ditau_CP_phi_star_cp_ip_rho + 3.1416) + (ditau_CP_phi_star_cp_ip_rho > 3.1416)*(ditau_CP_phi_star_cp_ip_rho -3.1416))*(tau_0_decay_mode == 0 && tau_1_decay_mode == 2)) + (((ditau_CP_phi_star_cp_rho_ip < 3.1416)*(ditau_CP_phi_star_cp_rho_ip + 3.1416) + (ditau_CP_phi_star_cp_rho_ip > 3.1416)*(ditau_CP_phi_star_cp_rho_ip -3.1416))*(tau_0_decay_mode == 2 && tau_1_decay_mode == 0))',
        '#varphi*_{CP}',
        '',
         Binning( 3, 0, 6.3)),
 
    'tau_0_leadTrk_d0_sig' : Variable(
        'tau_0_leadTrk_d0_sig',
        'fabs(tau_0_leadTrk_d0_sig)',
        'lead #tau d0 sgn',
        '',
        Binning(20, 0, 5)),

    'tau_1_leadTrk_d0_sig' : Variable(
        'tau_1_leadTrk_d0_sig',
        'fabs(tau_1_leadTrk_d0_sig)',
        'sublead #tau d0 sgn',
        '',
        Binning(20, 0, 5)),

    'tau_0_leadTrk_d0' : Variable(
        'tau_0_leadTrk_d0',
        'tau_0_leadTrk_d0',
        'lead #tau d0',
        '',
        Binning(20, -0.5, 0.5)),

    'tau_1_leadTrk_d0' : Variable(
        'tau_1_leadTrk_d0',
        'tau_1_leadTrk_d0',
        'sublead #tau d0',
        '',
        Binning(20, -0.5, 0.5)),

    'tau_0_leadTrk_z0' : Variable(
        'tau_0_leadTrk_z0',
        'tau_0_leadTrk_z0',
        'lead #tau z0',
        '',
        Binning(30, -150, 150)),

    'tau_1_leadTrk_z0' : Variable(
        'tau_1_leadTrk_z0',
        'tau_1_leadTrk_z0',
        'sublead #tau z0',
        '',
        Binning(30, -150, 150)),

    'tau_0_leadTrk_eta': Variable(
        'tau_0_leadTrk_eta',
        'tau_0_leadTrk_eta',
        'lead #tau lead track #eta',   
        '',
        Binning(26, -2.6, 2.6)),
        
    'tau_1_leadTrk_eta': Variable(
        'tau_1_leadTrk_eta',
        'tau_1_leadTrk_eta',
        'sublead #tau lead track #eta',   
        '',
        Binning(26, -2.6, 2.6)),
        
    'tau_0_leadTrk_phi': Variable(
        'tau_0_leadTrk_phi',
        'tau_0_leadTrk_phi',
        'lead #tau lead track #phi',   
        '',
        Binning(32, -3.2, 3.2)),
        
    'tau_1_leadTrk_phi': Variable(
        'tau_1_leadTrk_phi',
        'tau_1_leadTrk_phi',
        'sublead #tau lead track #phi',   
        '',
        Binning(32, -3.2, 3.2)),
        
    'tau_0_leadTrk_pt': Variable(
        'tau_0_leadTrk_pt',
        'tau_0_leadTrk_pt',
        'lead #tau lead track p_{T}',
        'GeV',
        Binning(22, 0., 220)),   

    'tau_1_leadTrk_pt': Variable(
        'tau_1_leadTrk_pt',
        'tau_1_leadTrk_pt',
        'sublead #tau lead track p_{T}',
        'GeV',
        Binning(22, 0., 220)),   


    'ditau_leadTrk_deta': Variable(
        'ditau_leadTrk_deta', 
        'fabs(tau_0_leadTrk_eta-tau_1_leadTrk_eta)',                               
        '#Delta#eta(lead track(#tau_{0}), lead track(#tau_{1}))', 
        '',
        Binning(20, 0, 2)),

    'ditau_leadTrk_dphi': Variable(
        'ditau_leadTrk_dphi',
        'fabs(tau_0_leadTrk_phi-tau_1_leadTrk_phi)',                               
        '#Delta#phi(lead track(#tau_{0}), lead track(#tau_{1}))', 
        '',
        Binning(13, 0, 2.6)),

    'tau_0_decay_neutral_pt' : Variable(
        'tau_0_decay_neutral_pt',
        'tau_0_decay_neutral_p4.Pt()',
        'lead #tau neutral p_{T}',
        '',
        Binning(22,0.,220)),

    'tau_1_decay_neutral_pt' : Variable(
        'tau_1_decay_neutral_pt',
        'tau_1_decay_neutral_p4.Pt()',
        'sublead #tau neutral p_{T}',
        '',
        Binning(22,0.,220)),

    'tau_0_decay_neutral_eta' : Variable(
        'tau_0_decay_neutral_eta',
        'tau_0_decay_neutral_p4.Eta()',
        'lead #tau neutral #eta',
        '',
        Binning(26,-2.6,2.6)),

    'tau_1_decay_neutral_eta' : Variable(
        'tau_1_decay_neutral_eta',
        'tau_1_decay_neutral_p4.Eta()',
        'sublead #tau neutral #eta',
        '',
        Binning(26,-2.6,2.6)),

    'tau_0_decay_neutral_phi' : Variable(
        'tau_0_decay_neutral_phi',
        'tau_0_decay_neutral_p4.Phi()',
        'lead #tau neutral #phi',
        '',
        Binning(32,-3.2,3.2)),

    'tau_1_decay_neutral_phi' : Variable(
        'tau_1_decay_neutral_phi',
        'tau_1_decay_neutral_p4.Phi()',
        'sublead #tau neutral #phi',
        '',
        Binning(32,-3.2,3.2)),

    'tau_0_decay_neutral_m' : Variable(
        'tau_0_decay_neutral_m',
        'tau_0_decay_neutral_p4.M()',
        'lead #tau neutral m',
        '',
        Binning(25,0.,0.5)),

    'tau_1_decay_neutral_m' : Variable(
        'tau_1_decay_neutral_m',
        'tau_1_decay_neutral_p4.M()',
        'sublead #tau neutral m',
        '',
        Binning(25,0.,0.5)),

    'tau_0_decay_neutral_E' : Variable(
        'tau_0_decay_neutral_E',
        'tau_0_decay_neutral_p4.Energy()',
        'lead #tau neutral E',
        '',
        Binning(45,0.,450)),

    'tau_1_decay_neutral_E' : Variable(
        'tau_1_decay_neutral_E',
        'tau_1_decay_neutral_p4.Energy()',
        'sublead #tau neutral E',
        '',
        Binning(45,0.,450)),

    'tau_0_decay_rho_m' : Variable(
        'tau_0_decay_rho_m',
        'TMath::Sqrt((tau_0_decay_charged_p4.E()+tau_0_decay_neutral_p4.E())**2-(tau_0_decay_charged_p4.Px()+tau_0_decay_neutral_p4.Px())**2-(tau_0_decay_charged_p4.Py()+tau_0_decay_neutral_p4.Py())**2-(tau_0_decay_charged_p4.Pz()+tau_0_decay_neutral_p4.Pz())**2)',
        #'(tau_0_decay_charged_p4+tau_0_decay_neutral_p4).M()',
        'lead #tau rho m',
        '',
        Binning(30,0.,3.)),
 
    'tau_1_decay_rho_m' : Variable(
        'tau_1_decay_rho_m',
        'TMath::Sqrt((tau_1_decay_charged_p4.E()+tau_1_decay_neutral_p4.E())**2-(tau_1_decay_charged_p4.Px()+tau_1_decay_neutral_p4.Px())**2-(tau_1_decay_charged_p4.Py()+tau_1_decay_neutral_p4.Py())**2-(tau_1_decay_charged_p4.Pz()+tau_1_decay_neutral_p4.Pz())**2)',
        #'(tau_1_decay_charged_p4+tau_1_decay_neutral_p4).M()',
        'sublead #tau rho m',
        '',
        Binning(30,0.,3.)),
 
    'ditau_decay_neutral_deta': Variable(
        'ditau_decay_neutral_deta', 
        'fabs(tau_0_decay_neutral_p4.Eta()-tau_1_decay_neutral_p4.Eta())',                               
        '#Delta#eta(neutral(#tau_{0}), neutral(#tau_{1}))', 
        '',
        Binning(20, 0, 2)),

    'ditau_decay_neutral_dphi': Variable(
        'ditau_decay_neutral_dphi', 
        'fabs(tau_0_decay_neutral_p4.Phi()-tau_1_decay_neutral_p4.Phi())',                               
        '#Delta#phi(neutral(#tau_{0}), neutral(#tau_{1}))', 
        '',
        Binning(13, 0, 2.6)),

    'tau_0_decay_charged_neutral_deta': Variable(
        'tau_0_decay_charged_neutral_deta', 
        'fabs(tau_0_decay_charged_p4.Eta()-tau_0_decay_neutral_p4.Eta())',                               
        '#Delta#eta(charged(#tau_{0}), neutral(#tau_{0}))', 
        '',
        Binning(20, 0., 0.2)),

    'tau_1_decay_charged_neutral_deta': Variable(
        'tau_1_decay_charged_neutral_deta', 
        'fabs(tau_1_decay_charged_p4.Eta()-tau_1_decay_neutral_p4.Eta())',                               
        '#Delta#eta(charged(#tau_{1}), neutral(#tau_{1}))', 
        '',
        Binning(20, 0., 0.2)),

    'tau_0_decay_charged_neutral_dphi': Variable(
        'tau_0_decay_charged_neutral_dphi', 
        'fabs(tau_0_decay_charged_p4.Phi()-tau_0_decay_neutral_p4.Phi())',                               
        '#Delta#phi(charged(#tau_{0}), neutral(#tau_{0}))', 
        '',
        Binning(20, 0., 0.2)),

    'tau_1_decay_charged_neutral_dphi': Variable(
        'tau_1_decay_charged_neutral_dphi', 
        'fabs(tau_1_decay_charged_p4.Phi()-tau_1_decay_neutral_p4.Phi())',                               
        '#Delta#phi(charged(#tau_{1}), neutral(#tau_{1}))', 
        '',
        Binning(20, 0, 0.2)),

    'tau_0_decay_charged_pt' : Variable(
        'tau_0_decay_charged_pt',
        'tau_0_decay_charged_p4.Pt()',
        'lead #tau charged p_{T}',
        '',
        Binning(22,0.,220)),

    'tau_1_decay_charged_pt' : Variable(
        'tau_1_decay_charged_pt',
        'tau_1_decay_charged_p4.Pt()',
        'sublead #tau charged p_{T}',
        '',
        Binning(22,0.,220)),

    'tau_0_decay_charged_eta' : Variable(
        'tau_0_decay_charged_eta',
        'tau_0_decay_charged_p4.Eta()',
        'lead #tau charged #eta',
        '',
        Binning(26,-2.6,2.6)),

    'tau_1_decay_charged_eta' : Variable(
        'tau_1_decay_charged_eta',
        'tau_1_decay_charged_p4.Eta()',
        'sublead #tau charged #eta',
        '',
        Binning(26,-2.6,2.6)),

    'tau_0_decay_charged_phi' : Variable(
        'tau_0_decay_charged_phi',
        'tau_0_decay_charged_p4.Phi()',
        'lead #tau charged #phi',
        '',
        Binning(32,-3.2,3.2)),

    'tau_1_decay_charged_phi' : Variable(
        'tau_1_decay_charged_phi',
        'tau_1_decay_charged_p4.Phi()',
        'sublead #tau charged #phi',
        '',
        Binning(32,-3.2,3.2)),

    'tau_0_decay_charged_m' : Variable(
        'tau_0_decay_charged_m',
        'tau_0_decay_charged_p4.M()',
        'lead #tau charged m',
        '',
        Binning(25,0.,0.5)),

    'tau_1_decay_charged_m' : Variable(
        'tau_1_decay_charged_m',
        'tau_1_decay_charged_p4.M()',
        'sublead #tau charged m',
        '',
        Binning(25,0.,0.5)),

    'tau_0_decay_charged_E' : Variable(
        'tau_0_decay_charged_E',
        'tau_0_decay_charged_p4.Energy()',
        'lead #tau charged E',
        '',
        Binning(45,0.,450)),

    'tau_1_decay_charged_E' : Variable(
        'tau_1_decay_charged_E',
        'tau_1_decay_charged_p4.Energy()',
        'sublead #tau charged E',
        '',
        Binning(45,0.,450)),

    'tau_0_track0_m' : Variable(
        'tau_0_track0_m',
        'tau_0_track0_p4.M()',
        'lead #tau lead track m',
        '',
        Binning(25,0.,0.5)),

    'tau_1_track0_m' : Variable(
        'tau_1_track0_m',
        'tau_1_track0_p4.M()',
        'sublead #tau lead track m',
        '',
        Binning(25,0.,0.5)),

    'tau_0_track0_pt' : Variable(
        'tau_0_track0_pt',
        'tau_0_track0_p4.Pt()',
        'lead #tau lead track p_{T}',
        '',
        Binning(22,0.,220)),

    'tau_1_track0_pt' : Variable(
        'tau_1_track0_pt',
        'tau_1_track0_p4.Pt()',
        'sublead #tau lead track p_{T}',
        '',
        Binning(22,0.,220)),

    'tau_0_track1_pt' : Variable(
        'tau_0_track1_pt',
        'tau_0_track1_p4.Pt()',
        'lead #tau second track p_{T}',
        '',
        Binning(22,0.,220)),

    'tau_1_track1_pt' : Variable(
        'tau_1_track1_pt',
        'tau_1_track1_p4.Pt()',
        'sublead #tau second track p_{T}',
        '',
        Binning(22,0.,220)),

    'tau_0_track2_pt' : Variable(
        'tau_0_track2_pt',
        'tau_0_track2_p4.Pt()',
        'lead #tau third track p_{T}',
        '',
        Binning(22,0.,220)),

    'tau_1_track2_pt' : Variable(
        'tau_1_track2_pt',
        'tau_1_track2_p4.Pt()',
        'sublead #tau third track p_{T}',
        '',
        Binning(22,0.,220)),

    'tau_0_track0_eta' : Variable(
        'tau_0_track0_eta',
        'tau_0_track0_p4.Eta()',
        'lead #tau lead track #eta',
        '',
        Binning(26,-2.6,2.6)),

    'tau_1_track0_eta' : Variable(
        'tau_1_track0_eta',
        'tau_1_track0_p4.Eta()',
        'sublead #tau lead track #eta',
        '',
        Binning(26,-2.6,2.6)),

    'tau_0_track1_eta' : Variable(
        'tau_0_track1_eta',
        'tau_0_track1_p4.Eta()',
        'lead #tau second track #eta',
        '',
        Binning(26,-2.6,2.6)),

    'tau_1_track1_eta' : Variable(
        'tau_1_track1_eta',
        'tau_1_track1_p4.Eta()',
        'sublead #tau second track #eta',
        '',
        Binning(26,-2.6,2.6)),

    'tau_0_track2_eta' : Variable(
        'tau_0_track2_eta',
        'tau_0_track2_p4.Eta()',
        'lead #tau third track #eta',
        '',
        Binning(26,-2.6,2.6)),

    'tau_1_track2_eta' : Variable(
        'tau_1_track2_eta',
        'tau_1_track2_p4.Eta()',
        'sublead #tau third track #eta',
        '',
        Binning(26,-2.6,2.6)),

    'tau_0_track0_phi' : Variable(
        'tau_0_track0_phi',
        'tau_0_track0_p4.Phi()',
        'lead #tau lead track #phi',
        '',
        Binning(32,-3.2,3.2)),

    'tau_1_track0_phi' : Variable(
        'tau_1_track0_phi',
        'tau_1_track0_p4.Phi()',
        'sublead #tau lead track #phi',
        '',
        Binning(32,-3.2,3.2)),

    'tau_0_track1_phi' : Variable(
        'tau_0_track1_phi',
        'tau_0_track1_p4.Phi()',
        'lead #tau second track #phi',
        '',
        Binning(32,-3.2,3.2)),

    'tau_1_track1_phi' : Variable(
        'tau_1_track1_phi',
        'tau_1_track1_p4.Phi()',
        'sublead #tau second track #phi',
        '',
        Binning(32,-3.2,3.2)),

    'tau_0_track2_phi' : Variable(
        'tau_0_track2_phi',
        'tau_0_track2_p4.Phi()',
        'lead #tau third track #phi',
        '',
        Binning(32,-3.2,3.2)),

    'tau_1_track2_phi' : Variable(
        'tau_1_track2_phi',
        'tau_1_track2_p4.Phi()',
        'sublead #tau third track #phi',
        '',
        Binning(32,-3.2,3.2)),

    'primary_vertex_x' : Variable(
        'primary_vertex_x',
        'primary_vertex_v.x()',
        'Primary vertex x',
        '',
        Binning(24,-1.2,1.2)),
        # Binning(20,-0.55,-0.45)),

    'primary_vertex_y' : Variable(
        'primary_vertex_y',
        'primary_vertex_v.y()',
        'Primary vertex y',
        '',
        Binning(24,-1.2,1.2)),
        # Binning(20,-0.55,-0.45)),

    'primary_vertex_z' : Variable(
        'primary_vertex_z',
        'primary_vertex_v.z()',
        'Primary vertex z',
        '',
        Binning(20,-200.,200.)),

    'meaninterbunchcrossing': Variable(
        'mean_interaction_bunch_crossing',
        'n_avg_int_cor',
        'Average Interactions Per Bunch Crossing', 
        '',
        VariableBinning([0, 15, 20, 25, 30, 35, 40, 45, 50, 55, 80])),

    'meaninterbunchcrossing_fine': Variable(
        'mean_interaction_bunch_crossing',
        'n_avg_int_cor',
        'Average Interactions Per Bunch Crossing', 
        '',
        Binning(40, 0, 80)),

    'actualinterbunchcrossing': Variable(
        'actual_interaction_bunch_crossing',
        'n_actual_int_cor',
        'Actual Interactions Per Bunch Crossing', 
        '',
        VariableBinning([0, 15, 20, 25, 30, 35, 40, 45, 50, 55, 80])),

    'actualinterbunchcrossing_fine': Variable(
        'actual_interaction_bunch_crossing',
        'n_actual_int_cor',
        'Actual Interactions Per Bunch Crossing', 
        '',
        Binning(40, 0, 80)),

    'n_vertices': Variable(
        'n_vertices',
        'n_vx',
        'Number of Vertices',
        '',
        Binning(12, 0, 60)),

    'tau_0_upsilon' : Variable(
        'tau_0_upsilon',
        'ditau_CP_tau0_upsilon',
        'lead #tau #Upsilon',
        '',
        Binning(22,-1.1,1.1)),

    'recalc_tau_0_upsilon' : Variable(
        'recalc_tau_0_upsilon',
        lead_tau_upsilon_expr,
        'lead #tau #Upsilon',
        '',
        Binning(22,-1.1,1.1)),

    'tau_1_upsilon' : Variable(
        'tau_1_upsilon',
        'ditau_CP_tau1_upsilon',
        'sublead #tau #Upsilon',
        '',
        Binning(22,-1.1,1.1)),

    'tau_0_pt_hhff_high_deta': Variable(
        'tau_0_pt_hhff_high_deta',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning([40, 45, 50, 60, 80, 200])),

    'tau_1_pt_hhff_high_deta': Variable(
        'tau_1_pt_hhff_high_deta',
        'tau_1_p4.Pt()',
        'subleading p_{T}',
        'GeV',
        VariableBinning([30, 34, 40, 45, 200])),

    'tau_0_eta_hhff_high_deta': Variable(
        'tau_0_eta_hhff_high_deta',
        'fabs(tau_0_p4.Eta())',
        'leading #eta',
        '',
        #VariableBinning( [0., 1.37,2.0, 2.5])),
        VariableBinning( [0., 1.37, 2.5])),

    'tau_1_eta_hhff_high_deta': Variable(
        'tau_1_eta_hhff_high_deta',
        'fabs(tau_1_p4.Eta())',
        'subleading #eta',
        '',
        #VariableBinning( [0., 1.37,2.0, 2.5])),
        VariableBinning( [0., 1.37, 2.5])),
    
    'tau_0_pt_hhff_param_lh': Variable(
        'tau_0_pt_hhff_param_lh',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning( [30, 40, 60, 500])),

    'tau_0_pt_hhff_samesign': Variable(
        'tau_0_pt_hhff_samesign',
        'tau_0_p4.Pt()',
        'leading p_{T}',
        'GeV',
        VariableBinning( [40, 60, 500])),

    'tau_1_pt_hhff_samesign': Variable(
        'tau_1_pt_hhff_samesign',
        'tau_1_p4.Pt()',
        'subleading p_{T}',
        'GeV',
        VariableBinning( [30, 40, 60, 500])),

    'tau_0_eta_hhff_samesign': Variable(
        'tau_0_eta_hhff_samesign',
        'fabs(tau_0_p4.Eta())',
        'leading #eta',
        '',
        VariableBinning( [0., 1.37, 2.5])),

    'tau_1_eta_hhff_samesign': Variable(
        'tau_1_eta_hhff_samesign',
        'fabs(tau_1_p4.Eta())',
        'subleading #eta',
        '',
        VariableBinning( [0., 1.37, 2.5])),
 
    'tau_0_1_upsilon_product' : Variable(
        'tau_0_1_upsilon_product',
        'ditau_CP_tau0_upsilon * ditau_CP_tau1_upsilon',
        'product of lead and sublead #tau #Upsilon',
        '',
        Binning(22,-1.1,1.1)),

    'fabs_tau_0_1_upsilon_product' : Variable(
        'fabs_tau_0_1_upsilon_product',
        'fabs(ditau_CP_tau0_upsilon * ditau_CP_tau1_upsilon)',
        '|product of lead and sublead #tau #Upsilon|',
        '',
        Binning(22,0,1.1)),

    'a1_upsilon' : Variable(
        'a1_upsilon',
        'ditau_CP_upsilon_a1',
        '#tau(3p) #Upsilon',
        '',
        Binning(32,-2.1,1.1)),

    'alphaminus_IP': Variable(
        'alphaminus_IP',
        'ditau_CP_alphaminus_ip',
        '#alpha_ (IP)',
        '',
        Binning(16, 0, 1.6)),

    'alphaminus_IPrho': Variable(
        'alphaminus_IPrho',
        'ditau_CP_alphaminus_ip_rho',
        '#alpha_ (IP#rho)',
        '',
        Binning(16, 0, 1.6)),

    'alphaminus_rhorho': Variable(
        'alphaminus_rhorho',
        'ditau_CP_alphaminus_rho_rho',
        '#alpha_ (#rho#rho)',
        '',
        Binning(16, 0, 1.6)),
 
    'recalc_phi_star_9bins' : Variable(
        'recalc_phi_star_9bins',
        phistar_expr,
        '#phi*_{CP}',
        '',
        Binning( 9, 0, 6.3)), 

    'recalc_phi_star_9bins_eta_smear' : Variable(
        'recalc_phi_star_9bins_eta_smear',
        phistar_eta_smear_expr,
        '#phi*_{CP}',
        '',
        Binning( 9, 0, 6.3)),
 
    'recalc_phi_star_9bins_phi_smear' : Variable(
        'recalc_phi_star_9bins_phi_smear',
        phistar_phi_smear_expr,
        '#phi*_{CP}',
        '',
        Binning( 9, 0, 6.3)),

    'recalc_phi_star_9bins_etaphi_smear' : Variable(
        'recalc_phi_star_9bins_etaphi_smear',
        phistar_etaphi_smear_expr,
        '#phi*_{CP}',
        '',
        Binning( 9, 0, 6.3)),

    'recalc_phi_star_9bins_energy_scale_up' : Variable(
        'recalc_phi_star_9bins_energy_scale_up',
        phistar_energy_scale_up_expr,
        '#phi*_{CP}',
        '',
        Binning( 9, 0, 6.3)), 

    'recalc_phi_star_9bins_energy_scale_down' : Variable(
        'recalc_phi_star_9bins_energy_scale_down',
        phistar_energy_scale_down_expr,
        '#phi*_{CP}',
        '',
        Binning( 9, 0, 6.3)),

    # fitting variables
    'recalc_phi_star_eta_smear_fit' : Variable(
        'recalc_phi_star_eta_smear_fit',
        phistar_eta_smear_expr,    
        '#varphi*_{CP}',
        '',
        Binning( 63, 0, 6.3)),

    'recalc_phi_star_phi_smear_fit' : Variable(
        'recalc_phi_star_phi_smear_fit',
        phistar_phi_smear_expr,
        '#varphi*_{CP}',
        '',
        Binning( 63, 0, 6.3)),

    'recalc_phi_star_etaphi_smear_fit' : Variable(
        'recalc_phi_star_etaphi_smear_fit',
        phistar_etaphi_smear_expr,
        '#varphi*_{CP}',
        '',
        Binning( 63, 0, 6.3)),

    'recalc_phi_star_energy_scale_up_fit' : Variable(
        'recalc_phi_energy_scale_smear_up_fit',
        phistar_energy_scale_up_expr,
        '#varphi*_{CP}',
        '',
        Binning( 63, 0, 6.3)),
   
    'recalc_phi_star_energy_scale_down_fit' : Variable(
        'recalc_phi_energy_scale_smear_down_fit',
        phistar_energy_scale_down_expr,
        '#varphi*_{CP}',
        '',
        Binning( 63, 0, 6.3)),

    'tau_vis_mass' : Variable(
        'tau_vis_mass',
        tauvismass_expr,
        '#tau visible mass',
        'GeV',
        Binning(60,0,3)), 
  
    'tau_vis_mass_eta_smear' : Variable(
        'tau_vis_mass_eta_smear',
        tauvismass_eta_smear_expr,
        '#tau visible mass',
        'GeV',
        Binning(60,0,3)),

    'tau_vis_mass_phi_smear' : Variable(
        'tau_vis_mass_phi_smear',
        tauvismass_phi_smear_expr,
        '#tau visible mass',
        'GeV',
        Binning(60,0,3)),

    'tau_vis_mass_etaphi_smear' : Variable(
        'tau_vis_mass_phi_smear',
        tauvismass_etaphi_smear_expr,
        '#tau visible mass',
        'GeV',
        Binning(60,0,3)),

    'tau_vis_mass_energy_scale_up' : Variable(
        'tau_vis_mass_energy_scale_up',
        tauvismass_energy_scale_up_expr,
        '#tau visible mass',
        'GeV',
        Binning(60,0,3)),

    'tau_vis_mass_energy_scale_down' : Variable(
        'tau_vis_mass_energy_scale_down',
        tauvismass_energy_scale_down_expr,
        '#tau visible mass',
        'GeV',
        Binning(60,0,3)),

  
    }
