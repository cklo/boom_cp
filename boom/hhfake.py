"""
Implementation of the hh fake systematics
"""
import ROOT
import os
from happy.systematics import Systematics, SystematicsSet

try:
    ROOT.gSystem.CompileMacro('boom/cpp/hhfakesyst_helper.cpp', 'k-', '', 'cache')
    print('BOOM:\t Fake syst helper tool loaded!')
except:
    raise RuntimeError

# this is needed because of
# https://sft.its.cern.ch/jira/browse/ROOT-7216
try:
    ROOT.HHFakeSystHelper()
except:
    print('Fake Syst tool not compiled')

__all__ = [
    'hh_fakesyst_expr',
]

######## same-sign files
_HH_SS_FAKE_FACTOR_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/hhfake/hhffsfromhh_obj_tau_0_pt_hhff_samesign.root") 
_hh_ss_ff_file = ROOT.TFile(_HH_SS_FAKE_FACTOR_FILE, "read")

####### high-deta files
_HH_HDETA_FAKE_FACTOR_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/hhfake/hhffsfromhh_obj_tau_0_pt_hhff_high_deta.root")
_hh_hdeta_ff_file = ROOT.TFile(_HH_HDETA_FAKE_FACTOR_FILE, "read")

######## wcr files
_HH_WCR_FAKE_FACTOR_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/hhfake/FFs_hadhad.root")
_hh_w_ff_file = ROOT.TFile(_HH_WCR_FAKE_FACTOR_FILE, "READ")

####### wcr mc subtr files 
_HH_WCR_MCSUBTR_FAKE_FACTOR_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/hhfake/FFs_hadhad_subtraction.root")
_hh_w_mcsubtr_ff_file = ROOT.TFile(_HH_WCR_MCSUBTR_FAKE_FACTOR_FILE, "READ")

######## param unc file
_HH_PARAM_FACTOR_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/hh_fake_param_uncert.root")
_hh_param_file = ROOT.TFile(_HH_PARAM_FACTOR_FILE, "READ")


######## reading param uncertainty
ROOT.hhfake_histDict.h_ff_param_weights[0]          = _hh_param_file.Get("hh_fake_param_uncert")

######## reading same-sign FFs
ROOT.hhfake_histDict.h_ff_2dobj_1p0n[0]             = _hh_ss_ff_file.Get("FF_preselection_comb1p0n")
ROOT.hhfake_histDict.h_ff_2dobj_1p1n[0]             = _hh_ss_ff_file.Get("FF_preselection_comb1p1n")
ROOT.hhfake_histDict.h_ff_2dobj_1pXn[0]             = _hh_ss_ff_file.Get("FF_preselection_comb1pXn")
ROOT.hhfake_histDict.h_ff_2dobj_3p0n[0]             = _hh_ss_ff_file.Get("FF_preselection_comb3p0n")

ROOT.hhfake_histDict.h_ff_2dobj_1p0n_lead_lnm[0]    = _hh_ss_ff_file.Get("FF_preselection_lead1p0n_fake_factors_hh_same_sign_anti_lead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_1p1n_lead_lnm[0]    = _hh_ss_ff_file.Get("FF_preselection_lead1p1n_fake_factors_hh_same_sign_anti_lead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_1pXn_lead_lnm[0]    = _hh_ss_ff_file.Get("FF_preselection_lead1pXn_fake_factors_hh_same_sign_anti_lead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_3p0n_lead_lnm[0]    = _hh_ss_ff_file.Get("FF_preselection_lead3p0n_fake_factors_hh_same_sign_anti_lead_tau_loose")

ROOT.hhfake_histDict.h_ff_2dobj_1p0n_sublead_lnm[0] = _hh_ss_ff_file.Get("FF_preselection_sublead1p0n_fake_factors_hh_same_sign_anti_sublead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_1p1n_sublead_lnm[0] = _hh_ss_ff_file.Get("FF_preselection_sublead1p1n_fake_factors_hh_same_sign_anti_sublead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_1pXn_sublead_lnm[0] = _hh_ss_ff_file.Get("FF_preselection_sublead1pXn_fake_factors_hh_same_sign_anti_sublead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_3p0n_sublead_lnm[0] = _hh_ss_ff_file.Get("FF_preselection_sublead3p0n_fake_factors_hh_same_sign_anti_sublead_tau_loose")

####### readinf high-deta FFs
ROOT.hhfake_histDict.h_ff_2dobj_1p0n_hd_lead[0]        = _hh_hdeta_ff_file.Get("FF_preselection_lead1p0n_high_deta_anti_lead_tau")
ROOT.hhfake_histDict.h_ff_2dobj_1p1n_hd_lead[0]        = _hh_hdeta_ff_file.Get("FF_preselection_lead1p1n_high_deta_anti_lead_tau")
ROOT.hhfake_histDict.h_ff_2dobj_1pXn_hd_lead[0]        = _hh_hdeta_ff_file.Get("FF_preselection_lead1pXn_high_deta_anti_lead_tau")
ROOT.hhfake_histDict.h_ff_2dobj_3p0n_hd_lead[0]        = _hh_hdeta_ff_file.Get("FF_preselection_lead3p0n_high_deta_anti_lead_tau")

ROOT.hhfake_histDict.h_ff_2dobj_1p0n_hd_sublead[0]     = _hh_hdeta_ff_file.Get("FF_preselection_sublead1p0n_high_deta_anti_sublead_tau")
ROOT.hhfake_histDict.h_ff_2dobj_1p1n_hd_sublead[0]     = _hh_hdeta_ff_file.Get("FF_preselection_sublead1p1n_high_deta_anti_sublead_tau")
ROOT.hhfake_histDict.h_ff_2dobj_1pXn_hd_sublead[0]     = _hh_hdeta_ff_file.Get("FF_preselection_sublead1pXn_high_deta_anti_sublead_tau")
ROOT.hhfake_histDict.h_ff_2dobj_3p0n_hd_sublead[0]     = _hh_hdeta_ff_file.Get("FF_preselection_sublead3p0n_high_deta_anti_sublead_tau")

ROOT.hhfake_histDict.h_ff_2dobj_1p0n_hd_lead_lnm[0]    = _hh_hdeta_ff_file.Get("FF_preselection_lead1p0n_high_deta_anti_lead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_1p1n_hd_lead_lnm[0]    = _hh_hdeta_ff_file.Get("FF_preselection_lead1p0n_high_deta_anti_lead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_1pXn_hd_lead_lnm[0]    = _hh_hdeta_ff_file.Get("FF_preselection_lead1pXn_high_deta_anti_lead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_3p0n_hd_lead_lnm[0]    = _hh_hdeta_ff_file.Get("FF_preselection_lead3p0n_high_deta_anti_lead_tau_loose")

ROOT.hhfake_histDict.h_ff_2dobj_1p0n_hd_sublead_lnm[0] = _hh_hdeta_ff_file.Get("FF_preselection_sublead1p0n_high_deta_anti_sublead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_1p1n_hd_sublead_lnm[0] = _hh_hdeta_ff_file.Get("FF_preselection_sublead1p1n_high_deta_anti_sublead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_1pXn_hd_sublead_lnm[0] = _hh_hdeta_ff_file.Get("FF_preselection_sublead1pXn_high_deta_anti_sublead_tau_loose")
ROOT.hhfake_histDict.h_ff_2dobj_3p0n_hd_sublead_lnm[0] = _hh_hdeta_ff_file.Get("FF_preselection_sublead3p0n_high_deta_anti_sublead_tau_loose")

######## reading WCR FFs
ROOT.hhfake_histDict.h_ff_2dobj_1p0n_w[0] = _hh_w_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_1p0n")
ROOT.hhfake_histDict.h_ff_2dobj_1p1n_w[0] = _hh_w_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_1p1n")
ROOT.hhfake_histDict.h_ff_2dobj_1pXn_w[0] = _hh_w_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_1pXn")
ROOT.hhfake_histDict.h_ff_2dobj_3p0n_w[0] = _hh_w_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_3p0n")

ROOT.hhfake_histDict.h_ff_2dobj_1p0n_w_lnm[0] = _hh_w_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_LNM_1p0n")
ROOT.hhfake_histDict.h_ff_2dobj_1p1n_w_lnm[0] = _hh_w_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_LNM_1p1n")
ROOT.hhfake_histDict.h_ff_2dobj_1pXn_w_lnm[0] = _hh_w_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_LNM_1pXn")
ROOT.hhfake_histDict.h_ff_2dobj_3p0n_w_lnm[0] = _hh_w_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_LNM_3p0n")


####### reading WCR FFs for MC subtraction
ROOT.hhfake_histDict.h_ff_2dobj_1p0n_w_mcsubtr[0] = _hh_w_mcsubtr_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_1p0n_MCSubtr")
ROOT.hhfake_histDict.h_ff_2dobj_1p1n_w_mcsubtr[0] = _hh_w_mcsubtr_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_1p1n_MCSubtr")
ROOT.hhfake_histDict.h_ff_2dobj_1pXn_w_mcsubtr[0] = _hh_w_mcsubtr_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_1pXn_MCSubtr")
ROOT.hhfake_histDict.h_ff_2dobj_3p0n_w_mcsubtr[0] = _hh_w_mcsubtr_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_3p0n_MCSubtr")

ROOT.hhfake_histDict.h_ff_2dobj_1p0n_w_lnm_mcsubtr[0] = _hh_w_mcsubtr_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_LNM_1p0n_MCSubtr")
ROOT.hhfake_histDict.h_ff_2dobj_1p1n_w_lnm_mcsubtr[0] = _hh_w_mcsubtr_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_LNM_1p1n_MCSubtr")
ROOT.hhfake_histDict.h_ff_2dobj_1pXn_w_lnm_mcsubtr[0] = _hh_w_mcsubtr_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_LNM_1pXn_MCSubtr")
ROOT.hhfake_histDict.h_ff_2dobj_3p0n_w_lnm_mcsubtr[0] = _hh_w_mcsubtr_ff_file.Get("FF_WCR_Presel_All_Comb_SLT_LNM_3p0n_MCSubtr")

_mapping_dict = {
    "Presel" : 0,
    "Boosted" : 0,
    "VBF" : 0,
    "VH" : 0
    }

_ff_cat = ['preselection', 'boost', 'vbf', 'vh']
transDict = {"preselection" : "Presel", "boost" : "Boosted", "vbf" : "VBF", "vh" : "VH"}

def _determine_ff_version(category):
    if 'boost' in category:
        _ff_cat = 'Boosted'
    elif 'vbf' in category:
        _ff_cat = 'VBF'
    elif 'vh' in category:
        _ff_cat = 'VH'
    elif ('presel'in category  or "tth" in category):
        _ff_cat = 'Presel'
    else:
        raise NotImplementedError
    return _mapping_dict[_ff_cat]

def hh_2dobj_ff_expr(category, syst = "nominal"):
    _ff_version = _determine_ff_version(category)
    expr = 'HHFakeSystHelper::read_hh_fakefactors({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8})'.format(
        'tau_0_p4.Pt()',
        'tau_1_p4.Pt()',
        'tau_0_decay_mode',
        'tau_1_decay_mode',
        'tau_0_jet_rnn_loose + tau_0_jet_rnn_medium',
        'tau_1_jet_rnn_loose + tau_1_jet_rnn_medium',
        'ditau_mmc_mlm_m',
        str(_ff_version),
        str(getattr(ROOT.hhfake_histDict, syst)))
    return expr
 
