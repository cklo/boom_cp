"""
Module to handle batch submission
"""
import os
import datetime
import itertools
from .systematics.summary import _is_valid_list

KINEMATICS = {
    'sys_trk' : [
        'TRK_BIAS_D0_WM',
        'TRK_BIAS_QOVERP_SAGITTA_WM',
        'TRK_BIAS_Z0_WM',
        'TRK_RES_D0_DEAD',
        'TRK_RES_D0_MEAS',
        'TRK_RES_Z0_DEAD',
        'TRK_RES_Z0_MEAS',
        ],

    'sys_l': [
        'EG_RESOLUTION_ALL',
        'EG_SCALE_ALL',
        'MUON_ID',
        'MUON_MS',
        'MUON_SAGITTA_RESBIAS',
        'MUON_SAGITTA_RHO',
        'MUON_SCALE',
        'TAUS_TRUEHADTAU_SME_TES_INSITUEXP',
        'TAUS_TRUEHADTAU_SME_TES_INSITUFIT',
        'TAUS_TRUEHADTAU_SME_TES_MODEL_CLOSURE',
        'TAUS_TRUEHADTAU_SME_TES_PHYSICSLIST',
        'TAUS_TRUEHADTAU_SME_TES_DETECTOR',
        'MET_SoftTrk_Scale',
        'MET_SoftTrk_ResoPara',
        'MET_SoftTrk_ResoPerp',
        ],

    'sys_j1': [
        'JET_BJES_Response',
        'JET_EffectiveNP_Statistical1',
        'JET_EffectiveNP_Statistical2',
        'JET_EffectiveNP_Statistical3',
        'JET_EffectiveNP_Statistical4',
        'JET_EffectiveNP_Statistical5',
        'JET_EffectiveNP_Statistical6',
        'JET_EtaIntercalibration_Modelling',
        'JET_EtaIntercalibration_NonClosure_2018data',
        'JET_EtaIntercalibration_NonClosure_highE', 
        'JET_EtaIntercalibration_NonClosure_negEta',
        'JET_EtaIntercalibration_NonClosure_posEta',
        'JET_EtaIntercalibration_TotalStat',
        'JET_PunchThrough_MC16',
        'JET_SingleParticle_HighPt',   
        ],

    'sys_j2': [
        'JET_EffectiveNP_Mixed1',
        'JET_EffectiveNP_Mixed2',
        'JET_EffectiveNP_Mixed3',
        'JET_Pileup_OffsetMu',
        'JET_Pileup_OffsetNPV',
        'JET_Pileup_PtTerm',
        'JET_Pileup_RhoTopology',
        'JET_EffectiveNP_Detector1',
        'JET_EffectiveNP_Detector2',
        'JET_EffectiveNP_Modelling1',
        'JET_EffectiveNP_Modelling2',
        'JET_EffectiveNP_Modelling3',
        'JET_EffectiveNP_Modelling4',
        ],

    'sys_j4': [
        'SMC_JET_JER_EffectiveNP_1',
        'SMC_JET_JER_EffectiveNP_2',
        'SMC_JET_JER_EffectiveNP_3',
        'SMC_JET_JER_EffectiveNP_4',
        'SMC_JET_JER_EffectiveNP_5',
        'SMC_JET_JER_EffectiveNP_6',
        'SMC_JET_JER_EffectiveNP_7',
        'SMC_JET_JER_EffectiveNP_8',
        'SMC_JET_JER_EffectiveNP_9',
        'SMC_JET_JER_EffectiveNP_10',
        'SMC_JET_JER_EffectiveNP_11',
        'SMC_JET_JER_EffectiveNP_12restTerm',
        'SMC_JET_JER_DataVsMC_MC16',
        ],

    'sys_j5': [
        'SPD_JET_JER_EffectiveNP_1',
        'SPD_JET_JER_EffectiveNP_2',
        'SPD_JET_JER_EffectiveNP_3',
        'SPD_JET_JER_EffectiveNP_4',
        'SPD_JET_JER_EffectiveNP_5',
        'SPD_JET_JER_EffectiveNP_6',
        'SPD_JET_JER_EffectiveNP_7',
        'SPD_JET_JER_EffectiveNP_8',
        'SPD_JET_JER_EffectiveNP_9',
        'SPD_JET_JER_EffectiveNP_10',
        'SPD_JET_JER_EffectiveNP_11',
        'SPD_JET_JER_EffectiveNP_12restTerm',
        'SPD_JET_JER_DataVsMC_MC16',
        ],

    'sys_j6': [
        'JET_Flavor_Composition',
        'JET_Flavor_Response',
        ],


}
"""
Blocks of kinematic variations
""" 

WEIGHT_VARIATIONS = {

    'JETPRW_SF': [
        # Jet
        'JET_JvtEfficiency',
        'JET_fJvtEfficiency',
        # PRW
        'PRW_DATASF'
        ],

    'TAU_ID_1': [
        'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025',
        'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530',
        'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040',
        'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40',
        'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025',
        'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530',
        'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040',
        'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40',
        ],

    'TAU_ID_2': [
        'TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL',
        'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT',
        'TAUS_TRUEHADTAU_EFF_RNNID_SYST',
        'TAUS_TRUEHADTAU_EFF_RECO_TOTAL',
        ],

    'TAU_TRUE_RECO_DECAYMODE' : [
        'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P0N_RECO_1P0N_TOTAL',
        'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1P0N_TOTAL',
        'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1P1N_TOTAL',
        'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1PXN_TOTAL',
        'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1PXN_RECO_1P1N_TOTAL',
        'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1PXN_RECO_1PXN_TOTAL',
        'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_3P0N_RECO_3P0N_TOTAL',
        'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_3PXN_RECO_3P0N_TOTAL',
        ],

    'TAU_TRIGGER_SF': [
        'TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718',
        'TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718',
        'TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718',
        'TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718',
        'TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018',
        'TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018',
        'TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018',
        'TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018',
        ],

     'THEORY_SYST_ZJETS': [
        'theory_z_ckk',
        'theory_z_qsf',
        'theory_z_lhe3weight_mur05_muf05_pdf261000',
        'theory_z_lhe3weight_mur05_muf1_pdf261000',
        'theory_z_lhe3weight_mur1_muf05_pdf261000',
        'theory_z_lhe3weight_mur1_muf2_pdf261000',
        'theory_z_lhe3weight_mur2_muf1_pdf261000',
        'theory_z_lhe3weight_mur2_muf2_pdf261000',
        'theory_z_MMHT_pdfset',
        'theory_z_CT14_pdfset',
        'theory_z_alphaS',
        ],

     'ALTERNATIVE_SAMPLES': [
        'altsamp_geo1',
        'altsamp_geo2',
        'altsamp_geo3',
        'altsamp_physlist',
        ],


     'THEORY_SYST_PDF_ZJETS_0': [
        'theory_z_pdf_0',
        'theory_z_pdf_1',
        'theory_z_pdf_2',
        'theory_z_pdf_3',
        'theory_z_pdf_4',
        'theory_z_pdf_5',
        'theory_z_pdf_6',
        'theory_z_pdf_7',
        'theory_z_pdf_8',
        'theory_z_pdf_9',
        ],

     'THEORY_SYST_PDF_ZJETS_1': [
        'theory_z_pdf_10',
        'theory_z_pdf_11',
        'theory_z_pdf_12',
        'theory_z_pdf_13',
        'theory_z_pdf_14',
        'theory_z_pdf_15',
        'theory_z_pdf_16',
        'theory_z_pdf_17',
        'theory_z_pdf_18',
        'theory_z_pdf_19',
        ],

     'THEORY_SYST_PDF_ZJETS_2': [
        'theory_z_pdf_20',
        'theory_z_pdf_21',
        'theory_z_pdf_22',
        'theory_z_pdf_23',
        'theory_z_pdf_24',
        'theory_z_pdf_25',
        'theory_z_pdf_26',
        'theory_z_pdf_27',
        'theory_z_pdf_28',
        'theory_z_pdf_29',
        ],

     'THEORY_SYST_PDF_ZJETS_3': [
        'theory_z_pdf_30',
        'theory_z_pdf_31',
        'theory_z_pdf_32',
        'theory_z_pdf_33',
        'theory_z_pdf_34',
        'theory_z_pdf_35',
        'theory_z_pdf_36',
        'theory_z_pdf_37',
        'theory_z_pdf_38',
        'theory_z_pdf_39',
        ],

     'THEORY_SYST_PDF_ZJETS_4': [
        'theory_z_pdf_40',
        'theory_z_pdf_41',
        'theory_z_pdf_42',
        'theory_z_pdf_43',
        'theory_z_pdf_44',
        'theory_z_pdf_45',
        'theory_z_pdf_46',
        'theory_z_pdf_47',
        'theory_z_pdf_48',
        'theory_z_pdf_49',
        ],

     'THEORY_SYST_PDF_ZJETS_5': [
        'theory_z_pdf_50',
        'theory_z_pdf_51',
        'theory_z_pdf_52',
        'theory_z_pdf_53',
        'theory_z_pdf_54',
        'theory_z_pdf_55',
        'theory_z_pdf_56',
        'theory_z_pdf_57',
        'theory_z_pdf_58',
        'theory_z_pdf_59',
        ],

     'THEORY_SYST_PDF_ZJETS_6': [
        'theory_z_pdf_60',
        'theory_z_pdf_61',
        'theory_z_pdf_62',
        'theory_z_pdf_63',
        'theory_z_pdf_64',
        'theory_z_pdf_65',
        'theory_z_pdf_66',
        'theory_z_pdf_67',
        'theory_z_pdf_68',
        'theory_z_pdf_69',
        ],

     'THEORY_SYST_PDF_ZJETS_7': [
        'theory_z_pdf_70',
        'theory_z_pdf_71',
        'theory_z_pdf_72',
        'theory_z_pdf_73',
        'theory_z_pdf_74',
        'theory_z_pdf_75',
        'theory_z_pdf_76',
        'theory_z_pdf_77',
        'theory_z_pdf_78',
        'theory_z_pdf_79',
        ],

     'THEORY_SYST_PDF_ZJETS_8': [
        'theory_z_pdf_80',
        'theory_z_pdf_81',
        'theory_z_pdf_82',
        'theory_z_pdf_83',
        'theory_z_pdf_84',
        'theory_z_pdf_85',
        'theory_z_pdf_86',
        'theory_z_pdf_87',
        'theory_z_pdf_88',
        'theory_z_pdf_89',
        ],

     'THEORY_SYST_PDF_ZJETS_9': [
        'theory_z_pdf_90',
        'theory_z_pdf_91',
        'theory_z_pdf_92',
        'theory_z_pdf_93',
        'theory_z_pdf_94',
        'theory_z_pdf_95',
        'theory_z_pdf_96',
        'theory_z_pdf_97',
        'theory_z_pdf_98',
        'theory_z_pdf_99',
        ],

     'THEORY_SYST_MUR_MUF_SGN': [
        'theory_sig_mur_muf_0',
        'theory_sig_mur_muf_1',
        'theory_sig_mur_muf_2',
        'theory_sig_mur_muf_3',
        'theory_sig_mur_muf_4',
        'theory_sig_mur_muf_5',
        'theory_sig_mur_muf_6',
        'theory_sig_mur_muf_7',
        ],

     'THEORY_SYST_QCD_SGN': [
        'theory_sig_qcd_0',
        'theory_sig_qcd_1',
        'theory_sig_qcd_2',
        'theory_sig_qcd_3',
        'theory_sig_qcd_4',
        'theory_sig_qcd_5',
        'theory_sig_qcd_6',
        'theory_sig_qcd_7',
        'theory_sig_qcd_8',
        'theory_sig_qcd_9',
        'theory_sig_qcd_10',
        'theory_sig_qcd_11',
        'theory_sig_qcd_12',
        'theory_sig_qcd_13',
        'theory_sig_st_4',
        'theory_sig_st_5',
        ],

     'THEORY_SYST_PDF_SGN_0': [
        'theory_sig_alphaS',
        'theory_sig_pdf_0',
        'theory_sig_pdf_1',
        'theory_sig_pdf_2',
        'theory_sig_pdf_3',
        'theory_sig_pdf_4',
        'theory_sig_pdf_5',
        'theory_sig_pdf_6',
        'theory_sig_pdf_7',
        'theory_sig_pdf_8',
        'theory_sig_pdf_9',
        ],

    'THEORY_SYST_PDF_SGN_1': [
        'theory_sig_pdf_10',
        'theory_sig_pdf_11',
        'theory_sig_pdf_12',
        'theory_sig_pdf_13',
        'theory_sig_pdf_14',
        'theory_sig_pdf_15',
        'theory_sig_pdf_16',
        'theory_sig_pdf_17',
        'theory_sig_pdf_18',
        'theory_sig_pdf_19',
        ],

    'THEORY_SYST_PDF_SGN_2': [ 
        'theory_sig_pdf_20',
        'theory_sig_pdf_21',
        'theory_sig_pdf_22',
        'theory_sig_pdf_23',
        'theory_sig_pdf_24',
        'theory_sig_pdf_25',
        'theory_sig_pdf_26',
        'theory_sig_pdf_27',
        'theory_sig_pdf_28',
        'theory_sig_pdf_29',
        ],

     'THEORY_SHAPE_SGN': [
        'phistar_theory_shape',
     ],
    

     'HH_FAKE' : [
        'hh_fake_ff_stat_1p0n_nm',
        'hh_fake_ff_mcsubtr_1p0n_nm', 
        'hh_fake_ff_stat_1p1n_nm',
        'hh_fake_ff_mcsubtr_1p1n_nm',
        'hh_fake_ff_stat_1pXn_nm',
        'hh_fake_ff_mcsubtr_1pXn_nm',
        'hh_fake_ff_stat_3p0n_nm',
        'hh_fake_ff_mcsubtr_3p0n_nm',
        'hh_fake_ff_stat_1p0n_lnm',
        'hh_fake_ff_mcsubtr_1p0n_lnm',
        'hh_fake_ff_stat_1p1n_lnm',
        'hh_fake_ff_mcsubtr_1p1n_lnm',
        'hh_fake_ff_stat_1pXn_lnm',
        'hh_fake_ff_mcsubtr_1pXn_lnm',
        'hh_fake_ff_stat_3p0n_lnm',
        'hh_fake_ff_mcsubtr_3p0n_lnm',
        'hh_fake_ff_param',
        'hh_fake_ff_composition_ss',
        'hh_fake_ff_composition_highdeta',
     ],
  
}
"""
Defining blocks of weight systematics
"""

PROCESSES = {
    'Data'  : ['Data'],
    'ZttQCD': ['ZttQCD'],
    'ZttEWK': ['ZttEWK'],
    'ZllQCD': ['ZllQCD'],
    'ZllEWK': ['ZllEWK'],
    'ZllMad': ['ZllMad'],
    'Top'   : ['Top'],
    'VV'    : ['VV'],
    'W'     : ['W'],
    'Fake'  : ['Fake'],
    'ggH'   : ['ggH'],
    'ggHWW' : ['ggHWW'],
    'VBFH'  : ['VBFH'],
    'VBFHWW': ['VBFHWW'],
    'WH'    : ['WH'],
    'ZH'    : ['ZH'],
    'ttH'   : ['ttH'],
    'ggH_FxFx' : ['ggH_FxFx'],
    'ggH_He7'  : ['ggH_He7'],
    'VBFH_He7' : ['VBFH_He7'],
    'WH_He7'   : ['WH_He7'],
    'ZH_He7'   : ['ZH_He7'],
    'VBFH_FxFx' : ['VBFH_FxFx'],
    'WH_FxFx'   : ['WH_FxFx'],
    'ZH_FxFx'   : ['ZH_FxFx'],
    #'ttH_afii' : ['ttH_afii'],
    #'ttH_afii_ME' : ['ttH_afii_ME'],
    #'ttH_afii_PS' : ['ttH_afii_PS'],

}
"""
Defining blocks of processes to be launched
"""

from .cpangles import CPANGLES
for k, v in list(CPANGLES.items()):
    _p = []
    for key in list(v.keys()):
        _p.append(k + key)
    PROCESSES[k + '_cpangles'] = _p

def _skip_group(group, channel, z_cr=False):
    """
    Small function to skip group of systematics
    (each systematic is documented in systematics/summary.py
    but we need an extra layer for a ntuple prod level splitting
    """

    _skip = False
    return _skip
        

def _expected_groups(channel, year, process_group=None, z_cr=False):
    """
    Small function to determine if a group of systematic is to 
    be expected for the given channel/year/process
    Parameters
    ----------
    channel: str
    year: str
    process_group: str
       default = None
    z_cr: bool
       default = False

    Returns
    -------
    _groups: list(str)
    """
    _groups = []
    for _group in sorted(list(WEIGHT_VARIATIONS.keys()) + list(KINEMATICS.keys())):
        _skip = _skip_group(_group, channel, z_cr=z_cr)
        if _skip:
            continue

        if _group in list(WEIGHT_VARIATIONS.keys()):
            _syst_list = WEIGHT_VARIATIONS[_group]
        else:
            _syst_list = KINEMATICS[_group]
        _valid_list = _is_valid_list(
            _syst_list, channel, year,
            process_group=process_group, z_cr=z_cr)

        if len(_valid_list) != 0:
            _groups.append(_group)

    return sorted(['nominal'] + _groups)

def expected_jobs(channel=None, year=None, process_group=None, z_cr=False):
    """
    Small function to return list of combination 
    (channel, year, process, variation group)
    Parameters
    ----------
    channel: str
    year: str
    process_group: str
    z_cr: bool
    """

    if channel == None:
        _channels = [
            'll', 
            'lh', 
            'hh', 
        ]
    else:
        _channels = [channel]

    if year == None:
        _years = [
            '15 16', 
            '17', 
            '18',
        ]
    else:
        if year == ['15', '16']:
            _years = ['15 16']
        else:
            _years = [year]

    if process_group == None:
        _processes = list(PROCESSES.keys())
    else:
        _processes = [process_group]

    _jobs = []
    for _c, _y, _p in itertools.product(_channels, _years, _processes):
        _groups = _expected_groups(_c, _y, process_group=_p, z_cr=z_cr)
        for _gr in _groups:
            _jobs.append([_c, _y, _p, _gr])
    return _jobs


KNOWN_ERROR_MESSAGES = {
    'out': [
        ],
    'err': [
        '.root.mimes',
        'fCurrentEntry should not be in between the two',
        'Error in <TChain::LoadTree>: Cannot find tree with name',
        ],
    'log': [
        ]
}
"""
Document list of harmless messages in log files
"""
WORRISOME_MESSAGES = {
    'out': [
        ],
    'err': [
        'Error',
        'C++ exception of type out_of_range',
        'Bus error',
        ],
    'log': [
        'Job removed',
        'Job was aborted by the user',
        ]
}
"""
Document list of harmfull messages in log files
"""


def _worrisome_lines(lines, log_flavor, disable_known_error_messages=False):
    """
    Parameters
    ----------
    lines: list(str)
    log_flavor: str
    disable_known_error_messages: bool

    Returns
    -------
    _unknown_worrisome_lines: list(str)
    """
    _worrisome_lines = []
    for _line in lines:
        for _msg in WORRISOME_MESSAGES[log_flavor]:
            if _msg in _line:
                _worrisome_lines.append(_line)
    # don't filter based on known errors
    if disable_known_error_messages:
        return _worrisome_lines

    _unknown_worrisome_lines = []
    for _line in _worrisome_lines:
        if len(KNOWN_ERROR_MESSAGES[log_flavor]) == 0:
            _unknown_worrisome_lines.append(_line)
        skip_line = False
        for _msg in KNOWN_ERROR_MESSAGES[log_flavor]:
            if _msg in _line:
                skip_line = True
                break
        if not skip_line:
            if not _line in _unknown_worrisome_lines:
                _unknown_worrisome_lines.append(_line)
    return _unknown_worrisome_lines


def inspect_log(condor_output_dir, info_dict, log_flavor='err', **kwargs):
    """
    Parameters
    ----------
    condor_output_dir: str
    info_dict: dict
    log_flavor: str
    
    Parameters
    ----------
    _unknown_worrisome_lines: str
    """
    _file = os.path.join(
        condor_output_dir, info_dict[log_flavor])
    try:
        with open(_file) as f:
            _lines = f.readlines()
            _unknown_worrisome_lines = _worrisome_lines(_lines, log_flavor, **kwargs)
            # if len(_unknown_worrisome_lines) != 0:
            # print 'Job might be corrupted: {}!'.format(info_dict['err'])
            # for _line in _unknown_worrisome_lines:
            #     print '\t', _line
            return _unknown_worrisome_lines
    except:
        print('BOOM: {} does not exist!'.format(_file))
        return []

def _str_to_time(str_time):
    """
    convert string to a date time object
    """
    h, m, s = str_time.replace(',', '').split(':')
    _time = datetime.timedelta(
        hours=int(h),
        minutes=int(m),
        seconds=int(s))
    return _time

def _runtime(log_file):
    """
    scrap time string from log file
    """
    try:
        with open(log_file) as f:
            _lines = f.readlines()
            for _l in _lines:
                if 'Total Remote Usage' in _l:
                    return _l
        return ''
    except:
        print(log_file, ' does not exist!')
        return ''

class JobLog(object):
    """JobLog

    Small helper class to interpret the result stored
    in the condor output folder
    """

    def __init__(self, dir_name):
        self._channel = self._check_channel(dir_name)
        self._year = self._check_year(dir_name)
        self._region = self._check_region(dir_name)
        self._variation = self._check_variation(dir_name)
        self._process = self._check_process(dir_name)
        self._infos = []
        self._output = None
        """
        Parameters
        ----------
        dir_name: str
        """

    def add(self, info_dict):
        """
        """
        self._infos.append(info_dict)
        self._determine_output()

    def _determine_output(self):
        """
        """
        if len(self._infos) == 0:
            self._output == None
        elif len(self._infos) == 1:
            self._output = self._infos[0]
        else:
            for _info in self._infos:
                if not _info['corrupted']:
                    _output_job_number = self._check_job_number(
                        self._output['dir'])
                    _current_job_number = self._check_job_number(
                        _info['dir'])
                    if _current_job_number > _output_job_number:
                        self._output = _info


    def __eq__(self, other):
        """
        """
        if self.channel == other.channel:
            if self.year == other.year:
                if self.region == other.region:
                    if self.variation == other.variation:
                        if self.process == other.process:
                            return True
        return False

    def __str__(self):
        """
        """
        return '{} {} {} {} {} {}'.format(
            self._channel,
            self._year,
            self._region,
            self._process,
            self._variation,
            self.job_number)

    def __repr__(self):
        """
        """
        return self.__str__()

    @property
    def corrupted(self):
        """
        """
        if len(self._infos) == 0:
            return False
        else:
            return self._output['corrupted']
                
    @property
    def done(self):
        """
        """
        try:
            log_file = os.path.join(
                self._output['path'],
                self._output['log'])
            with open(log_file) as f:
                _lines = f.readlines()
                for _l in _lines:
                    if 'Normal termination' in _l:
                        return True
            return False
        except:
            print(log_file, ' does not exist!')
            return False

    @property
    def minutes(self):
        return self._output['totalruntime'].total_seconds() / 60.

    @property
    def best_queue(self):
        if self.minutes < 20.:
            return 'espresso'
        elif self.minutes < 60.:
            return 'microcentury'
        elif self.minutes < 2. * 60:
            return 'longlunch'
        elif self.minutes < 8 * 60.:
            return 'workday'
        elif self.minutes < 24 * 60.:
            return 'tomorrow'
        elif self.minutes < 3 * 24 * 60:
            return 'testmatch'
        else:
            return 'nextweek'

    @property
    def job_number(self):
        return self._check_job_number(
            self._output['dir'])


    @property
    def output(self):
        """ """
        return self._output


    @property
    def channel(self):
        """ """
        return self._channel
        
    @property
    def year(self):
        """ """
        return self._year

    @property
    def region(self):
        """ """
        return self._region

    @property
    def variation(self):
        """ """
        return self._variation

    @property
    def process(self):
        """ """
        return self._process
    

    def _check_year(self, name):
        """ """
        if 'year_15_16' in name:
            return '15 16'
        elif 'year_17' in name:
            return '17'
        elif 'year_18' in name:
            return '18'
        else:
            return None

    def _check_channel(self, name):
        """ """
        if 'channel_ll' in name:
            return 'll'
        if 'channel_lh' in name:
            return 'lh'
        if 'channel_hh' in name:
            return 'hh'
        return None

    def _check_variation(self, name):
        """ """
        if 'nominal' in name:
            return 'nominal'
        for _group in list(WEIGHT_VARIATIONS.keys()):
            if _group in name:
                return _group
        for _group in list(KINEMATICS.keys()):
            if _group in name:
                return _group
        return None

    def _check_process(self, name):
        """ """
        _accepted = ''
        for _group in list(PROCESSES.keys()):
            if _group in name:
                if len(_group) > len(_accepted):
                    _accepted = _group
        if _accepted != '':
            return _accepted
        else:
            return None
        
    def _check_region(self, name):
        """ """
        if '_zcr' in name:
            return 'zcr'
        else:
            return 'sr'

    def _check_job_number(self, name):
        """ """
        _number = int(name.split('job')[-1])
        return _number


def filter_jobs(
    jobs, year=None, channel=None, process=None, 
    variation=None, region=None, corrupted=None):
    """
    Helper function to filter a list of jobs

    Parameters
    ----------
    jobs: list(JobLog)
    year: str or list(str)
    channel: str or list(str)
    process: str or list(str)
    variation: str or list(str)
    region: str or list(str)
    corrupted: bool
    """
    _jobs = jobs
    # year
    if year != None:
        if isinstance(year, (list, tuple)):
            _year = year
        else:
            _year = [year]
        _jobs = [j for j in _jobs if j.year in _year]

    # channel
    if channel != None:
        if isinstance(channel, (list, tuple)):
            _channel = channel
        else:
            _channel = [channel]
        _jobs = [j for j in _jobs if j.channel in _channel]

    # process
    if process != None:
        if isinstance(process, (list, tuple)):
            _process = process
        else:
            _process = [process]
        _jobs = [j for j in _jobs if j.process in _process]

    # variation
    if variation != None:
        if isinstance(variation, (list, tuple)):
            _variation = variation
        else:
            _variation = [variation]
        _jobs = [j for j in _jobs if j.variation in _variation]

    # region
    if region != None:
        if isinstance(region, (list, tuple)):
            _region = region
        else:
            _region = [region]
        _jobs = [j for j in _jobs if j.region in _region]

    # corrupted
    if corrupted != None:
        _jobs = [j for j in _jobs if j.corrupted == corrupted]

    return _jobs

def build_jobs(_dir):
    """
    Function to loop over a directory and build the list of JobLob
    outputs
    Parameters
    ----------
    _dir: str
    
    Returns
    -------
    jobs: list(JobLog)
    """

    jobs = []
    print('BOOM: building the list of jobs')
    for _d in os.listdir(_dir):
        if os.path.isdir(os.path.join(_dir, _d)):
            _rfile = os.listdir(os.path.join(_dir, _d))
            _info_dict = {
                'path': _dir,
                'dir': _d,
                'log': _d + '.log',
                'err': _d + '.err',
                'out': _d + '.out',}
            if len(_rfile) == 0:
                _info_dict['rfile'] = None
            elif len(_rfile) == 1:
                _info_dict['rfile'] = _rfile[0]
            else:
                raise IndexError

            _err = inspect_log(_dir, _info_dict, 'err')
            _log = inspect_log(_dir, _info_dict, 'log')
            _out = inspect_log(_dir, _info_dict, 'out')
            if len(_err) !=0 or len(_log) != 0 or len(_out) != 0:
                _info_dict['msg'] = {
                    'err': _err[0].strip('\n') if len(_err) != 0 else '',
                    'out': _out[0].strip('\n') if len(_out) != 0 else '',
                    'log': _log[0].strip('\n') if len(_log) != 0 else '',
                }
                _info_dict['corrupted'] = True
            else:
                _info_dict['corrupted'] = False

            _runtime_line = _runtime(os.path.join(_dir, _d + '.log'))
            if not _info_dict['corrupted']:
                try:
                    _t_usr = _str_to_time(_runtime_line.split(' ')[2])
                    _t_sys = _str_to_time(_runtime_line.split(' ')[5])
                    _info_dict['totalruntime'] = _t_usr + _t_sys
                    _info_dict['userruntime'] = _t_usr 
                    _info_dict['sysruntime'] = _t_sys
                except:
                    _info_dict['totalruntime'] = None
                    _info_dict['userruntime'] = None
                    _info_dict['sysruntime'] = None

            _job = JobLog(_d)
            _existing_jobs = [j for j in jobs if j == _job]
            if len(_existing_jobs) == 0:
                _job.add(_info_dict)
                jobs.append(_job)
            elif len(_existing_jobs) == 1:
                _existing_jobs[0].add(_info_dict)
            else:
                raise IndexError
    print('BOOM: ... done!')
    return jobs
