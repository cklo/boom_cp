"""
helper functions to build the list of selections needed in the analysis
"""
__all__ = [
    'get_selections',
]

from .selection import selection, ALL_SELECTIONS_DICT
from .cuts import CATEGORIES, YEARS

def filter_selections(
    selections,
    channels=None,
    categories=None,
    years=None,
    regions=None,
    triggers=None):
    """Function to filter a list of selections

    Arguments
    _________
    selections : list(selection)
       selection objects to filter on
    channels: str or list(str) 
       see CHANNELS in cuts/__init__.py
    categories: str or list(str) 
       see CATEGORIES in cuts/__init__.py
    years: str or list(str) 
       see YEARS in cuts/__init__.py
    regions: str or list(str) 
       see REGIONS in cuts/__init__.py
    triggers: str or list(str) 
       see TRIGGERS in cuts/__init__.py

    Returns
    _______
    _sels: list(selection)
       Filtered list of selection objects
    """

    _sels = selections
    if channels != None:
        if not isinstance(channels, (list, tuple)):
            channels = [channels]
        _sels = [s for s in _sels if s.channel in channels]

    if categories != None:
        if not isinstance(categories, (list, tuple)):
            categories = [categories]
        _sels = [s for s in _sels if s.category in categories]

    if years != None:
        if not isinstance(years, (list, tuple)):
            years = [years]
        _sels = [s for s in _sels if s.year in years]

    if regions != None:
        if not isinstance(regions, (list, tuple)):
            regions = [regions]
        _sels = [s for s in _sels if s.region in regions]

    if triggers != None:
        if not isinstance(triggers, (list, tuple)):
            triggers = [triggers]
        _sels = [s for s in _sels if s.trigger in triggers]
    
    return list(_sels)



def print_selection(selection):
    """Print the relevant information of a selection"""
    print('') 
    print('')
    print('BOOM: --> {}'.format(selection.name))
    print('BOOM:')
    print('BOOM: arguments')
    print('BOOM: ---------')
    print('BOOM: \t channel:   {}'.format(selection.channel))
    print('BOOM: \t year:      {}'.format(selection.year))
    print('BOOM: \t category:  {}'.format(selection.category))
    print('BOOM: \t trigger:   {}'.format(selection.trigger))
    print('BOOM: \t region:    {}'.format(selection.region))
    print('BOOM: \t fake cr:   {}'.format(selection._fake_region))
    print('BOOM:')
    print('BOOM: list of cuts')
    print('BOOM: ------------')
    for cut in selection._cut_list_mc:
        print('\t {}'.format(cut.cut.cut))
    print('BOOM:')
    print('BOOM: list of weights')
    print('BOOM: ---------------')
    for syst in selection._weight_list:
        _names = [w.nominal.weightExpression for w in syst.systSet]
        for w in sorted(list(set(_names))):
            print(f'BOOM: \t \t {w}')


def get_selections(
    channels=None,
    categories=None,
    years=None,
    regions=None,
    triggers=None):

    _ALL_SELECTIONS = []

    # Define valid options
    allowed_channels = ('1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','1p0n_1pXn','1pXn_1p0n','1p1n_3p0n','3p0n_1p1n')
    allowed_categories = CATEGORIES
    allowed_years = YEARS
    
    # Validate inputs (if input not specified, use the full list)
    valid_channels = []
    if channels == None:
        valid_channels = allowed_channels
    else:
        if not isinstance(channels, (list, tuple)):
            channels = [channels]
        for c in channels:
            if c in allowed_channels:
                valid_channels.append(c)
            else:
                print('ERROR! Skipping invalid Channel: {}'.format(c))

    valid_categories = []
    if categories == None:
        valid_categories = allowed_categories
    else:
        if not isinstance(categories, (list, tuple)):
            categories = [categories]
        for c in categories:
            if c in allowed_categories:
                valid_categories.append(c)
            else:
                print('ERROR! Skipping invalid Category: {}'.format(c))

    valid_years = []
    if years == None:
        valid_years = allowed_years
    else:
        if not isinstance(years, (list, tuple)):
            years = [years]
        for y in years:
            if y in allowed_years:
                valid_years.append(y)
            else:
                print('ERROR! Skipping invalid Year: {}'.format(y))

    # Loop over approved inputs to build selections
    for chan in valid_channels:
        for cat in valid_categories:
            for year in valid_years:
                _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'SR', year, fake_region='anti_tau'))
                _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'anti_tau', year))
                _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'same_sign', year))
                _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'same_sign_anti_tau', year))
             
            extra_selection_rules = [
                'high'  not in cat,
                'low'   not in cat,
            ]
            if all(extra_selection_rules):
                for year in valid_years:
                    _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'fake_factors_hh_same_sign_anti_lead_tau', year))
                    _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'fake_factors_hh_same_sign_anti_sublead_tau', year))
                    _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'fake_factors_hh_same_sign_anti_lead_tau_loose', year))
                    _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'fake_factors_hh_same_sign_anti_sublead_tau_loose', year))
                    _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'high_deta', year, fake_region='high_deta_anti_tau'))
                    _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'high_deta_same_sign', year, fake_region='high_deta_same_sign_anti_tau'))
                    _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'high_deta_anti_tau', year))
                    _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'high_deta_anti_lead_tau', year))
                    _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'high_deta_anti_sublead_tau', year))
                    _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'high_deta_anti_lead_tau_loose', year))
                    _ALL_SELECTIONS.append(selection(chan, 'di-had', cat, 'high_deta_anti_sublead_tau_loose', year))


    # Build the selection dictionary from the ALL_SELECTION list
    for sel in _ALL_SELECTIONS:
        if sel.name not in list(ALL_SELECTIONS_DICT.keys()):
            ALL_SELECTIONS_DICT[sel.name] = sel

    # Apply the filter as I am not sure how best to handle trigger/region selections
    _sels = filter_selections(_ALL_SELECTIONS,
                              channels=valid_channels,
                              categories=valid_categories,
                              years=valid_years,
                              regions=regions,
                              triggers=triggers
                            )
    return _sels


