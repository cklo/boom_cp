"""
Module encoding the xsec X kfactor X filter_efficiency / sum_of_weights maps
for all the MC datasets used in the analysis
"""

import os
import ROOT
import json


try:
    ROOT.gSystem.CompileMacro('boom/cpp/mc_norm_mult.cpp', 'k-', '', 'cache')
except:
    raise RuntimeError

if not hasattr(ROOT, 'MC_Norm'):
    print ("BOOM: Norm functions not compiled")

# try:
#     ROOT.MC_Norm()
# except:
#     print("Norm functions not compiled")
#     raise RuntimeError

__all__ = [
    "build",
]

def _fill_map(map, weight_list):
    if len(weight_list) == 0:
        print('BOOM: warning weight list is empty!')
    else:
        for dsid, xsec, sum_of_weights in weight_list:
            if sum_of_weights == 0:
                print('BOOM: warning dsid {}, sum_of_weight = 0'.format(dsid))
                map[dsid] = 0
            else:
                map[dsid] = xsec / sum_of_weights


def _fill_map_ztheo(map, weight_list, camp):
    for (dsid, pdflist) in list(weight_list[camp].items()):
        for variation in range(0,100):
            map[str(dsid)+"_"+str(variation)] = pdflist[variation]
        map[str(dsid)+"_"+'theory_z_lhe3weight_mur05_muf05_pdf261000']=pdflist[100]
        map[str(dsid)+"_"+'theory_z_lhe3weight_mur05_muf1_pdf261000']=pdflist[101]
        map[str(dsid)+"_"+'theory_z_lhe3weight_mur1_muf05_pdf261000']=pdflist[102]
        map[str(dsid)+"_"+'theory_z_lhe3weight_mur1_muf2_pdf261000']=pdflist[103]
        map[str(dsid)+"_"+'theory_z_lhe3weight_mur2_muf1_pdf261000']=pdflist[104]
        map[str(dsid)+"_"+'theory_z_lhe3weight_mur2_muf2_pdf261000']=pdflist[105]
        map[str(dsid)+"_"+'theory_z_CT14_pdfset']=pdflist[106]
        map[str(dsid)+"_"+'theory_z_MMHT_pdfset']=pdflist[107]
        map[str(dsid)+"_"+'theory_z_alphaS_up']=pdflist[108]
        map[str(dsid)+"_"+'theory_z_alphaS_down']=pdflist[109]

def _fill_map_sgntheo(map, weight_list, camp):
    for (dsid, pdflist) in list(weight_list[camp].items()):
        for variation in range(0,30):
            map[str(dsid)+"_"+str(variation)] = pdflist[variation]
        map[str(dsid)+"_"+'theory_sig_alphaS_low']=pdflist[30]
        map[str(dsid)+"_"+'theory_sig_alphaS_high']=pdflist[31]
        map[str(dsid)+"_"+'theory_sig_mur_muf_0']=pdflist[32] 
        map[str(dsid)+"_"+'theory_sig_mur_muf_1']=pdflist[33]
        map[str(dsid)+"_"+'theory_sig_mur_muf_2']=pdflist[34]
        map[str(dsid)+"_"+'theory_sig_mur_muf_3']=pdflist[35]
        map[str(dsid)+"_"+'theory_sig_mur_muf_4']=pdflist[36]
        map[str(dsid)+"_"+'theory_sig_mur_muf_5']=pdflist[37]
        map[str(dsid)+"_"+'theory_sig_mur_muf_6']=pdflist[38]
        map[str(dsid)+"_"+'theory_sig_mur_muf_7']=pdflist[39]

def _fill_json(ntuples_block='nom'):

    _ntuples_block = ntuples_block

    _file_name = 'xsec_sumofweights_{}.json'.format(_ntuples_block)
    _xsec_sumofweight_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__ )), 
        '..', 'data', _file_name)

    _xsec_weight_dict = {}
    with open(_xsec_sumofweight_file) as f:
        print('BOOM: reading {}'.format(_xsec_sumofweight_file))
        _xsec_weight_dict = json.load(f)
    return _xsec_weight_dict

def _fill_json_Ztheo():

    _xsec_sumofweight_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__ )),
        '..', 'data', 'xsec_sumofweights_theoryZ.json')

    f=open(_xsec_sumofweight_file)
    weights = json.load(f)
    return weights

def _fill_json_Ztheo():

    _xsec_sumofweight_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__ )),
        '..', 'data', 'xsec_sumofweights_theoryZ.json')

    f=open(_xsec_sumofweight_file)
    weights = json.load(f)
    return weights

def _fill_json_Sgntheo():

    _xsec_sumofweight_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__ )),
        '..', 'data', 'xsec_sumofweights_theorySgn.json')

    f=open(_xsec_sumofweight_file)
    weights = json.load(f)
    return weights


def build(ntuples_block='nom'):
    print('BOOM: filling the xsec X kfactor X filter_efficiency / sum_of_weights maps')
    _xsec_weight_dict = _fill_json(ntuples_block=ntuples_block)

    print('BOOM: filling mc16a hh')
    _fill_map(ROOT.crossSectionDict.m_Map_hh_a, _xsec_weight_dict['mc16a']['hh'])
    print('BOOM: filling mc16d hh')
    _fill_map(ROOT.crossSectionDict.m_Map_hh_d, _xsec_weight_dict['mc16d']['hh'])
    print('BOOM: filling mc16e hh')
    _fill_map(ROOT.crossSectionDict.m_Map_hh_e, _xsec_weight_dict['mc16e']['hh'])
 
    _xsec_weight_dict_Ztheo = _fill_json_Ztheo()
    print('BOOM: filling Ztheo ')
    _fill_map_ztheo(ROOT.crossSectionDict.m_Map_Ztheo_a, _xsec_weight_dict_Ztheo, "mc16a")
    _fill_map_ztheo(ROOT.crossSectionDict.m_Map_Ztheo_d, _xsec_weight_dict_Ztheo, "mc16d")
    _fill_map_ztheo(ROOT.crossSectionDict.m_Map_Ztheo_e, _xsec_weight_dict_Ztheo, "mc16e")

    _xsec_weight_dict_Sgntheo = _fill_json_Sgntheo()
    print('BOOM: filling Sgntheo ')
    _fill_map_sgntheo(ROOT.crossSectionDict.m_Map_Sgntheo_a, _xsec_weight_dict_Sgntheo, "mc16a")
    _fill_map_sgntheo(ROOT.crossSectionDict.m_Map_Sgntheo_d, _xsec_weight_dict_Sgntheo, "mc16d")
    _fill_map_sgntheo(ROOT.crossSectionDict.m_Map_Sgntheo_e, _xsec_weight_dict_Sgntheo, "mc16e")



    return True
