trigger_weights = {
  
        'hh' : {
            'tt' : {
                 '15' :  {
                         'nominal'       : 'tau_0_NOMINAL_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_NOMINAL_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statdata_down' : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statdata_up'   : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statmc_down'   : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statmc_up'     : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_down'     : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_up'       : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_mu_down'  : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_mu_up'    : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',

                         },
                '16' :  {
                         'nominal'       : 'tau_0_NOMINAL_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_NOMINAL_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statdata_down' : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statdata_up'   : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statmc_down'   : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statmc_up'     : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_down'     : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_up'       : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_mu_down'  : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_mu_up'    : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         },
                '17' :  {
                         'nominal'       : 'tau_0_NOMINAL_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_NOMINAL_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statdata_down' : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statdata_up'   : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statmc_down'   : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'statmc_up'     : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_down'     : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_up'       : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_mu_down'  : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1down_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1down_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         'syst_mu_up'    : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1up_TauEffSF_HLT_tau35_medium1_tracktwo_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718_1up_TauEffSF_HLT_tau25_medium1_tracktwo_JETIDRNNMEDIUM',
                         },
                '18' :  {
                         'nominal'       : 'tau_0_NOMINAL_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_NOMINAL_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'statdata_down' : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018_1down_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018_1down_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'statdata_up'   : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018_1up_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018_1up_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'statmc_down'   : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018_1down_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018_1down_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'statmc_up'     : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018_1up_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018_1up_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'syst_down'     : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018_1down_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018_1down_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'syst_up'       : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018_1up_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018_1up_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'syst_mu_down'  : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018_1down_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018_1down_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         'syst_mu_up'    : 'tau_0_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018_1up_TauEffSF_HLT_tau35_medium1_tracktwoEF_JETIDRNNMEDIUM * tau_1_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018_1up_TauEffSF_HLT_tau25_medium1_tracktwoEF_JETIDRNNMEDIUM',
                         },   
 
                   },
               },
} 

