from happy.systematics import SystematicsSet, Systematics

__all__ = ['kinematics']
    

KINEMATIC_VARIATIONS = [
    # egamma
    'EG_RESOLUTION_ALL',
    'EG_SCALE_ALL',
    # muons
    'MUON_MS',
    'MUON_ID',
    'MUON_SCALE',
    'MUON_SAGITTA_RESBIAS',
    'MUON_SAGITTA_RHO',
    # taus
    'TAUS_TRUEHADTAU_SME_TES_INSITUEXP',
    'TAUS_TRUEHADTAU_SME_TES_INSITUFIT',
    'TAUS_TRUEHADTAU_SME_TES_MODEL_CLOSURE',
    'TAUS_TRUEHADTAU_SME_TES_PHYSICSLIST',
    'TAUS_TRUEHADTAU_SME_TES_DETECTOR',
    # jets
    # j1
    'JET_BJES_Response',
    'JET_EffectiveNP_Statistical1',
    'JET_EffectiveNP_Statistical2',
    'JET_EffectiveNP_Statistical3',
    'JET_EffectiveNP_Statistical4',
    'JET_EffectiveNP_Statistical5',
    'JET_EffectiveNP_Statistical6',
    'JET_EtaIntercalibration_Modelling',
    'JET_EtaIntercalibration_NonClosure_2018data',
    'JET_EtaIntercalibration_NonClosure_highE', 
    'JET_EtaIntercalibration_NonClosure_negEta',
    'JET_EtaIntercalibration_NonClosure_posEta',
    'JET_EtaIntercalibration_TotalStat',
    'JET_PunchThrough_MC16',
    'JET_SingleParticle_HighPt',
    # j2
    'JET_EffectiveNP_Mixed1',
    'JET_EffectiveNP_Mixed2',
    'JET_EffectiveNP_Mixed3',
    'JET_Pileup_OffsetMu',
    'JET_Pileup_OffsetNPV',
    'JET_Pileup_PtTerm',
    'JET_Pileup_RhoTopology',
    'JET_EffectiveNP_Detector1',
    'JET_EffectiveNP_Detector2',
    'JET_EffectiveNP_Modelling1',
    'JET_EffectiveNP_Modelling2',
    'JET_EffectiveNP_Modelling3',
    'JET_EffectiveNP_Modelling4',
    # j4
    'SMC_JET_JER_EffectiveNP_1',
    'SMC_JET_JER_EffectiveNP_2',
    'SMC_JET_JER_EffectiveNP_3',
    'SMC_JET_JER_EffectiveNP_4',
    'SMC_JET_JER_EffectiveNP_5',
    'SMC_JET_JER_EffectiveNP_6',
    'SMC_JET_JER_EffectiveNP_7',
    'SMC_JET_JER_EffectiveNP_8',
    'SMC_JET_JER_EffectiveNP_9',
    'SMC_JET_JER_EffectiveNP_10',
    'SMC_JET_JER_EffectiveNP_11',
    'SMC_JET_JER_EffectiveNP_12restTerm',
    'SMC_JET_JER_DataVsMC_MC16',
    # j5
    'SPD_JET_JER_EffectiveNP_1',
    'SPD_JET_JER_EffectiveNP_2',
    'SPD_JET_JER_EffectiveNP_3',
    'SPD_JET_JER_EffectiveNP_4',
    'SPD_JET_JER_EffectiveNP_5',
    'SPD_JET_JER_EffectiveNP_6',
    'SPD_JET_JER_EffectiveNP_7',
    'SPD_JET_JER_EffectiveNP_8',
    'SPD_JET_JER_EffectiveNP_9',
    'SPD_JET_JER_EffectiveNP_10',
    'SPD_JET_JER_EffectiveNP_11',
    'SPD_JET_JER_EffectiveNP_12restTerm',
    'SPD_JET_JER_DataVsMC_MC16',
    # j6
    'JET_Flavor_Composition',
    'JET_Flavor_Response',

] 

kinematics = SystematicsSet()

for var_name in KINEMATIC_VARIATIONS:
    _upTreeName = '{}_1up'.format(var_name)
    _downTreeName = '{}_1down'.format(var_name)
    variation = Systematics.treeSystematics(
        var_name,
        var_name.replace('_', ' '),
        upTreeName = _upTreeName,
        downTreeName = _downTreeName,
        nominalTreeName='NOMINAL')
    kinematics.add(variation)

# adding the MET soft terms

met_scale = Systematics.treeSystematics(
    'MET_SoftTrk_Scale',  
    'MET SoftTrk Scale',
    upTreeName='MET_SoftTrk_ScaleUp',
    downTreeName='MET_SoftTrk_ScaleDown',
    nominalTreeName='NOMINAL')

met_para = Systematics.treeSystematics( 
    'MET_SoftTrk_ResoPara',  
    'MET SoftTrk ResoPara',
    upTreeName='MET_SoftTrk_ResoPara',
    downTreeName=None,
    nominalTreeName='NOMINAL')

met_perp = Systematics.treeSystematics( 
    'MET_SoftTrk_ResoPerp',  
    'MET_Soft Reso Perp',
    upTreeName='MET_SoftTrk_ResoPerp',
    downTreeName=None,
    nominalTreeName='NOMINAL')

trk_bias_do_wm = Systematics.treeSystematics(
    'TRK_BIAS_D0_WM',
    'TRK_BIAS_D0_WM',
    upTreeName = 'TRK_BIAS_D0_WM',
    downTreeName=None,
    nominalTreeName='NOMINAL')

trk_bias_qoverp_sagitta_wm = Systematics.treeSystematics(
    'TRK_BIAS_QOVERP_SAGITTA_WM',
    'TRK_BIAS_QOVERP_SAGITTA_WM',
    upTreeName = 'TRK_BIAS_QOVERP_SAGITTA_WM',
    downTreeName=None,
    nominalTreeName='NOMINAL')

trk_bias_z0_wm = Systematics.treeSystematics(
    'TRK_BIAS_Z0_WM',
    'TRK_BIAS_Z0_WM',
    upTreeName = 'TRK_BIAS_Z0_WM',
    downTreeName=None,
    nominalTreeName='NOMINAL')

trk_res_d0_dead = Systematics.treeSystematics(
    'TRK_RES_D0_DEAD',
    'TRK_RES_D0_DEAD',
    upTreeName = 'TRK_RES_D0_DEAD',
    downTreeName=None,
    nominalTreeName='NOMINAL')

trk_red_d0_meas = Systematics.treeSystematics(
    'TRK_RES_D0_MEAS',
    'TRK_RES_D0_MEAS',
    upTreeName = 'TRK_RES_D0_MEAS',
    downTreeName=None,
    nominalTreeName='NOMINAL')

trk_res_z0_dead = Systematics.treeSystematics(
    'TRK_RES_Z0_DEAD',
    'TRK_RES_Z0_DEAD',
    upTreeName = 'TRK_RES_Z0_DEAD',
    downTreeName=None,
    nominalTreeName='NOMINAL')

trk_red_z0_meas = Systematics.treeSystematics(
    'TRK_RES_Z0_MEAS',
    'TRK_RES_Z0_MEAS',
    upTreeName = 'TRK_RES_Z0_MEAS',
    downTreeName=None,
    nominalTreeName='NOMINAL')

for variation in (met_scale, met_para, met_perp, trk_bias_do_wm, trk_bias_qoverp_sagitta_wm, trk_bias_z0_wm, trk_res_d0_dead, trk_red_d0_meas, trk_res_z0_dead, trk_red_z0_meas):
    kinematics.add(variation)
