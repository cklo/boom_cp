"""
"""
SUMMARY = {
    'TRK_BIAS_D0_WM': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TRK_BIAS_QOVERP_SAGITTA_WM' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TRK_BIAS_Z0_WM' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TRK_RES_D0_DEAD' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
  
    'TRK_RES_D0_MEAS' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TRK_RES_Z0_DEAD' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TRK_RES_Z0_MEAS' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'MET_SoftTrk_ResoPara': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'MET_SoftTrk_ResoPerp': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'MET_SoftTrk_Scale': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_SME_TES_INSITUEXP': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
      
    'TAUS_TRUEHADTAU_SME_TES_INSITUFIT': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'}, 

    'TAUS_TRUEHADTAU_SME_TES_MODEL_CLOSURE': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_SME_TES_PHYSICSLIST': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
  
    'TAUS_TRUEHADTAU_SME_TES_DETECTOR': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
   
    'EG_RESOLUTION_ALL': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'EG_SCALE_ALL': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'MUON_ID': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'MUON_MS': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'MUON_SAGITTA_RESBIAS': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'MUON_SAGITTA_RHO': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'MUON_SCALE': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Statistical1': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Statistical2': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Statistical3': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Statistical4': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Statistical5': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Statistical6': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_BJES_Response': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EtaIntercalibration_Modelling': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EtaIntercalibration_NonClosure_2018data': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EtaIntercalibration_NonClosure_highE': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EtaIntercalibration_NonClosure_negEta': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EtaIntercalibration_NonClosure_posEta': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EtaIntercalibration_TotalStat': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_PunchThrough_MC16' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_SingleParticle_HighPt': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_Flavor_Composition': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_Flavor_Response': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SMC_JET_JER_DataVsMC_MC16': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SMC_JET_JER_EffectiveNP_1': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
   
    'SMC_JET_JER_EffectiveNP_2': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SMC_JET_JER_EffectiveNP_3': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SMC_JET_JER_EffectiveNP_4': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'}, 

    'SMC_JET_JER_EffectiveNP_5': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SMC_JET_JER_EffectiveNP_6': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SMC_JET_JER_EffectiveNP_7': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SMC_JET_JER_EffectiveNP_8': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SMC_JET_JER_EffectiveNP_9': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SMC_JET_JER_EffectiveNP_10': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SMC_JET_JER_EffectiveNP_11': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SMC_JET_JER_EffectiveNP_12restTerm': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_DataVsMC_MC16': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_1': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_2': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_3': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_4': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_5': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_6': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_7': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_8': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_9': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_10': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_11': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'SPD_JET_JER_EffectiveNP_12restTerm': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
   
    'JET_EffectiveNP_Mixed1': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Mixed2': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Mixed3': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_Pileup_OffsetMu': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_Pileup_OffsetNPV': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_Pileup_PtTerm': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_Pileup_RhoTopology': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Detector1': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Detector2': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Modelling1': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Modelling2': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Modelling3': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_EffectiveNP_Modelling4': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_JvtEfficiency': {
        'channels': ['ll', 'lh', 'hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'JET_fJvtEfficiency': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'PRW_DATASF': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_RNNID_SYST': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_RECO_TOTAL': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
    
    'TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
  
    # alternative sample systematics
    'altsamp_geo1' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
 
    'altsamp_geo2' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'altsamp_geo3' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'altsamp_physlist' : {
        'channels': ['lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    # decay mode systematics 
    'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P0N_RECO_1P0N_TOTAL' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'}, 

    'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1P0N_TOTAL' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
  
    'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1P1N_TOTAL' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'}, 

    'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1PXN_TOTAL' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1PXN_RECO_1P1N_TOTAL' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
 
    'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1PXN_RECO_1PXN_TOTAL' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_3P0N_RECO_3P0N_TOTAL' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},
 
    'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_3PXN_RECO_3P0N_TOTAL' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': 'all_mc'},

    #theory systematics for Z+jets
    'theory_z_ckk' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_qsf' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_lhe3weight_mur05_muf05_pdf261000' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_lhe3weight_mur05_muf1_pdf261000' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},
   
    'theory_z_lhe3weight_mur1_muf05_pdf261000' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_lhe3weight_mur1_muf2_pdf261000' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_lhe3weight_mur2_muf1_pdf261000' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},
    
    'theory_z_lhe3weight_mur2_muf2_pdf261000' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_CT14_pdfset' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_MMHT_pdfset' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_alphaS' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_0'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_1'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_2'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_3'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_4'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_5'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_6'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_7'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_8'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_9'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_10'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_11'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_12'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_13'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_14'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_15'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_16'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_17'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_18'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_19'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_20'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_21'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_22'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_23'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_24'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_25'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_26'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_27'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_28'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_29'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_30'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_31'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_32'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_33'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_34'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_35'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_36'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_37'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_38'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_39'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_40'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_41'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_42'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_43'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_44'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_45'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_46'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_47'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_48'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_49'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_50'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_51'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_52'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_53'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_54'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_55'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_56'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_57'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_58'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_59'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_60'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_61'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_62'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_63'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_64'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_65'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_66'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_67'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_68'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_69'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_70'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_71'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_72'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_73'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_74'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_75'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_76'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_77'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_78'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_79'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},
 
    'theory_z_pdf_80'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_81'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_82'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_83'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_84'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_85'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_86'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_87'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_88'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_89'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_90'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_91'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_92'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_93'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_94'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_95'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_96'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_97'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_98'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    'theory_z_pdf_99'  : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ZttQCD']},

    # theory systematics for signal
    'theory_sig_alphaS' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_mur_muf_0' : {
        'channels': ['hh','lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['VBFH','ZH','WH','ttH','VBFH_cpangles', 'ZH_cpangles','WH_cpangles','ttH_cpangles']},
   
    'theory_sig_mur_muf_1' : {
        'channels': ['hh','lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['VBFH','ZH','WH','ttH','VBFH_cpangles', 'ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_mur_muf_2' : {
        'channels': ['hh','lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['VBFH','ZH','WH','ttH','VBFH_cpangles', 'ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_mur_muf_3' : {
        'channels': ['hh','lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['VBFH','ZH','WH','ttH','VBFH_cpangles', 'ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_mur_muf_4' : {
        'channels': ['hh','lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['VBFH','ZH','WH','ttH','VBFH_cpangles', 'ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_mur_muf_5' : {
        'channels': ['hh','lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['VBFH','ZH','WH','ttH','VBFH_cpangles', 'ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_mur_muf_6' : {
        'channels': ['hh','lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['VBFH','ZH','WH','ttH','VBFH_cpangles', 'ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_mur_muf_7' : {
        'channels': ['hh','lh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['VBFH','ZH','WH','ttH','VBFH_cpangles', 'ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_qcd_0' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_1' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_2' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_3' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_4' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_5' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_6' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_7' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_8' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_9' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_10' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_11' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_12' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_qcd_13' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_st_4' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_st_5' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','ggH_cpangles']},

    'theory_sig_pdf_0' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_1' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},  
 
    'theory_sig_pdf_2' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},
 
    'theory_sig_pdf_3' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},
 
    'theory_sig_pdf_4' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_5' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_6' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_7' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_8' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_9' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_10' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_11' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_12' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_13' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']}, 

    'theory_sig_pdf_14' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_15' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_16' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_17' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},
 
    'theory_sig_pdf_18' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_19' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},
 
    'theory_sig_pdf_20' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_21' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_22' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_23' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_24' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_25' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_26' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_27' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_28' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'theory_sig_pdf_29' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'phistar_theory_shape' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes': ['ggH','VBFH','ZH','WH','ttH','ggH_cpangles','VBFH_cpangles','ZH_cpangles','WH_cpangles','ttH_cpangles']},

    'hh_fake_ff_stat_1p0n_nm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},  

    'hh_fake_ff_mcsubtr_1p0n_nm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_stat_1p1n_nm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},
   
    'hh_fake_ff_mcsubtr_1p1n_nm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_stat_1pXn_nm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_mcsubtr_1pXn_nm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_stat_3p0n_nm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_mcsubtr_3p0n_nm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_stat_1p0n_lnm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},
   
    'hh_fake_ff_mcsubtr_1p0n_lnm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_stat_1p1n_lnm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},
 
    'hh_fake_ff_mcsubtr_1p1n_lnm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_stat_1pXn_lnm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_mcsubtr_1pXn_lnm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_stat_3p0n_lnm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_mcsubtr_3p0n_lnm' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},
    
    'hh_fake_ff_composition_ss' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_composition_highdeta': {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

    'hh_fake_ff_param' : {
        'channels': ['hh'],
        'years': ['15', '16', '15 16', '15_16', '17', '18'],
        'processes' : ['all_mc','Fake']},

}





def _is_valid_systematic(name, channel, year, process_group=None, z_cr=False):
    """Helper function to decide if a systematic is valid

    Parameters
    ----------
    name: str
    channel: str
    year: str
    process_group: str
    z_cr: bool
    """
    if name not in SUMMARY.keys():
        raise ValueError(name)
    _info = SUMMARY[name]
    
    # channel
    if not isinstance(channel, (list, tuple)):
        _channels = [channel]
    else:
        _channels = channel

    _skip_channel = True
    if z_cr:
        #  z_cr and ll have same systs
        if 'll' in _info['channels']:
            _skip_channel = False
    else:
        for _channel in _channels:
            if _channel in _info['channels']:
                _skip_channel = False
    if _skip_channel == True:
        return False

    # years
    if not isinstance(year, (list, tuple)):
        _years = [year]
    else:
        _years = year

    _skip_year = True
    for _year in _years:
        if _year in _info['years']:
            _skip_year = False
    if _skip_year == True:
        return False

    # process
    _skip_process = True
    if process_group == None:
        _skip_process = False
    else:
        if _info['processes'] == ['all_mc','Fake']:
            if not process_group in ('Data',):
                _skip_process = False
        elif isinstance(_info['processes'], (list, tuple)):
            if process_group in _info['processes']:
                _skip_process = False
        elif _info['processes'] == 'all_mc':
            if not process_group in ('Data', 'Fake'):
                _skip_process = False
        else:
            raise ValueError('{} not implemented'.format(_info['processes']))

    if _skip_process == True:
        return False

    return True

def _is_valid_list(full_list, *args, **kwargs):
    """filter a list of systematics
    
    Parameters
    ----------
    full_list: list(str)
    """
    _valid_list = []
    for _syst in full_list:
        if _is_valid_systematic(_syst, *args, **kwargs):
            _valid_list.append(_syst)
    return _valid_list

