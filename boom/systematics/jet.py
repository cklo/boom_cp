from .base import DecoratedSystematics, SYSTBOOK
from ..cuts import REGIONS
from .object_weight_dictionary import scale_factors
from happy.systematics import SystematicsSet, Systematics

########################
# JET weight systematics
########################
sys_jet_jvt             = Systematics.weightSystematics( 'JET_JvtEfficiency',  'Jet JVT Eff',
                        '({})'.format(scale_factors['Jets']['JVT']['syst_up']),
                        '({})'.format(scale_factors['Jets']['JVT']['syst_down']),
                        '({})'.format(scale_factors['Jets']['JVT']['nominal']))

sys_jet_fjvt            = Systematics.weightSystematics( 'JET_fJvtEfficiency',  'Jet fJVT Eff',
                        '(MC_Norm::NaNProtectBetterThanRoot({}))'.format(scale_factors['Jets']['fJVT']['syst_up']),
                        '(MC_Norm::NaNProtectBetterThanRoot({}))'.format(scale_factors['Jets']['fJVT']['syst_down']),
                        '({})'.format(scale_factors['Jets']['fJVT']['nominal']))

weight_jvt = DecoratedSystematics(
           'jet_weight_jvt_systematics',
           'Jet weight (f)JVT systematics collection',
           systSet = SystematicsSet(set([ sys_jet_jvt,
                                          sys_jet_fjvt,
                                        ])))
SYSTBOOK.append(weight_jvt)





