from .base import DecoratedSystematics, SYSTBOOK
from .trigger_weight_dictionary import trigger_weights
from happy.systematics import SystematicsSet, Systematics

##############################
# hh channel (rel variation) 
##############################
sys_tau_trig_statdata_15_rel   = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718', 'Tau Trigger 15 Stat Data (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['15']['statdata_up'],trigger_weights['hh']['tt']['15']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['15']['statdata_down'],trigger_weights['hh']['tt']['15']['nominal']),
                            '1')

sys_tau_trig_statdata_16_rel   = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718', 'Tau Trigger 16 Stat Data (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['16']['statdata_up'],trigger_weights['hh']['tt']['16']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['16']['statdata_down'],trigger_weights['hh']['tt']['16']['nominal']),
                            '1')

sys_tau_trig_statdata_17_rel   = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718', 'Tau Trigger 17 Stat Data (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['17']['statdata_up'],trigger_weights['hh']['tt']['17']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['17']['statdata_down'],trigger_weights['hh']['tt']['17']['nominal']),
                            '1')

sys_tau_trig_statdata_18_rel   = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018', 'Tau Trigger 18 Stat Data (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['18']['statdata_up'],trigger_weights['hh']['tt']['18']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['18']['statdata_down'],trigger_weights['hh']['tt']['18']['nominal']),
                            '1')

sys_tau_trig_statmc_15_rel     = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718', 'Tau Trigger 15 Stat MC (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['15']['statmc_up'],trigger_weights['hh']['tt']['15']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['15']['statmc_down'],trigger_weights['hh']['tt']['15']['nominal']),
                            '1')

sys_tau_trig_statmc_16_rel     = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718', 'Tau Trigger 16 Stat MC (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['16']['statmc_up'],trigger_weights['hh']['tt']['16']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['16']['statmc_down'],trigger_weights['hh']['tt']['16']['nominal']),
                            '1')

sys_tau_trig_statmc_17_rel     = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718', 'Tau Trigger 17 Stat MC (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['17']['statmc_up'],trigger_weights['hh']['tt']['17']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['17']['statmc_down'],trigger_weights['hh']['tt']['17']['nominal']),
                            '1')

sys_tau_trig_statmc_18_rel     = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018', 'Tau Trigger 18 Stat MC (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['18']['statmc_up'],trigger_weights['hh']['tt']['18']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['18']['statmc_down'],trigger_weights['hh']['tt']['18']['nominal']),
                            '1')

sys_tau_trig_syst_15_rel       = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718', 'Tau Trigger 15 Syst (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['15']['syst_up'],trigger_weights['hh']['tt']['15']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['15']['syst_down'],trigger_weights['hh']['tt']['15']['nominal']),
                            '1')

sys_tau_trig_syst_16_rel       = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718', 'Tau Trigger 16 Syst (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['16']['syst_up'],trigger_weights['hh']['tt']['16']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['16']['syst_down'],trigger_weights['hh']['tt']['16']['nominal']),
                            '1')

sys_tau_trig_syst_17_rel       = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718', 'Tau Trigger 17 Syst (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['17']['syst_up'],trigger_weights['hh']['tt']['17']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['17']['syst_down'],trigger_weights['hh']['tt']['17']['nominal']),
                            '1')

sys_tau_trig_syst_18_rel       = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018', 'Tau Trigger 18 Syst (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['18']['syst_up'],trigger_weights['hh']['tt']['18']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['18']['syst_down'],trigger_weights['hh']['tt']['18']['nominal']),
                            '1')

sys_tau_trig_syst_mu_15_rel    = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718', 'Tau Trigger 15 Syst Mu (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['15']['syst_mu_up'],trigger_weights['hh']['tt']['15']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['15']['syst_mu_down'],trigger_weights['hh']['tt']['15']['nominal']),
                            '1')

sys_tau_trig_syst_mu_16_rel    = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718', 'Tau Trigger 16 Syst Mu (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['16']['syst_mu_up'],trigger_weights['hh']['tt']['16']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['16']['syst_mu_down'],trigger_weights['hh']['tt']['16']['nominal']),
                            '1')

sys_tau_trig_syst_mu_17_rel    = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718', 'Tau Trigger 17 Syst Mu (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['17']['syst_mu_up'],trigger_weights['hh']['tt']['17']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['17']['syst_mu_down'],trigger_weights['hh']['tt']['17']['nominal']),
                            '1')

sys_tau_trig_syst_mu_18_rel    = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018', 'Tau Trigger 18 Syst Mu (medium-medium)',
                            '({}/{})'.format(trigger_weights['hh']['tt']['18']['syst_mu_up'],trigger_weights['hh']['tt']['18']['nominal']),
                            '({}/{})'.format(trigger_weights['hh']['tt']['18']['syst_mu_down'],trigger_weights['hh']['tt']['18']['nominal']),
                            '1')

weight_tau_trigger_151617_rel      = DecoratedSystematics(
                        'tau_trigger_151617_weight_rel_systematics',
                        'Tau Trigger 151617 weight rel systematic collection',
                        systSet = SystematicsSet(set([ sys_tau_trig_statdata_15_rel,
                                                       sys_tau_trig_statmc_15_rel,
                                                       sys_tau_trig_syst_15_rel,
                                                       sys_tau_trig_syst_mu_15_rel,
                                                       sys_tau_trig_statdata_16_rel,
                                                       sys_tau_trig_statmc_16_rel,
                                                       sys_tau_trig_syst_16_rel,
                                                       sys_tau_trig_syst_mu_16_rel,
                                                       sys_tau_trig_statdata_17_rel,
                                                       sys_tau_trig_statmc_17_rel,
                                                       sys_tau_trig_syst_17_rel,
                                                       sys_tau_trig_syst_mu_17_rel,
                                                     ])),
                        years = ['15','16','17'],
                        channels = ['1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','1p0n_1pXn','1pXn_1p0n','1p1n_3p0n','3p0n_1p1n'])
SYSTBOOK.append(weight_tau_trigger_151617_rel)

weight_tau_trigger_18_rel      = DecoratedSystematics(
                        'tau_trigger_18_weight_rel_systematics',
                        'Tau Trigger 18  weight rel systematic collection',
                        systSet = SystematicsSet(set([ sys_tau_trig_statdata_18_rel,
                                                       sys_tau_trig_statmc_18_rel,
                                                       sys_tau_trig_syst_18_rel,
                                                       sys_tau_trig_syst_mu_18_rel])),
                        years = ['18'],
                        channels = ['1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','1p0n_1pXn','1pXn_1p0n','1p1n_3p0n','3p0n_1p1n'])
SYSTBOOK.append(weight_tau_trigger_18_rel)




