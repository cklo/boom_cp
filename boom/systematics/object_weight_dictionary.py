scale_factors = {
           'Tau'  : { 
                    'Reco'   : {
                             'nominal'    : 'NOMINAL_TauEffSF_reco', 
                             'total_down' : 'TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco', 
                             'total_up'   : 'TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco', 
                               },
                    'mediumID' : {
                             'nominal'        : 'NOMINAL_TauEffSF_JetRNNmedium',
                             '1p_pt2025_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium',
                             '1p_pt2025_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium',
                             '1p_pt2530_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium',
                             '1p_pt2530_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium',
                             '1p_pt3040_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium',
                             '1p_pt3040_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium',
                             '1p_pt40_down'   : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium',
                             '1p_pt40_up'     : 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium',  
                             '3p_pt2025_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium',
                             '3p_pt2025_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium',
                             '3p_pt2530_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium',
                             '3p_pt2530_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium',
                             '3p_pt3040_down' : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium',
                             '3p_pt3040_up'   : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium',
                             '3p_pt40_down'   : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium',
                             '3p_pt40_up'     : 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium',
                             'syst_down'      : 'TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNmedium',
                             'syst_up'        : 'TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNmedium',
                             'highpt_down'    : 'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNmedium',
                             'highpt_up'      : 'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNmedium',
                               },

                    'TauOLR'    : {
                                   'nominal'         : 'NOMINAL_TauEffSF_HadTauEleOLR_tauhad', 
                                   'truehadtau_down' : 'TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad',
                                   'truehadtau_up'   : 'TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad'
                                  },
                    },
           'Jets' : {
                    'JVT'   : {
                               'nominal'    : 'MC_Norm::check_0_weight(jet_NOMINAL_central_jets_global_effSF_JVT) * MC_Norm::check_0_weight(jet_NOMINAL_central_jets_global_ineffSF_JVT)', 
                               'syst_down'  : 'MC_Norm::check_0_weight(jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT) * MC_Norm::check_0_weight(jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT)',
                               'syst_up'    : 'MC_Norm::check_0_weight(jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT) * MC_Norm::check_0_weight(jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT)'
                              }, 
                    'fJVT'  : {
                               'nominal'    : 'MC_Norm::check_0_weight(jet_NOMINAL_forward_jets_global_effSF_JVT) * MC_Norm::check_0_weight(jet_NOMINAL_forward_jets_global_ineffSF_JVT)', 
                               'syst_down'  : 'MC_Norm::check_0_weight(jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT) * MC_Norm::check_0_weight(jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT)',
                               'syst_up'    : 'MC_Norm::check_0_weight(jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT) * MC_Norm::check_0_weight(jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT)',
                              }, 

	            },
}


