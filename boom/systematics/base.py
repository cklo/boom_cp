from ..cuts import CHANNELS, TRIGGERS, YEARS, CATEGORIES, REGIONS, SYSTEMATICS
from happy.systematics import SystematicsSet, Systematics

# building the syst book
SYSTBOOK = []

class DecoratedSystematics(object):
    def __init__(
        self,
        name,
        title,
        systSet,
        bkg_only=False,
        signal_only=False,
        channels=CHANNELS,
        triggers=TRIGGERS,
        years=YEARS,
        categories=CATEGORIES,
        regions=REGIONS):
        self.name = name
        self.title = title
        self.systSet = systSet
        self.channels = channels
        self.triggers = triggers
        self.years = years
        self.categories = categories
        self.regions = regions   
        self.bkg_only = bkg_only
        self.signal_only = signal_only

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.channels

