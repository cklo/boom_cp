from ..cuts import REGIONS
from .base import DecoratedSystematics, SYSTBOOK
from .object_weight_dictionary import scale_factors
from happy.systematics import SystematicsSet, Systematics

##############################
# relative variation
##############################

sys_taus_reco_total_hh_rel  = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RECO_TOTAL',  'Tau Reco Total',
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['Reco']['total_up'],'tau_1_'+scale_factors['Tau']['Reco']['total_up'],
                                                   'tau_0_'+scale_factors['Tau']['Reco']['nominal'],'tau_1_'+scale_factors['Tau']['Reco']['nominal']),
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['Reco']['total_down'],'tau_1_'+scale_factors['Tau']['Reco']['total_down'],
                                                   'tau_0_'+scale_factors['Tau']['Reco']['nominal'],'tau_1_'+scale_factors['Tau']['Reco']['nominal']),
                        '1')

sys_taus_eleolr_truehadtau_rel  = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL',  'Tau EleOLR True HadTau ',
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['TauOLR']['truehadtau_up'],'tau_1_'+scale_factors['Tau']['TauOLR']['truehadtau_up'],
                                                   'tau_0_'+scale_factors['Tau']['TauOLR']['nominal'],'tau_1_'+scale_factors['Tau']['TauOLR']['nominal']),
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['TauOLR']['truehadtau_down'],'tau_1_'+scale_factors['Tau']['TauOLR']['truehadtau_down'],
                                                   'tau_0_'+scale_factors['Tau']['TauOLR']['nominal'],'tau_1_'+scale_factors['Tau']['TauOLR']['nominal']),
                        '1')

syst_taus_jetid_syst_hh_rel = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_SYST',  'Tau ID (medium-medium) syst',
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['syst_up'],'tau_1_'+scale_factors['Tau']['mediumID']['syst_up'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['syst_down'],'tau_1_'+scale_factors['Tau']['mediumID']['syst_down'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_taus_jetid_highpt_hh_rel = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT',  'Tau ID (medium-medium) highpt',
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['highpt_up'],'tau_1_'+scale_factors['Tau']['mediumID']['highpt_up'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['highpt_down'],'tau_1_'+scale_factors['Tau']['mediumID']['highpt_down'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

############ 1p1p ############

syst_taus_jetid_1p_pt2025_hh_rel  = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025',  'Tau ID (medium-medium) 1p_pt2025',
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2025_up'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2025_up'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2025_down'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2025_down'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_taus_jetid_1p_pt2530_hh_rel   = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530',  'Tau ID (medium-medium) 1p_pt2530',
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2530_up'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2530_up'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2530_down'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2530_down'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_taus_jetid_1p_pt3040_hh_rel   = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040',  'Tau ID (medium-medium) 1p_pt3040',
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt3040_up'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt3040_up'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt3040_down'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt3040_down'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_taus_jetid_1p_pt40_hh_rel   = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40',  'Tau ID (medium-medium) 1p_pt40',
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt40_up'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt40_up'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt40_down'],'tau_1_'+scale_factors['Tau']['mediumID']['1p_pt40_down'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_taus_jetid_syst_hh_rel = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_SYST',  'Tau ID (medium-medium) syst',
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['syst_up'],'tau_1_'+scale_factors['Tau']['mediumID']['syst_up'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['syst_down'],'tau_1_'+scale_factors['Tau']['mediumID']['syst_down'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_taus_jetid_highpt_hh_rel = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT',  'Tau ID (medium-medium) highpt',
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['highpt_up'],'tau_1_'+scale_factors['Tau']['mediumID']['highpt_up'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '(({}*{})/({}*{}))'.format('tau_0_'+scale_factors['Tau']['mediumID']['highpt_down'],'tau_1_'+scale_factors['Tau']['mediumID']['highpt_down'],
                                                   'tau_0_'+scale_factors['Tau']['mediumID']['nominal'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

############ 1p3p ############

syst_lead_tau_jetid_1p_pt2025_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025',  'Tau ID (tight) 1p_pt2025',
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2025_up'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2025_down'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_lead_tau_jetid_1p_pt2530_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530',  'Tau ID (tight) 1p_pt2530',
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2530_up'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt2530_down'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_lead_tau_jetid_1p_pt3040_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040',  'Tau ID (tight) 1p_pt3040',
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt3040_up'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt3040_down'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_lead_tau_jetid_1p_pt40_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40',  'Tau ID (tight) 1p_pt40',
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt40_up'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['1p_pt40_down'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_sublead_tau_jetid_3p_pt2025_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025',  'Tau ID (tight) 3p_pt2025',
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2025_up'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2025_down'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_sublead_tau_jetid_3p_pt2530_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530',  'Tau ID (tight) 3p_pt2530',
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2530_up'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt2530_down'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_sublead_tau_jetid_3p_pt3040_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040',  'Tau ID (tight) 3p_pt3040',
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt3040_up'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt3040_down'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_sublead_tau_jetid_3p_pt40_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40',  'Tau ID (tight) 3p_pt40',
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt40_up'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['3p_pt40_down'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

############ 3p1p ############

syst_lead_tau_jetid_3p_pt2025_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025',  'Tau ID (tight) 3p_pt2025',
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2025_up'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2025_down'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_lead_tau_jetid_3p_pt2530_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530',  'Tau ID (tight) 3p_pt2530',
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2530_up'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt2530_down'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_lead_tau_jetid_3p_pt3040_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040',  'Tau ID (tight) 3p_pt3040',
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt3040_up'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt3040_down'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_lead_tau_jetid_3p_pt40_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40',  'Tau ID (tight) 3p_pt40',
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt40_up'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_0_'+scale_factors['Tau']['mediumID']['3p_pt40_down'],'tau_0_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_sublead_tau_jetid_1p_pt2025_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025',  'Tau ID (tight) 1p_pt2025',
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2025_up'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2025_down'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_sublead_tau_jetid_1p_pt2530_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530',  'Tau ID (tight) 1p_pt2530',
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2530_up'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt2530_down'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_sublead_tau_jetid_1p_pt3040_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040',  'Tau ID (tight) 1p_pt3040',
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt3040_up'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt3040_down'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

syst_sublead_tau_jetid_1p_pt40_hh       = Systematics.weightSystematics( 'TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40',  'Tau ID (tight) 1p_pt40',
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt40_up'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '({}/{})'.format('tau_1_'+scale_factors['Tau']['mediumID']['1p_pt40_down'],'tau_1_'+scale_factors['Tau']['mediumID']['nominal']),
                        '1')

###############################

weight_common_TauSys_hh_rel = DecoratedSystematics(
                  'tau_common_weight_rel_systematics_hh',
                  'Tau common weight rel systematics collection for HH',
                  systSet = SystematicsSet(set([ sys_taus_reco_total_hh_rel,
                                                 sys_taus_eleolr_truehadtau_rel,
                                                 syst_taus_jetid_syst_hh_rel,
                                                 syst_taus_jetid_highpt_hh_rel,
                                               ])),
                  channels=['1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','1p0n_1pXn','1pXn_1p0n','1p1n_3p0n','3p0n_1p1n'])
SYSTBOOK.append(weight_common_TauSys_hh_rel)
                  
weight_common_TauSys_hh_1p1p_rel = DecoratedSystematics(
                  'tau_common_weight_1p1p_rel_systematics_hh',
                  'Tau common weight 1p1p rel systematics collection for HH',
                  systSet = SystematicsSet(set([ 
                                                 syst_taus_jetid_1p_pt2025_hh_rel,
                                                 syst_taus_jetid_1p_pt2530_hh_rel,
                                                 syst_taus_jetid_1p_pt3040_hh_rel,
                                                 syst_taus_jetid_1p_pt40_hh_rel,
                                               ])),
                  channels=['1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','1p0n_1pXn','1pXn_1p0n'])
SYSTBOOK.append(weight_common_TauSys_hh_1p1p_rel)

weight_common_TauSys_hh_1p3p_rel = DecoratedSystematics(
                  'tau_common_weight_1p3p_rel_systematics_hh',
                  'Tau common weight 1p3p rel systematics collection for HH',
                  systSet = SystematicsSet(set([ 
                                                 syst_lead_tau_jetid_1p_pt2025_hh,
                                                 syst_lead_tau_jetid_1p_pt2530_hh,
                                                 syst_lead_tau_jetid_1p_pt3040_hh,
                                                 syst_lead_tau_jetid_1p_pt40_hh,
                                                 syst_sublead_tau_jetid_3p_pt2025_hh,
                                                 syst_sublead_tau_jetid_3p_pt2530_hh,
                                                 syst_sublead_tau_jetid_3p_pt3040_hh,
                                                 syst_sublead_tau_jetid_3p_pt40_hh,
                                               ])),
                  channels=['1p1n_3p0n'])
SYSTBOOK.append(weight_common_TauSys_hh_1p3p_rel)

weight_common_TauSys_hh_3p1p_rel = DecoratedSystematics(
                  'tau_common_weight_3p1p_rel_systematics_hh',
                  'Tau common weight 3p1p rel systematics collection for HH',
                  systSet = SystematicsSet(set([
                                                 syst_sublead_tau_jetid_1p_pt2025_hh,
                                                 syst_sublead_tau_jetid_1p_pt2530_hh,
                                                 syst_sublead_tau_jetid_1p_pt3040_hh,
                                                 syst_sublead_tau_jetid_1p_pt40_hh,
                                                 syst_lead_tau_jetid_3p_pt2025_hh,
                                                 syst_lead_tau_jetid_3p_pt2530_hh,
                                                 syst_lead_tau_jetid_3p_pt3040_hh,
                                                 syst_lead_tau_jetid_3p_pt40_hh,
                                               ])),
                  channels=['3p0n_1p1n'])
SYSTBOOK.append(weight_common_TauSys_hh_3p1p_rel)

