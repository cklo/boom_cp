#LUMINOSITY stored in pb
LUMI = {
    '15': 3219.56,
    '16': 32988.1,
    '17': 44307.4,
    '18': 58450.1,
}
"""dict: report the integrated pp luminosity for each year of LHC Run2"""

#LUMI_SCALE factor
LUMI_SCALE = {
    '15': (3219.56+32988.1)/3219.56,
    '16': (3219.56+32988.1)/32988.1,
    '17': 1,
    '18': 1,
}
"""dict: luminosity scaling factor to be applied to the MC campaigns"""
