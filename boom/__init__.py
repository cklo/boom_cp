"""
BOOM analysis package
"""
import ROOT
ROOT.gROOT.SetBatch(True)

print('')
print('------> BOOM!')
print('')

import random

# importing cpp module (load all cpp helpers)
from . import cpp

# importing mva module (load and configure mva readers)
from . import mva

from happy import scheduler
print('BOOM: activate the HAPPy scheduler')
scheduler.isActive = True


