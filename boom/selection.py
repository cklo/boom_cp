# HAPPy imports
from happy.systematics import SystematicsSet
# local imports
from .cuts import CHANNELS, TRIGGERS, YEARS, CATEGORIES, REGIONS
from .cuts import CUTBOOK
from .fake import get_fake_weights
from .norm import get_mc_norm
from .systematics import SYSTBOOK
from .systematics.theory import *
from .ztt  import get_ztt_weights
from .sgn  import get_sgn_weights 
from .decaymode import get_decaymode_weights
from .rnd  import *
from .altsamp import get_altsamp_weights

ALL_SELECTIONS_DICT = {}
class selection(object):
    """Define a boom selection 

    class to define a given selection. This includes
    a set of cuts and weights to be applied.

    """
    def __init__(self, channel, trigger, category, 
                 region, year, fake_region='anti_tau'):
        """
        Parameters
        __________
        channel : str
           see cuts/__init__.py for the list of possible values
        trigger: str
           see cuts/__init__.py for the list of possible values
        category: str
           see cuts/__init__.py for the list of possible values
        region: str
           see cuts/__init__.py for the list of possible values
        year: str
           see cuts/__init__.py for the list of possible values
        fake_region: str
           see cuts/__init__.py for the list of possible values
        """

        self._channel = channel
        self._trigger = trigger
        self._category = category
        self._region = region
        self._year = year
        self._fake_region = fake_region

        self._name = 'sel__{}__{}__{}__{}__{}'.format(
            self._channel, 
            self._trigger, 
            self._category, 
            self._region, 
            self._year)

        self._cut_list_data = []
        self._cut_list_mc = []
        self._weight_list = []
        self._ignored_cuts_data = []
        self._ignored_cuts_mc = []
        self._apply_decision = {}
        self._build()

    def __str__(self):
        return self._name

    @property
    def channel(self):
        """str: channel name"""
        return self._channel

    @property
    def category(self):
        """str: category name"""
        return self._category

    @property
    def trigger(self):
        """str: trigger name"""
        return self._trigger

    @property
    def region(self):
        """str: region name"""
        return self._region

    @property
    def year(self):
        """str: year name"""
        return self._year

    @property 
    def fake_cr(self):
        """selection: associated fake selection"""
        fake_cr_name = 'sel__{}__{}__{}__{}__{}'.format(
            self._channel, 
            self.trigger, 
            self._category, 
            self._fake_region, 
            self._year)
        if fake_cr_name in list(ALL_SELECTIONS_DICT.keys()):
            return ALL_SELECTIONS_DICT[fake_cr_name]
        else:
            fake_cr = selection(
                self._channel, 
                self._trigger, 
                self._category, 
                self._fake_region, 
                self._year)
            ALL_SELECTIONS_DICT[fake_cr.name] = fake_cr
            return fake_cr
    
    @property
    def name(self):
        """str: name of the selection object"""
        return self._name

    def is_valid(self, dataset):
        """
        Parameters
        ---------
        dataset: HAPPy RootDataset

        Returns
        ------
        : bool
           boolean describing if the selection is valid for the dataset
        """
        # result already encoded, return
        if dataset.name in list(self._apply_decision.keys()):
            return self._apply_decision[dataset.name]

        # figure out if the dataset suits the year
        valid_year = False
        if 'mc16a' in dataset.name:
            if self.year in ('15', '16'):
                valid_year = True
        elif 'mc16d' in dataset.name:
            if self.year == '17':
                valid_year = True
        elif 'mc16e' in dataset.name:
            if self.year == '18':
                valid_year = True 
        elif '_15' in dataset.name:
            if self.year == '15':
                valid_year = True
        elif '_16' in dataset.name:
            if self.year == '16':
                valid_year = True
        elif '_17' in dataset.name:
            if self.year == '17':
                valid_year = True
        elif '_18' in dataset.name:
            if self.year == '18':
                valid_year = True
        else:
            raise ValueError('cannot figure out data taking year or mc campaign for {}'.format(dataset.name))

        # figure out if the dataset suits the channel / region
        valid_stream = False
        if '_hh' in dataset.name:
            if self._channel in ('1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','1p0n_1pXn','1pXn_1p0n','1p1n_3p0n','3p0n_1p1n'):
                valid_stream = True
        else:
            raise ValueError('cannot figure out stream for {}'.format(dataset.name))

        # if it fits year, channel and region returns True
        if valid_year and valid_stream:
            self._apply_decision[dataset.name] = True
            return True
        else:
            self._apply_decision[dataset.name] = False
            return False


    def _build(self, verbose=False):
        cut_list = _get_cut_list(
            self._channel, 
            self._trigger,
            self._year, 
            self._category, 
            self._region, 
            verbose=verbose)

        self._cut_list_data = list([c for c in cut_list if c.mc_only == False])
        self._cut_list_mc = list([c for c in cut_list if c.data_only == False])

        self._weight_list = _get_weight_list(
            self._channel, 
            self._trigger,
            self._year, 
            self._category, 
            self._region, 
            verbose=verbose)
        
        self._ignored_weights_signal = _get_list_of_ignored_weights(
            self._weight_list, is_signal=True)

        self._ignored_weights_bkg = _get_list_of_ignored_weights(
            self._weight_list, is_signal=False)
        
    def get_ignored_cuts(self, is_data):
        return []
    
    def get_ignored_weights(self, is_signal):
        """Retrieve list of weights to ignore

        Parameters
        __________   
        is_signal: bool
           True if dataset is signal, False otherwise
        
        Returns
        _______
        filtered list of weights
        """
        if is_signal:
            return self._ignored_weights_signal
        else:
            return self._ignored_weights_bkg       

    def get_cut(self, is_data):
        """
        Parameters
        __________
        is_data : bool

        Returns
        _______
        _cut : HAPPy Cut
           the final cut to apply for this selection object 
           (AND of all the cuts)
        """
        if is_data:
            _cut_list = self._cut_list_data
        else:
            _cut_list = self._cut_list_mc

        _cut = _cut_list[0].cut
        for c in _cut_list[1:]:
            _cut = _cut.AND(c.cut)
        return _cut 

    def get_weight(self, process_name, is_data, is_signal):
        """
        Parameters
        __________
        process_name : str

        is_data : bool

        is_signal : bool

        Returns
        _______
        sys_set : HAPPy SystematicsSet
           the final set of weights to apply for this selection object (product of all the weights)
        """
        if is_data and process_name != 'Fake':
            return None

        sys_set = SystematicsSet()
        # Add MC weights
        if not is_data:
            for w in self._weight_list:
                sys_set |= w.systSet

            decaymode_weights = get_decaymode_weights(self._channel)
            sys_set |= decaymode_weights

        if 'FxFx' not in process_name and 'He7' not in process_name:
            theory_sys_set = _theory_weights(process_name, self._channel)
            sys_set |= theory_sys_set 

        alt_sample_sys_set = get_altsamp_weights() 
        sys_set |= alt_sample_sys_set

        if process_name == 'Fake':
            fake_weights = get_fake_weights(
                self._channel, self._category, 
                self._year, self._region, self._trigger, is_data)
            sys_set |= fake_weights

        if not is_data:
            for systSet in self.get_ignored_weights(is_signal):
                for w in systSet:
                    sys_set.remove(w) 

        if not is_data:
            mc_norm = get_mc_norm(self._channel, self._region)
            sys_set |= mc_norm

        return sys_set



def _get_cut_list(channel, trigger, year, 
                  category, region, verbose=False):
    """Helper function to filter the CUTBOOK 
    (see cuts module) for a given selection

    Parameters
    __________
    channel : str
       see CHANNELS in cuts/__init__.py

    trigger : str
       see TRIGGERS in cuts/__init__.py

    year: str
       see YEARS in cuts/__init__.py

    category : str
       see CATEGORIES in cuts/__init__.py

    region: str or list(str) 
       see REGIONS in cuts/__init__.py

    verbose : bool
       print-outs, default = False

    Returns
    _______
    cut_list : list(DecoratedCut)
       list of DecoratedCut objects
    """
    if not channel in CHANNELS:
        raise ValueError('get_cut: {} not in {}'.format(
                channel, CHANNELS))

    if not trigger in TRIGGERS:
        raise ValueError('get_cut: {} not in {}'.format(
                trigger, TRIGGERS))

    if not year in YEARS:
        raise ValueError('get_cut: {} not in {}'.format(
                year, YEARS))

    if not category in CATEGORIES:
        raise ValueError('get_cut: {} not in {}'.format(
                category, CATEGORIES))

    if not region in REGIONS:
        raise ValueError('get_cut: {} not in {}'.format(
                region, REGIONS))

    # channel filtering
    cut_list = list([t for t in CUTBOOK if channel in t.channels])
    # trigger filtering
    cut_list = list([t for t in cut_list if trigger in t.triggers])
    # year filtering
    cut_list = list([t for t in cut_list if year in t.years])
    # category filtering
    cut_list = list([t for t in cut_list if category in t.categories])
    # region filtering
    cut_list = list([t for t in cut_list if region in t.regions])

    if verbose:
        print(cut_list)
    return list(cut_list)

def _get_weight_list(channel, trigger, year, category, 
                    region, verbose=False):
    """Helper function to filter the SYSTBOOK 
    (see systematics module) for a given selection

    Arguments
    _________
    channel : str
       see CHANNELS in cuts/__init__.py

    trigger : str
       see TRIGGERS in cuts/__init__.py

    year: str
       see YEARS in cuts/__init__.py

    category : str
       see CATEGORIES in cuts/__init__.py

    region: str or list(str) 
       see REGIONS in cuts/__init__.py

    verbose : bool
       print-outs, default = False
    """
    if not channel in CHANNELS:
        raise ValueError('get_weight: {} not in {}'.format(
                channel, CHANNELS))

    if not trigger in TRIGGERS:
        raise ValueError('get_weight: {} not in {}'.format(
                trigger, TRIGGERS))

    if not year in YEARS:
        raise ValueError('get_weight: {} not in {}'.format(
                year, YEARS))

    if not category in CATEGORIES:
        raise ValueError('get_weight: {} not in {}'.format(
                category, CATEGORIES))

    if not region in REGIONS:
        raise ValueError('get_weight: {} not in {}'.format(
                region, REGIONS))

    # channel filtering
    weight_list = list([t for t in SYSTBOOK if channel in t.channels])
    # trigger filtering
    weight_list = list([t for t in weight_list if trigger in t.triggers])
    # year filtering
    weight_list = list([t for t in weight_list if year in t.years])
    # category filtering
    weight_list = list([t for t in weight_list if category in t.categories])
    # region filtering
    weight_list = list([t for t in weight_list if region in t.regions])

    if verbose:
        print(weight_list)
    return weight_list


def _get_list_of_ignored_cuts(cut_list, is_data=False):
    if is_data:
        ignored_cuts = list([c for c in cut_list if c.mc_only == True])

    else:
        ignored_cuts = list([c for c in cut_list if c.data_only == True])

    ignored_cuts = [c.cut for c in ignored_cuts]
    return ignored_cuts

def _get_list_of_ignored_weights(weight_list, is_signal=False):
  
    if is_signal:
        ignored_weights = [w for w in weight_list if w.bkg_only == True]#list(filter(lambda w : w.bkg_only == True, weight_list))

    else:
        ignored_weights = [w for w in weight_list if w.signal_only == True]#list(filter(lambda w : w.signal_only == True, weight_list))

    ignored_weights = [w.systSet for w in ignored_weights]
    return ignored_weights

def _theory_weights(process_name, channel):

    sys_set = SystematicsSet()
    if 'ZttQCD' in process_name or 'ZllQCD' in process_name:
        # adding theory variations from external input (ckkw, qsf) 
        add_weights = get_ztt_weights(channel)
        sys_set |= add_weights
        #  adding theory variations from ntuples (mur-muf, alphaS)
        sys_set |= weight_z_theory_syst.systSet
    elif 'ggH' in process_name:
        add_weights = get_sgn_weights(process_name)
        sys_set |= add_weights
        sys_set |= weight_ggh_theory_syst.systSet
        sys_set |= weight_signal_theory_syst.systSet 
    elif 'VBFH' in process_name:
        add_weights = get_sgn_weights(process_name)
        sys_set |= add_weights
        sys_set |= weight_vbf_theory_syst.systSet
        sys_set |= weight_signal_theory_syst.systSet
    elif 'ZH' in process_name or 'WH' in process_name:
        add_weights = get_sgn_weights(process_name)
        sys_set |= add_weights
        sys_set |= weight_vh_theory_syst.systSet
        sys_set |= weight_signal_theory_syst.systSet   
    elif 'ttH' in process_name:
        add_weights = get_sgn_weights(process_name)
        sys_set |= add_weights
        sys_set |= weight_tth_theory_syst.systSet
        sys_set |= weight_signal_theory_syst.systSet

    return sys_set      

