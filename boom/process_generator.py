"""
Database generation from the list of ntuples
"""
# happy imports
from happy.dataset import RootDataset, CombinedDataset

class process_generator(object):
    '''Class which auto-generates HAPPY.PhysicsProcesses from basic information'''

    def __init__(
        self, path, 
        ):

        # Internal dictionary containing physics processes
        self._processes = []

        # Local path for datasets
        self._path = path

        # Channel name book-keeping
        self._short_streams = {
                'hadhad': 'hh'}

        self._process_info = {}

    def process_info(self, process):
        try:
            return self._process_info[process.name]
        except KeyError:
            raise NotImplementedError('process %s does not exist' % process.name)

    @property
    def path(self):
        return self._path

    @property
    def processes(self):
        """return the generated list of processes"""
        return self._processes
    

    def generate_mc(
        self, name, title, dsids, 
        isSignal=False, 
        campaigns=['mc16a', 'mc16d', 'mc16e'], 
        style=None, 
        ignoreChannel=[],
        weightExpression='1',
        prefix=None,
        ntuples_block='nom'):
        """
        """
        if name in list(self._process_info.keys()):
            raise RuntimeError('process with name %s already created' % name)

        self._process_info[name] = {
            'campaigns': [],
            'dsids': dsids,
            'streams': []
            }

        # internal list of datasets for process creation
        _dataset_list = []

        for _dsid in dsids:
            for _campaign in campaigns:
                for _stream in list(self._short_streams.keys()):
                    _skip = False
                 
                    for entry in ignoreChannel:
                        if _stream == entry[0] and _dsid not in entry[1]:
                            _skip = True
                    
                    if _skip == True:
                        continue

                    if not _campaign in self._process_info[name]['campaigns']:
                        self._process_info[name]['campaigns'].append(_campaign)

                    if not self._short_streams[_stream] in self._process_info[name]['streams']:
                        self._process_info[name]['streams'].append(self._short_streams[_stream])
  
                    file_path = self._path       
                    # set the path to the files : HACK because some files need to be reproduced due to lack of phi* info
                    if _dsid in (344779, 364199, 410155) and _campaign == 'mc16a':
                        file_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/SM_Htautau_R21/HiggsCP/V03/" 
                    elif _dsid == 410155 and _campaign == 'mc16d':
                        file_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/SM_Htautau_R21/HiggsCP/V03/"
                    elif _dsid in (364101, 364198, 410155) and _campaign == 'mc16e':
                        file_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/SM_Htautau_R21/HiggsCP/V03/"
                    elif _dsid in (361108,):
                        file_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/SM_Htautau_R21/HiggsCP/Test"
                    #elif _dsid in (346564, 346573, 346909, 346912, 346915, 346919, 346923, 346927) and 'nom' in ntuples_block:
                    #    file_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/SM_Htautau_R21/HiggsCP/Test"                 
          
                    if 'sys_geo1' in ntuples_block or 'sys_geo2' in ntuples_block or 'sys_geo3' in ntuples_block or 'sys_physlist' in ntuples_block:
                        file_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/SM_Htautau_R21/HiggsCP/AlternativeSamples/" 

                    # Generate filename & dataset name
                    _files = "".join((file_path, "/mc/", _stream, "/" + _campaign + "/" + ntuples_block + "/*", str(_dsid), "*/*.root*"))
 
                    _name = _campaign + "_" + str(_dsid) + "_" + self._short_streams[_stream]

                    if prefix != None:
                        _name = prefix + "_" + _name

                    # additional requirement due to xCompression applied only in kin syst ntuples          
                    if ntuples_block != 'nom' and ntuples_block != 'sys_afii' and ntuples_block != 'sys_geo1' and ntuples_block != 'sys_geo2' and ntuples_block != 'sys_geo3' and ntuples_block != 'sys_physlist':
                        # xCompression not available in CP odd/unpolarised samples
                        if _dsid not in (346564, 346573, 346562, 346571, 346909, 346912, 346915, 346919, 346923, 346927):                   
                            weightExpression += ' *(useEvent==1)'

              

                    # Create dataset
                    _dataset_list += [RootDataset(
                            _name, 
                            fileNames=[_files], 
                            weightExpression=weightExpression, 
                            isSignal=isSignal)]

                    # Auto-set cross section information
                    _dataset_list[-1].dsid = _dsid

        self._processes += [CombinedDataset(
                name, title, 
                style=style, 
                datasets=_dataset_list, 
                isSignal=isSignal)]

    def generate_data(
        self, 
        name="Data", 
        title="Data", 
        years=['15', '16', '17', '18'], 
        style=None, 
        prefix=None):
        """
        """
        if name in list(self._process_info.keys()):
            raise RuntimeError('process with name %s already created' % name)

        self._process_info[name] = {
            'years': years,
            }

        # internal list of datasets for process creation        
        _dataset_list = []

        for _year in years:
            for _stream in list(self._short_streams.keys()):

                # Generate filename & dataset name
                _files = "".join((self._path, "/data/", _stream, "/data", str(_year), "/*/*.root*"))
                _name = "data_" + str(_year) + "_" + self._short_streams[_stream]

                if prefix != None:
                    _name = prefix + "_" + _name

                # Create dataset
                _dataset_list += [RootDataset(_name, fileNames=[_files], isData=True)]

        self._processes += [CombinedDataset(name, title, style=style, datasets=_dataset_list, isData=True)]
