# python / ROOT imports
import os
import ROOT

# local imports
from . import scheduler
from .lumi import LUMI, LUMI_SCALE
from .selection_utils import print_selection
from .utils import hist_sum
from .systematics import kinematics
from .fake import hh_fake_systs  

def declare_stores(physics_processes, recreate=False):
    """Loop over the processes and create a ROOT.TFile to cache histograms

    Parameters
    __________
    physics_processes : list
       HAPPy combined datasets
    """
    from happy.histogramStore import HistogramStore, MODES
    for p in physics_processes:
        for d in p.datasets:
            if p.isSignal:
                store_name = 'cache/store_{}_{}.root'.format(p.name, d.name)
            else:
                store_name = 'cache/store_{}.root'.format(d.name)

            if not os.path.exists(store_name) or recreate:
                ROOT.TFile.Open(store_name, 'recreate')
            d.histogramStore = HistogramStore(store_name)
#             d.histogramStore.mode = MODES.NAME

def close_stores(physics_processes):
    """close the ROOT.TFile cache for each dataset

    Parameters
    __________
    physics_processes : list
       HAPPy combined datasets
    """
    for p in physics_processes:
        for d in p.datasets:
            d.histogramStore.close()

def configure_kinematic_variations(process):
    """
    Parameters
    __________
    process: HAPPy combined dataset
    """
    if process.isData:
        pass
    else:
        for dataset in process.datasets:
            dataset.systematicsSet = kinematics

                
DEFAULT_PROCESS_MERGING_SCHEME = {
    'Signal_SM'  : [('ggH', 'VBFH', 'WH', 'ZH', 'ttH'), 'Signal #phi_{#tau} = 0^{o}'],
    'Signal_CPodd' : [('ggH_CPodd', 'VBFH_CPodd'), 'Signal #phi_{#tau} = 90^{o}'],
    'Signal_unpol_SM' : [('ggH_theta_0', 'VBFH_theta_0', 'WH_theta_0', 'ZH_theta_0', 'ttH_theta_0'), 'Signal #phi_{#tau} = 0^{o}'],
    'Signal_unpol_CPodd' : [('ggH_theta_90', 'VBFH_theta_90', 'WH_theta_90', 'ZH_theta_90', 'ttH_theta_90'), 'Signal #phi_{#tau} = 90^{o}'],
    'Ztt'   : [('ZttQCD', 'ZttEWK'), 'Z#rightarrow#tau#tau'],
    'Others_Bkg': [('Top','W','VV','ZllQCD', 'ZllEWK','VBFHWW', 'ggHWW'),  'Other Bkg'],
}

class boom_processor(object):
    """Processor

    This is the main class of boom.
    It uses the HAPPy scheduler functionality to 
    book histograms and fill them according to
    pre-defined set of selections (see selection.py)

    """
    def __init__(
        self,
        physics_processes,
        selections,
        variables,
        variables2=[],
        systematic_names=None, 
        do_fake_mc_subtraction=True,
        force_fake_mc_subtraction=False,
        verbose=False,
        recreate_stores=False):
        """

        Parameters
        __________
        physics_processes : list
            HAPPy combined datasets
        selections : list(selection)
            single instance or list of boom.selection objects
        variables : list(happy.variable.Variable)
            single instance or list of HAPPy Variable objects
        systematic_names: str
            default=None, systematic variations to consider
        do_fake_mc_subtraction : bool
           default=True, build MC subtraction part of fakes
        force_fake_mc_subtraction : bool
           default=False, force to compute the mc subtraction 
           (useful for batch submission)
        recreate_stores : bool
           default=False, if turned to true ROOT file for stores are recreated

        """
        self._physics_processes = physics_processes
        self._physics_process_names = [p.name for p in self._physics_processes]
        self._recreate_stores = recreate_stores
        # declare stores
        self._declare_stores()

        # processes that are not data and not signal
        self._mc_backgrounds = list([p for p in self._physics_processes if p.isData == False and (p.isSignal == False or 'HWW' in p.name)])
        self._process_merging_scheme = DEFAULT_PROCESS_MERGING_SCHEME

        # configure the processes to our needs
        for process in self._physics_processes:
            configure_kinematic_variations(process)
        #
        if not isinstance(selections, (list, tuple)):
            self._selections = [selections]
        else:
            self._selections = selections
        #
        if not isinstance(variables, (list, tuple)):
            self._variables = [variables]
        else:
            self._variables = variables

        if not isinstance(variables2, (list, tuple)):
            self._variables2 = [variables2]
        else:
            self._variables2 = variables2

        
        if systematic_names == None:
            self._systematic_names = None
        else:
            if not isinstance(systematic_names, (list, tuple)):
                self._systematic_names = [systematic_names]
            else:
                self._systematic_names = systematic_names


        self._do_fake_mc_subtraction = do_fake_mc_subtraction
        if not 'Fake' in self._physics_process_names:
            self._do_fake_mc_subtraction = False
        
        if force_fake_mc_subtraction:
            self._do_fake_mc_subtraction = True
            print('BOOM: fake MC subtraction calculation enforced')
 
        if self._do_fake_mc_subtraction and not 'Fake' in self._physics_process_names:
            if len(self.mc_backgrounds) == 0:
                raise ValueError('No MC background to subtract!')
   
        print('BOOM: Physics processes considered')
        print('BOOM: ----------------------------')
        for p in self._physics_processes:
            print('BOOM: \t', p.name)
        print()

        print('BOOM: Selection criteria considered')
        print('BOOM: -----------------------------')
        for sel in self._selections:
            if verbose:
                print_selection(sel)
            else:
                print('BOOM: \t', sel.name)
        print()

        print('BOOM: Variables considered (X axis)')
        print('BOOM: --------------------')
        for var in self._variables:
            print('BOOM: \t', var.name, ":", var.command)
            var.binning.includeUnderflowBin = True
            var.binning.includeOverflowBin  = True
        print()

        print('BOOM: Variables considered (Y axis)')
        print('BOOM: --------------------')
        for var2 in self._variables2:
            print('BOOM: \t', var2.name, ":", var2.command)
            var2.binning.includeUnderflowBin = True
            var2.binning.includeOverflowBin  = True
        print()

        print('BOOM: Systematic Variations considered')
        print('BOOM: --------------------------------')
        if self._systematic_names == None:
            print('BOOM: \t None')
        else:
            for _sys_name in self._systematic_names:
                print('BOOM: \t', _sys_name)
        print()


    def _declare_stores(self):
        declare_stores(self._physics_processes, recreate=self._recreate_stores)

    def _build_syst_object(self, dataset, systematics_set, syst_name, syst_type):

        _kine_variation = dataset.systematicsSet.getSystematic(syst_name)
        if _kine_variation != None:
            if syst_type == 'up':
                return _kine_variation.up
            elif syst_type == 'down':
                return _kine_variation.down
            else:
                raise ValueError
                
        if systematics_set == None:
            return None
        
        if syst_name == None:
            return None

        _systematic_variation = systematics_set.getSystematic(syst_name)
        if _systematic_variation == None:
            return None

        if syst_type == 'up':
            _syst = _systematic_variation.up
        elif syst_type == 'down':
            _syst = _systematic_variation.down
        else:
            raise ValueError

        return _syst

    @property
    def systematic_names(self):
        """systematic variation used for the run"""
        return self._systematic_names

    @property
    def physics_processes(self):
        """List of physics processes"""
        return self._physics_processes

    @property
    def merged_physics_process_names(self):
        """List of merged physics process names"""
        # list of all the process names
        _process_names = [p.name for p in self._physics_processes]

        # iterate on the merging scheme and replace the grouped processes 
        # by the group name

        for process_name, sub_process_names in list(self._process_merging_scheme.items()):
            index = -1
            for n in sub_process_names[0]:
                if n in _process_names:
                    index = _process_names.index(n)
                    _process_names.remove(n)
            _process_names.insert(index, process_name)
        return _process_names
        

    @property
    def mc_backgrounds(self):
        """List the processes qualifying as MC background"""
        return self._mc_backgrounds

    def get_physics_process(self, name):
        """
        Parameters
        __________
        name : str
           name of the process to retrieve

        Returns
        _______
        processes : list
           list of HAPPy processes as defined in the database
        """
        processes = list([p for p in self._physics_processes if p.name == name])
        if len(processes) == 0:
            if name in list(self._process_merging_scheme.keys()):
                processes = list([p for p in self._physics_processes if p.name in self._process_merging_scheme[name][0]])
                print("length of processes: ", len(processes))
                print("length of process merging scheme: ", len(self._process_merging_scheme[name][0]))
                if len(processes) != len(self._process_merging_scheme[name][0]):
                    raise ValueError('Some of these processes are not defined: {}'.format(
                            self._process_merging_scheme[name][0]))
                else:
                    return processes
        elif len(processes) == 1:
            if name in list(self._process_merging_scheme.keys()):
                raise ValueError('{} is ambiguous (a process or a list of processes?)'.format(name))
            return processes
        else:
            raise ValueError('{} is not a valid process name'.format(name))

    def get_hist_fake(self, fake, selections, variable, syst_name=None, syst_type='up'):
        """
        Parameters
        __________
        fake: HAPPy CommbinedDataset
           HAPPy process with isData == True
        selections: list(selection)
           a list of boom.selection object
        variable: HAPPy variable

        Returns
        -------
        hist: ROOT.TH1F 
           histogram representing the Fake template
        """
        _hists = []
        if fake != None:
            for dataset in fake.datasets:
                for sel in selections:
                    if sel.is_valid(dataset):
                        sel = sel.fake_cr
                        dataset.style = fake.style
                        dataset.title = fake.title
                        selection_cut = sel.get_cut(dataset.isData)
                        systematics_set = sel.get_weight(fake.name, dataset.isData, fake.isSignal)
                        _syst = self._build_syst_object(dataset, systematics_set, syst_name, syst_type)
                        h = dataset.getHistogram(
                            variable, 
                            cut=selection_cut, 
                            systematicVariation=_syst,
                            systematicsSet=systematics_set)
                        _hists.append(h)
        else:
            print('BOOM: will only build fake MC subtraction!')

        if self._do_fake_mc_subtraction:
            for process in self.mc_backgrounds:
                for dataset in process.datasets:
                    for sel in selections:
                        sel = sel.fake_cr
                        if sel.is_valid(dataset):
                            selection_cut = sel.get_cut(dataset.isData)
                            systematics_set = sel.get_weight('Fake', dataset.isData, process.isSignal)
                            _syst = self._build_syst_object(dataset, systematics_set, syst_name, syst_type)
                            h = dataset.getHistogram(
                                variable, 
                                cut=selection_cut,
                                systematicVariation=_syst,
                                systematicsSet=systematics_set)
                            h.Scale(float(LUMI[sel.year]) * float(LUMI_SCALE[sel.year]))
                            _hists.append(h)
        return hist_sum(_hists)

    def get_hist_fake_2D(self, fake, selections, variable, variable2, syst_name=None, syst_type='up'):
        """
        """
        _hists = []
        if fake != None:
            for dataset in fake.datasets:
                for sel in selections:
                    if sel.is_valid(dataset):
                        sel = sel.fake_cr
                        dataset.style = fake.style
                        dataset.title = fake.title
                        selection_cut = sel.get_cut(dataset.isData)
                        systematics_set = sel.get_weight(fake.name, dataset.isData, fake.isSignal)
                        _syst = self._build_syst_object(dataset, systematics_set, syst_name, syst_type)
                        h = dataset.getHistogram2D(
                            variable,
                            variable2,
                            cut=selection_cut, 
                            systematicVariation=_syst,
                            systematicsSet=systematics_set)
                        _hists.append(h)
        else:
            print('BOOM: will only build fake MC subtraction!')

        if self._do_fake_mc_subtraction:
            for process in self.mc_backgrounds:
                for dataset in process.datasets:
                    for sel in selections:
                        sel = sel.fake_cr
                        if sel.is_valid(dataset):
                            selection_cut = sel.get_cut(dataset.isData)
                            systematics_set = sel.get_weight('Fake', dataset.isData, process.isSignal)
                            _syst = self._build_syst_object(dataset, systematics_set, syst_name, syst_type)
                            h = dataset.getHistogram2D(
                                variable,
                                variable2, 
                                cut=selection_cut,
                                systematicVariation=_syst,
                                systematicsSet=systematics_set)
                            h.Scale(float(LUMI[sel.year] * LUMI_SCALE[sel.year]))
                            _hists.append(h)
        return hist_sum(_hists)
                

    def get_hist_physics_process(self, process_name, selections, variable, syst_name=None, syst_type='up', lumi_scale=1):
        """
        Parameters
        ----------
        process_name: str 
           name of a HAPPy combined dataset
        selections: list(selection)
           list of selections to retrieve
        variable: HAPPy variable
           variable to plot
        lumi_scale: float
           arbitrary scaling of the histogram

        Returns
        -------
        hist: ROOT.TH1F 
           histogram representing the process
        """
        if not isinstance(selections, (tuple, list)):
            selections = [selections]

        if not variable in self._variables:
            raise RuntimeError('{} was not included in the scheduler run'.format(variable))

        # if we pass the Fake_subtraction string, use a None arg for get_hist_fake
        if process_name == 'Fake_subtraction':
            return self.get_hist_fake(None, selections, variable, syst_name=syst_name, syst_type=syst_type)

        process = self.get_physics_process(process_name)
        if len(process) == 1 and process[0].name == 'Fake':
            return self.get_hist_fake(process[0], selections, variable, syst_name=syst_name, syst_type=syst_type)


        _hists = []
        # loop over HAPPy processes 
        # (most cases process is a list of size 1)

        for p in process:
          
            # change style and titlte of the process in case of merged processes 
            p.style = process[0].style
            for process_name, sub_process_names in list(self._process_merging_scheme.items()):
                if p.name in sub_process_names[0]:
                    p.title = sub_process_names[1]
            
            for sel in selections:
                selection_cut = sel.get_cut(p.isData)
                systematics_set = sel.get_weight(p.name, p.isData, p.isSignal)
                recalculate_sel = False

                for dataset in p.datasets:
                    if sel.is_valid(dataset):
                        dataset.style = p.style
                        dataset.title = p.title
                        if p.isData != dataset.isData or recalculate_sel == True:
                            print('ERROR! Process datasets are mix of MC and data!')
                            recalculate_sel = True
                            selection_cut = sel.get_cut(dataset.isData)
                            systematics_set = sel.get_weight(p.name, dataset.isData, p.isSignal)
                        _syst = self._build_syst_object(dataset, systematics_set, syst_name, syst_type)
                        # retrieve histogram
                        h = dataset.getHistogram(
                            variable, 
                            cut=selection_cut,
                            systematicVariation=_syst,
                            systematicsSet=systematics_set)
                        if not dataset.isData:
                            h.Scale(float(LUMI[sel.year] * LUMI_SCALE[sel.year] * lumi_scale))
                        _hists.append(h)
        return hist_sum(_hists)


    def get_hist_physics_process_2D(self, process_name, selections, variable, variable2, syst_name=None, syst_type='up', lumi_scale=1):
        """
        """
        if not isinstance(selections, (tuple, list)):
            selections = [selections]

        if not variable in self._variables:
            raise RuntimeError('{} was not included in the scheduler run'.format(variable))

        if not variable2 in self._variables2:
            raise RuntimeError('{} was not included in the scheduler run'.format(variable2))

        # if we pass the Fake_subtraction string, use a None arg for get_hist_fake
        if process_name == 'Fake_subtraction':
            return self.get_hist_fake_2D(None, selections, variable, variable2, syst_name=syst_name, syst_type=syst_type)

        process = self.get_physics_process(process_name)

        if len(process) == 1 and process[0].name == 'Fake':
            return self.get_hist_fake_2D(process[0], selections, variable, variable2, syst_name=syst_name, syst_type=syst_type)

        _hists = []
        # loop over HAPPy processes                                                                                                                                                                     
        # (most cases process is a list of size 1)

        for p in process:

            # change style and titlte of the process in case of merged processes 
            p.style = process[0].style
            for process_name, sub_process_names in list(self._process_merging_scheme.items()):
                if p.name in sub_process_names[0]:
                    p.title = sub_process_names[1]

            for sel in selections:
                selection_cut = sel.get_cut(p.isData)
                systematics_set = sel.get_weight(p.name, p.isData, p.isSignal)
                recalculate_sel = False
                
                for dataset in p.datasets:
                    if sel.is_valid(dataset):
                        dataset.style = p.style
                        dataset.title = p.title
                        if p.isData != dataset.isData or recalculate_sel == True:
                            print('ERROR! Process datasets are mix of MC and data!')
                            recalculate_sel = True
                            selection_cut = sel.get_cut(dataset.isData)
                            systematics_set = sel.get_weight(p.name, dataset.isData, p.isSignal)
                        #TODO: _syst object never used here, remove???
                        _syst = self._build_syst_object(dataset, systematics_set, syst_name, syst_type)
                        # retrieve histogram
                        h = dataset.getHistogram2D(
                            variable,
                            variable2,
                            cut=selection_cut,
                            systematicsSet=systematics_set)
                        if not dataset.isData:
                            h.Scale(float(LUMI[sel.year] * LUMI_SCALE[sel.year] * lumi_scale))
                        _hists.append(h)
        return hist_sum(_hists)


    def book(self):
        """Booking histogram requests"""

        print('BOOM: booking histograms')
        stop_watch = ROOT.TStopwatch()
        stop_watch.Start()
        n_requests = 0
        for process in self._physics_processes:
            print('BOOM: \t booking requests for {}'.format(process.name))
            n_requests_per_process = 0
            for sel in self._selections:
                if process.name == 'Fake':
                    sel = sel.fake_cr
                selection_cut = sel.get_cut(process.isData)
                systematics_set = sel.get_weight(process.name, process.isData, process.isSignal)
                recalculate_sel = False

                for dataset in process.datasets:
                    if sel.is_valid(dataset):
                        if process.isData != dataset.isData or recalculate_sel == True:
                            print('ERROR! Process datasets are mix of MC and data!')
                            recalculate_sel = True
                            selection_cut = sel.get_cut(dataset.isData)
                            systematics_set = sel.get_weight(process.name, dataset.isData, process.isSignal)
                        
                        for variable in self._variables:
                            n_requests += 1
                            n_requests_per_process += 1
                            dataset.getHistogram(
                                variable,
                                cut=selection_cut,
                                systematicVariation=None,
                                systematicsSet=systematics_set)
                            # register systematic variations
                            if self._systematic_names != None:
                                if process.isData == False or process.name == 'Fake':
                                    for _syst_name in self._systematic_names:
                                        if process.name != 'Fake' and _syst_name in hh_fake_systs :
                                            continue
                                        # retrieve Systematic objects
                                        n_requests += 2
                                        n_requests_per_process += 2
                                        _syst_up = self._build_syst_object(dataset, systematics_set, _syst_name, 'up')
                                        _syst_down = self._build_syst_object(dataset, systematics_set, _syst_name, 'down')
                                        dataset.getHistogram(
                                            variable,
                                            cut=selection_cut,
                                            systematicVariation=_syst_up,
                                            systematicsSet=systematics_set)
                                        dataset.getHistogram(
                                            variable,
                                            cut=selection_cut,
                                            systematicVariation=_syst_down,
                                            systematicsSet=systematics_set)

                            # perform mc subtraction
                            if self._do_fake_mc_subtraction and process in self.mc_backgrounds:
                                n_requests += 1
                                n_requests_per_process += 1
                                selection_fake_cr_cut = sel.fake_cr.get_cut(dataset.isData)
                                systematics_fake_cr_set = sel.fake_cr.get_weight('Fake', False, process.isSignal)
                                dataset.getHistogram(
                                    variable,
                                    cut=selection_fake_cr_cut,
                                    systematicVariation=None,
                                    systematicsSet=systematics_fake_cr_set)
                                if self._systematic_names != None:
                                    # register systematic variations
                                    for _syst_name in self._systematic_names:
                                        n_requests += 2
                                        n_requests_per_process += 2
                                        _syst_up = self._build_syst_object(dataset, systematics_fake_cr_set, _syst_name, 'up')
                                        _syst_down = self._build_syst_object(dataset, systematics_fake_cr_set, _syst_name, 'down')
                                        dataset.getHistogram(
                                            variable,
                                            cut=selection_fake_cr_cut,
                                            systematicVariation=_syst_up,
                                            systematicsSet=systematics_fake_cr_set)
                                        dataset.getHistogram(
                                            variable,
                                            cut=selection_fake_cr_cut,
                                            systematicVariation=_syst_down,
                                            systematicsSet=systematics_fake_cr_set)

            print('BOOM: \t\t {}: {} histogram requests'.format(process.name, n_requests_per_process))
        print('BOOM: Total: {} histogram requests'.format(n_requests))
        stop_watch.Stop()
        stop_watch.Print()

    def book_2D(self):
        """Booking histogram requests"""

        print('BOOM: booking histograms')
        stop_watch = ROOT.TStopwatch()
        n_requests = 0
        for process in self._physics_processes:
            print('BOOM: \t booking requests for {}'.format(process.name))
            n_requests_per_process = 0
            for dataset in process.datasets:
                for sel in self._selections:
                    if process.name == 'Fake':
                        sel = sel.fake_cr
                    if sel.is_valid(dataset):
                        for variable in self._variables:
                            for variable2 in self._variables2:
                                n_requests += 1
                                n_requests_per_process += 1
                                systematics_set = sel.get_weight(
                                    process.name, dataset.isData, process.isSignal)
                                dataset.getHistogram2D(
                                    variable,
                                    variable2,
                                    cut=sel.get_cut(dataset.isData),
                                    systematicVariation=None,
                                    systematicsSet=systematics_set)
                                # register systematic variations
                                if self._systematic_names != None:
                                    if process.isData == False or process.name == 'Fake':
                                        for _syst_name in self._systematic_names:
                                            if process.name != 'Fake' and _syst_name in hh_fake_systs :
                                                continue
                                            # retrieve Systematic object up
                                            _syst_up = self._build_syst_object(dataset, systematics_set, _syst_name, 'up')
                                            n_requests += 1
                                            n_requests_per_process += 1
                                            dataset.getHistogram2D(
                                                variable,
                                                variabl2,
                                                cut=sel.get_cut(dataset.isData),
                                                systematicVariation=_syst_up,
                                                systematicsSet=systematics_set)
                                            # retrieve Systematic object down
                                            n_requests += 1
                                            n_requests_per_process += 1
                                            _syst_down = self._build_syst_object(dataset, systematics_set, _syst_name, 'down')
                                            dataset.getHistogram2D(
                                                variable,
                                                variable2,
                                                cut=sel.get_cut(dataset.isData),
                                                systematicVariation=_syst_down,
                                                systematicsSet=systematics_set)
                                # perform mc subtraction
                                if self._do_fake_mc_subtraction and process in self.mc_backgrounds:
                                    n_requests += 1
                                    n_requests_per_process += 1
                                    systematics_set = sel.fake_cr.get_weight(
                                        'Fake', False, process.isSignal)
                                    dataset.getHistogram2D(
                                        variable,
                                        variable2,
                                        cut=sel.fake_cr.get_cut(dataset.isData),
                                        systematicVariation=None,
                                        systematicsSet=systematics_set)
                                    if self._systematic_names != None:
                                        # register systematic variations
                                        for _syst_name in self._systematic_names:
                                            # retrieve Systematic object up
                                            _syst = self._build_syst_object(dataset, systematics_set, _syst_name, 'up')
                                            n_requests += 1
                                            n_requests_per_process += 1
                                            dataset.getHistogram2D(
                                                variable,
                                                variable2,
                                                cut=sel.fake_cr.get_cut(dataset.isData),
                                                systematicVariation=_syst,
                                                systematicsSet=systematics_set)
                                            # retrieve Systematic object down
                                            _syst = self._build_syst_object(dataset, systematics_set, _syst_name, 'down')
                                            n_requests += 1
                                            n_requests_per_process += 1
                                            dataset.getHistogram2D(
                                                variable,
                                                variable2,
                                                cut=sel.fake_cr.get_cut(dataset.isData),
                                                systematicVariation=_syst,
                                                systematicsSet=systematics_set)


            print('BOOM: \t\t {}: {} histogram requests'.format(process.name, n_requests_per_process))
        print('BOOM: Total: {} histogram requests'.format(n_requests))
        stop_watch.Stop()
        stop_watch.Print()



    def run(self, n_cores=1):
        """
        Parameters
        ----------
        n_cores: int 
           the number of cores to run (recommended N_hyperthreadedcores - 1)
        """
        print('BOOM: run scheduler')
        stop_watch = ROOT.TStopwatch()
        scheduler.process(n_cores=n_cores)
        stop_watch.Stop()
        stop_watch.Print()
        print('BOOM: done')

