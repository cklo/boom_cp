"""
Implementation of the DecayMode scale factors/systematics on top of the TauID
"""
from happy.systematics import Systematics, SystematicsSet

def get_decaymode_weights(channel):

    from .decaymodesyst import decaymode_syst_singletau_true_1p0n_reco_1p0n_expr,decaymode_syst_singletau_true_1p1n_reco_1p0n_expr,decaymode_syst_singletau_true_1p1n_reco_1p1n_expr,decaymode_syst_singletau_true_1p1n_reco_1pXn_expr,decaymode_syst_singletau_true_1pXn_reco_1p1n_expr,decaymode_syst_singletau_true_1pXn_reco_1pXn_expr,decaymode_syst_singletau_true_3p0n_reco_3p0n_expr,decaymode_syst_singletau_true_3pXn_reco_3p0n_expr  


    sys_set = SystematicsSet()

    lead_tau_reco_decay_mode = 'tau_0_decay_mode' 
    lead_tau_truth_decay_mode = 'tau_0_matched_decay_mode'
    lead_tau_eta = 'tau_0_p4.Eta()'

    sublead_tau_reco_decay_mode = 'tau_1_decay_mode'
    sublead_tau_truth_decay_mode = 'tau_1_matched_decay_mode'
    sublead_tau_eta = 'tau_1_p4.Eta()'

    # true 1p0n reco 1p0n
    hh_decaymode_syst_true_1p0n_reco_1p0n = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P0N_RECO_1P0N_TOTAL', 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P0N_RECO_1P0N_TOTAL',
                                                               '('+decaymode_syst_singletau_true_1p0n_reco_1p0n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'*'+decaymode_syst_singletau_true_1p0n_reco_1p0n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+')',
                                                               '(2-('+decaymode_syst_singletau_true_1p0n_reco_1p0n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'))*(2-('+decaymode_syst_singletau_true_1p0n_reco_1p0n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+'))',
                                                               '1')
    sys_set.add(hh_decaymode_syst_true_1p0n_reco_1p0n)

    # true 1p1n reco 1p0n
    hh_decaymode_syst_true_1p1n_reco_1p0n = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1P0N_TOTAL', 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1P0N_TOTAL',
                                                               '('+decaymode_syst_singletau_true_1p1n_reco_1p0n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'*'+decaymode_syst_singletau_true_1p1n_reco_1p0n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+')',
                                                               '(2-('+decaymode_syst_singletau_true_1p1n_reco_1p0n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'))*(2-('+decaymode_syst_singletau_true_1p1n_reco_1p0n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+'))',
                                                               '1')
    sys_set.add(hh_decaymode_syst_true_1p1n_reco_1p0n)

    # true 1p1n reco 1p1n
    hh_decaymode_syst_true_1p1n_reco_1p1n = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1P1N_TOTAL', 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1P1N_TOTAL',
                                                               '('+decaymode_syst_singletau_true_1p1n_reco_1p1n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'*'+decaymode_syst_singletau_true_1p1n_reco_1p1n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+')',
                                                               '(2-('+decaymode_syst_singletau_true_1p1n_reco_1p1n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'))*(2-('+decaymode_syst_singletau_true_1p1n_reco_1p1n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+'))',
                                                               '1')
    sys_set.add(hh_decaymode_syst_true_1p1n_reco_1p1n)

    # true 1p1n reco 1pXn
    hh_decaymode_syst_true_1p1n_reco_1pXn = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1PXN_TOTAL', 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1P1N_RECO_1PXN_TOTAL',
                                                               '('+decaymode_syst_singletau_true_1p1n_reco_1pXn_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'*'+decaymode_syst_singletau_true_1p1n_reco_1pXn_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+')',
                                                               '(2-('+decaymode_syst_singletau_true_1p1n_reco_1pXn_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'))*(2-('+decaymode_syst_singletau_true_1p1n_reco_1pXn_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+'))',
                                                               '1')
    sys_set.add(hh_decaymode_syst_true_1p1n_reco_1pXn) 

    # true 1pXn reco 1p1n
    hh_decaymode_syst_true_1pXn_reco_1p1n = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1PXN_RECO_1P1N_TOTAL', 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1PXN_RECO_1P1N_TOTAL',
                                                               '('+decaymode_syst_singletau_true_1pXn_reco_1p1n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'*'+decaymode_syst_singletau_true_1pXn_reco_1p1n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+')',
                                                               '(2-('+decaymode_syst_singletau_true_1pXn_reco_1p1n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'))*(2-('+decaymode_syst_singletau_true_1pXn_reco_1p1n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+'))',
                                                               '1')
    sys_set.add(hh_decaymode_syst_true_1pXn_reco_1p1n)

    # true 1pXn reco 1pXn
    hh_decaymode_syst_true_1pXn_reco_1pXn = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1PXN_RECO_1PXN_TOTAL', 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_1PXN_RECO_1PXN_TOTAL',
                                                               '('+decaymode_syst_singletau_true_1pXn_reco_1pXn_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'*'+decaymode_syst_singletau_true_1pXn_reco_1pXn_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+')',
                                                               '(2-('+decaymode_syst_singletau_true_1pXn_reco_1pXn_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'))*(2-('+decaymode_syst_singletau_true_1pXn_reco_1pXn_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+'))',
                                                               '1')
    sys_set.add(hh_decaymode_syst_true_1pXn_reco_1pXn)

    # true 3p0n reco 3p0n
    hh_decaymode_syst_true_3p0n_reco_3p0n = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_3P0N_RECO_3P0N_TOTAL', 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_3P0N_RECO_3P0N_TOTAL',
                                                               '('+decaymode_syst_singletau_true_3p0n_reco_3p0n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'*'+decaymode_syst_singletau_true_3p0n_reco_3p0n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+')',
                                                               '(2-('+decaymode_syst_singletau_true_3p0n_reco_3p0n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'))*(2-('+decaymode_syst_singletau_true_3p0n_reco_3p0n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+'))',
                                                               '1')
    sys_set.add(hh_decaymode_syst_true_3p0n_reco_3p0n)

    # true 3pXn reco 3p0n
    hh_decaymode_syst_true_3pXn_reco_3p0n = Systematics.weightSystematics('TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_3PXN_RECO_3P0N_TOTAL', 'TAUS_TRUEHADTAU_EFF_JETID_DECAY_MODE_TRUE_3PXN_RECO_3P0N_TOTAL',
                                                               '('+decaymode_syst_singletau_true_3pXn_reco_3p0n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'*'+decaymode_syst_singletau_true_3pXn_reco_3p0n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+')',
                                                               '(2-('+decaymode_syst_singletau_true_3pXn_reco_3p0n_expr(lead_tau_reco_decay_mode,lead_tau_truth_decay_mode,lead_tau_eta)+'))*(2-('+decaymode_syst_singletau_true_3pXn_reco_3p0n_expr(sublead_tau_reco_decay_mode,sublead_tau_truth_decay_mode,sublead_tau_eta)+'))',
                                                               '1')
    sys_set.add(hh_decaymode_syst_true_3pXn_reco_3p0n)

    return sys_set

 


 
