# python imports
import os

# happy imports
from happy.crossSectionDB import crossSectionDB, PmgCrossSectionFile
from happy.style import Style
from happy.systematics import WeightSystematicVariation
from happy.dataset import RootDataset, CombinedDataset

# local imports
from boom.process_generator import process_generator
from boom.systematics.theory import Z_pdf_theory_syst, norm_ggh_theoy_syst, norm_vbf_vh_tth_theoy_syst

__all__ = [
    'get_processes',
    'sum_of_weight_bin',
]

# Need to initialize the database singleton
crossSectionFile = os.path.join(os.path.dirname(os.path.abspath(__file__ )), "../data/PMGxsecDB_mc16.txt")
crossSectionDB.readTextFile(crossSectionFile, 'new')

# Look for pre-defined environment variable, default to EOS
PATH = os.getenv("BOOM_NTUPLE_PATH", "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/SM_Htautau_R21/V03/")

# Arrays of DSIDs
## Ztautau
ZttPow = [
    361108
    ]

ZttQCD = [
    344772, 
    344774, 344775, 344776, 
    344778, 344779, 344780, 344781, 344782, 
    364137, 364138, 364139, 364140, 364141, 
    364210, 364211, 364212, 364213, 364214, 364215
    ]
ZttEWK = [308094]
Ztt = ZttQCD + ZttEWK

ZttMad = [
   361510, 361511, 361512, 361513, 361514,
   361638, 361639, 361640, 361641, 361642,
   ]

# Diboson
VV = [
    363355, 363356, 363357, 363358, 363359, 
    363489, 
    363494, 
    364250, 
    364253, 364254, 364255, 
    364288, 364289, 364290
]

# Single-Top and ttbar
Top = [
    410470, 410471,  
    410644, 410645, 410646, 410647, 
    410658, 410659,
    410155, 
    410218, 410219, 410220,
    412043, 304014,
]

# W(tau/mu/e + nu) + jets
W = [
    364156, 364157, 364158, 364159, 364160, 364161, 
    364162, 364163, 364164, 364165, 364166, 364167, 
    364168, 364169, 364170, 364171, 364172, 364173, 
    364174, 364175, 364176, 364177, 364178, 364179, 
    364180, 364181, 364182, 364183, 364184, 364185, 
    364186, 364187, 364188, 364189, 364190, 364191, 
    364192, 364193, 364194, 364195, 364196, 364197, 
    308096, 308097, 308098
]

# Zll + jets
ZllQCD = [
    345099, 345100, 345101, 345102, 
    364100, 364101, 364102, 364103, 364104, 364105, 364106, 
    364107, 364108, 364109, 364110, 364111, 364112, 364113, 
    364114, 364115, 364116, 364117, 364118, 364119, 364120, 
    364121, 364122, 364123, 364124, 364125, 364126, 364127, 
    364198, 364199, 364200, 364201, 364202, 364203, 
    364204, 364205, 364206, 364207, 364208, 364209, 
]
ZllEWK = [308092, 308093]
Zll = ZllQCD + ZllEWK

# Signal samples
ggH     = [345123]
ggHWW   = [345324]
VBFH    = [346193]
VBFHWW  = [345948]

ggH_unpol  = [346564]
VBFH_unpol = [346573]

ggH_CPodd  = [346562]
VBFH_CPodd = [346571]

WH       = [345211, 345212]
WH_unpol = [346909, 346912]

ZH       = [345217]
ZH_unpol = [346915] 

ttH       = [346343, 346344, 346345]
ttH_unpol = [346919, 346923, 346927] 

ggH_He7    = [ 600565, 600566, 600567, 600568 ]
VBFH_He7   = [ 600569, 600570, 600571, 600572 ]
WH_He7     = [ 600573, 600574 ]
ZH_He7     = [ 600575 ]

VBFH_FxFx  = [ 506101, 506102, 506103, 506104 ]
WH_FxFx    = [ 506098, 506100 ]
ZH_FxFx    = [ 506099 ]

ttH_afii    = [346343, 346344, 346345]
ttH_afii_ME = [346443, 346444, 346445]
ttH_afii_PS = [346346, 346347, 346348]

ggH_FxFx = [345697, 345698, 345699]

ggH_unpol_geo1 = [346564]
ggH_unpol_geo2 = [346564]
ggH_unpol_geo3 = [346564]
ggH_unpol_physlist = [346564]

VBFH_unpol_geo1 = [346573]
VBFH_unpol_geo2 = [346573]
VBFH_unpol_geo3 = [346573]
VBFH_unpol_physlist = [346573]

WH_unpol_geo1 = [346909, 346912]
WH_unpol_geo2 = [346909, 346912]
WH_unpol_geo3 = [346909, 346912]
WH_unpol_physlist = [346909, 346912]

ZH_unpol_geo1 = [346915]
ZH_unpol_geo2 = [346915]
ZH_unpol_geo3 = [346915]
ZH_unpol_physlist = [346915]


# Process Generator Singleton
generator = process_generator(PATH)

def _load_xsec_sumofweights(ntuples_block='nom'):
    """Helper function to load xsecxsumofweights

    Parameters
    ----------
    ntuples_block: str
    """
    from .xsec_sumofweights import build
    try:
        build(ntuples_block=ntuples_block)
    except:
        print('BOOM: c++ weights map are not properly loaded')
        raise RuntimeError


def _squash_process(generator, process):
    """Helper function to squash DSIDs into one process
    
    Parameters
    ----------
    generator: process_generator
    process: happy.dataset.CombinedDataset
    """
    # leave data untouched
    if process.isData:
        return process

    # build a dictionnary of empty lists to store the files to squash
    squashing_files = {}
    for _campaign in generator.process_info(process)['campaigns']:
        squashing_files[_campaign] = {}
        for _stream in generator.process_info(process)['streams']:
            squashing_files[_campaign][_stream] = []

    # fill the dictionary with the list of files to squash
    for dataset in process.datasets:
        info_name = dataset.name.split('_')
        campaign = info_name[0]
        if len(info_name) == 3:
            stream = info_name[2]
        elif len(info_name) == 4:
            stream = info_name[2] + '_' + info_name[3]
        else:
            raise ValueError('dataset name %s is not recognized' % dataset.name)

        for file in dataset.fileNames:
            squashing_files[campaign][stream].append(file)
            
    # Flush the squashing_files dict in dataset list
    _dataset_list = []
    for _campaign in sorted(list(squashing_files.keys())):
        for _stream in sorted(list(squashing_files[_campaign].keys())):
            _files = squashing_files[_campaign][_stream]
            if len(_files) != 0:
                _dataset_list.append(RootDataset(
                        _campaign + '_' + process.name + '_' + _stream,
                        fileNames=_files,
                        weightExpression=process.datasets[0].weightExpression, 
                        isSignal=process.isSignal))
    # Define a combined dataset from the squashing
    _squashed_process = CombinedDataset(
        process.name, 
        process.title, 
        style=process.style, 
        datasets=_dataset_list, 
        isSignal=process.isSignal)
    return _squashed_process


def _build_processes( 
    cpangles=True,
    squash=True, 
    split_ztt=False,
    split_zll=False,
    years=[15, 16, 17, 18],
    campaigns=['mc16a', 'mc16d', 'mc16e'],
    ntuples_block='nom',
    xsec_weights_loading=True):
    """return list of processes"""
    
    if xsec_weights_loading:
        # load xsec*sumofweight map
        _load_xsec_sumofweights(ntuples_block=ntuples_block)


    # Generate all processes in plotting order
    ##########################################

    # Data
    generator.generate_data(
        years=years, 
        style=Style(color=1, markerSize=1.2))

    # MC Backgrounds
    # VV
    generator.generate_mc(
        'VV', 'VV', VV, 
        isSignal=False, 
        campaigns=campaigns,
        style=Style(color=880),
        ntuples_block=ntuples_block)
    # Top
    generator.generate_mc(
        'Top', 'Top', Top, 
        isSignal=False, 
        campaigns=campaigns,
        style=Style(color=801),
        ntuples_block=ntuples_block)

    # W + jets
    generator.generate_mc(
        'W', 'W', W, 
        isSignal=False, 
        campaigns=campaigns,
        style=Style(color=3), 
        ignoreChannel=[['leplep', [364156]]], #Skip all for this channel except listed one
        ntuples_block=ntuples_block)


    # Zll ( split in truth categories as done for Ztt)
    generator.generate_mc(
        'ZllQCD', 'Z#rightarrowll (QCD)', ZllQCD, 
        campaigns=campaigns,
        isSignal=False,
        style=Style(color=422),
        ntuples_block=ntuples_block)

    generator.generate_mc(
        'ZllEWK', 'Z#rightarrowll (EWK)', ZllEWK,
        campaigns=campaigns,
        isSignal=False,
        style=Style(color=420),
        ntuples_block=ntuples_block)


    # HWW  
    generator.generate_mc( 
        'ggHWW', 'H#rightarrowWW', ggHWW,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=910),
        ntuples_block=ntuples_block)

    
    generator.generate_mc(
        'VBFHWW', 'H#rightarrowWW', VBFHWW,
        campaigns=campaigns, 
        isSignal=True, 
        style=Style(color=910),
        ntuples_block=ntuples_block)

    # Fakes
    generator.generate_data(
        name="Fake", 
        title="Fake", 
        years=years, 
        style=Style(color=400), 
        prefix="anti" )

    generator.generate_mc(
        'ZttQCD', 'Z#rightarrow#tau#tau (QCD)', ZttQCD, 
        isSignal=False, 
        campaigns=campaigns,
        style=Style(color=861),
        ntuples_block=ntuples_block)

    generator.generate_mc(
        'ZttEWK', 'Z#rightarrow#tau#tau (EWK)', ZttEWK, 
        isSignal=False, 
        campaigns=campaigns,
        style=Style(color=862),
        ntuples_block=ntuples_block)

    generator.generate_mc(
        'ZttPow', 'Z#rightarrow#tau#tau (Pow)', ZttPow,
        isSignal=False,
        campaigns=campaigns,
        style=Style(color=863),
        ntuples_block=ntuples_block)

    generator.generate_mc(
        'ZttMad', 'Z#rightarrow#tau#tau (Mad)', ZttMad,
        isSignal=False,
        campaigns=campaigns,
        style=Style(color=863),
        ntuples_block=ntuples_block)


    # Signal Samples
    for prod_mod, dsids in zip(('ggH', 'VBFH', 'ggH_unpol','VBFH_unpol', 'ggH_CPodd', 'VBFH_CPodd', 'WH', 'ZH', 'ttH', 'WH_unpol', 'ZH_unpol', 'ttH_unpol', 'ggH_He7', 'VBFH_He7', 'WH_He7', 'ZH_He7','ttH_afii', 'ttH_afii_ME', 'ttH_afii_PS','ggH_FxFx', 'WH_FxFx','ZH_FxFx', 'VBFH_FxFx', 'ggH_unpol_geo1', 'VBFH_unpol_geo1', 'WH_unpol_geo1', 'ZH_unpol_geo1', 'ggH_unpol_geo2', 'VBFH_unpol_geo2', 'WH_unpol_geo2', 'ZH_unpol_geo2', 'ggH_unpol_geo3', 'VBFH_unpol_geo3', 'WH_unpol_geo3', 'ZH_unpol_geo3', 'ggH_unpol_physlist', 'VBFH_unpol_physlist', 'WH_unpol_physlist', 'ZH_unpol_physlist' ), 
                               (ggH, VBFH, ggH_unpol, VBFH_unpol, ggH_CPodd, VBFH_CPodd, WH, ZH, ttH, WH_unpol, ZH_unpol, ttH_unpol, ggH_He7, VBFH_He7, WH_He7, ZH_He7, ttH_afii, ttH_afii_ME, ttH_afii_PS, ggH_FxFx, WH_FxFx, ZH_FxFx, VBFH_FxFx, ggH_unpol_geo1, VBFH_unpol_geo1, WH_unpol_geo1, ZH_unpol_geo1, ggH_unpol_geo2, VBFH_unpol_geo2, WH_unpol_geo2, ZH_unpol_geo2, ggH_unpol_geo3, VBFH_unpol_geo3, WH_unpol_geo3, ZH_unpol_geo3, ggH_unpol_physlist, VBFH_unpol_physlist, WH_unpol_physlist, ZH_unpol_physlist)):

        if 'afii' in prod_mod:
            _isSignal = False
            if 'afii' not in ntuples_block:
                 continue
        elif 'geo1' in prod_mod:
            _isSignal = True
            if 'geo1' not in ntuples_block:
                 continue
        elif 'geo2' in prod_mod:
            _isSignal = True
            if 'geo2' not in ntuples_block:
                 continue
        elif 'geo3' in prod_mod:
            _isSignal = True
            if 'geo3' not in ntuples_block:
                 continue
        elif 'physlist' in prod_mod:
            _isSignal = True
            if 'physlist' not in ntuples_block:
                 continue
        elif 'FxFx' in prod_mod:
            _isSignal = False
        else:
            _isSignal = True

        if 'unpol' in prod_mod and cpangles:
            from .cpangles import CPANGLES
            if 'geo' in prod_mod or 'physlist' in prod_mod:
                prod_mod = prod_mod.replace('_unpol','')
            else:
                prod_mod = prod_mod[:-6]

            for cpangles_mix in sorted(list(CPANGLES[prod_mod].keys())):
                _name = prod_mod + cpangles_mix
                generator.generate_mc(
                    _name,
                    'H#rightarrow#tau#tau',
                    dsids,
                    isSignal=_isSignal,
                    campaigns=campaigns,
                    weightExpression='('+CPANGLES[prod_mod][cpangles_mix]+')',
                    style=Style(lineColor=632),
                    ntuples_block=ntuples_block)
        else:
            if 'CPodd' in prod_mod : linecolor = 416
            else : linecolor = 632

            if 'ggH' in prod_mod or 'VBFH' in prod_mod:
                generator.generate_mc(
                    prod_mod, 
                    'H#rightarrow#tau#tau', 
                    dsids,   
                    isSignal=_isSignal, 
                    campaigns=campaigns,
                    style=Style(lineColor=linecolor),
                    ntuples_block=ntuples_block)
            else:
                generator.generate_mc(
                    prod_mod,
                    'H#rightarrow#tau#tau',
                    dsids,
                    isSignal=_isSignal,
                    campaigns=campaigns,
                    style=Style(color=801),
                    ntuples_block=ntuples_block)


    if not squash:
        return generator.processes

    if squash == True:
        _squashed_processes = []
        for process in generator.processes:
            squashed_process = _squash_process(generator, process)
            _squashed_processes.append(squashed_process)

        # overwrite the sumOfWeights
        for _process in _squashed_processes:
            if not _process.isData:
                _process.sumOfWeights = 1
                for _dataset in _process.datasets:
                    _dataset.sumOfWeights = 1
        return _squashed_processes


def get_processes(signal_only=False, mc_only=False, no_signal=False, no_fake=False, add_he7_samples=False, add_afii_samples=False, add_mad_ztt=False, add_ggh_fxfx=False, add_vbf_vh_fxfx=False, add_pow_ztt=False, add_geo1_samples=False, add_geo2_samples=False, add_geo3_samples=False, add_physlist_samples=False, **kwargs):
    """
    """
    _processes = _build_processes(**kwargs)

    if not add_he7_samples:
        _processes = [p for p in _processes if 'He7' not in p.name]

    if not add_afii_samples:
        _processes = [p for p in _processes if 'afii' not in p.name]
 
    if not add_mad_ztt:
        _processes = [p for p in _processes if 'Mad' not in p.name]

    if not add_pow_ztt:
        _processes = [p for p in _processes if 'Pow' not in p.name]

    if not add_ggh_fxfx:
        _processes = [p for p in _processes if 'ggH_FxFx' not in p.name]

    if not add_vbf_vh_fxfx:
        _processes = [p for p in _processes if p.name not in ('WH_FxFx','ZH_FxFx', 'VBFH_FxFx')]

    if not add_geo1_samples:
        _processes = [p for p in _processes if 'geo1' not in p.name]
 
    if not add_geo2_samples:
        _processes = [p for p in _processes if 'geo2' not in p.name]

    if not add_geo3_samples:
        _processes = [p for p in _processes if 'geo3' not in p.name]

    if not add_physlist_samples:
        _processes = [p for p in _processes if 'physlist' not in p.name]

    if signal_only:
        _processes = [p for p in _processes if p.isSignal]
        return _processes

    if mc_only:
        _processes = [p for p in _processes if not p.isData]
        return _processes

    if no_signal:
       _processes = [p for p in _processes if not p.isSignal]

    if no_fake:
        _processes = [p for p in _processes if p.name != 'Fake']
        return _processes

    return list(_processes)

def sum_of_weight_bin(process, sumweight_sys=''):
    """
    Parameters
    __________
    process: HAPPy combined dataset
    """
    from happy.dataset import HistogramSumOfWeightsCalculator
    if process.isSignal:
        for dataset in process.datasets:     
            if 'ggH_He7' in process.name:
                dataset.sumOfWeightsCalculator = HistogramSumOfWeightsCalculator('h_metadata_theory_weights', 180)
            elif 'VBFH_He7' in process.name:
                dataset.sumOfWeightsCalculator = HistogramSumOfWeightsCalculator('h_metadata_theory_weights', 105)
            elif 'He7' in process.name : # VH and ZH
                dataset.sumOfWeightsCalculator = HistogramSumOfWeightsCalculator('h_metadata_theory_weights', 102)
            elif 'ggH' in process.name:
                dataset.sumOfWeightsCalculator = HistogramSumOfWeightsCalculator('h_metadata_theory_weights', 152)
                if sumweight_sys!='':
                    dataset.sumOfWeightsCalculator = HistogramSumOfWeightsCalculator(norm_ggh_theoy_syst[sumweight_sys][0],norm_ggh_theoy_syst[sumweight_sys][1])
            else:
                dataset.sumOfWeightsCalculator = HistogramSumOfWeightsCalculator('h_metadata_theory_weights', 110)
                if sumweight_sys!='': 
                    dataset.sumOfWeightsCalculator = HistogramSumOfWeightsCalculator(norm_vbf_vh_tth_theoy_syst[sumweight_sys][0],norm_vbf_vh_tth_theoy_syst[sumweight_sys][1]) 

    if ('ZttQCD' in process.name) and sumweight_sys!='':
        for dataset in process.datasets:
            dataset.sumOfWeightsCalculator = HistogramSumOfWeightsCalculator(Z_pdf_theory_syst[sumweight_sys][0],Z_pdf_theory_syst[sumweight_sys][1])


