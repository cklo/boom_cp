"""
Small utils module to hold functions 
for loading and configuring bdts
"""
import array

def _load_bdt(variable_list, reader, xml_file, name='BDT'):
    """
    Arguments
    _________
    variable_list : list(str)
    reader : ROOT.TMVA.Reader
    xml_file : str
    """
    dummy_val = array.array('f', [0.])
    for v in variable_list:
        reader.AddVariable(v, dummy_val)
    reader.BookMVA(name, xml_file)
