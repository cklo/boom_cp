"""
Module to load and configure TMVA Readers
needed for bdt evaluation throughout BOOM
"""

import ROOT
ROOT.TMVA.gConfig().SetSilent(True)

# vbf bdts
from . import vbf

