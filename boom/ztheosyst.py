import ROOT
import os

try:
    ROOT.gSystem.CompileMacro('boom/cpp/ztheosyst_helper.cpp', 'k-', '', 'cache')
    print('BOOM:\t Z theory syst helper tool loaded!')
except:
    raise RuntimeError

# this is needed because of
# https://sft.its.cern.ch/jira/browse/ROOT-7216
try:
    ROOT.ZTheoSystHelper()
except:
    print('Z Theo Syst tool not compiled')

__all__ = [
    'ztheosyst_expr',
]

_THEOSYST_FILE = os.path.join(os.path.dirname(
        os.path.abspath(__file__ )), "../data/alltheovar_r21.root")

_root_file = ROOT.TFile(_THEOSYST_FILE, "read") 

ROOT.histoDict.h_ckkw[0] = _root_file.Get("ckk")
ROOT.histoDict.h_qsf[0]  = _root_file.Get("qsf")

def ztheosyst_expr(variation, channel):
           
    expr = 'ZTheoSystHelper::theo_weight(boson_0_truth_p4.Pt(),n_truth_jets_pt20_eta45,{syst},{channel},0)'.format(
         syst = str(variation),
         channel = str(channel)
         )

    return expr
