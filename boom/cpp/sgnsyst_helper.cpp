#include <TH1F.h>
#include <unordered_map>

class sgn_histDic
{
public:
  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1p0n_ggH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1p0n_ggH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1p1n_ggH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1p1n_ggH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1pXn_ggH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1pXn_1p1n_ggH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1pXn_ggH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1pXn_1p0n_ggH;
  static std::unordered_map<int, TH1F> h_sgn_syst_3p0n_1p1n_ggH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_3p0n_ggH;

  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1p0n_VBFH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1p0n_VBFH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1p1n_VBFH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1p1n_VBFH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1pXn_VBFH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1pXn_1p1n_VBFH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1pXn_VBFH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1pXn_1p0n_VBFH;
  static std::unordered_map<int, TH1F> h_sgn_syst_3p0n_1p1n_VBFH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_3p0n_VBFH;

  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1p0n_WH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1p0n_WH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1p1n_WH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1p1n_WH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1pXn_WH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1pXn_1p1n_WH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1pXn_WH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1pXn_1p0n_WH;
  static std::unordered_map<int, TH1F> h_sgn_syst_3p0n_1p1n_WH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_3p0n_WH;

  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1p0n_ZH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1p0n_ZH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1p1n_ZH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1p1n_ZH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1pXn_ZH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1pXn_1p1n_ZH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1pXn_ZH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1pXn_1p0n_ZH;
  static std::unordered_map<int, TH1F> h_sgn_syst_3p0n_1p1n_ZH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_3p0n_ZH;

  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1p0n_ttH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1p0n_ttH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1p1n_ttH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1p1n_ttH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_1pXn_ttH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1pXn_1p1n_ttH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p0n_1pXn_ttH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1pXn_1p0n_ttH;
  static std::unordered_map<int, TH1F> h_sgn_syst_3p0n_1p1n_ttH;
  static std::unordered_map<int, TH1F> h_sgn_syst_1p1n_3p0n_ttH;

};

std::unordered_map<int, TH1F> sgn_createEmptyTH1F()
{
  std::unordered_map<int, TH1F> internalMap;
  return internalMap;
}

std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1p0n_ggH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1p1n_ggH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1p0n_ggH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1p1n_ggH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1pXn_ggH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1pXn_1p1n_ggH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1pXn_ggH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1pXn_1p0n_ggH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_3p0n_1p1n_ggH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_3p0n_ggH   = sgn_createEmptyTH1F();

std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1p0n_VBFH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1p1n_VBFH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1p0n_VBFH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1p1n_VBFH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1pXn_VBFH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1pXn_1p1n_VBFH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1pXn_VBFH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1pXn_1p0n_VBFH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_3p0n_1p1n_VBFH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_3p0n_VBFH   = sgn_createEmptyTH1F();

std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1p0n_WH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1p1n_WH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1p0n_WH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1p1n_WH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1pXn_WH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1pXn_1p1n_WH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1pXn_WH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1pXn_1p0n_WH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_3p0n_1p1n_WH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_3p0n_WH   = sgn_createEmptyTH1F();

std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1p0n_ZH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1p1n_ZH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1p0n_ZH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1p1n_ZH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1pXn_ZH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1pXn_1p1n_ZH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1pXn_ZH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1pXn_1p0n_ZH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_3p0n_1p1n_ZH   = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_3p0n_ZH   = sgn_createEmptyTH1F();

std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1p0n_ttH  = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1p1n_ttH  = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1p0n_ttH  = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1p1n_ttH  = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_1pXn_ttH  = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1pXn_1p1n_ttH  = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p0n_1pXn_ttH  = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1pXn_1p0n_ttH  = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_3p0n_1p1n_ttH  = sgn_createEmptyTH1F();
std::unordered_map<int, TH1F> sgn_histDic::h_sgn_syst_1p1n_3p0n_ttH  = sgn_createEmptyTH1F();

namespace SgnSystHelper {

  float sgnsyst_weight(float phistar, float tau_0_truth_decay_mode, float tau_1_truth_decay_mode, int process, int version)
  {

    if(process==1){ //ggH
      if(tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1p0n_ggH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1p0n_ggH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1p1n_ggH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1p1n_ggH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1p0n_ggH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1p0n_ggH.at(version).GetBinContent(bin); 
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1p1n_ggH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1p1n_ggH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 2){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1pXn_ggH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1pXn_ggH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 2 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1pXn_1p1n_ggH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1pXn_1p1n_ggH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 2){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1pXn_ggH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1pXn_ggH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 2 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1pXn_1p0n_ggH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1pXn_1p0n_ggH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 3){
         int bin = sgn_histDic::h_sgn_syst_1p1n_3p0n_ggH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_3p0n_ggH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 3 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_3p0n_1p1n_ggH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_3p0n_1p1n_ggH.at(version).GetBinContent(bin);
      } else {
         return 1;
      }
    } else if(process == 2) { // VBFH
      if(tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1p0n_VBFH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1p0n_VBFH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1p1n_VBFH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1p1n_VBFH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1p0n_VBFH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1p0n_VBFH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1p1n_VBFH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1p1n_VBFH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 2){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1pXn_VBFH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1pXn_VBFH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 2 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1pXn_1p1n_VBFH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1pXn_1p1n_VBFH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 2){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1pXn_VBFH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1pXn_VBFH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 2 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1pXn_1p0n_VBFH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1pXn_1p0n_VBFH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 3){
         int bin = sgn_histDic::h_sgn_syst_1p1n_3p0n_VBFH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_3p0n_VBFH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 3 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_3p0n_1p1n_VBFH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_3p0n_1p1n_VBFH.at(version).GetBinContent(bin);
      } else {
         return 1;
      } 
    } else if(process == 3) { // WH
      if(tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1p0n_WH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1p0n_WH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1p1n_WH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1p1n_WH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1p0n_WH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1p0n_WH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1p1n_WH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1p1n_WH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 2){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1pXn_WH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1pXn_WH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 2 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1pXn_1p1n_WH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1pXn_1p1n_WH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 2){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1pXn_WH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1pXn_WH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 2 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1pXn_1p0n_WH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1pXn_1p0n_WH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 3){
         int bin = sgn_histDic::h_sgn_syst_1p1n_3p0n_WH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_3p0n_WH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 3 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_3p0n_1p1n_WH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_3p0n_1p1n_WH.at(version).GetBinContent(bin);
      } else {
         return 1;
      }
    } else if(process == 4) { // ZH
      if(tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1p0n_ZH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1p0n_ZH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1p1n_ZH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1p1n_ZH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1p0n_ZH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1p0n_ZH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1p1n_ZH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1p1n_ZH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 2){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1pXn_ZH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1pXn_ZH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 2 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1pXn_1p1n_ZH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1pXn_1p1n_ZH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 2){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1pXn_ZH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1pXn_ZH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 2 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1pXn_1p0n_ZH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1pXn_1p0n_ZH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 3){
         int bin = sgn_histDic::h_sgn_syst_1p1n_3p0n_ZH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_3p0n_ZH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 3 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_3p0n_1p1n_ZH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_3p0n_1p1n_ZH.at(version).GetBinContent(bin);
      } else {
         return 1;
      }
    } else if(process == 5) { // ttH
      if(tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1p0n_ttH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1p0n_ttH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1p1n_ttH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1p1n_ttH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1p0n_ttH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1p0n_ttH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1p1n_ttH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1p1n_ttH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 2){
         int bin = sgn_histDic::h_sgn_syst_1p1n_1pXn_ttH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_1pXn_ttH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 2 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_1pXn_1p1n_ttH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1pXn_1p1n_ttH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 0 && tau_1_truth_decay_mode == 2){
         int bin = sgn_histDic::h_sgn_syst_1p0n_1pXn_ttH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p0n_1pXn_ttH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 2 && tau_1_truth_decay_mode == 0){
         int bin = sgn_histDic::h_sgn_syst_1pXn_1p0n_ttH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1pXn_1p0n_ttH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 1 && tau_1_truth_decay_mode == 3){
         int bin = sgn_histDic::h_sgn_syst_1p1n_3p0n_ttH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_1p1n_3p0n_ttH.at(version).GetBinContent(bin);
      } else if (tau_0_truth_decay_mode == 3 && tau_1_truth_decay_mode == 1){
         int bin = sgn_histDic::h_sgn_syst_3p0n_1p1n_ttH.at(version).FindBin(phistar);
         return sgn_histDic::h_sgn_syst_3p0n_1p1n_ttH.at(version).GetBinContent(bin);
      } else {
         return 1;
      }
    } else{
      return 1;
    }  
  }
}; // end the namespace
