#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include <TRandom3.h>
#include "TStopwatch.h"
#include <TLorentzVector.h>
#include <TVector3.h>


namespace TauVisMassCalc {

float calc_tau_vismass(
                    float tau_0_decay_mode, 
                    float tau_0_decay_charged_p4_pt,
                    float tau_0_decay_charged_p4_eta,
                    float tau_0_decay_charged_p4_phi,
                    float tau_0_decay_charged_p4_m,
                    float tau_0_decay_neutral_p4_pt,
                    float tau_0_decay_neutral_p4_eta,
                    float tau_0_decay_neutral_p4_phi,
                    float tau_0_decay_neutral_p4_m,
                    float event_number,
                    bool apply_energy_scale_up,
                    bool apply_energy_scale_down,
                    bool apply_eta_smearing,
                    bool apply_phi_smearing,
                    bool apply_etaphi_smearing
                  ) 
{
  
    TLorentzVector tau_0_decay_charged_p4;
    TLorentzVector tau_0_decay_neutral_p4;

    tau_0_decay_charged_p4.SetPtEtaPhiM(tau_0_decay_charged_p4_pt, tau_0_decay_charged_p4_eta, tau_0_decay_charged_p4_phi, tau_0_decay_charged_p4_m);
    tau_0_decay_neutral_p4.SetPtEtaPhiM(tau_0_decay_neutral_p4_pt, tau_0_decay_neutral_p4_eta, tau_0_decay_neutral_p4_phi, tau_0_decay_neutral_p4_m);

    if(apply_energy_scale_up){
        float tau_0_energy_scale = 1.0;
        if(tau_0_decay_mode == 1) tau_0_energy_scale = 1.2;
        else if (tau_0_decay_mode == 2) tau_0_energy_scale = 1.4;

        tau_0_decay_neutral_p4.SetE(tau_0_decay_neutral_p4.E()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPx(tau_0_decay_neutral_p4.Px()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPy(tau_0_decay_neutral_p4.Py()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPz(tau_0_decay_neutral_p4.Pz()*tau_0_energy_scale);
    }

    if(apply_energy_scale_down){
        float tau_0_energy_scale = 1.0;
        if(tau_0_decay_mode == 1) tau_0_energy_scale = 0.8;
        else if (tau_0_decay_mode == 2) tau_0_energy_scale = 0.6;

        tau_0_decay_neutral_p4.SetE(tau_0_decay_neutral_p4.E()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPx(tau_0_decay_neutral_p4.Px()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPy(tau_0_decay_neutral_p4.Py()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPz(tau_0_decay_neutral_p4.Pz()*tau_0_energy_scale);
    }

    if(apply_eta_smearing){
       TRandom3 eta_smear(event_number);
       float smearEta = 5*4e-3;
       tau_0_decay_neutral_p4.SetPtEtaPhiM(tau_0_decay_neutral_p4_pt, tau_0_decay_neutral_p4_eta + eta_smear.Gaus(0, smearEta), tau_0_decay_neutral_p4_phi, tau_0_decay_neutral_p4_m);
    }

    if(apply_phi_smearing){
       TRandom3 phi_smear(event_number);
       float smearPhi = 5*10e-3;
       tau_0_decay_neutral_p4.SetPtEtaPhiM(tau_0_decay_neutral_p4_pt, tau_0_decay_neutral_p4_eta, tau_0_decay_neutral_p4_phi + phi_smear.Gaus(0, smearPhi), tau_0_decay_neutral_p4_m);
    }

    if(apply_etaphi_smearing){
       TRandom3 smear(event_number);
       float smearEta = 5*4e-3;
       float smearPhi = 5*10e-3;
       tau_0_decay_neutral_p4.SetPtEtaPhiM(tau_0_decay_neutral_p4_pt, tau_0_decay_neutral_p4_eta + smear.Gaus(0, smearEta), tau_0_decay_neutral_p4_phi + smear.Gaus(0, smearPhi), tau_0_decay_neutral_p4_m);
    }

    return (tau_0_decay_charged_p4+tau_0_decay_neutral_p4).M();

}

}
