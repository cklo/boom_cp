#include <TH1F.h>
#include <TH2D.h>
#include <unordered_map>
#include <iostream>

class hhfake_histDict
{
public:
  // sameSign FFs
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p0n;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p1n;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1pXn;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_3p0n;
 
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p0n_lead_lnm;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p1n_lead_lnm;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1pXn_lead_lnm;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_3p0n_lead_lnm;

  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p0n_sublead_lnm;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p1n_sublead_lnm;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1pXn_sublead_lnm;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_3p0n_sublead_lnm;

  // highDeta FFs
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p0n_hd_lead;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p1n_hd_lead;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1pXn_hd_lead;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_3p0n_hd_lead;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p0n_hd_lead_lnm;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p1n_hd_lead_lnm;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1pXn_hd_lead_lnm;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_3p0n_hd_lead_lnm;

  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p0n_hd_sublead;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p1n_hd_sublead;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1pXn_hd_sublead;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_3p0n_hd_sublead;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p0n_hd_sublead_lnm;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1p1n_hd_sublead_lnm;  
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_1pXn_hd_sublead_lnm;
  static inline std::unordered_map<int, TH1F> h_ff_2dobj_3p0n_hd_sublead_lnm;

  // WCR FFs
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1p0n_w;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1p1n_w;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1pXn_w;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_3p0n_w;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1p0n_w_lnm;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1p1n_w_lnm;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1pXn_w_lnm;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_3p0n_w_lnm;

  // WCR MC subtraction FFs
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1p0n_w_mcsubtr;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1p1n_w_mcsubtr;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1pXn_w_mcsubtr;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_3p0n_w_mcsubtr;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1p0n_w_lnm_mcsubtr;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1p1n_w_lnm_mcsubtr;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_1pXn_w_lnm_mcsubtr;
  static inline std::unordered_map<int, TH1D> h_ff_2dobj_3p0n_w_lnm_mcsubtr;

  // parametrization uncertainty weights
  static inline std::unordered_map<int, TH1F> h_ff_param_weights;


  enum category{ preselection, boost, vbf, vh };
  enum syst{
    nominal,
    hh_fake_ff_stat_1p0n_nm,
    hh_fake_ff_mcsubtr_1p0n_nm,
    hh_fake_ff_stat_1p1n_nm,
    hh_fake_ff_mcsubtr_1p1n_nm,
    hh_fake_ff_stat_1pXn_nm,
    hh_fake_ff_mcsubtr_1pXn_nm,
    hh_fake_ff_stat_3p0n_nm,
    hh_fake_ff_mcsubtr_3p0n_nm,
    hh_fake_ff_stat_1p0n_lnm,
    hh_fake_ff_mcsubtr_1p0n_lnm,
    hh_fake_ff_stat_1p1n_lnm,
    hh_fake_ff_mcsubtr_1p1n_lnm,
    hh_fake_ff_stat_1pXn_lnm,
    hh_fake_ff_mcsubtr_1pXn_lnm,
    hh_fake_ff_stat_3p0n_lnm,
    hh_fake_ff_mcsubtr_3p0n_lnm,
    hh_fake_ff_param,
    hh_fake_ff_composition_ss,
    hh_fake_ff_composition_highdeta};

};

namespace HHFakeSystHelper {
  // helper functions for accessing FF histograms, first sameSign, then highDeta then WCR

  
  float _read_ff_lnm_lead(float taupt, int decaymode, int version){
    // using tau eta parametrization for now
    if(decaymode == 0){
      int bin = hhfake_histDict::h_ff_2dobj_1p0n_lead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p0n_lead_lnm.at(version).GetBinContent(bin);
    }
    else if(decaymode == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p1n_lead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p1n_lead_lnm.at(version).GetBinContent(bin);
    } 
    else if(decaymode == 2){
      int bin = hhfake_histDict::h_ff_2dobj_1pXn_lead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1pXn_lead_lnm.at(version).GetBinContent(bin);
    }
     else if(decaymode == 3){
      int bin = hhfake_histDict::h_ff_2dobj_3p0n_lead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_3p0n_lead_lnm.at(version).GetBinContent(bin);
    } 
    else{
      return 1;
    }
  }

  float _read_ff_lnm_sublead(float taupt, int decaymode, int version){
    if(decaymode == 0){
      int bin = hhfake_histDict::h_ff_2dobj_1p0n_sublead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p0n_sublead_lnm.at(version).GetBinContent(bin);
    }
    else if (decaymode == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p1n_sublead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p1n_sublead_lnm.at(version).GetBinContent(bin);
    } 
    else if (decaymode == 2){
      int bin = hhfake_histDict::h_ff_2dobj_1pXn_sublead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1pXn_sublead_lnm.at(version).GetBinContent(bin);
    }
    else if (decaymode == 3){
      int bin = hhfake_histDict::h_ff_2dobj_3p0n_sublead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_3p0n_sublead_lnm.at(version).GetBinContent(bin);
    }
    else{
      return 1;
    }
  }

  float _read_ff_nm(float taupt, int decaymode, int version){
    if(decaymode == 0){
      int bin = hhfake_histDict::h_ff_2dobj_1p0n.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p0n.at(version).GetBinContent(bin);
    }
    else if(decaymode == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p1n.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p1n.at(version).GetBinContent(bin);
    }
    else if(decaymode == 2){
      int bin = hhfake_histDict::h_ff_2dobj_1pXn.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1pXn.at(version).GetBinContent(bin);
    }
    else if(decaymode == 3){
      int bin = hhfake_histDict::h_ff_2dobj_3p0n.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_3p0n.at(version).GetBinContent(bin);
    }
    else {
      return 1;
    }
  }

  // copy of previous functions, but accessing high-deta FFs
  float _read_ff_hd_lead_nm(float taupt, int decaymode, int version){
    if(decaymode == 0){
      int bin = hhfake_histDict::h_ff_2dobj_1p0n_hd_lead.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p0n_hd_lead.at(version).GetBinContent(bin);
    }
    else if (decaymode == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p1n_hd_lead.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p1n_hd_lead.at(version).GetBinContent(bin);
    }
    else if (decaymode == 2){
      int bin = hhfake_histDict::h_ff_2dobj_1pXn_hd_lead.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1pXn_hd_lead.at(version).GetBinContent(bin);
    }
    else if (decaymode == 3){
      int bin = hhfake_histDict::h_ff_2dobj_3p0n_hd_lead.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_3p0n_hd_lead.at(version).GetBinContent(bin);
    }
    else {
      return 1;
    }
  }

  float _read_ff_hd_lead_lnm(float taupt, int decaymode, int version){
    if(decaymode == 0){
      int bin = hhfake_histDict::h_ff_2dobj_1p0n_hd_lead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p0n_hd_lead_lnm.at(version).GetBinContent(bin);
    }
    else if (decaymode == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p1n_hd_lead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p1n_hd_lead_lnm.at(version).GetBinContent(bin);
    }
    else if (decaymode == 2){
      int bin = hhfake_histDict::h_ff_2dobj_1pXn_hd_lead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1pXn_hd_lead_lnm.at(version).GetBinContent(bin);
    }
    else if (decaymode == 3){
      int bin = hhfake_histDict::h_ff_2dobj_3p0n_hd_lead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_3p0n_hd_lead_lnm.at(version).GetBinContent(bin);
    }
    else {
      return 1;
    }
  }

  float _read_ff_hd_sublead_nm(float taupt, int decaymode, int version){
    if(decaymode == 0){
      int bin = hhfake_histDict::h_ff_2dobj_1p0n_hd_sublead.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p0n_hd_sublead.at(version).GetBinContent(bin);
    }
    else if (decaymode == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p1n_hd_sublead.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p1n_hd_sublead.at(version).GetBinContent(bin);
    }
    else if (decaymode == 2){
      int bin = hhfake_histDict::h_ff_2dobj_1pXn_hd_sublead.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1pXn_hd_sublead.at(version).GetBinContent(bin);
    }
    else if (decaymode == 3){
      int bin = hhfake_histDict::h_ff_2dobj_3p0n_hd_sublead.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_3p0n_hd_sublead.at(version).GetBinContent(bin);
    }
    else {
      return 1;
    }   
  }

  float _read_ff_hd_sublead_lnm(float taupt, int decaymode, int version){
    if(decaymode == 0){
      int bin = hhfake_histDict::h_ff_2dobj_1p0n_hd_sublead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p0n_hd_sublead_lnm.at(version).GetBinContent(bin);
    }
    else if (decaymode == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p1n_hd_sublead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1p1n_hd_sublead_lnm.at(version).GetBinContent(bin);
    }
    else if (decaymode == 2){
      int bin = hhfake_histDict::h_ff_2dobj_1pXn_hd_sublead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_1pXn_hd_sublead_lnm.at(version).GetBinContent(bin);
    } 
    else if (decaymode == 3){
      int bin = hhfake_histDict::h_ff_2dobj_3p0n_hd_sublead_lnm.at(version).FindBin(taupt);
      return hhfake_histDict::h_ff_2dobj_3p0n_hd_sublead_lnm.at(version).GetBinContent(bin);
    }
    else {
      return 1;
    } 
  }
 
  // and for the FFs from the lephad w region
  float _read_ff_w_nm(float taupt, int decaymode, int version, int syst){
    if(decaymode == 0){
      int bin = hhfake_histDict::h_ff_2dobj_1p0n_w.at(version).FindBin(taupt);
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_1p0n_nm){
        return hhfake_histDict::h_ff_2dobj_1p0n_w.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_1p0n_w.at(version).GetBinError(bin);
      } else if(syst == hhfake_histDict::syst::hh_fake_ff_mcsubtr_1p0n_nm){
        bin = hhfake_histDict::h_ff_2dobj_1p0n_w_mcsubtr.at(version).FindBin(taupt);
        return hhfake_histDict::h_ff_2dobj_1p0n_w_mcsubtr.at(version).GetBinContent(bin);
      }
      else{
        return hhfake_histDict::h_ff_2dobj_1p0n_w.at(version).GetBinContent(bin);
      }
    }
    else if( decaymode == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p1n_w.at(version).FindBin(taupt);
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_1p1n_nm){
        return hhfake_histDict::h_ff_2dobj_1p1n_w.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_1p1n_w.at(version).GetBinError(bin);
      } else if(syst == hhfake_histDict::syst::hh_fake_ff_mcsubtr_1p1n_nm){
        bin = hhfake_histDict::h_ff_2dobj_1p1n_w_mcsubtr.at(version).FindBin(taupt);
        return hhfake_histDict::h_ff_2dobj_1p1n_w_mcsubtr.at(version).GetBinContent(bin);
      } 
      else{
        return hhfake_histDict::h_ff_2dobj_1p1n_w.at(version).GetBinContent(bin);
      }
    }
    else if( decaymode == 2){
      int bin = hhfake_histDict::h_ff_2dobj_1pXn_w.at(version).FindBin(taupt);
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_1pXn_nm){
        return hhfake_histDict::h_ff_2dobj_1pXn_w.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_1pXn_w.at(version).GetBinError(bin);
      } else if(syst == hhfake_histDict::syst::hh_fake_ff_mcsubtr_1pXn_nm){
        bin = hhfake_histDict::h_ff_2dobj_1pXn_w_mcsubtr.at(version).FindBin(taupt);
        return hhfake_histDict::h_ff_2dobj_1pXn_w_mcsubtr.at(version).GetBinContent(bin);
      }
      else{
        return hhfake_histDict::h_ff_2dobj_1pXn_w.at(version).GetBinContent(bin);
      }
    }
    else if( decaymode == 3){
      int bin = hhfake_histDict::h_ff_2dobj_3p0n_w.at(version).FindBin(taupt);
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_3p0n_nm){
        return hhfake_histDict::h_ff_2dobj_3p0n_w.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_3p0n_w.at(version).GetBinError(bin);
      } else if(syst == hhfake_histDict::syst::hh_fake_ff_mcsubtr_3p0n_nm){
        bin = hhfake_histDict::h_ff_2dobj_3p0n_w_mcsubtr.at(version).FindBin(taupt);
        return hhfake_histDict::h_ff_2dobj_3p0n_w_mcsubtr.at(version).GetBinContent(bin);
      }
      else{
        return hhfake_histDict::h_ff_2dobj_3p0n_w.at(version).GetBinContent(bin);
      }
    }
    else{
      return 1;
    }
  }

  float _read_ff_w_lnm(float taupt, int decaymode, int version, int syst){
    if(decaymode == 0){
      int bin = hhfake_histDict::h_ff_2dobj_1p0n_w_lnm.at(version).FindBin(taupt);
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_1p0n_lnm){
        return hhfake_histDict::h_ff_2dobj_1p0n_w_lnm.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_1p0n_w_lnm.at(version).GetBinError(bin);
      } else if(syst == hhfake_histDict::syst::hh_fake_ff_mcsubtr_1p0n_lnm){
        bin = hhfake_histDict::h_ff_2dobj_1p0n_w_lnm_mcsubtr.at(version).FindBin(taupt);
        return hhfake_histDict::h_ff_2dobj_1p0n_w_lnm_mcsubtr.at(version).GetBinContent(bin);
      }
      else{
        return hhfake_histDict::h_ff_2dobj_1p0n_w_lnm.at(version).GetBinContent(bin);
      }
    }
    else if( decaymode == 1){
      int bin = hhfake_histDict::h_ff_2dobj_1p1n_w_lnm.at(version).FindBin(taupt);  
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_1p1n_lnm){
        return hhfake_histDict::h_ff_2dobj_1p1n_w_lnm.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_1p1n_w_lnm.at(version).GetBinError(bin);
      } else if(syst == hhfake_histDict::syst::hh_fake_ff_mcsubtr_1p1n_lnm){
        bin = hhfake_histDict::h_ff_2dobj_1p1n_w_lnm_mcsubtr.at(version).FindBin(taupt);
        return hhfake_histDict::h_ff_2dobj_1p1n_w_lnm_mcsubtr.at(version).GetBinContent(bin);
      }
      else{
        return hhfake_histDict::h_ff_2dobj_1p1n_w_lnm.at(version).GetBinContent(bin);
      }
    }
    else if( decaymode == 2){
      int bin = hhfake_histDict::h_ff_2dobj_1pXn_w_lnm.at(version).FindBin(taupt);
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_1pXn_lnm){
        return hhfake_histDict::h_ff_2dobj_1pXn_w_lnm.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_1pXn_w_lnm.at(version).GetBinError(bin);
      } else if(syst == hhfake_histDict::syst::hh_fake_ff_mcsubtr_1pXn_lnm){
        bin = hhfake_histDict::h_ff_2dobj_1pXn_w_lnm_mcsubtr.at(version).FindBin(taupt);
        return hhfake_histDict::h_ff_2dobj_1pXn_w_lnm_mcsubtr.at(version).GetBinContent(bin);
      }
      else{
        return hhfake_histDict::h_ff_2dobj_1pXn_w_lnm.at(version).GetBinContent(bin);
      }
    }
    else if( decaymode == 3){
      int bin = hhfake_histDict::h_ff_2dobj_3p0n_w_lnm.at(version).FindBin(taupt);
      if(syst == hhfake_histDict::syst::hh_fake_ff_stat_3p0n_lnm){
        return hhfake_histDict::h_ff_2dobj_3p0n_w_lnm.at(version).GetBinContent(bin) + hhfake_histDict::h_ff_2dobj_3p0n_w_lnm.at(version).GetBinError(bin);
      } else if(syst == hhfake_histDict::syst::hh_fake_ff_mcsubtr_3p0n_lnm){
        bin = hhfake_histDict::h_ff_2dobj_3p0n_w_lnm_mcsubtr.at(version).FindBin(taupt);
        return hhfake_histDict::h_ff_2dobj_3p0n_w_lnm_mcsubtr.at(version).GetBinContent(bin);
      }
      else{
        return hhfake_histDict::h_ff_2dobj_3p0n_w_lnm.at(version).GetBinContent(bin);
      }
    }
    else { 
      return 1;
    }
  }

  // param unc 
  float _read_param_unc_weight(float mmc, int version){
    int bin = hhfake_histDict::h_ff_param_weights.at(version).FindBin(mmc);
    return hhfake_histDict::h_ff_param_weights.at(version).GetBinContent(bin);
  }
 

  // full FF application functions
  float _read_ff_objlvl_single_fake_same_sign(float tau1pt, float tau2pt, int tau1decaymode, int tau2decaymode, int tau1id, int tau2id, int version){
    // id: tau_loose_rnn + tau_medium_rnn, so it is 0 for notloose, 1 for loosenotmedium, 2 for medium
    if(tau1id == 1 and tau2id == 0){
      return -.5*_read_ff_lnm_lead(tau1pt, tau1decaymode, version)*_read_ff_nm(tau2pt, tau2decaymode, version); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 0 and tau2id == 1){
      return -.5*_read_ff_nm(tau1pt, tau1decaymode, version)*_read_ff_lnm_sublead(tau2pt, tau2decaymode, version); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 1 and tau2id == 1){ //loose-loose case, overlap of two templates, need to "double-count" that by adding up previous FF expressions
      return -.5*(_read_ff_nm(tau1pt, tau1decaymode, version)*_read_ff_lnm_sublead(tau2pt, tau2decaymode, version) + _read_ff_lnm_lead(tau1pt, tau1decaymode, version)*_read_ff_nm(tau2pt, tau2decaymode, version)); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 2 and tau2id < 2){
      return _read_ff_nm(tau2pt, tau2decaymode, version);
    }
    else if(tau1id < 2 and tau2id == 2){
      return _read_ff_nm(tau1pt, tau1decaymode, version);
    }
    else{
      return 0.;
    }
  }

  
  float _read_ff_objlvl_single_fake_high_deta(float tau1pt, float tau2pt, int tau1decaymode, int tau2decaymode, int tau1id, int tau2id, int version){
    // id: tau_loose_rnn + tau_medium_rnn, so it is 0 for notloose, 1 for loosenotmedium, 2 for medium
    if(tau1id == 1 and tau2id == 0){
      return -.5*_read_ff_hd_lead_lnm(tau1pt, tau1decaymode, version)*_read_ff_hd_sublead_nm(tau2pt, tau2decaymode, version); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 0 and tau2id == 1){
      return -.5*_read_ff_hd_lead_nm(tau1pt, tau1decaymode, version)*_read_ff_hd_sublead_lnm(tau2pt, tau2decaymode, version); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 1 and tau2id == 1){ //loose-loose case, overlap of two templates, need to "double-count" that by adding up previous FF expressions
      return -.5*(_read_ff_hd_lead_nm(tau1pt, tau1decaymode, version)*_read_ff_hd_sublead_lnm(tau2pt, tau2decaymode, version) + _read_ff_hd_lead_lnm(tau1pt, tau1decaymode, version)*_read_ff_hd_sublead_nm(tau2pt, tau2decaymode, version)); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 2 and tau2id < 2){
      return _read_ff_hd_sublead_nm(tau2pt, tau2decaymode, version);
    }
    else if(tau1id < 2 and tau2id == 2){
      return _read_ff_hd_lead_nm(tau1pt, tau1decaymode, version);
    }
    else{
      return 0.;
    }
  }
  

  float _read_ff_objlvl_single_fake_wcr(float tau1pt, float tau2pt, int tau1decaymode, int tau2decaymode, int tau1id, int tau2id, int version, int syst){
    // id: tau_loose_rnn + tau_medium_rnn, so it is 0 for notloose, 1 for loosenotmedium, 2 for medium
    if(tau1id == 1 and tau2id == 0){
      return -.5*_read_ff_w_lnm(tau1pt, tau1decaymode, version, syst)*_read_ff_w_nm(tau2pt, tau2decaymode, version, syst); // factor 0.5 to allow for using bigger template
    }
    else if(tau1id == 0 and tau2id == 1){
      return -.5*_read_ff_w_nm(tau1pt, tau1decaymode, version, syst)*_read_ff_w_lnm(tau2pt, tau2decaymode, version, syst);
    }
    else if(tau1id == 1 and tau2id == 1){ //loose-loose case, overlap of two templates, need to "double-count" that by adding up previous FF expressions
      return -.5*(_read_ff_w_nm(tau1pt, tau1decaymode, version, syst)*_read_ff_w_lnm(tau2pt, tau2decaymode, version, syst) + _read_ff_w_lnm(tau1pt, tau1decaymode, version, syst)*_read_ff_w_nm(tau2pt, tau2decaymode, version, syst));
    }
    else if(tau1id == 2 and tau2id < 2){
      return _read_ff_w_nm(tau2pt, tau2decaymode, version, syst);
    }
    else if(tau1id < 2 and tau2id == 2){
      return _read_ff_w_nm(tau1pt, tau1decaymode, version, syst);
    }
    else{
      return 0.;
    }
  }

  // wrapper function that decides which FFs to apply
  float read_hh_fakefactors(float tau1pt, float tau2pt, int tau1decaymode, int tau2decaymode, int tau1id, int tau2id, float mmc, int version, int syst){
    
    if(syst == hhfake_histDict::syst::hh_fake_ff_composition_ss){
      return _read_ff_objlvl_single_fake_same_sign(tau1pt, tau2pt, tau1decaymode, tau2decaymode, tau1id, tau2id, version);
    }
    else if(syst == hhfake_histDict::syst::hh_fake_ff_composition_highdeta){
      return _read_ff_objlvl_single_fake_high_deta(tau1pt, tau2pt, tau1decaymode, tau2decaymode, tau1id, tau2id, version);
    }
    else{  
      float mmcWeight = 1.;
      if(syst == hhfake_histDict::syst::hh_fake_ff_param){
        mmcWeight = _read_param_unc_weight(mmc, version); // insert some function reading a histogram here
      }
      return mmcWeight*_read_ff_objlvl_single_fake_wcr(tau1pt, tau2pt, tau1decaymode, tau2decaymode, tau1id, tau2id, version, syst);
    }
  }
}; // end the namespace
