#include <TLorentzVector.h>
#include <TMVA/Reader.h>

////////////////
class mvaDict
{
public:
  static inline TMVA::Reader* reader_0p00_to_0p25 = new TMVA::Reader();
  static inline TMVA::Reader* reader_0p25_to_0p50 = new TMVA::Reader();
  static inline TMVA::Reader* reader_0p50_to_0p75 = new TMVA::Reader();
  static inline TMVA::Reader* reader_0p75_to_1p00 = new TMVA::Reader();

};

namespace VBF_Tagger {

  float vbf_bdt_score(float pt_total, 
		      float jet_0_pt, 
		      float jet_0_eta, 
		      float jet_0_phi, 
		      float jet_0_m, 
		      float jet_1_pt, 
		      float jet_1_eta, 
		      float jet_1_phi, 
		      float jet_1_m,
		      float mva_random_number)
  {

    TLorentzVector jet_0_p4;
    TLorentzVector jet_1_p4;

    jet_0_p4.SetPtEtaPhiM(jet_0_pt, jet_0_eta, jet_0_phi, jet_0_m);
    jet_1_p4.SetPtEtaPhiM(jet_1_pt, jet_1_eta, jet_1_phi, jet_1_m);
    std::vector<double> inputs = {
        (double)pt_total, // pt_total
        (double)(jet_0_eta * jet_1_eta), // dijet_prod_eta
        (double)(jet_0_p4+jet_1_p4).Pt(), // diet_pt
        fabs(jet_0_p4.Eta() - jet_1_p4.Eta()), // dijet_deta
        (double)(jet_0_p4+jet_1_p4).M(), // dijet_m
        (double)jet_1_pt, // jet_1_pt
        fabs(jet_0_p4.DeltaPhi(jet_1_p4)), // DeltaPhi(j , j)
    };

    if (mva_random_number <= 0.25) 
      return mvaDict::reader_0p00_to_0p25->EvaluateMVA(inputs, "BDT");
    else if (mva_random_number <= 0.50) 
      return mvaDict::reader_0p25_to_0p50->EvaluateMVA(inputs, "BDT");
    else if (mva_random_number <= 0.75) 
      return mvaDict::reader_0p50_to_0p75->EvaluateMVA(inputs, "BDT");
    else if (mva_random_number <= 1.0) 
      return mvaDict::reader_0p75_to_1p00->EvaluateMVA(inputs, "BDT");
    else
      return  -9999.;

  }

  float pt_jj(float jet_0_pt, 
	      float jet_0_eta, 
	      float jet_0_phi, 
	      float jet_0_m, 
	      float jet_1_pt, 
	      float jet_1_eta, 
	      float jet_1_phi, 
	      float jet_1_m) 
  {
    TLorentzVector jet_0_p4;
    TLorentzVector jet_1_p4;
    jet_0_p4.SetPtEtaPhiM(jet_0_pt, jet_0_eta, jet_0_phi, jet_0_m);
    jet_1_p4.SetPtEtaPhiM(jet_1_pt, jet_1_eta, jet_1_phi, jet_1_m);
    return (jet_0_p4 + jet_1_p4).Pt();
  }

  float dr(float jet_0_pt, 
	   float jet_0_eta, 
	   float jet_0_phi, 
	   float jet_0_m, 
	   float jet_1_pt, 
	   float jet_1_eta, 
	   float jet_1_phi, 
	   float jet_1_m) 
  {
    TLorentzVector jet_0_p4;
    TLorentzVector jet_1_p4;
    jet_0_p4.SetPtEtaPhiM(jet_0_pt, jet_0_eta, jet_0_phi, jet_0_m);
    jet_1_p4.SetPtEtaPhiM(jet_1_pt, jet_1_eta, jet_1_phi, jet_1_m);
    return jet_0_p4.DeltaR(jet_1_p4);
  }



}// end the namespace
