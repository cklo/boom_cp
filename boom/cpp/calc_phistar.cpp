#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include <chrono>
#include <glob.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TRandom3.h>

namespace PhiStarCalc {

void ROOT_Swap_TLorentzVector(TLorentzVector& TLV_0, TLorentzVector& TLV_1 ){
    TLorentzVector TLV_Temp = TLV_0;
    TLV_0 = TLV_1;
    TLV_1 = TLV_Temp;
    return;
}

float Cal_1p_Upsilon(TLorentzVector charged, TLorentzVector neutral, const TLorentzVector &referenceFrame) {
    // only boost if we can boost into the reference frame
    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        charged.Boost(boostIntoReferenceFrame);
        neutral.Boost(boostIntoReferenceFrame);
    }
    return (charged.E() - neutral.E()) / (charged.E() + neutral.E());
}

float Cal_xTauFW_Upsilon_a1(TLorentzVector pion, TLorentzVector intermediate_rho, const TLorentzVector &referenceFrame) {

    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        pion.Boost(boostIntoReferenceFrame);
        intermediate_rho.Boost(boostIntoReferenceFrame);
    }
    float a1mass2 = (intermediate_rho + pion).M2();
    float y_a1 = (intermediate_rho.E() - pion.E()) / (intermediate_rho.E() + pion.E()) -
                 (a1mass2 - pion.M2() + intermediate_rho.M2()) / (2 * a1mass2);

    return y_a1;

}

float Cal_3p_rho0_Upsilon(TLorentzVector charged_same, TLorentzVector charged_oppo, const TLorentzVector &referenceFrame) {
    // only boost if we can boost into the reference frame
    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        charged_same.Boost(boostIntoReferenceFrame);
        charged_oppo.Boost(boostIntoReferenceFrame);
    }
    return (charged_same.E() - charged_oppo.E()) / (charged_same.E() + charged_oppo.E());
}


float Acoplanarity_RhoRho(TLorentzVector chargedPlus, TLorentzVector neutralPlus, TLorentzVector chargedMinus,
                                TLorentzVector neutralMinus, const TLorentzVector& referenceFrame) {
    // check that we do have valid charged and neutral 4 vectors
    //  if (chargedPlus.Mag() == 0 or neutralPlus.Mag() == 0 or chargedMinus.Mag() == 0 or neutralMinus.Mag() == 0) {
    //    return -5.;
    //  }
    // boost all 4-vectors into the reference frame
    TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
    chargedPlus.Boost(boostIntoReferenceFrame);
    neutralPlus.Boost(boostIntoReferenceFrame);
    chargedMinus.Boost(boostIntoReferenceFrame);
    neutralMinus.Boost(boostIntoReferenceFrame);

    // parallel projections of pi0 onto pi+/- direction
    TVector3 neutralParallelPlus = (neutralPlus.Vect() * chargedPlus.Vect().Unit()) * chargedPlus.Vect().Unit();
    TVector3 neutralParallelMinus = (neutralMinus.Vect() * chargedMinus.Vect().Unit()) * chargedMinus.Vect().Unit();

    // perpendicular component of pi0
    TVector3 neutralPerpPlus = (neutralPlus.Vect() - neutralParallelPlus).Unit();
    TVector3 neutralPerpMinus = (neutralMinus.Vect() - neutralParallelMinus).Unit();

    float triplecorr =
        chargedMinus.Vect().Dot(neutralPerpPlus.Cross(neutralPerpMinus));  // the T-odd triple correlation \mathcal(O)^{*}_{CP}

    float phistar = TMath::ACos(neutralPerpPlus.Dot(neutralPerpMinus));
    if (triplecorr < 0) phistar = 2 * TMath::Pi() - phistar;

    return phistar;

    // Do upsilon separation in xVarGroups.cxx
}


float Acoplanarity_IP(TLorentzVector piPlus, TLorentzVector ipVectorPlus, TLorentzVector piMinus, TLorentzVector ipVectorMinus,
                            const TLorentzVector& referenceFrame) {
    TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
    piPlus.Boost(boostIntoReferenceFrame);
    piMinus.Boost(boostIntoReferenceFrame);
    ipVectorPlus.Boost(boostIntoReferenceFrame);
    ipVectorMinus.Boost(boostIntoReferenceFrame);
    TVector3 ip3VectorPlus = ipVectorPlus.Vect();
    TVector3 ip3VectorMinus = ipVectorMinus.Vect();
    // parallel projections of impact vector onto pion direction
    TVector3 ipVectorParallelPlus = (ip3VectorPlus * piPlus.Vect().Unit()) * piPlus.Vect().Unit();
    TVector3 ipVectorParallelMinus = (ip3VectorMinus * piMinus.Vect().Unit()) * piMinus.Vect().Unit();
    // perpendicular component of impact vector
    TVector3 ipVectorPerpPlus = (ip3VectorPlus - ipVectorParallelPlus).Unit();
    TVector3 ipVectorPerpMinus = (ip3VectorMinus - ipVectorParallelMinus).Unit();
    // Triple-odd correlation
    float triplecorr = piMinus.Vect().Unit().Dot(ipVectorPerpPlus.Cross(ipVectorPerpMinus));

    float phistar = TMath::ACos(ipVectorPerpPlus * ipVectorPerpMinus);
    if (triplecorr < 0) { phistar = TMath::TwoPi() - phistar; }

    return phistar;
}

float Acoplanarity_IP_rho(TLorentzVector track, TLorentzVector ipVector_track, TLorentzVector charged_rho, TLorentzVector neutral_rho,
                                const TLorentzVector& referenceFrame, bool track_is_plus) {
    TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
    track.Boost(boostIntoReferenceFrame);
    ipVector_track.Boost(boostIntoReferenceFrame);
    charged_rho.Boost(boostIntoReferenceFrame);
    neutral_rho.Boost(boostIntoReferenceFrame);

    // IP vector
    TVector3 ip3Vector_track = ipVector_track.Vect();

    // parallel projection of impact vector onto track direction
    TVector3 ipVectorParallel_track = (ip3Vector_track * track.Vect().Unit()) * track.Vect().Unit();
    // Perpendicular component of impact vector
    TVector3 ipVectorPerp_track = (ip3Vector_track - ipVectorParallel_track).Unit();

    // Parallel projections of pi0 onto pi+/- direction
    TVector3 neutralParallel = (neutral_rho.Vect() * charged_rho.Vect().Unit()) * charged_rho.Vect().Unit();
    // Perpendicular component of pi0
    TVector3 neutralPerp = (neutral_rho.Vect() - neutralParallel).Unit();

    // the T-odd triple correlation (\mathcal(O)^{*}_{CP}) depends on the charge
    float triplecorr = track.Vect().Dot(neutralPerp.Cross(ipVectorPerp_track));
    if (track_is_plus) triplecorr = charged_rho.Vect().Dot(ipVectorPerp_track.Cross(neutralPerp));

    float phistar = TMath::ACos(neutralPerp.Dot(ipVectorPerp_track));
    if (triplecorr < 0) phistar = 2 * TMath::Pi() - phistar;

    return phistar;
}

TLorentzVector Retrieve_IP_from_track(float ditau_CP_ip_tau1_x_ip,float ditau_CP_ip_tau1_y_ip,float ditau_CP_ip_tau1_z_ip){
    TVector3 Impact_parameter_From_ntuple;
    Impact_parameter_From_ntuple.SetXYZ(ditau_CP_ip_tau1_x_ip,ditau_CP_ip_tau1_y_ip,ditau_CP_ip_tau1_z_ip);
    TLorentzVector TLV_IP = TLorentzVector(Impact_parameter_From_ntuple.Unit(),0); //need normalize
    return TLV_IP;
}



vector<TLorentzVector> Get_C_Closest(TLorentzVector tau_0_track0_p4,TLorentzVector tau_0_track1_p4,TLorentzVector tau_0_track2_p4,float tau_0_track0_q,float tau_0_track1_q,float tau_0_track2_q,float tau_0_q){

    vector<TLorentzVector> Components;


    TLorentzVector Pion_3p_OS_C;
    TLorentzVector rho_3p_OS_C;
    TLorentzVector Same_charge_OS_C;
    TLorentzVector Oppo_Charge_OS_C;


    if(tau_0_q*tau_0_track0_q<0){
        TLorentzVector ir_0 =tau_0_track0_p4 +tau_0_track1_p4;
        TLorentzVector ir_1 =tau_0_track0_p4 +tau_0_track2_p4;
        if(abs(ir_0.M()-0.775) < abs(ir_1.M()-0.775)){
            rho_3p_OS_C = ir_0;
            Pion_3p_OS_C =tau_0_track2_p4;
            Same_charge_OS_C =tau_0_track1_p4;
            Oppo_Charge_OS_C =tau_0_track0_p4;
        }
        else{
            rho_3p_OS_C = ir_1;
            Pion_3p_OS_C =tau_0_track1_p4;
            Same_charge_OS_C =tau_0_track2_p4;
            Oppo_Charge_OS_C =tau_0_track0_p4;
        }

    }
    else if(tau_0_q*tau_0_track1_q<0){
        TLorentzVector ir_0 =tau_0_track1_p4 +tau_0_track0_p4;
        TLorentzVector ir_1 =tau_0_track1_p4 +tau_0_track2_p4;
        if(abs(ir_0.M()-0.775) < abs(ir_1.M()-0.775)){
            rho_3p_OS_C = ir_0;
            Pion_3p_OS_C =tau_0_track2_p4;
            Same_charge_OS_C =tau_0_track0_p4;
            Oppo_Charge_OS_C =tau_0_track1_p4;
        }
        else{
            rho_3p_OS_C = ir_1;
            Pion_3p_OS_C =tau_0_track0_p4;
            Same_charge_OS_C =tau_0_track2_p4;
            Oppo_Charge_OS_C =tau_0_track1_p4;
        }
    }
    else if(tau_0_q*tau_0_track2_q<0){
        TLorentzVector ir_0 =tau_0_track2_p4 +tau_0_track0_p4;
        TLorentzVector ir_1 =tau_0_track2_p4 +tau_0_track1_p4;
        if(abs(ir_0.M()-0.775) < abs(ir_1.M()-0.775)){
            rho_3p_OS_C = ir_0;
            Pion_3p_OS_C =tau_0_track1_p4;
            Same_charge_OS_C =tau_0_track0_p4;
            Oppo_Charge_OS_C =tau_0_track2_p4;
        }
        else{
            rho_3p_OS_C = ir_1;
            Pion_3p_OS_C =tau_0_track0_p4;
            Same_charge_OS_C =tau_0_track1_p4;
            Oppo_Charge_OS_C =tau_0_track2_p4;
        }
    }
    else{
        cout<<"Error"<<endl;
    }


    Components.push_back(Same_charge_OS_C);
    Components.push_back(Oppo_Charge_OS_C);




}


float Calculate_phistar_1p1p(int tau_0_decay_mode,int tau_1_decay_mode,float ditau_qxq,float tau_0_q,float tau_1_q,TLorentzVector tau_0_decay_charged_p4,TLorentzVector tau_0_decay_neutral_p4,TLorentzVector tau_1_decay_charged_p4,TLorentzVector tau_1_decay_neutral_p4,float ditau_CP_ip_tau0_x_ip,float ditau_CP_ip_tau0_y_ip,float ditau_CP_ip_tau0_z_ip,float ditau_CP_ip_tau1_x_ip,float ditau_CP_ip_tau1_y_ip,float ditau_CP_ip_tau1_z_ip){

    TLorentzVector reference_frame = tau_0_decay_charged_p4 + tau_0_decay_neutral_p4 + tau_1_decay_charged_p4 + tau_1_decay_neutral_p4;

    if(ditau_qxq>0){
        return -1234;
    }

    if((tau_0_decay_mode == 1 || tau_0_decay_mode == 2 ) && (tau_1_decay_mode == 1 || tau_1_decay_mode == 2 )){ //1p1n-1p1n or 1pXn, I will not add specific shifting for 1pXn. Please add additional cut if needed.
        TLorentzVector Charged_plus = tau_0_decay_charged_p4;
        TLorentzVector Neutral_plus = tau_0_decay_neutral_p4;
        TLorentzVector Charged_minus = tau_1_decay_charged_p4;
        TLorentzVector Neutral_minus = tau_1_decay_neutral_p4;
        if(tau_1_q>0){
            ROOT_Swap_TLorentzVector(Charged_plus,Charged_minus);
            ROOT_Swap_TLorentzVector(Neutral_plus,Neutral_minus);
        }
        float phistar_1p1n_1p1n = Acoplanarity_RhoRho(Charged_plus, Neutral_plus, Charged_minus,Neutral_minus,reference_frame);
        float y0y1 = Cal_1p_Upsilon(Charged_plus,Neutral_plus,reference_frame) *Cal_1p_Upsilon(Charged_minus,Neutral_minus,reference_frame);
        if (y0y1 < 0) {
            if (phistar_1p1n_1p1n > TMath::Pi())
                phistar_1p1n_1p1n -= TMath::Pi();
            else
                phistar_1p1n_1p1n += TMath::Pi();
        }
        return phistar_1p1n_1p1n;
    }


    if((tau_0_decay_mode == 1 || tau_0_decay_mode == 2 ) && tau_1_decay_mode == 0){ //1p1n-1p0n or 1pXn-1p0n
        TLorentzVector tau1_ip_retrieved = Retrieve_IP_from_track(ditau_CP_ip_tau1_x_ip,ditau_CP_ip_tau1_y_ip,ditau_CP_ip_tau1_z_ip);
        bool tau1_is_positive = true;
        if(tau_1_q<0){
            tau1_is_positive = false;
        }

        float phistar_1p1n_1p0n = Acoplanarity_IP_rho(tau_1_decay_charged_p4,tau1_ip_retrieved,tau_0_decay_charged_p4,tau_0_decay_neutral_p4,reference_frame,tau1_is_positive);

        float the_rho_part_upsilon = Cal_1p_Upsilon(tau_0_decay_charged_p4,tau_0_decay_neutral_p4,reference_frame);
        if (the_rho_part_upsilon > 0) { /* OK */
        } else {
            // Add +/- pi
            if (phistar_1p1n_1p0n < TMath::Pi())
                phistar_1p1n_1p0n += TMath::Pi();
            else
                phistar_1p1n_1p0n -= TMath::Pi();
        }
        return phistar_1p1n_1p0n;
    }


    if(tau_0_decay_mode == 0 && (tau_1_decay_mode == 1 || tau_1_decay_mode == 2 )){ //1p0n-1p1n or 1p0n-1pXn
        TLorentzVector tau0_ip_retrieved = Retrieve_IP_from_track(ditau_CP_ip_tau0_x_ip,ditau_CP_ip_tau0_y_ip,ditau_CP_ip_tau0_z_ip);
        bool tau0_is_positive = true;
        if(tau_0_q<0){
            tau0_is_positive = false;
        }

        float phistar_1p0n_1p1n = Acoplanarity_IP_rho(tau_0_decay_charged_p4,tau0_ip_retrieved,tau_1_decay_charged_p4,tau_1_decay_neutral_p4,reference_frame,tau0_is_positive);

        float the_rho_part_upsilon = Cal_1p_Upsilon(tau_1_decay_charged_p4,tau_1_decay_neutral_p4,reference_frame);
        if (the_rho_part_upsilon > 0) { /* OK */
        } else {
            // Add +/- pi
            if (phistar_1p0n_1p1n < TMath::Pi())
                phistar_1p0n_1p1n += TMath::Pi();
            else
                phistar_1p0n_1p1n -= TMath::Pi();
        }
        return phistar_1p0n_1p1n;


    }
    if(tau_0_decay_mode == 0 && tau_1_decay_mode == 0){
        TLorentzVector tau0_ip_retrieved = Retrieve_IP_from_track(ditau_CP_ip_tau0_x_ip,ditau_CP_ip_tau0_y_ip,ditau_CP_ip_tau0_z_ip);
        TLorentzVector tau1_ip_retrieved = Retrieve_IP_from_track(ditau_CP_ip_tau1_x_ip,ditau_CP_ip_tau1_y_ip,ditau_CP_ip_tau1_z_ip);

        TLorentzVector Charged_plus = tau_0_decay_charged_p4;
        TLorentzVector IP_plus = tau0_ip_retrieved;
        TLorentzVector Charged_minus = tau_1_decay_charged_p4;
        TLorentzVector IP_minus = tau1_ip_retrieved;
        if(tau_1_q>0){
            ROOT_Swap_TLorentzVector(Charged_plus,Charged_minus);
            ROOT_Swap_TLorentzVector(IP_plus,IP_minus);
        }

        float phistar_1p0n_1p0n = Acoplanarity_IP(Charged_plus,IP_plus,Charged_minus,IP_minus,reference_frame);

        return phistar_1p0n_1p0n;

    }

    return -1234;


}

float Calculate_phistar_3p1p(int tau_0_decay_mode,int tau_1_decay_mode,float ditau_qxq,float tau_0_q,float tau_1_q,TLorentzVector tau_0_decay_charged_p4,TLorentzVector tau_0_decay_neutral_p4,TLorentzVector tau_1_decay_charged_p4,TLorentzVector tau_1_decay_neutral_p4,float ditau_CP_ip_tau0_x_ip,float ditau_CP_ip_tau0_y_ip,float ditau_CP_ip_tau0_z_ip,float ditau_CP_ip_tau1_x_ip,float ditau_CP_ip_tau1_y_ip,float ditau_CP_ip_tau1_z_ip,TLorentzVector tau_0_track0_p4,TLorentzVector tau_0_track1_p4,TLorentzVector tau_0_track2_p4, TLorentzVector tau_1_track0_p4,TLorentzVector tau_1_track1_p4,TLorentzVector tau_1_track2_p4   ){ //,float tau_0_track0_q,float tau_0_track1_q,float tau_0_track2_q){

    TLorentzVector reference_frame = tau_0_decay_charged_p4 + tau_0_decay_neutral_p4 + tau_1_decay_charged_p4 + tau_1_decay_neutral_p4;

    if(ditau_qxq>0){
        return -1234;
    }

    if(tau_0_decay_mode == 3 && tau_1_decay_mode == 1){ //3p0n-1p1n


        TLorentzVector LTV_3p_pion_lowest_pT = tau_0_track0_p4;
        TLorentzVector LTV_3p_rho_lowest_pT = tau_0_track1_p4+ tau_0_track2_p4;

        TLorentzVector Charged_plus = LTV_3p_pion_lowest_pT;
        TLorentzVector Neutral_plus = LTV_3p_rho_lowest_pT;
        TLorentzVector Charged_minus = tau_1_decay_charged_p4;
        TLorentzVector Neutral_minus = tau_1_decay_neutral_p4;


        if(tau_1_q>0){
            ROOT_Swap_TLorentzVector(Charged_plus,Charged_minus);
            ROOT_Swap_TLorentzVector(Neutral_plus,Neutral_minus);
        }
        float phistar_1p3n_1p1n_lowest_pT = Acoplanarity_RhoRho(Charged_plus, Neutral_plus, Charged_minus,Neutral_minus,reference_frame);
        float y0a1y1 = Cal_xTauFW_Upsilon_a1(LTV_3p_pion_lowest_pT,LTV_3p_rho_lowest_pT,reference_frame) *Cal_1p_Upsilon(tau_1_decay_charged_p4,tau_1_decay_neutral_p4,reference_frame);

        if (phistar_1p3n_1p1n_lowest_pT < TMath::Pi())
            phistar_1p3n_1p1n_lowest_pT += TMath::Pi();
        else
            phistar_1p3n_1p1n_lowest_pT -= TMath::Pi();
  
        if (y0a1y1 < 0) {
            if (phistar_1p3n_1p1n_lowest_pT < TMath::Pi())
	         phistar_1p3n_1p1n_lowest_pT += TMath::Pi();
            else
                phistar_1p3n_1p1n_lowest_pT -= TMath::Pi();
        }
             
        return phistar_1p3n_1p1n_lowest_pT; //3p0n-1p1n-lowest_pT-a1-method

        /*

        vector<TLorentzVector> TLV_rho0rho_components = Get_C_Closest(tau_0_track0_p4,tau_0_track1_p4,tau_0_track2_p4,tau_0_track0_q,tau_0_track1_q,tau_0_track2_q,tau_0_q);
        Charged_plus = TLV_rho0rho_components[0];
        Charged_minus = TLV_rho0rho_components[1];
        Charged_minus = tau_1_decay_charged_p4;
        Neutral_minus = tau_1_decay_neutral_p4;
        if(tau_1_q>0){
            ROOT_Swap_TLorentzVector(Charged_plus,Charged_minus);
            ROOT_Swap_TLorentzVector(Neutral_plus,Neutral_minus);
        }
        float phistar_1p3n_1p1n_C_closest = Acoplanarity_RhoRho(Charged_plus, Neutral_plus, Charged_minus,Neutral_minus,reference_frame);
        float y0rho0y1 = Cal_3p_rho0_Upsilon(TLV_rho0rho_components[0],TLV_rho0rho_components[1],reference_frame) *Cal_1p_Upsilon(tau_1_decay_charged_p4,tau_1_decay_neutral_p4,reference_frame);
        if (y0rho0y1 < 0) {
            if (phistar_1p3n_1p1n_C_closest > TMath::Pi())
                phistar_1p3n_1p1n_C_closest -= TMath::Pi();
            else
                phistar_1p3n_1p1n_C_closest += TMath::Pi();
        }
        Phistar_result.push_back(phistar_1p3n_1p1n_C_closest);//3p0n-1p1n-Oppo_charged_closest_mass_rho0-method
      
        */

    } else if( tau_1_decay_mode == 3 && tau_0_decay_mode == 1){ //1p1n-3p0n


        TLorentzVector LTV_3p_pion_lowest_pT = tau_1_track0_p4;
        TLorentzVector LTV_3p_rho_lowest_pT = tau_1_track1_p4 + tau_1_track2_p4;

        TLorentzVector Charged_plus = LTV_3p_pion_lowest_pT;
        TLorentzVector Neutral_plus = LTV_3p_rho_lowest_pT;
        TLorentzVector Charged_minus = tau_0_decay_charged_p4;
        TLorentzVector Neutral_minus = tau_0_decay_neutral_p4;


        if(tau_0_q>0){
            ROOT_Swap_TLorentzVector(Charged_plus,Charged_minus);
            ROOT_Swap_TLorentzVector(Neutral_plus,Neutral_minus);
        }
        float phistar_1p3n_1p1n_lowest_pT = Acoplanarity_RhoRho(Charged_plus, Neutral_plus, Charged_minus,Neutral_minus,reference_frame);
        float y0a1y1 = Cal_xTauFW_Upsilon_a1(LTV_3p_pion_lowest_pT,LTV_3p_rho_lowest_pT,reference_frame) *Cal_1p_Upsilon(tau_0_decay_charged_p4,tau_0_decay_neutral_p4,reference_frame);

        if (phistar_1p3n_1p1n_lowest_pT < TMath::Pi())
            phistar_1p3n_1p1n_lowest_pT += TMath::Pi();
        else
            phistar_1p3n_1p1n_lowest_pT -= TMath::Pi();

        if (y0a1y1 < 0) {
            if (phistar_1p3n_1p1n_lowest_pT < TMath::Pi())
                phistar_1p3n_1p1n_lowest_pT += TMath::Pi();
            else
                phistar_1p3n_1p1n_lowest_pT -= TMath::Pi();
        }

        return phistar_1p3n_1p1n_lowest_pT;
    }

    return -1234;


}

float calc_phistar( float tau_0_decay_mode,
                    float tau_1_decay_mode,
                    float ditau_qxq,
                    float tau_0_q,
                    float tau_1_q,
                    float tau_0_decay_charged_p4_pt,
                    float tau_0_decay_charged_p4_eta,
                    float tau_0_decay_charged_p4_phi,
                    float tau_0_decay_charged_p4_m,
                    float tau_0_decay_neutral_p4_pt,
                    float tau_0_decay_neutral_p4_eta,
                    float tau_0_decay_neutral_p4_phi,
                    float tau_0_decay_neutral_p4_m, 
                    float tau_1_decay_charged_p4_pt,
                    float tau_1_decay_charged_p4_eta,
                    float tau_1_decay_charged_p4_phi,
                    float tau_1_decay_charged_p4_m,
                    float tau_1_decay_neutral_p4_pt,
                    float tau_1_decay_neutral_p4_eta,
                    float tau_1_decay_neutral_p4_phi,
                    float tau_1_decay_neutral_p4_m,
                    float ditau_CP_ip_tau0_x_ip,
                    float ditau_CP_ip_tau0_y_ip, 
                    float ditau_CP_ip_tau0_z_ip,
                    float ditau_CP_ip_tau1_x_ip,
                    float ditau_CP_ip_tau1_y_ip,
                    float ditau_CP_ip_tau1_z_ip,
                    float tau_0_track0_p4_pt,
                    float tau_0_track0_p4_eta,
                    float tau_0_track0_p4_phi,
                    float tau_0_track0_p4_m,
                    float tau_0_track1_p4_pt,
                    float tau_0_track1_p4_eta,
                    float tau_0_track1_p4_phi,
                    float tau_0_track1_p4_m,
                    float tau_0_track2_p4_pt,
                    float tau_0_track2_p4_eta,
                    float tau_0_track2_p4_phi,
                    float tau_0_track2_p4_m,
                    float tau_1_track0_p4_pt,
                    float tau_1_track0_p4_eta,
                    float tau_1_track0_p4_phi,
                    float tau_1_track0_p4_m,
                    float tau_1_track1_p4_pt,
                    float tau_1_track1_p4_eta,
                    float tau_1_track1_p4_phi,
                    float tau_1_track1_p4_m,
                    float tau_1_track2_p4_pt,
                    float tau_1_track2_p4_eta,
                    float tau_1_track2_p4_phi,
                    float tau_1_track2_p4_m,
                    float event_number,
                    bool apply_energy_scale_up,
                    bool apply_energy_scale_down,
                    bool apply_eta_smearing,
                    bool apply_phi_smearing,
                    bool apply_etaphi_smearing)
                    
{

    // set four-momentum
    TLorentzVector tau_0_decay_charged_p4;
    TLorentzVector tau_0_decay_neutral_p4;
    TLorentzVector tau_1_decay_charged_p4;
    TLorentzVector tau_1_decay_neutral_p4;
    TLorentzVector tau_0_track0_p4;
    TLorentzVector tau_0_track1_p4;
    TLorentzVector tau_0_track2_p4;
    TLorentzVector tau_1_track0_p4;
    TLorentzVector tau_1_track1_p4;
    TLorentzVector tau_1_track2_p4;

    tau_0_decay_charged_p4.SetPtEtaPhiM(tau_0_decay_charged_p4_pt, tau_0_decay_charged_p4_eta, tau_0_decay_charged_p4_phi, tau_0_decay_charged_p4_m);
    tau_0_decay_neutral_p4.SetPtEtaPhiM(tau_0_decay_neutral_p4_pt, tau_0_decay_neutral_p4_eta, tau_0_decay_neutral_p4_phi, tau_0_decay_neutral_p4_m);
    tau_1_decay_charged_p4.SetPtEtaPhiM(tau_1_decay_charged_p4_pt, tau_1_decay_charged_p4_eta, tau_1_decay_charged_p4_phi, tau_1_decay_charged_p4_m);
    tau_1_decay_neutral_p4.SetPtEtaPhiM(tau_1_decay_neutral_p4_pt, tau_1_decay_neutral_p4_eta, tau_1_decay_neutral_p4_phi, tau_1_decay_neutral_p4_m);
    tau_0_track0_p4.SetPtEtaPhiM(tau_0_track0_p4_pt, tau_0_track0_p4_eta, tau_0_track0_p4_phi, tau_0_track0_p4_m);
    tau_0_track1_p4.SetPtEtaPhiM(tau_0_track1_p4_pt, tau_0_track1_p4_eta, tau_0_track1_p4_phi, tau_0_track1_p4_m);
    tau_0_track2_p4.SetPtEtaPhiM(tau_0_track2_p4_pt, tau_0_track2_p4_eta, tau_0_track2_p4_phi, tau_0_track2_p4_m);    
    tau_1_track0_p4.SetPtEtaPhiM(tau_1_track0_p4_pt, tau_1_track0_p4_eta, tau_1_track0_p4_phi, tau_1_track0_p4_m);
    tau_1_track1_p4.SetPtEtaPhiM(tau_1_track1_p4_pt, tau_1_track1_p4_eta, tau_1_track1_p4_phi, tau_1_track1_p4_m);
    tau_1_track2_p4.SetPtEtaPhiM(tau_1_track2_p4_pt, tau_1_track2_p4_eta, tau_1_track2_p4_phi, tau_1_track2_p4_m);

    // apply energy scaling 
    if(apply_energy_scale_up){
        float tau_0_energy_scale = 1.0;
        if(tau_0_decay_mode == 1) tau_0_energy_scale = 1.2;
        else if (tau_0_decay_mode == 2) tau_0_energy_scale = 1.4;

        tau_0_decay_neutral_p4.SetE(tau_0_decay_neutral_p4.E()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPx(tau_0_decay_neutral_p4.Px()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPy(tau_0_decay_neutral_p4.Py()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPz(tau_0_decay_neutral_p4.Pz()*tau_0_energy_scale);


        float tau_1_energy_scale = 1.0;
        if(tau_1_decay_mode == 1) tau_1_energy_scale = 1.2;
        else if (tau_1_decay_mode == 2) tau_1_energy_scale = 1.4;

        tau_1_decay_neutral_p4.SetE(tau_1_decay_neutral_p4.E()*tau_1_energy_scale);
        tau_1_decay_neutral_p4.SetPx(tau_1_decay_neutral_p4.Px()*tau_1_energy_scale);
        tau_1_decay_neutral_p4.SetPy(tau_1_decay_neutral_p4.Py()*tau_1_energy_scale);
        tau_1_decay_neutral_p4.SetPz(tau_1_decay_neutral_p4.Pz()*tau_1_energy_scale);
    
    }
 
    if(apply_energy_scale_down){
        float tau_0_energy_scale = 1.0;
        if(tau_0_decay_mode == 1) tau_0_energy_scale = 0.8;
        else if (tau_0_decay_mode == 2) tau_0_energy_scale = 0.6;

        tau_0_decay_neutral_p4.SetE(tau_0_decay_neutral_p4.E()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPx(tau_0_decay_neutral_p4.Px()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPy(tau_0_decay_neutral_p4.Py()*tau_0_energy_scale);
        tau_0_decay_neutral_p4.SetPz(tau_0_decay_neutral_p4.Pz()*tau_0_energy_scale);

        float tau_1_energy_scale = 1.0;
        if(tau_1_decay_mode == 1) tau_1_energy_scale = 0.8;
        else if (tau_1_decay_mode == 2) tau_1_energy_scale = 0.6;

        tau_1_decay_neutral_p4.SetE(tau_1_decay_neutral_p4.E()*tau_1_energy_scale);
        tau_1_decay_neutral_p4.SetPx(tau_1_decay_neutral_p4.Px()*tau_1_energy_scale);
        tau_1_decay_neutral_p4.SetPy(tau_1_decay_neutral_p4.Py()*tau_1_energy_scale);
        tau_1_decay_neutral_p4.SetPz(tau_1_decay_neutral_p4.Pz()*tau_1_energy_scale);
    }

    if(apply_eta_smearing){
   
       TRandom3 eta_smear(event_number);
       float smearEta = 5*4e-3;
       tau_0_decay_neutral_p4.SetPtEtaPhiM(tau_0_decay_neutral_p4_pt, tau_0_decay_neutral_p4_eta + eta_smear.Gaus(0, smearEta), tau_0_decay_neutral_p4_phi, tau_0_decay_neutral_p4_m);
       tau_1_decay_neutral_p4.SetPtEtaPhiM(tau_1_decay_neutral_p4_pt, tau_1_decay_neutral_p4_eta + eta_smear.Gaus(0, smearEta), tau_1_decay_neutral_p4_phi, tau_1_decay_neutral_p4_m);  
    } 

    if(apply_phi_smearing){

       TRandom3 phi_smear(event_number);
       float smearPhi = 5*10e-3;
       tau_0_decay_neutral_p4.SetPtEtaPhiM(tau_0_decay_neutral_p4_pt, tau_0_decay_neutral_p4_eta, tau_0_decay_neutral_p4_phi + phi_smear.Gaus(0, smearPhi), tau_0_decay_neutral_p4_m);
       tau_1_decay_neutral_p4.SetPtEtaPhiM(tau_1_decay_neutral_p4_pt, tau_1_decay_neutral_p4_eta, tau_1_decay_neutral_p4_phi + phi_smear.Gaus(0, smearPhi), tau_1_decay_neutral_p4_m);
    }

    if(apply_etaphi_smearing){ 
       TRandom3 smear(event_number);
       float smearEta = 5*4e-3;
       float smearPhi = 5*10e-3;
       tau_0_decay_neutral_p4.SetPtEtaPhiM(tau_0_decay_neutral_p4_pt, tau_0_decay_neutral_p4_eta + smear.Gaus(0, smearEta), tau_0_decay_neutral_p4_phi, tau_0_decay_neutral_p4_m);
       tau_1_decay_neutral_p4.SetPtEtaPhiM(tau_1_decay_neutral_p4_pt, tau_1_decay_neutral_p4_eta + smear.Gaus(0, smearEta), tau_1_decay_neutral_p4_phi, tau_1_decay_neutral_p4_m);
       tau_0_decay_neutral_p4.SetPtEtaPhiM(tau_0_decay_neutral_p4_pt, tau_0_decay_neutral_p4_eta, tau_0_decay_neutral_p4_phi + smear.Gaus(0, smearPhi), tau_0_decay_neutral_p4_m);
       tau_1_decay_neutral_p4.SetPtEtaPhiM(tau_1_decay_neutral_p4_pt, tau_1_decay_neutral_p4_eta, tau_1_decay_neutral_p4_phi + smear.Gaus(0, smearPhi), tau_1_decay_neutral_p4_m);
    }

    if( tau_0_decay_mode <= 2 && tau_1_decay_mode  <= 2){ // two 1 prong taus

        return  Calculate_phistar_1p1p(tau_0_decay_mode,
                                       tau_1_decay_mode,
                                       ditau_qxq,
                                       tau_0_q,
                                       tau_1_q,
                                       tau_0_decay_charged_p4,
                                       tau_0_decay_neutral_p4,
                                       tau_1_decay_charged_p4,
                                       tau_1_decay_neutral_p4,
                                       ditau_CP_ip_tau0_x_ip,
                                       ditau_CP_ip_tau0_y_ip,
                                       ditau_CP_ip_tau0_z_ip,
                                       ditau_CP_ip_tau1_x_ip,
                                       ditau_CP_ip_tau1_y_ip,
                                       ditau_CP_ip_tau1_z_ip);

    } else if ( (tau_0_decay_mode == 1 && tau_1_decay_mode == 3) || (tau_0_decay_mode == 3 && tau_1_decay_mode == 1)) { // 1 tau is 1 prong and the other is 3 prong 

        return  Calculate_phistar_3p1p(tau_0_decay_mode,
                                      tau_1_decay_mode,
                                      ditau_qxq,
                                      tau_0_q,
                                      tau_1_q,
                                      tau_0_decay_charged_p4,
                                      tau_0_decay_neutral_p4,
                                      tau_1_decay_charged_p4,
                                      tau_1_decay_neutral_p4,
                                      ditau_CP_ip_tau0_x_ip,
                                      ditau_CP_ip_tau0_y_ip,
                                      ditau_CP_ip_tau0_z_ip,
                                      ditau_CP_ip_tau1_x_ip,
                                      ditau_CP_ip_tau1_y_ip,
                                      ditau_CP_ip_tau1_z_ip, 
                                      tau_0_track0_p4,
                                      tau_0_track1_p4,
                                      tau_0_track2_p4,
                                      tau_1_track0_p4,
                                      tau_1_track1_p4,
                                      tau_1_track2_p4);
    } else {
       return -1234;
    }
}

}

