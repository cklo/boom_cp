#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include <TRandom3.h>
#include "TStopwatch.h"
#include <TLorentzVector.h>
#include <TVector3.h>


namespace TauUpsilonCalc {

float calc_tau_upsilon(
                    float tau_decay_mode, 
                    float tau_decay_charged_p4_pt,
                    float tau_decay_charged_p4_eta,
                    float tau_decay_charged_p4_phi,
                    float tau_decay_charged_p4_m,
                    float tau_decay_neutral_p4_pt,
                    float tau_decay_neutral_p4_eta,
                    float tau_decay_neutral_p4_phi,
                    float tau_decay_neutral_p4_m,
                    float event_number,
                    bool apply_energy_scale_up,
                    bool apply_energy_scale_down
                  ) 
{
  
    TLorentzVector tau_decay_charged_p4;
    TLorentzVector tau_decay_neutral_p4;

    tau_decay_charged_p4.SetPtEtaPhiM(tau_decay_charged_p4_pt, tau_decay_charged_p4_eta, tau_decay_charged_p4_phi, tau_decay_charged_p4_m);
    tau_decay_neutral_p4.SetPtEtaPhiM(tau_decay_neutral_p4_pt, tau_decay_neutral_p4_eta, tau_decay_neutral_p4_phi, tau_decay_neutral_p4_m);

    if(apply_energy_scale_up){
        float tau_energy_scale = 1.0;
        if(tau_decay_mode == 1) tau_energy_scale = 1.2;
        else if (tau_decay_mode == 2) tau_energy_scale = 1.4;

        tau_decay_neutral_p4.SetE(tau_decay_neutral_p4.E()*tau_energy_scale);
        tau_decay_neutral_p4.SetPx(tau_decay_neutral_p4.Px()*tau_energy_scale);
        tau_decay_neutral_p4.SetPy(tau_decay_neutral_p4.Py()*tau_energy_scale);
        tau_decay_neutral_p4.SetPz(tau_decay_neutral_p4.Pz()*tau_energy_scale);
    }

    if(apply_energy_scale_down){
        float tau_energy_scale = 1.0;
        if(tau_decay_mode == 1) tau_energy_scale = 0.8;
        else if (tau_decay_mode == 2) tau_energy_scale = 0.6;

        tau_decay_neutral_p4.SetE(tau_decay_neutral_p4.E()*tau_energy_scale);
        tau_decay_neutral_p4.SetPx(tau_decay_neutral_p4.Px()*tau_energy_scale);
        tau_decay_neutral_p4.SetPy(tau_decay_neutral_p4.Py()*tau_energy_scale);
        tau_decay_neutral_p4.SetPz(tau_decay_neutral_p4.Pz()*tau_energy_scale);
    }

    float upsilon = (tau_decay_charged_p4.E()-tau_decay_neutral_p4.E())/(tau_decay_charged_p4.E()+tau_decay_neutral_p4.E());
    return upsilon;

}

}
