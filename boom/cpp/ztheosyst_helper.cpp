#include <TH2F.h>
#include <unordered_map>

class histoDict
{
public:
  static inline std::unordered_map<int, TH2F> h_qsf;
  static inline std::unordered_map<int, TH2F> h_ckkw;
};

namespace ZTheoSystHelper {

  float theo_weight(float Z_pt, unsigned int n_jets, int syst, int channel, int version)
  {  

    // correct the number of jets for hadhad and lephad
    if(channel == 2){
      n_jets = n_jets -2;
    } else if( channel == 1){
      n_jets = n_jets -1;
    }

    if( syst == 1){
      int bin = histoDict::h_qsf.at(version).FindBin(Z_pt, n_jets);
      return histoDict::h_qsf.at(version).GetBinContent(bin);
    } else{
      int bin = histoDict::h_ckkw.at(version).FindBin(Z_pt, n_jets);
      return histoDict::h_ckkw.at(version).GetBinContent(bin);
    }
  }

}; // end the namespace
