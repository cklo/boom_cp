#include <TRandom3.h>
#include <iostream>

namespace Rnd {

  bool zcr_split_cut(int event_number, float low, float high) {
    
    TRandom3 generator(event_number);
    float rnd_number = generator.Rndm();
    if (rnd_number >= low && rnd_number < high) {
      return true;
    } else {
      return false;
    }
    
  } // end of zll_split_cut

  // random number splitting for mva training and evaluation
  float mva_random_number(long event_number) {
    TRandom3 generator(event_number + 2020);
    float rnd_number = generator.Rndm();
    return rnd_number;
  }

};// end of the namespace



