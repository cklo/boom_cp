#include <TH1F.h>
#include <unordered_map>

class altsamp_histDic
{
public:

  static std::unordered_map<int, TH1F> h_altsampl_geo1_syst_1p0n;
  static std::unordered_map<int, TH1F> h_altsampl_geo2_syst_1p0n;
  static std::unordered_map<int, TH1F> h_altsampl_geo3_syst_1p0n;
  static std::unordered_map<int, TH1F> h_altsampl_physlist_syst_1p0n;

  static std::unordered_map<int, TH1F> h_altsampl_geo1_syst_1p1n;
  static std::unordered_map<int, TH1F> h_altsampl_geo2_syst_1p1n;
  static std::unordered_map<int, TH1F> h_altsampl_geo3_syst_1p1n;
  static std::unordered_map<int, TH1F> h_altsampl_physlist_syst_1p1n;

  static std::unordered_map<int, TH1F> h_altsampl_geo1_syst_1pXn;
  static std::unordered_map<int, TH1F> h_altsampl_geo2_syst_1pXn;
  static std::unordered_map<int, TH1F> h_altsampl_geo3_syst_1pXn;
  static std::unordered_map<int, TH1F> h_altsampl_physlist_syst_1pXn;

  static std::unordered_map<int, TH1F> h_altsampl_geo1_syst_3p0n;
  static std::unordered_map<int, TH1F> h_altsampl_geo2_syst_3p0n;
  static std::unordered_map<int, TH1F> h_altsampl_geo3_syst_3p0n;
  static std::unordered_map<int, TH1F> h_altsampl_physlist_syst_3p0n;

};

std::unordered_map<int, TH1F> altsampl_createEmptyTH1F()
{
  std::unordered_map<int, TH1F> internalMap;
  return internalMap;
}

// 1p0n
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo1_syst_1p0n = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo2_syst_1p0n = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo3_syst_1p0n = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_physlist_syst_1p0n = altsampl_createEmptyTH1F();

// 1p1n
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo1_syst_1p1n = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo2_syst_1p1n = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo3_syst_1p1n = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_physlist_syst_1p1n = altsampl_createEmptyTH1F();

// 1pXn
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo1_syst_1pXn = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo2_syst_1pXn = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo3_syst_1pXn = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_physlist_syst_1pXn = altsampl_createEmptyTH1F();

// 3p0n
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo1_syst_3p0n = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo2_syst_3p0n = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_geo3_syst_3p0n = altsampl_createEmptyTH1F();
std::unordered_map<int, TH1F> altsamp_histDic::h_altsampl_physlist_syst_3p0n = altsampl_createEmptyTH1F();

namespace AltSampSystHelper {

  float altsample_param(float tau_pt, float tau_decay_mode, int syst_type, int version)
  {
    if(syst_type == 0){ // geo1
      if(tau_decay_mode == 0){
        int bin = altsamp_histDic::h_altsampl_geo1_syst_1p0n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo1_syst_1p0n.at(version).GetBinContent(bin); 
      } 
      else if(tau_decay_mode == 1){
        int bin = altsamp_histDic::h_altsampl_geo1_syst_1p1n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo1_syst_1p1n.at(version).GetBinContent(bin);
      } 
      else if(tau_decay_mode == 2){
        int bin = altsamp_histDic::h_altsampl_geo1_syst_1pXn.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo1_syst_1pXn.at(version).GetBinContent(bin);
      }
      else if(tau_decay_mode == 3){
        int bin = altsamp_histDic::h_altsampl_geo1_syst_3p0n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo1_syst_3p0n.at(version).GetBinContent(bin);
      } else {
        return 1;
      }
   } 
   else if(syst_type == 1){ // geo2
      if(tau_decay_mode == 0){
        int bin = altsamp_histDic::h_altsampl_geo2_syst_1p0n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo2_syst_1p0n.at(version).GetBinContent(bin);
      } 
      else if(tau_decay_mode == 1){
        int bin = altsamp_histDic::h_altsampl_geo2_syst_1p1n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo2_syst_1p1n.at(version).GetBinContent(bin);
      } 
      else if(tau_decay_mode == 2){
        int bin = altsamp_histDic::h_altsampl_geo2_syst_1pXn.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo2_syst_1pXn.at(version).GetBinContent(bin);
      }
      else if(tau_decay_mode == 3){
        int bin = altsamp_histDic::h_altsampl_geo2_syst_3p0n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo2_syst_3p0n.at(version).GetBinContent(bin);
      } else { 
        return 1;
      }  
   }
   else if(syst_type == 2){ // geo3
      if(tau_decay_mode == 0){
        int bin = altsamp_histDic::h_altsampl_geo3_syst_1p0n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo3_syst_1p0n.at(version).GetBinContent(bin);
      }
      else if(tau_decay_mode == 1){
        int bin = altsamp_histDic::h_altsampl_geo3_syst_1p1n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo3_syst_1p1n.at(version).GetBinContent(bin);
      } 
      else if(tau_decay_mode == 2){
        int bin = altsamp_histDic::h_altsampl_geo3_syst_1pXn.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo3_syst_1pXn.at(version).GetBinContent(bin);
      }
      else if(tau_decay_mode == 3){
        int bin = altsamp_histDic::h_altsampl_geo3_syst_3p0n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_geo3_syst_3p0n.at(version).GetBinContent(bin);
      } else { 
        return 1;
      }
   }
   else if(syst_type == 3){ // physlist
      if(tau_decay_mode == 0){
        int bin = altsamp_histDic::h_altsampl_physlist_syst_1p0n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_physlist_syst_1p0n.at(version).GetBinContent(bin);
      }
      else if(tau_decay_mode == 1){
        int bin = altsamp_histDic::h_altsampl_physlist_syst_1p1n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_physlist_syst_1p1n.at(version).GetBinContent(bin);
      }
      else if(tau_decay_mode == 2){
        int bin = altsamp_histDic::h_altsampl_physlist_syst_1pXn.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_physlist_syst_1pXn.at(version).GetBinContent(bin);
      }
      else if(tau_decay_mode == 3){
        int bin = altsamp_histDic::h_altsampl_physlist_syst_3p0n.at(version).FindBin(tau_pt);
        return altsamp_histDic::h_altsampl_physlist_syst_3p0n.at(version).GetBinContent(bin);
      } else {
        return 1;
      }
   } 
   else {
      return 1;
   }
         
  }
}; // end the namespace
