#include <TH2F.h>
#include <unordered_map>

class decaymode_histDict
{
public:
  static std::unordered_map<int, TH2F> h_sf_decaymode_1p0n;
  static std::unordered_map<int, TH2F> h_sf_decaymode_1p1n;
  static std::unordered_map<int, TH2F> h_sf_decaymode_1pXn;
  static std::unordered_map<int, TH2F> h_sf_decaymode_3p0n;
  static std::unordered_map<int, TH2F> h_sf_decaymode_3pXn;

  static std::unordered_map<int, TH2F> h_total_decaymode_1p0n;
  static std::unordered_map<int, TH2F> h_total_decaymode_1p1n;
  static std::unordered_map<int, TH2F> h_total_decaymode_1pXn; 
  static std::unordered_map<int, TH2F> h_total_decaymode_3p0n;
  static std::unordered_map<int, TH2F> h_total_decaymode_3pXn;

};

std::unordered_map<int, TH2F> DecayModeEmptyTH2F()
{
  std::unordered_map<int, TH2F> internalMap;
  return internalMap;
}

std::unordered_map<int, TH2F> decaymode_histDict::h_sf_decaymode_1p0n  = DecayModeEmptyTH2F();
std::unordered_map<int, TH2F> decaymode_histDict::h_sf_decaymode_1p1n  = DecayModeEmptyTH2F();
std::unordered_map<int, TH2F> decaymode_histDict::h_sf_decaymode_1pXn  = DecayModeEmptyTH2F();
std::unordered_map<int, TH2F> decaymode_histDict::h_sf_decaymode_3p0n  = DecayModeEmptyTH2F();
std::unordered_map<int, TH2F> decaymode_histDict::h_sf_decaymode_3pXn  = DecayModeEmptyTH2F();
std::unordered_map<int, TH2F> decaymode_histDict::h_total_decaymode_1p0n  = DecayModeEmptyTH2F();
std::unordered_map<int, TH2F> decaymode_histDict::h_total_decaymode_1p1n  = DecayModeEmptyTH2F();
std::unordered_map<int, TH2F> decaymode_histDict::h_total_decaymode_1pXn  = DecayModeEmptyTH2F();
std::unordered_map<int, TH2F> decaymode_histDict::h_total_decaymode_3p0n  = DecayModeEmptyTH2F();
std::unordered_map<int, TH2F> decaymode_histDict::h_total_decaymode_3pXn  = DecayModeEmptyTH2F();

namespace DecayModeSystHelper {

  float decaymode_singletau_weight_true_1p0n_reco_1p0n(float reco_tau_decaymode, float truth_tau_decaymode, float tau_eta, int version)
  {
    float sf = 1, total = 0;
    if(truth_tau_decaymode == 0 && reco_tau_decaymode == 0){
       int bin_tau = decaymode_histDict::h_sf_decaymode_1p0n.at(version).FindBin(truth_tau_decaymode,tau_eta);
       sf = decaymode_histDict::h_sf_decaymode_1p0n.at(version).GetBinContent(bin_tau);
       total = decaymode_histDict::h_total_decaymode_1p0n.at(version).GetBinContent(bin_tau);
       
    }
    return ((sf+total)/sf);
  }

  float decaymode_singletau_weight_true_1p1n_reco_1p0n(float reco_tau_decaymode, float truth_tau_decaymode, float tau_eta, int version)
  {
    float sf = 1, total = 0;
    if(truth_tau_decaymode == 1 && reco_tau_decaymode == 0){
       int bin_tau = decaymode_histDict::h_sf_decaymode_1p0n.at(version).FindBin(truth_tau_decaymode,tau_eta);
       sf = decaymode_histDict::h_sf_decaymode_1p0n.at(version).GetBinContent(bin_tau);
       total = decaymode_histDict::h_total_decaymode_1p0n.at(version).GetBinContent(bin_tau);

    }
    return ((sf+total)/sf);
  }
  
  float decaymode_singletau_weight_true_1p1n_reco_1p1n(float reco_tau_decaymode, float truth_tau_decaymode, float tau_eta, int version)
  {
    float sf = 1, total = 0;
    if(truth_tau_decaymode == 1 && reco_tau_decaymode == 1){
       int bin_tau = decaymode_histDict::h_sf_decaymode_1p1n.at(version).FindBin(truth_tau_decaymode,tau_eta);
       sf = decaymode_histDict::h_sf_decaymode_1p1n.at(version).GetBinContent(bin_tau);
       total = decaymode_histDict::h_total_decaymode_1p1n.at(version).GetBinContent(bin_tau);

    }
    return ((sf+total)/sf);
  }

  float decaymode_singletau_weight_true_1p1n_reco_1pXn(float reco_tau_decaymode, float truth_tau_decaymode, float tau_eta, int version)
  {
    float sf = 1, total = 0;
    if(truth_tau_decaymode == 1 && reco_tau_decaymode == 2){
       int bin_tau = decaymode_histDict::h_sf_decaymode_1pXn.at(version).FindBin(truth_tau_decaymode,tau_eta);
       sf = decaymode_histDict::h_sf_decaymode_1pXn.at(version).GetBinContent(bin_tau);
       total = decaymode_histDict::h_total_decaymode_1pXn.at(version).GetBinContent(bin_tau);

    }
    return ((sf+total)/sf);
  } 

  float decaymode_singletau_weight_true_1pXn_reco_1p1n(float reco_tau_decaymode, float truth_tau_decaymode, float tau_eta, int version)
  {
    float sf = 1, total = 0;
    if(truth_tau_decaymode == 2 && reco_tau_decaymode == 1){
       int bin_tau = decaymode_histDict::h_sf_decaymode_1p1n.at(version).FindBin(truth_tau_decaymode,tau_eta);
       sf = decaymode_histDict::h_sf_decaymode_1p1n.at(version).GetBinContent(bin_tau);
       total = decaymode_histDict::h_total_decaymode_1p1n.at(version).GetBinContent(bin_tau);

    }
    return ((sf+total)/sf);
  } 

  float decaymode_singletau_weight_true_1pXn_reco_1pXn(float reco_tau_decaymode, float truth_tau_decaymode, float tau_eta, int version)
  {
    float sf = 1, total = 0;
    if(truth_tau_decaymode == 2 && reco_tau_decaymode == 2){
       int bin_tau = decaymode_histDict::h_sf_decaymode_1pXn.at(version).FindBin(truth_tau_decaymode,tau_eta);
       sf = decaymode_histDict::h_sf_decaymode_1pXn.at(version).GetBinContent(bin_tau);
       total = decaymode_histDict::h_total_decaymode_1pXn.at(version).GetBinContent(bin_tau);

    }
    return ((sf+total)/sf);
  }

  float decaymode_singletau_weight_true_3p0n_reco_3p0n(float reco_tau_decaymode, float truth_tau_decaymode, float tau_eta, int version)
  {
    float sf = 1, total = 0;
    if(truth_tau_decaymode == 3 && reco_tau_decaymode == 3){
       int bin_tau = decaymode_histDict::h_sf_decaymode_3p0n.at(version).FindBin(truth_tau_decaymode,tau_eta);
       sf = decaymode_histDict::h_sf_decaymode_3p0n.at(version).GetBinContent(bin_tau);
       total = decaymode_histDict::h_total_decaymode_3p0n.at(version).GetBinContent(bin_tau);

    }
    return ((sf+total)/sf);
  } 
 
  float decaymode_singletau_weight_true_3pXn_reco_3p0n(float reco_tau_decaymode, float truth_tau_decaymode, float tau_eta, int version)
  {
    float sf = 1, total = 0;
    if(truth_tau_decaymode == 4 && reco_tau_decaymode == 3){
       int bin_tau = decaymode_histDict::h_sf_decaymode_3p0n.at(version).FindBin(truth_tau_decaymode,tau_eta);
       sf = decaymode_histDict::h_sf_decaymode_3p0n.at(version).GetBinContent(bin_tau);
       total = decaymode_histDict::h_total_decaymode_3p0n.at(version).GetBinContent(bin_tau);
    }
    return ((sf+total)/sf);
  }


}; // end the namespace
