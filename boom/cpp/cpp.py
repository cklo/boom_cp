__all__ = [
    '_load_decaymode_helper_cpp',
    '_load_vbf_tagger_cpp',
    '_load_ztheosys_helper_cpp',
    '_load_mc_norm_mult_cpp',
    # '_load_ff_helper',
    # '_load_if_helper',
    '_load_random_cpp',
    '_load_hhfakesyst_helper_cpp',
    '_load_sgnsyst_helper_cpp',
    # '_load_zll_pt_cpp',
    # '_load_tth_bdts_cpp',
    # '_load_vh_tagger_cpp',
    '_load_phistarcalc_cpp',
    '_load_tauvismassvcalc_cpp',
    '_load_tauupsiloncalc_cpp',
    '_load_altsamp_helper_cpp',
    ]
  
import ROOT
def _load_sgnsyst_helper_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/sgnsyst_helper.cpp', 'k-', '', 'cache')
        print('BOOM: \t sgnsyst helper loaded!')
    except:
        raise RuntimeError
    
    if not hasattr(ROOT, 'SgnSystHelper'):
        print('Sgn syst Helper not compiled')

    # try:
    #     ROOT.SgnSystHelper()
    # except:
    #     print("Sgn syst Helper not compiled")


def _load_decaymode_helper_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/decaymode_helper.cpp', 'k-', '', 'cache')
        print('BOOM: \t decaymode helper loaded!')
    except:
        raise RuntimeError


    if not hasattr(ROOT, 'DecayModeSystHelper'):
        print('Decaymode Helper not compliled')
    
    # try:
    #     ROOT.DecayModeSystHelper()
    # except:
    #     print("Decaymode Helper not compiled")


def _load_vbf_tagger_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/vbf_tagger.cpp', 'k-', '', 'cache')
        print('BOOM: \t vbf tagger loaded!')
    except:
        raise RuntimeError

    if not hasattr(ROOT, 'VBF_Tagger'):
        print('VBF Tagger not compliled')
    
    # try:
    #     ROOT.VBF_Tagger()
    # except:
    #     print("VBF Tagger not compiled")


def _load_ztheosys_helper_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/ztheosyst_helper.cpp', 'k-', '', 'cache')
        print('BOOM:\t Z theory syst helper tool loaded!')
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216

    if not hasattr(ROOT, 'ZTheoSystHelper'):
        print('Z Theo Syst tool not compliled')

    # try:
    #     ROOT.ZTheoSystHelper()
    # except:
    #     print('Z Theo Syst tool not compiled')


def _load_mc_norm_mult_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/mc_norm_mult.cpp', 'k-', '', 'cache')
        print('BOOM:\t cross-section * sum-of-weights tool loaded!')
    except:
        raise RuntimeError

    if not hasattr(ROOT, 'MC_Norm'):
        print('BOOM: WARNING: cross-section * sum-of-weights tool not loaded')

    # below was used in the python2 version, not 100% sure if it's equivalent to what's above.
    # try:
    #     ROOT.MC_Norm()
    # except:
    #     print("Norm functions not compiled")
    #     raise RuntimeError


def _load_ff_helper():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/ff_helper.cpp', 'k-', '', 'cache')
        print('BOOM:\t fake factor helper tool loaded!')
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216

    if not hasattr(ROOT, 'FakeHelper'):
        print('Fake Factor tool not compliled')

    # try:
    #     ROOT.FakeHelper()
    # except:
    #     print('Fake Factor tool not compiled')


def _load_if_helper():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/iso_factor_helper.cpp', 'k-', '', 'cache')
        print('BOOM:\t iso factor helper tool loaded!')
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216

    if not hasattr(ROOT, 'IsoFactorHelper'):
        print('Iso Factor tool not compliled')

    # try:
    #     ROOT.IsoFactorHelper()
    # except:
    #     print('Iso Factor tool not compiled')


def _load_random_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/random.cpp', 'k-', '', 'cache')
        print('BOOM:\t randomizer loaded!')
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216

    if not hasattr(ROOT, 'Rnd'):
        print('randomizer not compiled!')

    # try:
    #     ROOT.Rnd()
    # except:
    #     print('randomizer not compiled!')

def _load_hhfakesyst_helper_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/hhfakesyst_helper.cpp', 'k-', '', 'cache')
        print('BOOM:\t hhfakesyst_helper tool loaded!')
    except:
        raise RuntimeError

    if not hasattr(ROOT, 'HHFakeSystHelper'):
        print('BOOM: WANRING: HH Fake systematics helper not loaded')

    # below was used in the python2 version, not 100% sure if it's equivalent to what's above.
    # try:
    #     ROOT.HHFakeSystHelper()
    # except:
    #     print("HH Fake syst helper function not compiled")
    #     raise RuntimeError

def _load_zll_pt_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/zll_pt.cpp', 'k-', '', 'cache')
        print('BOOM:\t zll_pt tool loaded!')
    except:
        print('BOOM:\t Zll pt correction helper tool NOT LOADED!')
        raise RuntimeError

    if not hasattr(ROOT, 'PtCorrHelper'):
        print('Zll pt correction tool not compiled')
        raise RuntimeError

    # try:
    #     ROOT.PtCorrHelper()
    # except:
    #     print('Zll pt correction tool not compiled')
    #     raise RuntimeError

def _load_tth_bdts_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/tth_bdts.cpp', 'k-', '', 'cache')
        print('BOOM: \t tth bdts loaded!')
    except:
        raise RuntimeError

    if not hasattr(ROOT, 'ttH_BDTs'):
        print('ttH BDTs not compliled')
    
    # try:
    #     ROOT.ttH_BDTs()
    # except:
    #     print("ttH BDTs not compiled")

def _load_vh_tagger_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/vh_tagger.cpp', 'k-', '', 'cache')
        print('BOOM: \t vh tagger loaded!')
    except:
        raise RuntimeError
    
    if not hasattr(ROOT, 'VH_Tagger'):
        print('VH_Tagger not compliled')

    # try:
    #     ROOT.VH_Tagger()
    # except:
    #     print("ttH BDTs not compiled")


def _load_phistarcalc_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/calc_phistar.cpp', 'k-', '', 'cache')
        print('BOOM:\t phistar calculation helper tool loaded!')
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216

    if not hasattr(ROOT, 'PhiStarCalc'):
        print('PhiStarCalc tool not compiled')

    # try:
    #     ROOT.PhiStarCalc()
    # except:
    #     print('PhiStarCalc tool not compiled')

def _load_tauvismassvcalc_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/calc_tau_vismass.cpp', 'k-', '', 'cache')
        print('BOOM:\t tau vis mass calculation helper tool loaded!')
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216

    if not hasattr(ROOT, 'TauVisMassCalc'):
        print('TauVisMassCalc tool not compiled')

    # try:
    #     ROOT.TauVisMassCalc()
    # except:
    #     print('TauVisMassCalc tool not compiled')

def _load_tauupsiloncalc_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/calc_tau_upsilon.cpp', 'k-', '', 'cache')
        print('BOOM:\t tau upsilon calculation helper tool loaded!')
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216

    if not hasattr(ROOT, 'TauUpsilonCalc'):
        print('TauUpsilonCalc tool not compliled')

    # try:
    #     ROOT.TauUpsilonCalc()
    # except:
    #     print('TauVisMassCalc tool not compiled')

def _load_altsamp_helper_cpp():
    try:
        ROOT.gSystem.CompileMacro('boom/cpp/altsamp_helper.cpp', 'k-', '', 'cache')
        print('BOOM:\t Alternative sample helper tool loaded!')
    except:
        raise RuntimeError

    # this is needed because of
    # https://sft.its.cern.ch/jira/browse/ROOT-7216

    if not hasattr(ROOT, 'AltSampSystHelper'):
        print('Alternative sample helper tool not compliled')

    # try:
    #     ROOT.AltSampSystHelper()
    # except:
    #     print('Alternative sample helper tool not compiled')


