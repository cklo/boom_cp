#include <unordered_map>
#include <array>
#include <algorithm>
#include <iostream>
#include <math.h>

#ifdef __CINT__
#pragma link C++ class std::unordered_map<string, float>;
#endif


class crossSectionDict
{
public:
  static inline std::unordered_map<int, float> m_Map_hh_a;
  static inline std::unordered_map<int, float> m_Map_hh_d;
  static inline std::unordered_map<int, float> m_Map_hh_e;
  static inline std::unordered_map<string, float> m_Map_Ztheo_a;
  static inline std::unordered_map<string, float> m_Map_Ztheo_d;
  static inline std::unordered_map<string, float> m_Map_Ztheo_e;
  static inline std::unordered_map<string, float> m_Map_Sgntheo_a;
  static inline std::unordered_map<string, float> m_Map_Sgntheo_d;
  static inline std::unordered_map<string, float> m_Map_Sgntheo_e;
};

const std::vector<int> dsid_sherpa_samples{ /*ZttQCD */
                                            344772, 
                                            344774, 
                                            344775, 
                                            344776, 
                                            344778, 
                                            344779, 
                                            344780, 
                                            344781, 
                                            344782, 
                                            364137, 
                                            364138, 
                                            364139, 
                                            364140, 
                                            364141,
                                            364210, 
                                            364211, 
                                            364212, 
                                            364213, 
                                            364214, 
                                            364215, 
                                            /*ZttQCD EWK*/
                                            308094, 
                                            /* VV */
                                            363355, 
                                            363356, 
                                            363357, 
                                            363358, 
                                            363359, 
                                            363489, 
                                            363494, 
                                            364250, 
                                            364253, 
                                            364254, 
                                            364255, 
                                            364288, 
                                            364289, 
                                            364290,
                                            /* W */
                                            364156, 
                                            364157, 
                                            364158, 
                                            364159, 
                                            364160, 
                                            364161, 
                                            364162,
                                            364163, 
                                            364164, 
                                            364165, 
                                            364166, 
                                            364167, 
                                            364168, 
                                            364169, 
                                            364170, 
                                            364171, 
                                            364172, 
                                            364173, 
                                            364174, 
                                            364175, 
                                            364176,
                                            364177, 
                                            364178, 
                                            364179, 
                                            364180, 
                                            364181, 
                                            364182, 
                                            364183, 
                                            364184, 
                                            364185, 
                                            364186, 
                                            364187, 
                                            364188, 
                                            364189, 
                                            364190,
                                            364191, 
                                            364192, 
                                            364193, 
                                            364194, 
                                            364195, 
                                            364196, 
                                            364197, 
                                            308096, 
                                            308097, 
                                            308098, 
                                            /* Zll QCD */
                                            345099, 
                                            345100, 
                                            345101, 
                                            345102,
                                            364100, 
                                            364101, 
                                            364102, 
                                            364103, 
                                            364104, 
                                            364105, 
                                            364106, 
                                            364107, 
                                            364108, 
                                            364109, 
                                            364110, 
                                            364111, 
                                            364112, 
                                            364113,
                                            364114, 
                                            364115, 
                                            364116, 
                                            364117, 
                                            364118, 
                                            364119, 
                                            364120, 
                                            364121, 
                                            364122, 
                                            364123, 
                                            364124, 
                                            364125, 
                                            364126, 
                                            364127,
                                            364198, 
                                            364199, 
                                            364200, 
                                            364201, 
                                            364202, 
                                            364203, 
                                            364204, 
                                            364205, 
                                            364206, 
                                            364207, 
                                            364208, 
                                            364209, 
                                            /* Zll EWK */
                                            308092, 
                                            308093,
                                          };

namespace MC_Norm{

  float TotalWeight_hadhad(int DSID, int runNumber)
  {
    
      if(runNumber > 0 && runNumber <= 311563)
	return crossSectionDict::m_Map_hh_a.at(DSID);
      else if(runNumber > 311563 && runNumber <= 341649)
	return crossSectionDict::m_Map_hh_d.at(DSID);
      else if(runNumber > 341649)
        return crossSectionDict::m_Map_hh_e.at(DSID);
      else 
	return 0;
  }

  float get_mc_weight( int dsid, float weight_mc)
  {
     // check if the sample is a sherpa sample
     if ( std::find(dsid_sherpa_samples.begin(), dsid_sherpa_samples.end(), dsid) != dsid_sherpa_samples.end() ) {
        if( fabs(weight_mc) > 100) {
            return 1;
        }
     }
     return weight_mc;
  }

  float correct_weightZtheo(int dsid, int pdf_sys, int runNumber){

    string variation ="";
    if (pdf_sys == 100 ) variation = "theory_z_lhe3weight_mur05_muf05_pdf261000";
    else if (pdf_sys == 101 ) variation = "theory_z_lhe3weight_mur05_muf1_pdf261000";
    else if (pdf_sys == 102 ) variation = "theory_z_lhe3weight_mur1_muf05_pdf261000";
    else if (pdf_sys == 103 ) variation = "theory_z_lhe3weight_mur1_muf2_pdf261000";
    else if (pdf_sys == 104 ) variation = "theory_z_lhe3weight_mur2_muf1_pdf261000";
    else if (pdf_sys == 105 ) variation = "theory_z_lhe3weight_mur2_muf2_pdf261000";
    else if (pdf_sys == 106 ) variation = "theory_z_CT14_pdfset";
    else if (pdf_sys == 107 ) variation = "theory_z_MMHT_pdfset";
    else if (pdf_sys == 108 ) variation = "theory_z_alphaS_up";
    else if (pdf_sys == 109 ) variation = "theory_z_alphaS_down";
    else {
        variation=std::to_string(pdf_sys);
    }

    if(runNumber > 0 && runNumber <= 311563)
        return crossSectionDict::m_Map_Ztheo_a.at( std::to_string(dsid)+"_"+variation);
    else if(runNumber > 311563 && runNumber <= 341649)
        return crossSectionDict::m_Map_Ztheo_d.at( std::to_string(dsid)+"_"+variation );
    else if(runNumber > 341649) 
        return crossSectionDict::m_Map_Ztheo_e.at( std::to_string(dsid)+"_"+variation );
    else
        return 0;

  }


  float correct_weightSgntheo(int dsid, int pdf_sys, int runNumber){

    string variation ="";
    
    if (pdf_sys == 30 ) variation = "theory_sig_alphaS_low";
    else if (pdf_sys == 31 ) variation = "theory_sig_alphaS_high";
    else if (pdf_sys == 32 ) variation = "theory_sig_mur_muf_0";
    else if (pdf_sys == 33 ) variation = "theory_sig_mur_muf_1";
    else if (pdf_sys == 34 ) variation = "theory_sig_mur_muf_2";
    else if (pdf_sys == 35 ) variation = "theory_sig_mur_muf_3";
    else if (pdf_sys == 36 ) variation = "theory_sig_mur_muf_4";
    else if (pdf_sys == 37 ) variation = "theory_sig_mur_muf_5";
    else if (pdf_sys == 38 ) variation = "theory_sig_mur_muf_6";
    else if (pdf_sys == 39 ) variation = "theory_sig_mur_muf_7";
    else variation=std::to_string(pdf_sys);

    if(runNumber > 0 && runNumber <= 311563)
        return crossSectionDict::m_Map_Sgntheo_a.at( std::to_string(dsid)+"_"+variation);
    else if(runNumber > 311563 && runNumber <= 341649)
        return crossSectionDict::m_Map_Sgntheo_d.at( std::to_string(dsid)+"_"+variation);
    else if(runNumber > 341649)
        return crossSectionDict::m_Map_Sgntheo_e.at( std::to_string(dsid)+"_"+variation);
    else
        return 0;

  }

  float NaNProtectBetterThanRoot ( float w ) {
     if (w!=w) return 0;
     else if (isinf (w) ) return 0;
     else return w;
  }


  float check_0_weight (float w ) {

     if( w == 0 ) return 1;
     else return w;   
  }
};
