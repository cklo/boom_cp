"""
"""
import ROOT
import uuid
# HAPPy imports
from happy.plot import Plot, TextElement
from happy.doublePlot import DoublePlot
from happy.dataMcPlot import DataMcPlot
from happy.dataMcRatioPlot import DataMcRatioPlot
from happy.variable import Binning, Variable, var_Ratio, var_Entries
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import hist_sum, blinded_hist, yield_table, lumi_calc, plot_title_extension, hist_max




def make_fake_template_plot(
    processor,
    selections, 
    variable, 
    title='All channels, All SRs',    
    print_lumi=True,
    **kwargs):
    """
    """
    
    _sels = filter_selections(selections, **kwargs)

    print('BOOM: make fake template plot for', title)
    plot = DataMcPlot(
        uuid.uuid4().hex, 
        variable) 
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 3
#     plot.topMargin = 0.25
    plot.topMargin = 0.4
    sgn_scale = 1

    hH = processor.get_hist_physics_process('Signal', _sels, variable, lumi_scale=sgn_scale)
    print('BOOM: \t Signal: {}'.format(hH.Integral()/sgn_scale))

    hD = processor.get_hist_physics_process('Data', _sels, variable)
    print('BOOM: \t Data: {}'.format(hD.Integral()))
    plot.setDataHistogram( hD )

    bkgs = []
    bkg_names = []
    for name in processor.merged_physics_process_names[::-1]:
        p = processor.get_physics_process(name)[0]
        if p.isSignal:
            continue
        if p.name == 'Data':
            continue
        h = processor.get_hist_physics_process(name, _sels, variable)
        plot.addHistogram(h)
        bkgs.append(h)
        bkg_names.append(name)
        print('BOOM: \t {}: {}'.format(name, h.Integral()))
  
    plot.addHistogram(hH, stacked=False)
    plot.titles.append(title)
    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )
    plot.draw()
    plot.saveAs('./plots/plot_fake_template_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_')))
    print(yield_table(hD, hH, bkgs, bkg_names=bkg_names))


def derive_fake_shape_comp(
    lselse_histo,
    medmed_histo,
    variable,
    channel,
    category,
    sr='Anti-ID SR',
    cr='ID SS',
    up_range=1.0,
    title='fake_shape_comparison'):

    """
    """

    lselse_histo.SetTitle(sr)
    lselse_histo.SetLineColor(ROOT.kRed)
    lselse_histo.SetMarkerColor(ROOT.kRed)

    medmed_histo.SetTitle(cr)
    medmed_histo.SetLineColor(ROOT.kBlue)
    medmed_histo.SetMarkerColor(ROOT.kBlue)

    if lselse_histo.Integral() != 0:
        lselse_histo.Scale(1/lselse_histo.Integral())
    if medmed_histo.Integral() != 0:
        medmed_histo.Scale(1/medmed_histo.Integral())

    var_au    = Variable('au', '', 'A.U.', '', Binning(1, 0, up_range))
    var_ratio = Variable('Ratio', binning=Binning(low=0.0, high=2.0))
    var_ratio.binning.nDivisions = 5

    plot = DoublePlot(
        uuid.uuid4().hex,
        variable,
        var_au,
        var_ratio, 0.8, 0.2)

    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 4
    plot.topMargin = 0.4

    plot.plotUp.addHistogram(lselse_histo, 'E0')
    plot.plotUp.addHistogram(medmed_histo, 'E0')

    h_ratio  = lselse_histo.Clone()
    h_ref    = medmed_histo.Clone()
    h_ratio.Divide(medmed_histo)
    h_ratio.SetLineColor(ROOT.kBlack)
    h_ratio.SetMarkerColor(ROOT.kBlack)
    h_ref.Divide(medmed_histo)
    h_ref.SetTitle('MC Stat.')
    h_ref.SetFillColor(1);
    h_ref.SetLineColor(0);
    h_ref.SetMarkerStyle(1);
    h_ref.SetMarkerSize(0.0001);
    h_ref.SetMarkerColor(1);
    h_ref.SetFillStyle(3004);
    r = plot.plotDown.addHistogram(h_ref, 'E2')
    plot.plotDown.addHistogram(h_ratio, 'HIST')

    tit = 'Boost ' if 'boost' in category else 'VBF '
    tit += 'Tight ' if 'tight' in category else 'Loose ' if 'loose' in category else 'Incl '
    tit += 'High' if 'high' in category else 'Low'
    if 'presel' in category : tit = 'Preselection'
  

    plot.titles.append('{}'.format(tit))
    plot.titles.append('{}'.format(channel))
   

    plot.draw()
    plot.saveAs('plots/hh_{}_{}_{}_{}.pdf'.format(title, variable.name, channel, category ))
    

    #outfile = ROOT.TFile('hh_{}_{}_{}.root'.format(title, variable.name, channel),'recreate')
    #h_ratio.SetName('hh_{}_{}_{}'.format(title, variable.name, channel))
    #h_ratio.Write()
    #outfile.Close()

