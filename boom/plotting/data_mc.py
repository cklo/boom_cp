"""
"""
import ROOT
import uuid
# HAPPy imports
from happy.plot import Plot, TextElement
from happy.dataMcPlot import DataMcPlot
from happy.dataMcRatioPlot import DataMcRatioPlot
from happy.variable import Binning, Variable, var_Ratio
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator

from ..selection_utils import filter_selections
from ..utils import (blinded_hist, yield_table, lumi_calc, plot_title_extension, 
                     hist_max, significance_counting, hist_sum)


def make_data_mc_plot(
    processor,
    selections, 
    variable, 
    systematics=None,
    title=None,
    print_lumi=True,
    signal_scaling_factor=10,
    force_unblind=False,
    compute_significance=False,
    split_signal=False,
    ratio_range=0.75,
    print_chisquare=True,
    **kwargs):
    """
    """
    
    _sels = filter_selections(selections, **kwargs)

    # plot title, file extension
    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    print('BOOM: make plot for', _title)
    var_dataMc = Variable('Data / Pred.', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_dataMc.binning.nDivisions = 5
    plot = DataMcRatioPlot(
        uuid.uuid4().hex, 
        variable, 
        yVariableDown=var_dataMc, 
        upperFraction=0.80, 
        lowerFraction=0.20)

    if systematics != None:
        plot.statErrorTitle = 'Uncertainty' #'Stat. #oplus Syst.'
    else:
        plot.statErrorTitle = 'Stat.'

    # plot style alteration
    #plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 5
    plot.topMargin = 0.5

    # retrieve background histograms
    bkgs = []
    bkg_names = []
    for name in processor.merged_physics_process_names[::-1]:
        if name == 'Data':
            continue
        h = processor.get_hist_physics_process(name, _sels, variable)
 
        if systematics != None:
            for sysname in systematics:
                h_sys_up = processor.get_hist_physics_process(name, _sels, variable, syst_name=sysname, syst_type='up')
                for i in range(h.GetNbinsX()):
                    if (h_sys_up.GetBinContent(i+1) != 0):
                        #print("bin {0}\t{1}\t{2}\t{3}".format(i,h.GetBinContent(i+1), h.GetBinError(i+1),h_sys_up.GetBinContent(i+1)))
                        h.SetBinError(i+1,(h.GetBinError(i+1)**2 + ( h_sys_up.GetBinContent(i+1) - h.GetBinContent(i+1) )**2 )**(0.5))


        if 'Signal_SM' in name:
           h.SetFillColor(ROOT.kRed)
           h.SetTitle('Signal SM')
           plot.addHistogram(h)
        elif 'Signal_CPodd' in name or 'Others_Sgn' in name or 'unpol' in name or 'theta' in name:
           continue
        else:
           plot.addHistogram(h)   
           bkgs.append(h)
           bkg_names.append(name)

    hBkg = hist_sum(bkgs)

    # retrieve signal histograms for all signal samples
    hH_no_scaling       = processor.get_hist_physics_process('Signal_SM', _sels, variable)
    #hH_no_scaling_CPodd = processor.get_hist_physics_process('Signal_CPodd', _sels, variable)

    hH       = processor.get_hist_physics_process('Signal_unpol_SM', _sels, variable, lumi_scale=signal_scaling_factor)
    hH_CPodd = processor.get_hist_physics_process('Signal_unpol_CPodd', _sels, variable, lumi_scale=signal_scaling_factor)
    hH_CPodd.SetLineStyle(2)
    hH_CPodd.SetLineColor(ROOT.kGreen)
    if signal_scaling_factor != 1:
        hH.SetTitle(hH.GetTitle() + ' (x{})'.format(signal_scaling_factor))
        hH_CPodd.SetTitle(hH_CPodd.GetTitle() + ' (x{})'.format(signal_scaling_factor))
    plot.addHistogram(hH, stacked=False)
    plot.addHistogram(hH_CPodd, stacked=False)

    # total signal
    tot_sgn = hH_no_scaling.Clone("total_signal")
 
    # retrieve data histogram, blind if necessary and plot
    hD = processor.get_hist_physics_process('Data', _sels, variable)

    # figure out if blinding is needed
    if not force_unblind:
        if 'mmc' in variable.name:
            hD_blinded = blinded_hist(hD)
            plot.setDataHistogram(hD_blinded)
        elif 'visible_mass' in variable.name:
            hD_blinded = blinded_hist(hD, var_name='visible')
            plot.setDataHistogram(hD_blinded)
        elif 'ditau_coll_approx_m' in variable.name:
            hD_blinded = blinded_hist(hD, var_name='collinear')
            plot.setDataHistogram(hD_blinded)
        elif 'phi_star' in variable.name:
            hD_blinded = blinded_hist(hD, var_name=variable.name)
            plot.setDataHistogram(hD_blinded)
        elif 'vbf_tagger' in variable.name:
            hD_blinded = blinded_hist(hD, var_name=variable.name, signal=hH_no_scaling, rtol_blind_range = False)
            plot.setDataHistogram(hD_blinded)
        else:
            plot.setDataHistogram(hD)
    else:
        plot.setDataHistogram(hD)
    
    te = TextElement(_title, x=0.26, y=0.94)
    plot.textElements.append(te)
    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )

    plot.draw()
    if print_chisquare and hD.GetNbinsX() > 1:
        chi2  = ROOT.Double(0)
        ndf   = ROOT.Long(0)
        igood = ROOT.Long(0)
        chi2Prob = hD.Chi2TestX( plot._mcSumHist,chi2,ndf,igood, 'UW' )
        #ksDist   = hD.KolmogorovTest( plot._mcSumHist, 'NDM' )
        #plot.titles.append( 'KS MD = %.2f' %(ksDist)) 
        plot.titles.append( 'chi^{2}/ndf= %.2f' % (chi2/ndf))
    plot.draw()
  
    plot.saveAs('./plots/plot_{}_{}.pdf'.format(variable.name, _ext))
    print('')
    print(yield_table(hD, tot_sgn, bkgs, bkg_names=bkg_names))
    print('')
