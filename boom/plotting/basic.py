"""
"""
import ROOT
import uuid
# HAPPy imports
from happy.plot import Plot, TextElement, LegendElement
from happy.style import Style
from happy.plotDecorator import AtlasTitleDecorator
from happy.variable import Variable, Binning
from happy.doublePlot import DoublePlot, PlotMatrix
from happy.variable import Binning, Variable, var_Ratio, var_Entries

from ..selection_utils import filter_selections
from ..utils import lumi_calc, plot_title_extension, hist_sum

def basic_plot(
    selections,
    hist,
    variable, 
    print_lumi=True,
    **kwargs):
    """
    """
    _sels = filter_selections(selections, **kwargs)
    _title, _ext = plot_title_extension(_sels, **kwargs)

    print('BOOM: make plot for', _title, variable.name)

    plot = Plot(
        uuid.uuid4().hex,
        variable
        )

    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 3
    plot.addHistogram(hist, 'PE')

    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )
    plot.draw()
    plot.saveAs('plots/basic_plot_{}_{}.pdf'.format(variable.name, _ext))

def make_datadriven_ztt_comparison_plot(
    processor,
    selections,
    variable,
    category ='boost_tight_high'):

    title = 'Boost ' if 'boost' in category else 'VBF '
    title += 'Tight ' if 'tight' in category else 'Loose ' if 'loose' in category else 'Incl '
    title += 'High' if 'high' in category else 'Low'

    print('BOOM: make plot for', title, variable.name)
    sels_sr     = filter_selections(selections, categories=category+'_sr')
    sels_zpeak  = filter_selections(selections, categories=category+'_zcr_1_zpeak')

    h_sr  = processor.get_hist_physics_process('Ztt', sels_sr, variable)
    h_zpeak = processor.get_hist_physics_process('Data', sels_zpeak, variable)

    bkgs = []
    for name in processor.merged_physics_process_names[::-1]:
        p = processor.get_physics_process(name)[0]
        if (p.name == 'Data' or 'Ztt' in p.name or (p.isSignal and 'HWW' not in p.name)):
            continue
        print(name)
        h = processor.get_hist_physics_process(name, sels_zpeak, variable)
        bkgs.append(h)
    hBkg = hist_sum(bkgs)    
    h_zpeak.Add(hBkg,-1)

    h_sr.SetTitle('MC SR')
    h_zpeak.SetTitle('Data-Driven Z-peak')

    h_sr.Scale(1. / h_sr.Integral())
    h_zpeak.Scale(1. / h_zpeak.Integral())
 
    h_sr.SetLineColor(ROOT.kBlue)
    h_zpeak.SetLineColor(ROOT.kRed)
    h_sr.SetMarkerColor(ROOT.kBlue)
    h_zpeak.SetMarkerColor(ROOT.kRed)

    var_au = Variable('var_au', '', 'A.U.', '', Binning(1, 0.00, 0.50))
    var_ratio = Variable('var. / nom.', binning=Binning(low=0.8, high=1.2))
    var_ratio.binning.nDivisions = 4

    plot = DoublePlot(
        uuid.uuid4().hex,
        variable,
        var_au,
        var_ratio, 0.8, 0.2)

    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 4
    plot.topMargin = 0.4
    plot.plotUp.addHistogram(h_zpeak, 'E0')
    plot.plotUp.addHistogram(h_sr, 'E0')
    h_ratio  = h_zpeak.Clone()
    h_ref    = h_sr.Clone()
    h_ratio.Divide(h_sr)

    h_ref.Divide(h_sr)
    h_ref.SetTitle('MC Stat.')
    h_ref_line = h_ref.Clone()
    h_ref_line.SetLineColor(h_sr.GetLineColor())

    r = plot.plotDown.addHistogram(h_ref, 'E2')
    plot.statErrorStyle.applyTo(r)
    plot.plotDown.addHistogram(h_ref_line, 'HIST')
    plot.plotDown.addHistogram(h_ratio, 'E0')
    plot.titles.append('{}'.format(title))
    plot.draw()
    plot.saveAs('./plots/plot_datadriven_ztt_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_')))
       
    h_mc_zpeak = processor.get_hist_physics_process('Ztt', sels_zpeak, variable)  
    h_mc_zpeak.SetTitle('MC Z-peak') 
    h_mc_zpeak.Scale(1. / h_mc_zpeak.Integral())
    h_mc_zpeak.SetLineColor(ROOT.kBlue)
    h_mc_zpeak.SetMarkerColor(ROOT.kBlue)

    plot = DoublePlot(
        uuid.uuid4().hex,
        variable,
        var_au,
        var_ratio, 0.8, 0.2)

    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 4
    plot.topMargin = 0.4
    plot.plotUp.addHistogram(h_mc_zpeak, 'E0')
    plot.plotUp.addHistogram(h_zpeak, 'E0')
    h_ratio  = h_zpeak.Clone("ratio1")
    h_ref    = h_mc_zpeak.Clone("ref1")
    h_ratio.Divide(h_mc_zpeak)

    h_ref.Divide(h_mc_zpeak)
    h_ref.SetTitle('MC Stat.')
    h_ref_line = h_ref.Clone("refline1")
    h_ref_line.SetLineColor(h_mc_zpeak.GetLineColor())

    r = plot.plotDown.addHistogram(h_ref, 'E2')
    plot.statErrorStyle.applyTo(r)
    plot.plotDown.addHistogram(h_ref_line, 'HIST')
    plot.plotDown.addHistogram(h_ratio, 'E0')
    plot.titles.append('{}'.format(title))
    plot.draw()
    plot.saveAs('./plots/plot_ztt_datadriven_mc_comparison_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_')))


    plot = DoublePlot(
        uuid.uuid4().hex,
        variable,
        var_au,
        var_ratio, 0.8, 0.2)

    h_sr.SetLineColor(ROOT.kViolet)
    h_sr.SetMarkerColor(ROOT.kViolet)
    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 4
    plot.topMargin = 0.4
    plot.plotUp.addHistogram(h_sr, 'E0')
    plot.plotUp.addHistogram(h_mc_zpeak, 'E0')
    h_ratio  = h_mc_zpeak.Clone("ratio2")
    h_ref    = h_sr.Clone("ref2")
    h_ratio.Divide(h_sr)

    h_ref.Divide(h_sr)
    h_ref.SetTitle('MC Stat.')
    h_ref_line = h_ref.Clone("refline2")
    h_ref_line.SetLineColor(h_sr.GetLineColor())

    r = plot.plotDown.addHistogram(h_ref, 'E2')
    plot.statErrorStyle.applyTo(r)
    plot.plotDown.addHistogram(h_ref_line, 'HIST')
    plot.plotDown.addHistogram(h_ratio, 'E0')
    plot.titles.append('{}'.format(title))
    plot.draw()
    plot.saveAs('./plots/plot_ztt_zpeak_mc_vs_ztt_mc_sr_comparison_{}_{}.pdf'.format(variable.name, title.replace(',', '').replace(' ', '_')))


def make_region_comparison_plot(
    processor,
    selections,
    variable,
    process_name='Ztt',
    category ='boost_tight_high'):
    
    title = 'Boost ' if 'boost' in category else 'VBF '
    title += 'Tight ' if 'tight' in category else 'Loose ' if 'loose' in category else 'Incl '
    title += 'High' if 'high' in category else 'Low'

    print('BOOM: make plot for', title, process_name, variable.name)
    sels_sr   = filter_selections(selections, categories=category+'_sr')
    sels_zcr  = filter_selections(selections, categories=category+'_zcr')

    process = processor.get_physics_process(process_name)
    h_sr  = processor.get_hist_physics_process(process_name, sels_sr, variable)
    h_zcr = processor.get_hist_physics_process(process_name, sels_zcr, variable)

    h_sr.SetTitle('SR')
    h_zcr.SetTitle('ZCR')

    h_sr.Scale(1. / h_sr.Integral())
    h_zcr.Scale(1. / h_zcr.Integral())

    h_sr.SetLineColor(ROOT.kBlue)
    h_zcr.SetLineColor(ROOT.kRed)
    h_sr.SetMarkerColor(ROOT.kBlue)
    h_zcr.SetMarkerColor(ROOT.kRed)

    var_au = Variable('var_au', '', 'A.U.', '', Binning(1, 0.05, 0.55))
    plot = Plot(
        uuid.uuid4().hex,
        variable,
        var_au)
    plot.addHistogram(h_sr, 'E0')
    plot.addHistogram(h_zcr, 'E0')
    plot.drawStatError = True

    plot.titles.append('{}: {}'.format(title, process[0].title))
    plot.draw()
    plot.saveAs('./plots/plot_compare_region_{}_{}_{}.pdf'.format(process_name, variable.name, title.replace(',', '').replace(' ', '_')))

def make_channel_comparison_plot(
    processor,
    selections,
    variable,
    process_name='Ztt',
    title='Boost SR'):

    print('BOOM: make plot for', title, process_name, variable.name)
    sels_ipip   = filter_selections(selections, channels=('1p0n_1p0n'))
    sels_iprho  = filter_selections(selections, channels=('1p1n_1p0n','1p0n_1p1n'))
    sels_rhorho = filter_selections(selections, channels=('1p1n_1p1n'))

    process = processor.get_physics_process(process_name)
    h_ipip   = processor.get_hist_physics_process(process_name, sels_ipip, variable)
    h_iprho  = processor.get_hist_physics_process(process_name, sels_iprho, variable)
    h_rhorho = processor.get_hist_physics_process(process_name, sels_rhorho, variable)

    h_ipip.SetTitle('IP-IP')
    h_iprho.SetTitle('IP-#rho')
    h_rhorho.SetTitle('#rho-#rho')

    h_ipip.Scale(1. / h_ipip.Integral())
    h_iprho.Scale(1. / h_iprho.Integral())
    h_rhorho.Scale(1. / h_rhorho.Integral())

    h_ipip.SetLineColor(ROOT.kBlue)
    h_iprho.SetLineColor(ROOT.kRed)
    h_rhorho.SetLineColor(ROOT.kViolet)

    h_ipip.SetMarkerColor(ROOT.kBlue)
    h_iprho.SetMarkerColor(ROOT.kRed)
    h_rhorho.SetMarkerColor(ROOT.kViolet)

    var_au = Variable('var_au', '', 'A.U.', '', Binning(1, 0, 0.5))
    plot = Plot(
        uuid.uuid4().hex,
        variable,
        var_au)
    plot.addHistogram(h_ipip, 'E0')
    plot.addHistogram(h_iprho, 'E0')
    plot.addHistogram(h_rhorho, 'E0')
    plot.drawStatError = True

    plot.titles.append('{}: {}'.format(title, process[0].title))
    plot.draw()
    plot.saveAs('./plots/plot_compare_channel_{}_{}_{}.pdf'.format(process_name, variable.name, title.replace(',', '').replace(' ', '_')))

 
def make_sample_vs_reweighted_comparison_plot(
    processor,
    selections,
    variable,
    title='Boost SR',
    ratio_range=0.2):

    print('BOOM:  SM from sample vs reweighted comparison  plot for', title, variable.name)
    # retrieve the histograms
    h_ggH_SM_sample  = processor.get_hist_physics_process('ggH', selections, variable)
    h_VBFH_SM_sample = processor.get_hist_physics_process('VBFH', selections, variable)
    h_ggH_SM_reweight  = processor.get_hist_physics_process('ggH_theta_0', selections, variable)
    h_VBFH_SM_reweight = processor.get_hist_physics_process('VBFH_theta_0', selections, variable)

    h_ggH_SM_sample.Add(h_VBFH_SM_sample,1)
    h_ggH_SM_reweight.Add(h_VBFH_SM_reweight,1)
    h_ggH_SM_sample.SetTitle('Signal #phi = 0^{o} (from sample)')
    h_ggH_SM_reweight.SetTitle('Signal #phi = 0^{o} (from unpol)')

    h_ggH_SM_sample.SetLineColor(ROOT.kBlue)
    h_ggH_SM_reweight.SetLineColor(ROOT.kRed)

    h_ggH_SM_sample.SetMarkerColor(ROOT.kBlue)
    h_ggH_SM_reweight.SetMarkerColor(ROOT.kRed)

    var_ratio = Variable('var. / nom.', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_ratio.binning.nDivisions = 5
    plot = DoublePlot(
        uuid.uuid4().hex,
        variableX=variable,
        variableYDown=var_ratio, upperSize=0.8, lowerSize=0.2)
    plot.topMargin = 0.4
    plot.plotUp.addHistogram(h_ggH_SM_sample,   'E0L')
    plot.plotUp.addHistogram(h_ggH_SM_reweight, 'E0L')
    ratio_SM = h_ggH_SM_sample.Clone()
    ratio_SM.Divide(h_ggH_SM_reweight)
    ratio_SM.SetMarkerColor(ROOT.kBlack)
    ratio_SM.SetLineColor(ROOT.kBlack)
    plot.plotDown.addHistogram(ratio_SM, 'E0L')
    plot.titles.append('{}'.format(title))
    plot.draw()
    plot.saveAs('./plots/plot_sample_vs_reweighted_comparison_phi0_{}_{}.pdf'.format(variable.name,title.replace(',', '').replace(' ', '_')))

     
    # retrieve the histograms
    h_ggH_CPodd_sample  = processor.get_hist_physics_process('ggH_CPodd', selections, variable)
    h_VBFH_CPodd_sample = processor.get_hist_physics_process('VBFH_CPodd', selections, variable)
    h_ggH_CPodd_reweight  = processor.get_hist_physics_process('ggH_theta_90', selections, variable)
    h_VBFH_CPodd_reweight = processor.get_hist_physics_process('VBFH_theta_90', selections, variable)
    
    h_ggH_CPodd_sample.Add(h_VBFH_CPodd_sample,1)
    h_ggH_CPodd_reweight.Add(h_VBFH_CPodd_reweight,1)
    h_ggH_CPodd_sample.SetTitle('Signal #phi = 90^{o} (from sample)')
    h_ggH_CPodd_reweight.SetTitle('Signal #phi = 90^{o} (from unpol)')

    h_ggH_CPodd_sample.SetLineColor(ROOT.kBlue)
    h_ggH_CPodd_reweight.SetLineColor(ROOT.kRed)

    h_ggH_CPodd_sample.SetMarkerColor(ROOT.kBlue)
    h_ggH_CPodd_reweight.SetMarkerColor(ROOT.kRed)


    var_ratio = Variable('var. / nom.', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_ratio.binning.nDivisions = 5
    plot = DoublePlot(
        uuid.uuid4().hex,
        variableX=variable,
        variableYDown=var_ratio, upperSize=0.8, lowerSize=0.2)
    plot.topMargin = 0.4 
    plot.plotUp.addHistogram(h_ggH_CPodd_sample,   'E0L')
    plot.plotUp.addHistogram(h_ggH_CPodd_reweight, 'E0L')   
    ratio_CPodd = h_ggH_CPodd_sample.Clone()
    ratio_CPodd.Divide(h_ggH_CPodd_reweight)
    ratio_CPodd.SetMarkerColor(ROOT.kBlack)
    ratio_CPodd.SetLineColor(ROOT.kBlack)
    plot.plotDown.addHistogram(ratio_CPodd, 'E0L')
    plot.titles.append('{}'.format(title))
    plot.draw()
    plot.saveAs('./plots/plot_sample_vs_reweighted_comparison_phi90_{}_{}.pdf'.format(variable.name,title.replace(',', '').replace(' ', '_'))) 

       
    ###
    h_ggH_CPodd_reweight.SetLineColor(ROOT.kBlue)
    h_ggH_CPodd_reweight.SetMarkerColor(ROOT.kBlue)

    plot = DoublePlot(
        uuid.uuid4().hex,
        variableX=variable,
        variableYDown=var_ratio, upperSize=0.8, lowerSize=0.2)
    plot.topMargin = 0.4
    plot.plotUp.addHistogram(h_ggH_SM_reweight,   'E0L')
    plot.plotUp.addHistogram(h_ggH_CPodd_reweight, 'E0L')
    ratio = h_ggH_SM_reweight.Clone()
    ratio.Divide(h_ggH_CPodd_reweight)
    ratio.SetLineColor(ROOT.kBlack)
    ratio.SetMarkerColor(ROOT.kBlack)
    plot.plotDown.addHistogram(ratio, 'E0L')
    plot.titles.append('{}'.format(title))
    plot.draw()
    plot.saveAs('./plots/plot_reweighted_comparison_phi0_90_{}_{}.pdf'.format(variable.name,title.replace(',', '').replace(' ', '_')))

    if 'norm' in variable.name:
       from array import array
       error = array('d', [0])
       integral = h_ggH_SM_sample.IntegralAndError(0, h_ggH_SM_sample.GetNbinsX() + 1, error)
       print('Integral %s : %f +- %f' %(h_ggH_SM_sample.GetTitle(),integral,error[0]))
       integral = h_ggH_SM_reweight.IntegralAndError(0, h_ggH_SM_reweight.GetNbinsX() + 1, error)
       print('Integral %s : %f +- %f' %(h_ggH_SM_reweight.GetTitle(),integral,error[0]))
       integral = h_ggH_CPodd_sample.IntegralAndError(0, h_ggH_CPodd_sample.GetNbinsX() + 1, error)
       print('Integral %s : %f +- %f' %(h_ggH_CPodd_sample.GetTitle(),integral,error[0]))
       integral = h_ggH_CPodd_reweight.IntegralAndError(0, h_ggH_CPodd_reweight.GetNbinsX() + 1, error)
       print('Integral %s : %f +- %f' %(h_ggH_CPodd_reweight.GetTitle(),integral,error[0]))

def make_phistar_comparison_plot(
    processor,
    selections,
    variable,
    title=None,
    do_ggH_only=False,
    do_VBFH_only=False,
    **kwargs):
    """
    """
    
    _sels = filter_selections(selections, **kwargs)

    # plot title, file extension
    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    print('BOOM: phi star shape comparison make plot for', _title, variable.name)
    if do_ggH_only:
        signal_SM      = processor.get_hist_physics_process('ggH_theta_0', _sels, variable)
        signal_mix40   = processor.get_hist_physics_process('ggH_theta_40', _sels, variable)
        signal_CPodd   = processor.get_hist_physics_process('ggH_theta_90', _sels, variable)
        signal_mix140  = processor.get_hist_physics_process('ggH_theta_140', _sels, variable)
        process_name = "ggH"
    elif do_VBFH_only:
        signal_SM     = processor.get_hist_physics_process('VBFH_theta_0', _sels, variable)
        signal_mix40  = processor.get_hist_physics_process('VBFH_theta_40', _sels, variable)
        signal_CPodd  = processor.get_hist_physics_process('VBFH_theta_90', _sels, variable)
        signal_mix140 = processor.get_hist_physics_process('VBFH_theta_140', _sels, variable) 
        process_name = "VBFH"
    else:
        signal_SM      = processor.get_hist_physics_process('ggH_theta_0', _sels, variable)
        signal_mix40   = processor.get_hist_physics_process('ggH_theta_40', _sels, variable)
        signal_CPodd   = processor.get_hist_physics_process('ggH_theta_90', _sels, variable)
        signal_mix140  = processor.get_hist_physics_process('ggH_theta_140', _sels, variable)

        h_VBFH_SM     = processor.get_hist_physics_process('VBFH_theta_0', _sels, variable)
        h_VBFH_mix40  = processor.get_hist_physics_process('VBFH_theta_40', _sels, variable)
        h_VBFH_CPodd  = processor.get_hist_physics_process('VBFH_theta_90', _sels, variable)
        h_VBFH_mix140 = processor.get_hist_physics_process('VBFH_theta_140', _sels, variable)

        signal_SM.Add(h_VBFH_SM,1)
        signal_CPodd.Add(h_VBFH_CPodd,1)
        signal_mix40.Add(h_VBFH_mix40,1)
        signal_mix140.Add(h_VBFH_mix140,1)

        process_name = "Signal"

    h_Ztt = processor.get_hist_physics_process('ZttQCD', _sels, variable)

    signal_SM.SetTitle( process_name + ' #phi = 0')
    signal_mix40.SetTitle( process_name + ' #phi = 40^{o}')
    signal_CPodd.SetTitle( process_name + ' #phi = 90^{o}')
    signal_mix140.SetTitle( process_name + ' #phi = 140^{o}')
    h_Ztt.SetTitle('Z#rightarrow#tau#tau')

    signal_SM.Scale(1. / signal_SM.Integral())
    signal_CPodd.Scale(1. / signal_CPodd.Integral())
    signal_mix40.Scale(1. / signal_mix40.Integral())
    signal_mix140.Scale(1. / signal_mix140.Integral())
    h_Ztt.Scale(1. / h_Ztt.Integral())

    signal_SM.SetLineColor(ROOT.kBlue)
    signal_CPodd.SetLineColor(ROOT.kRed)
    signal_mix40.SetLineColor(ROOT.kViolet)
    signal_mix140.SetLineColor(ROOT.kGreen)
    h_Ztt.SetLineColor(ROOT.kBlack)

    signal_SM.SetMarkerColor(ROOT.kBlue)
    signal_CPodd.SetMarkerColor(ROOT.kRed)
    signal_mix40.SetMarkerColor(ROOT.kViolet)
    signal_mix140.SetMarkerColor(ROOT.kGreen)
    h_Ztt.SetMarkerColor(ROOT.kBlack)

    yaxis_var = Variable('yaxis_var', '1', 'A.U.', '', Binning(1, 0.05, 0.20))
    plot = Plot(
        uuid.uuid4().hex,
        variableX=variable,
        variableY=yaxis_var)
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.addHistogram(signal_SM, 'E0L')
    plot.addHistogram(signal_CPodd, 'E0L')
    plot.addHistogram(h_Ztt, 'E0L')
    #plot.drawStatError = True

    plot.titles.append('{}'.format(_title))
    plot.draw()
    plot.saveAs('./plots/plot_{}_shape_comparison_comp1_unpol_{}_{}.pdf'.format(variable.name,process_name,_ext))

    plot = Plot(
        uuid.uuid4().hex,
        variableX=variable,
        variableY=yaxis_var)
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.addHistogram(signal_mix40, 'E0L')
    plot.addHistogram(signal_mix140, 'E0L')
    plot.addHistogram(h_Ztt, 'E0L')
    #plot.drawStatError = True

    plot.titles.append('{}'.format(_title))
    plot.draw()
    plot.saveAs('./plots/plot_{}_shape_comparison_comp2_unpol_{}_{}.pdf'.format(variable.name,process_name,_ext)) 


def make_phistar_theory_syst_plot(
    processor,
    selections,
    channel,
    variable,
    title='Preselection',
    ratio_range=0.2,
    do_ggH_only=True,
    do_VBFH_only=True,
    do_WH_only=True,
    do_ZH_only=True,
    do_ttH_only=True,
    ):

    print('BOOM:  Phi star theory syst extraction for', title, variable.name)
    # retrieve the histograms
    if do_ggH_only :
        signal_SM_sample  = processor.get_hist_physics_process('ggH', selections, variable)
        signal_SM_reweight  = processor.get_hist_physics_process('ggH_theta_0', selections, variable)
        process_name = 'ggH'
    elif do_VBFH_only:
        signal_SM_sample  = processor.get_hist_physics_process('VBFH', selections, variable)
        signal_SM_reweight  = processor.get_hist_physics_process('VBFH_theta_0', selections, variable)
        process_name = 'VBFH'
    elif do_WH_only :
        signal_SM_sample  = processor.get_hist_physics_process('WH', selections, variable)
        signal_SM_reweight  = processor.get_hist_physics_process('WH_theta_0', selections, variable)
        process_name = 'WH'
    elif do_ZH_only :
        signal_SM_sample  = processor.get_hist_physics_process('ZH', selections, variable)
        signal_SM_reweight  = processor.get_hist_physics_process('ZH_theta_0', selections, variable)
        process_name = 'ZH'
    elif do_ttH_only :
        signal_SM_sample  = processor.get_hist_physics_process('ttH', selections, variable)
        signal_SM_reweight  = processor.get_hist_physics_process('ttH_theta_0', selections, variable)
        process_name = 'ttH'
    else:
        signal_SM_sample   = processor.get_hist_physics_process('ggH', selections, variable)
        signal_SM_reweight = processor.get_hist_physics_process('ggH_theta_0', selections, variable)
        h_VBFH_SM_sample   = processor.get_hist_physics_process('VBFH', selections, variable)
        h_VBFH_SM_reweight = processor.get_hist_physics_process('VBFH_theta_0', selections, variable)
        h_WH_SM_sample     = processor.get_hist_physics_process('WH', selections, variable)
        h_WH_SM_reweight   = processor.get_hist_physics_process('WH_theta_0', selections, variable)
        h_ZH_SM_sample     = processor.get_hist_physics_process('ZH', selections, variable)
        h_ZH_SM_reweight   = processor.get_hist_physics_process('ZH_theta_0', selections, variable)
        h_ttH_SM_sample    = processor.get_hist_physics_process('ttH', selections, variable)
        h_ttH_SM_reweight  = processor.get_hist_physics_process('ttH_theta_0', selections, variable)

        signal_SM_sample.Add(h_VBFH_SM_sample,1)
        signal_SM_sample.Add(h_WH_SM_sample,1)
        signal_SM_sample.Add(h_ZH_SM_sample,1)
        signal_SM_sample.Add(h_ttH_SM_sample,1)

        signal_SM_reweight.Add(h_VBFH_SM_reweight,1)
        signal_SM_reweight.Add(h_WH_SM_reweight,1)
        signal_SM_reweight.Add(h_ZH_SM_reweight,1)
        signal_SM_reweight.Add(h_ttH_SM_reweight,1)

        process_name = 'Signal'
    

    signal_SM_sample.SetTitle(process_name + ' #phi = 0^{o} (from sample)')
    signal_SM_reweight.SetTitle(process_name + ' #phi = 0^{o} (from unpol)')
    signal_SM_sample.SetLineColor(ROOT.kBlue)
    signal_SM_reweight.SetLineColor(ROOT.kRed)
    signal_SM_sample.SetMarkerColor(ROOT.kBlue)
    signal_SM_reweight.SetMarkerColor(ROOT.kRed)

    signal_SM_sample.Scale(1/signal_SM_sample.Integral())
    signal_SM_reweight.Scale(1/signal_SM_reweight.Integral())
 
    var_au    = Variable('au', '', 'A.U.', '', Binning(1, 0, 0.7))
    var_ratio = Variable('SM / unpol', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_ratio.binning.nDivisions = 5
    plot = DoublePlot(
        uuid.uuid4().hex,
        variableX=variable,
        variableYUp=var_au,
        variableYDown=var_ratio, upperSize=0.8, lowerSize=0.2)
    plot.topMargin = 0.4
    plot.plotUp.addHistogram(signal_SM_sample,   'E0L')
    plot.plotUp.addHistogram(signal_SM_reweight, 'E0L')
 
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.04
    plot.titleDecorator.labelTextSize = 0.04

    h_ratio  = signal_SM_sample.Clone()
    h_ref    = signal_SM_reweight.Clone()
    h_ratio.Divide(signal_SM_reweight)
    h_ratio.SetLineColor(ROOT.kBlack)
    h_ratio.SetMarkerColor(ROOT.kBlack)
    h_ref.Divide(signal_SM_reweight)
    h_ref.SetTitle('MC Stat.')
    h_ref.SetFillColor(1);
    h_ref.SetLineColor(0);
    h_ref.SetMarkerStyle(1);
    h_ref.SetMarkerSize(0.0001);
    h_ref.SetMarkerColor(1);
    h_ref.SetFillStyle(3004);
    r = plot.plotDown.addHistogram(h_ref, 'E2')
    plot.plotDown.addHistogram(h_ratio, 'HIST')

    plot.titles.append('{}'.format(title))

    if '1p1n_1p1n' in channel : channel = '1p1n_1p1n'
    elif '1p0n_1p0n' in channel : channel = '1p0n_1p0n'
    elif '1p1n_1p0n' in channel : channel = '1p1n_1p0n'
    elif '1p0n_1p1n' in channel : channel = '1p0n_1p1n'
    elif '1p1n_1pXn' in channel : channel = '1p1n_1pXn'
    elif '1pXn_1p1n' in channel : channel = '1pXn_1p1n'
    elif '1p0n_1pXn' in channel : channel = '1p0n_1pXn'
    elif '1pXn_1p0n' in channel : channel = '1pXn_1p0n'
    elif '3p0n_1p1n' in channel : channel = '3p0n_1p1n'
    elif '1p1n_3p0n' in channel : channel = '1p1n_3p0n' 

    plot.titles.append('{}'.format(channel))
    plot.draw()
    plot.saveAs('./plots/plot_phistar_theory_syst_{}_{}_{}_{}.pdf'.format(variable.name,channel,process_name,title.replace(',', '').replace(' ', '_')))

    outfile = ROOT.TFile('hh_theory_uncert_{}_{}_{}.root'.format(variable.name, channel, process_name),'recreate')
    h_ratio.SetName('hh_theory_uncert_{}_{}_{}'.format(variable.name, channel, process_name))
    h_ratio.Write()
    outfile.Close()
    
 
def make_sample_vs_reweighted_comparison_plot(
    processor,
    selections,
    variable,
    title='Boost SR',
    ratio_range=0.2,
    do_ggH_only=False,
    do_VBFH_only=False):
 

    print('BOOM:  SM from sample vs reweighted comparison  plot for', title, variable.name)
    # retrieve the histograms
    if do_ggH_only:
        signal_SM_sample  = processor.get_hist_physics_process('ggH', selections, variable)
        signal_SM_reweight = processor.get_hist_physics_process('ggH_theta_0', selections, variable)
        process_name = 'ggH'
    elif do_VBFH_only:
        signal_SM_sample = processor.get_hist_physics_process('VBFH', selections, variable)
        signal_SM_reweight = processor.get_hist_physics_process('VBFH_theta_0', selections, variable)
        process_name = 'VBFH'
    else:
        signal_SM_sample  = processor.get_hist_physics_process('ggH', selections, variable)
        signal_SM_reweight = processor.get_hist_physics_process('ggH_theta_0', selections, variable)
        h_VBFH_SM_sample = processor.get_hist_physics_process('VBFH', selections, variable)
        h_VBFH_SM_reweight = processor.get_hist_physics_process('VBFH_theta_0', selections, variable)
        signal_SM_sample.Add(h_VBFH_SM_sample,1)
        signal_SM_reweight.Add(h_VBFH_SM_reweight,1)
        process_name = 'Signal'

    signal_SM_sample.SetTitle( process_name +' #phi = 0^{o} (from sample)')
    signal_SM_reweight.SetTitle(process_name +' #phi = 0^{o} (from unpol)')
    signal_SM_sample.SetLineColor(ROOT.kBlue)
    signal_SM_reweight.SetLineColor(ROOT.kRed)
    signal_SM_sample.SetMarkerColor(ROOT.kBlue)
    signal_SM_reweight.SetMarkerColor(ROOT.kRed)

    var_ratio = Variable('unpol / SM.', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_ratio.binning.nDivisions = 5
    plot = DoublePlot(
        uuid.uuid4().hex,
        variableX=variable,
        variableYDown=var_ratio, upperSize=0.8, lowerSize=0.2)
    plot.topMargin = 0.4
    plot.plotUp.addHistogram(signal_SM_sample,   'E0L')
    plot.plotUp.addHistogram(signal_SM_reweight, 'E0L')

    h_ratio  = signal_SM_reweight.Clone()
    h_ref    = signal_SM_sample.Clone()
    h_ratio.SetLineColor(ROOT.kBlack)
    h_ratio.SetMarkerColor(ROOT.kBlack)
    h_ratio.Divide(signal_SM_sample)
    h_ref.Divide(signal_SM_sample)
    h_ref.SetTitle('MC Stat.')
    h_ref.SetFillColor(1);
    h_ref.SetLineColor(0);
    h_ref.SetMarkerStyle(1);
    h_ref.SetMarkerSize(0.0001);
    h_ref.SetMarkerColor(1);
    h_ref.SetFillStyle(3004); 
    r = plot.plotDown.addHistogram(h_ref, 'E2')
    plot.plotDown.addHistogram(h_ratio, 'HIST')

    plot.titles.append('{}'.format(title))
    plot.draw()
    plot.saveAs('./plots/plot_sample_vs_reweighted_comparison_phi0_{}_{}_{}.pdf'.format(variable.name,process_name,title.replace(',', '').replace(' ', '_')))

     
    # retrieve the histograms
    if do_ggH_only:
        signal_CPodd_sample  = processor.get_hist_physics_process('ggH_CPodd', selections, variable)
        signal_CPodd_reweight  = processor.get_hist_physics_process('ggH_theta_90', selections, variable)
        process_name = 'ggH'
    elif do_VBFH_only:
        signal_CPodd_sample = processor.get_hist_physics_process('VBFH_CPodd', selections, variable)
        signal_CPodd_reweight  = processor.get_hist_physics_process('VBFH_theta_90', selections, variable)
        process_name = 'VBFH'
    else:
        signal_CPodd_sample  = processor.get_hist_physics_process('ggH_CPodd', selections, variable)
        signal_CPodd_reweight  = processor.get_hist_physics_process('ggH_theta_90', selections, variable)
        h_VBFH_CPodd_sample = processor.get_hist_physics_process('VBFH_CPodd', selections, variable)
        h_VBFH_CPodd_reweight = processor.get_hist_physics_process('VBFH_theta_90', selections, variable)
        signal_CPodd_sample.Add(h_VBFH_CPodd_sample,1)
        signal_CPodd_reweight.Add(h_VBFH_CPodd_reweight,1)
        process_name = 'Signal'

    signal_CPodd_sample.SetTitle(process_name + ' #phi = 90^{o} (from sample)')
    signal_CPodd_reweight.SetTitle(process_name + ' #phi = 90^{o} (from unpol)')
    signal_CPodd_sample.SetLineColor(ROOT.kBlue)
    signal_CPodd_reweight.SetLineColor(ROOT.kRed)
    signal_CPodd_sample.SetMarkerColor(ROOT.kBlue)
    signal_CPodd_reweight.SetMarkerColor(ROOT.kRed)

    var_ratio = Variable('unpol. / CP Odd.', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_ratio.binning.nDivisions = 5
    plot = DoublePlot(
        uuid.uuid4().hex,
        variableX=variable,
        variableYDown=var_ratio, upperSize=0.8, lowerSize=0.2)
    plot.topMargin = 0.4 
    plot.plotUp.addHistogram(signal_CPodd_sample,   'E0L')
    plot.plotUp.addHistogram(signal_CPodd_reweight, 'E0L')   

    h_ratio  = signal_CPodd_reweight.Clone()
    h_ref    = signal_CPodd_sample.Clone()
    h_ratio.Divide(signal_CPodd_sample)
    h_ratio.SetLineColor(ROOT.kBlack)
    h_ratio.SetMarkerColor(ROOT.kBlack)
    h_ref.Divide(signal_CPodd_sample)
    h_ref.SetTitle('MC Stat.')
    h_ref.SetFillColor(1);
    h_ref.SetLineColor(0);
    h_ref.SetMarkerStyle(1);
    h_ref.SetMarkerSize(0.0001);
    h_ref.SetMarkerColor(1);
    h_ref.SetFillStyle(3004);
    r = plot.plotDown.addHistogram(h_ref, 'E2')
    plot.plotDown.addHistogram(h_ratio, 'HIST')
    plot.titles.append('{}'.format(title))
    plot.draw()
    plot.saveAs('./plots/plot_sample_vs_reweighted_comparison_phi90_{}_{}_{}.pdf'.format(variable.name,process_name,title.replace(',', '').replace(' ', '_'))) 

       
    ###
    signal_CPodd_reweight.SetLineColor(ROOT.kBlue)
    signal_CPodd_reweight.SetMarkerColor(ROOT.kBlue)

    plot = DoublePlot(
        uuid.uuid4().hex,
        variableX=variable,
        variableYDown=var_ratio, upperSize=0.8, lowerSize=0.2)
    plot.topMargin = 0.4
    plot.plotUp.addHistogram(signal_SM_reweight,   'E0L')
    plot.plotUp.addHistogram(signal_CPodd_reweight, 'E0L')
    ratio = signal_SM_reweight.Clone()
    ratio.Divide(signal_CPodd_reweight)
    ratio.SetLineColor(ROOT.kBlack)
    ratio.SetMarkerColor(ROOT.kBlack)
    plot.plotDown.addHistogram(ratio, 'E0L')
    plot.titles.append('{}'.format(title))
    plot.draw()
    plot.saveAs('./plots/plot_reweighted_comparison_phi0_90_{}_{}_{}.pdf'.format(variable.name,process_name,title.replace(',', '').replace(' ', '_')))

    if 'norm' in variable.name:
       from array import array
       error = array('d', [0])
       integral = signal_SM_sample.IntegralAndError(0, signal_SM_sample.GetNbinsX() + 1, error)
       print('Integral %s : %f +- %f' %(signal_SM_sample.GetTitle(),integral,error[0]))
       integral = signal_SM_reweight.IntegralAndError(0, signal_SM_reweight.GetNbinsX() + 1, error)
       print('Integral %s : %f +- %f' %(signal_SM_reweight.GetTitle(),integral,error[0]))
       integral = signal_CPodd_sample.IntegralAndError(0, signal_CPodd_sample.GetNbinsX() + 1, error)
       print('Integral %s : %f +- %f' %(signal_CPodd_sample.GetTitle(),integral,error[0]))
       integral = signal_CPodd_reweight.IntegralAndError(0, signal_CPodd_reweight.GetNbinsX() + 1, error)
       print('Integral %s : %f +- %f' %(signal_CPodd_reweight.GetTitle(),integral,error[0]))

def make_sys_nom_plot(
    processor,
    selections,
    variable,
    process_name,
    syst_name,
    title=None,
    print_lumi=True,
    ratio_range=0.2,
    **kwargs):
    """
    """

    _sels = filter_selections(selections, **kwargs)

    # plot title, file extension
    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    nom = processor.get_hist_physics_process(process_name, _sels, variable)
    sys = processor.get_hist_physics_process(process_name, _sels, variable, syst_name=syst_name, syst_type='up')

    nom.SetLineColor(ROOT.kBlue)
    nom.SetMarkerColor(ROOT.kBlue)
    sys.SetLineColor(ROOT.kRed)
    sys.SetMarkerColor(ROOT.kRed)

    print(nom.Integral())
    print(sys.Integral())

    sys.SetTitle(syst_name)
    nom.SetTitle('nominal')

    var_ratio = Variable('var. / nom.', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_ratio.binning.nDivisions = 5

    plot = DoublePlot(
        uuid.uuid4().hex,
        variable,
        var_Entries,
        var_ratio, 0.8, 0.2)

    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Internal')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 4
    plot.topMargin = 0.4

    plot.plotUp.addHistogram(nom, 'HIST')
    plot.plotUp.addHistogram(sys, 'HIST')

    h_ratio  = sys.Clone()
    h_ref    = sys.Clone()
    h_ratio.Divide(nom)
    h_ref.Divide(sys)
    h_ref.SetTitle('MC Stat.')
    h_ref_line = h_ref.Clone()
    h_ref_line.SetLineColor(nom.GetLineColor())

    r = plot.plotDown.addHistogram(h_ref, 'E2')
    plot.statErrorStyle.applyTo(r)
    plot.plotDown.addHistogram(h_ref_line, 'HIST')
    plot.plotDown.addHistogram(h_ratio, 'HIST')
    te = TextElement(_title, x=0.26, y=0.94)
    plot.textElements.append(te)

    plot.legendElements.append(LegendElement(
            h_ref, 'MC Stat.', 'f'))
    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )


    plot.draw()
    plot.saveAs('plots/envelope_{}_{}_{}.pdf'.format(syst_name, variable.name, _ext))

def compare_upsilon_reco(
    processor,
    selections,
    variable1,
    variable2,
    process_name='ZttQCD',
    title=None,
    print_lumi=True,
    ratio_range=0.05,
    **kwargs):
    """
    """

    _sels = filter_selections(selections, **kwargs)

    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    var1 = processor.get_hist_physics_process(process_name, _sels, variable1)
    var2 = processor.get_hist_physics_process(process_name, _sels, variable2)

    var1.SetLineColor(ROOT.kRed)
    var1.SetMarkerColor(ROOT.kRed)
    var2.SetLineColor(ROOT.kBlue)
    var2.SetMarkerColor(ROOT.kBlue)

    var1.SetTitle('#Ypsilon  ntuples')
    var2.SetTitle('#Ypsilon  code')

    var_ratio = Variable('Var. / Ntup.', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_ratio.binning.nDivisions = 5

    plot = DoublePlot(
        uuid.uuid4().hex,
        variable1,
        var_Entries,
        var_ratio, 0.8, 0.2)

    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Simulation')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 10
    plot.topMargin = 0.6
    plot.titles.append(process_name)

    plot.plotUp.addHistogram(var1,   'E0')
    plot.plotUp.addHistogram(var2,   'E0')
    h_ratio_2   = var2.Clone("ratio_2")
    h_ratio_2.Divide(var1)
    plot.plotDown.addHistogram(h_ratio_2,   'E0')

    te = TextElement(_title, x=0.26, y=0.94)
    plot.textElements.append(te)

    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )

    plot.draw()
    plot.saveAs('plots/plot_upsilon_ntuples_vs_reco_{}_{}_{}.pdf'.format(process_name, variable1.name, _ext))



def compare_phistar_reco(
    processor,
    selections,
    variable1,
    variable2,
    variable3,
    variable4,
    variable5,
    process_name='ZttQCD',
    title=None,
    print_lumi=True,
    ratio_range=0.05,
    **kwargs):
    """
    """

    _sels = filter_selections(selections, **kwargs)

    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    var1 = processor.get_hist_physics_process(process_name, _sels, variable1)
    var2 = processor.get_hist_physics_process(process_name, _sels, variable2) 
    var3 = processor.get_hist_physics_process(process_name, _sels, variable3)
    var4 = processor.get_hist_physics_process(process_name, _sels, variable4)
    var5 = processor.get_hist_physics_process(process_name, _sels, variable5)

    var1.SetLineColor(ROOT.kBlack)
    var1.SetMarkerColor(ROOT.kBlack)
    var2.SetLineColor(ROOT.kBlue)
    var2.SetMarkerColor(ROOT.kBlue)
    var3.SetLineColor(ROOT.kRed)
    var3.SetMarkerColor(ROOT.kRed)
    var4.SetLineColor(ROOT.kViolet)
    var4.SetMarkerColor(ROOT.kViolet)
    var5.SetLineColor(ROOT.kGreen)
    var5.SetMarkerColor(ROOT.kGreen)

    var1.SetTitle('#phi*  ntuples')
    var2.SetTitle('#phi*  code')
    var3.SetTitle('#phi*  code #eta smearing') 
    var4.SetTitle('#phi*  code #phi smearing')
    var5.SetTitle('#phi*  code #pi_{0} energy scale')

    var_ratio = Variable('Var. / Ntup.', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_ratio.binning.nDivisions = 5

    plot = DoublePlot(
        uuid.uuid4().hex,
        variable1,
        var_Entries,
        var_ratio, 0.8, 0.2)

    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Simulation')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 10
    plot.topMargin = 0.6
    plot.titles.append(process_name)

    plot.plotUp.addHistogram(var1,   'E0')
    plot.plotUp.addHistogram(var2,   'E0')
    plot.plotUp.addHistogram(var3,   'E0')
    plot.plotUp.addHistogram(var4,   'E0')
    plot.plotUp.addHistogram(var5,   'E0')
    h_ratio_2   = var2.Clone("ratio_2")
    h_ratio_2.Divide(var1)
    plot.plotDown.addHistogram(h_ratio_2,   'E0')
    h_ratio_3   = var3.Clone("ratio_3")
    h_ratio_3.Divide(var1)
    plot.plotDown.addHistogram(h_ratio_3,   'E0')
    h_ratio_4   = var4.Clone("ratio_4")
    h_ratio_4.Divide(var1)
    plot.plotDown.addHistogram(h_ratio_4,   'E0')
    h_ratio_5   = var5.Clone("ratio_5")
    h_ratio_5.Divide(var1)
    plot.plotDown.addHistogram(h_ratio_5,   'E0')

    te = TextElement(_title, x=0.26, y=0.94)
    plot.textElements.append(te)

    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )

    plot.draw()
    plot.saveAs('plots/plot_phistar_ntuples_vs_reco_{}_{}_{}.pdf'.format(process_name, variable1.name, _ext))



def make_popy_vs_he7_plot(
    processor,
    selections,
    variable,
    title=None,
    print_lumi=True,
    ratio_range=0.2,
    **kwargs):
    """
    """

    _sels = filter_selections(selections, **kwargs)

    if title is None:
        _title, _ext = plot_title_extension(_sels, **kwargs)
    else:
        _title = title
        _ext = title.replace(',', '').replace(' ', '_')

    popy8     = processor.get_hist_physics_process('ggH', _sels, variable)
    popy8_vbf = processor.get_hist_physics_process('VBFH', _sels, variable)
    popy8_wh  = processor.get_hist_physics_process('WH', _sels, variable)
    popy8_zh  = processor.get_hist_physics_process('ZH', _sels, variable)

    popy8.Add(popy8_vbf,1)
    popy8.Add(popy8_wh,1)
    popy8.Add(popy8_zh,1)
    print(popy8.Integral())

    pohe7 = processor.get_hist_physics_process('ggH_He7', _sels, variable)
    pohe7_vbf = processor.get_hist_physics_process('VBFH_He7', _sels, variable)
    pohe7_wh  = processor.get_hist_physics_process('WH_He7', _sels, variable)
    pohe7_zh  = processor.get_hist_physics_process('ZH_He7', _sels, variable)

    pohe7.Add(pohe7_vbf,1)
    pohe7.Add(pohe7_wh,1)
    pohe7.Add(pohe7_zh,1)
    print(pohe7.Integral())

    pohe7.SetLineColor(ROOT.kRed)
    pohe7.SetMarkerColor(ROOT.kRed)
    popy8.SetLineColor(ROOT.kBlue)
    popy8.SetMarkerColor(ROOT.kBlue)

    pohe7.SetTitle('Powheg+Herwig7')
    popy8.SetTitle('Powheg+Pythia8')

    var_ratio = Variable('PoHe7 / PoPy8', binning=Binning(low=1 - ratio_range, high=1 + ratio_range))
    var_ratio.binning.nDivisions = 5

    plot = DoublePlot(
        uuid.uuid4().hex,
        variable,
        var_Entries,
        var_ratio, 0.8, 0.2)

    plot.statErrorTitle = 'Stat.'
    plot.statErrorStyle = Style(ROOT.kBlack, lineWidth=0, fillStyle=3354, markerStyle=0)
    plot.drawStatError = True
    plot.titleDecorator = AtlasTitleDecorator('Simulation')
    plot.titleDecorator.textSize = 0.03
    plot.titleDecorator.labelTextSize = 0.03
    plot.legendDecorator.textSize = 0.03
    plot.legendDecorator.maxEntriesPerColumn = 4

    plot.topMargin = 0.4

    plot.plotUp.addHistogram(popy8,   'E0')
    plot.plotUp.addHistogram(pohe7,   'E0')
    h_ratio   = pohe7.Clone("ratio")
    h_ratio.SetLineColor(ROOT.kBlack)
    h_ratio.SetMarkerColor(ROOT.kBlack)
    h_ratio.Divide(popy8)
    plot.plotDown.addHistogram(h_ratio,   'E0')

    te = TextElement(_title, x=0.26, y=0.94)
    plot.textElements.append(te)

    if print_lumi:
        luminosity = lumi_calc(_sels)
        plot.titles.append('L = %.3g fb^{-1}' % (luminosity / 1000.) )

    plot.draw()
    plot.saveAs('plots/plot_popy_vs_pohe_{}_{}.pdf'.format(variable.name, _ext))


def make_yield_comparison_plot(
    processor,
    selections,
    category,
    variable,
    reference,
    print_lumi=True,
    **kwargs):
    
    from ..cpangles import  CPANGLES

    _sels = filter_selections(selections, categories=category, **kwargs)
    _title, _ext = plot_title_extension(_sels, **kwargs)

    c = ROOT.TCanvas()
    c.SetBottomMargin(0.3)
    h_template = ROOT.TH1F('h_temp', '', len(CPANGLES[reference]), 0, len(CPANGLES[reference]))

    i = 0
    for sample in list(CPANGLES[reference].keys()):
        h_template.GetXaxis().SetBinLabel(i + 1, str(i*10))
        h_template.GetXaxis().SetTitle('#phi')
        h_template.GetYaxis().SetTitle('Templates/Reference')
        i += 1
    h_template.GetYaxis().SetRangeUser(0.95, 1.05)
    h_template.Draw()
   
    # retrieve reference histogram
    nom = processor.get_hist_physics_process(reference+'_theta_0', _sels, variable)

    i = 0
    graph = ROOT.TGraphErrors(len(CPANGLES[reference]))
    for sample in list(CPANGLES[reference].keys()):
        var = processor.get_hist_physics_process(reference+'_theta_'+str(i*10), _sels, variable)
        h_ratio = var.Clone("clone"+reference+sample)
        h_ratio.Divide(nom)
        graph.SetPoint(i, i + 0.5, h_ratio.GetBinContent(1))
        graph.SetPointError( i, 0, h_ratio.GetBinError(1))
        i += 1
        graph.SetLineColor(ROOT.kRed)
    graph.SetMarkerColor(ROOT.kRed)

    graph.Draw('samePE')
    # plot title, file extension
    _ext = _title.replace(',', '').replace(' ', '_')

    label = ROOT.TLatex(
       c.GetLeftMargin() + 0.02, 1 - c.GetTopMargin() - 0.07, 
       'Category : {}, Sample = {}'.format(category,reference))
    label.SetNDC()
    label.Draw()
    l_1 = ROOT.TLine(0, 1, len(CPANGLES[reference]), 1)
    l_1.Draw()
    c.RedrawAxis()
    c.SaveAs('plots/plot_norm_study_{}_{}.pdf'.format(reference, _ext))

