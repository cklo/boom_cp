"""
Implementation of the Z+jets ckkw and qsf weights
"""
from happy.systematics import Systematics, SystematicsSet

def get_ztt_weights(channel):
    """Retrieve the ckkq and qsf weights

    Parameters
    __________
    channel : str
       see CHANNELS in cuts/__init__.py

    Returns
    _______
    sys_set : HAPPy SystematicsSet
       set of weights for qcd and ckkw Z+jets theory uncertainties

    Raises
    ______
    NotImplementedError
       If there is no weight for a given combination of input parameters
    """

    from .ztheosyst import ztheosyst_expr
    # hadhad
    if channel in ('1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','1p0n_1pXn','1pXn_1p0n','1p1n_3p0n','3p0n_1p1n'):
        ztheo_ckkw_expr = ztheosyst_expr(2,2)
        ztheo_qsf_expr  = ztheosyst_expr(1,2)
    else:
        raise NotImplementedError

    sys_ckkw = Systematics.weightSystematics('theory_z_ckk', 'theory_z_ckk',
                                                    '('+ztheo_ckkw_expr+')',  
                                                    '(2-'+ztheo_ckkw_expr+')',
                                                    '(1)')

    sys_qsf  = Systematics.weightSystematics('theory_z_qsf', 'theory_z_qsf',
                                                    '('+ztheo_qsf_expr+')',
                                                    '(2-'+ztheo_qsf_expr+')',
                                                    '(1)') 
  
    sys_set  = SystematicsSet(set([ sys_ckkw,
                                    sys_qsf]))

    return sys_set
        
