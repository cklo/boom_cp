# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_phistar_comparison_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes(cpangles=True)

do_ggH_only    = False
do_VBFH_only   = True

if do_ggH_only :
    physicsProcesses = [p for p in physicsProcesses if 'ggH' in p.name or 'Ztt' in p.name]
elif do_VBFH_only :
    physicsProcesses = [p for p in physicsProcesses if 'VBFH' in p.name or 'Ztt' in p.name]
else:
    physicsProcesses = [p for p in physicsProcesses if 'ggH' in p.name or 'VBFH' in p.name or 'Ztt' in p.name]

categories =( #'preselection',
#              'preselection_high',
#              'preselection_low',
              # pileup check
              'preselection_mu_0_20',
              'preselection_mu_20_30',
              'preselection_mu_30_40',
              'preselection_mu_40_50',
              'preselection_mu_50_60',
              'preselection_mu_50_80',
#              'boost_sr',
              #'boost_tight_sr',
              #'boost_loose_sr',
#              'vbf_sr',
              #'vbf_0_sr',
              #'vbf_1_sr',
#              'boost_zcr',
#              'vbf_zcr',
#              'boost_zcr_0',
#              'vbf_zcr_0',
              # 'boost_zcr_1',
              # 'vbf_zcr_1',
              ## optimised signal regions
#             'boost_high_sr',
#             'boost_low_sr',
#             'boost_high_zcr',
#             'boost_low_zcr',
#             'boost_high_zcr_0',
#             'boost_low_zcr_0',
#             'vbf_high_sr',
#             'vbf_low_sr',
#             'vbf_high_zcr',
#             'vbf_high_zcr_0',
#             'vbf_low_zcr',
#             'vbf_low_zcr_0',
             # 'boost_loose_high_sr',
             # 'boost_loose_low_sr',
             # 'boost_loose_high_zcr_0',
             # 'boost_loose_low_zcr_0',
             # 'boost_tight_high_sr',
             # 'boost_tight_low_sr',
             # 'boost_tight_high_zcr_0',
             # 'boost_tight_low_zcr_0', 
             # 'vbf_0_high_sr',
             # 'vbf_0_low_sr',
             # 'vbf_1_high_sr',
             # 'vbf_1_low_sr',
             # 'vbf_0_high_zcr_0',
             # 'vbf_0_low_zcr_0',
             # 'vbf_1_high_zcr_0',
             # 'vbf_1_low_zcr_0',
            )

### define your selection objects 
sels = get_selections(
    channels=('1p0n_1p0n', '1p0n_1p1n', '1p1n_1p0n', '1p1n_1p1n', '1p1n_1pXn', '1p1n_3p0n', '1pXn_1p1n', '3p0n_1p1n', '1p0n_1pXn', '1pXn_1p0n'), 
    regions='SR',
#    years=('18'),
    categories= categories)

### define your list of variables
variables = [
    VARIABLES['phi_star_9bins'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

title = 'preselection'

### plot making
for var in variables:
  for category in categories:
    make_phistar_comparison_plot(processor, sels, var, categories=category, do_ggH_only=do_ggH_only, do_VBFH_only=do_VBFH_only)
    make_phistar_comparison_plot(processor, sels, var, categories=category, do_ggH_only=do_ggH_only, do_VBFH_only=do_VBFH_only, channels=('1p0n_1p0n'))
    make_phistar_comparison_plot(processor, sels, var, categories=category, do_ggH_only=do_ggH_only, do_VBFH_only=do_VBFH_only, channels=('1p0n_1p1n','1p1n_1p0n'))
    make_phistar_comparison_plot(processor, sels, var, categories=category, do_ggH_only=do_ggH_only, do_VBFH_only=do_VBFH_only, channels=('1p1n_1p1n'))
    make_phistar_comparison_plot(processor, sels, var, categories=category, do_ggH_only=do_ggH_only, do_VBFH_only=do_VBFH_only, channels=('1p0n_1pXn','1pXn_1p0n'))
    make_phistar_comparison_plot(processor, sels, var, categories=category, do_ggH_only=do_ggH_only, do_VBFH_only=do_VBFH_only, channels=('1p1n_1pXn','1pXn_1p1n'))
    make_phistar_comparison_plot(processor, sels, var, categories=category, do_ggH_only=do_ggH_only, do_VBFH_only=do_VBFH_only, channels=('1p1n_3p0n','3p0n_1p1n'))

print('closing stores...')
close_stores(physicsProcesses)
print('done')
