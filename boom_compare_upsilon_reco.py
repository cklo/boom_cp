# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import compare_upsilon_reco
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# Switch on debug output
#import logging
#logging.root.setLevel( logging.DEBUG )

# filter out unused samples
process_name =  'ggH'
physicsProcesses = get_processes(squash=True, ntuples_block='nom')
ztt_processes = [p for p in physicsProcesses if process_name == p.name]

categories =( 
                'preselection',
            )

channels=('1p0n_1p0n', '1p0n_1p1n', '1p1n_1p0n', '1p1n_1p1n', '1p1n_1pXn', '1p1n_3p0n', '1pXn_1p1n', '3p0n_1p1n', '1p0n_1pXn', '1pXn_1p0n')

### define your selection objects 
sels = get_selections(
            channels=channels,
            categories= categories,
            regions='SR'
            )


# define your list of variables
variables = [
     VARIABLES['tau_0_upsilon'],
     VARIABLES['recalc_tau_0_upsilon'],
]

# #### processor declaration, booking and running
processor = boom_processor(ztt_processes, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)


# plot making
import ROOT
stop_watch = ROOT.TStopwatch()
for chan in channels:
    for category in categories:
        compare_upsilon_reco(processor, 
                         sels, 
                         VARIABLES['tau_0_upsilon'], 
                         VARIABLES['recalc_tau_0_upsilon'], 
                         process_name=process_name, 
                         categories=category,
                         channels=chan)

stop_watch.Stop()
stop_watch.Print()

print('closing stores...')
close_stores(ztt_processes)
print('done')
