# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_region_comparison_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# retrieve physics processes
physicsProcesses = get_processes()
physicsProcesses = [p for p in physicsProcesses if 'Ztt' in p.name]

channels = ('1p0n_1p0n','1p0n_1p1n','1p1n_1p0n','1p1n_1p1n','1p1n_1pXn','1pXn_1p1n','1p1n_3p0n','3p0n_1p1n')
category = 'boost_loose_high'

### define your selection objects 
sels = get_selections(
    channels=channels,
    regions='SR',
    categories=(category+'_sr',category+'_zcr'),
    )

### define your list of variables
variables = [
    VARIABLES['phi_star_3bins'],
]

#### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)

### plot making
for var in variables:
    make_region_comparison_plot(processor, sels, var, process_name='Ztt', category=category)

print('closing stores...')
close_stores(physicsProcesses)
print('done')
