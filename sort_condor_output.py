import os
import itertools
import json

#with open('data/condor_queue.json') as f:
#    _QUEUE_DICT = json.load(f)

_QUEUES = [
    'espresso',
    'microcentury',
    'longlunch',
    'workday',
    'tomorrow',
]

def _rerun_cmd(job):
    args = [
        'python',
        'submit_workspace.py',
        '--year {}'.format(job.year),
        '--channel {}'.format(job.channel),
        '--process-group-name {}'.format(job.process),
        '--var-group-name {}'.format(job.variation),
    ]
    if job.variation != 'nominal':
        args += ['--skip-nominal']
    
    if job.region == 'zcr':
        args += ['--zcr']
    
    _queue_key = '_'.join([
        job.year,
        job.channel,
        job.region,
        job.variation,
        job.process])

    #if not _queue_key in _QUEUE_DICT.keys():
    #    print 'BOOM: {} is missing from the queue dict'.format(_queue_key)
    #else:
    #    _old_queue = _QUEUE_DICT[_queue_key]
    #    if 'aborted' in job.output['msg']['log']:
    #        _idx = _QUEUES.index(_old_queue)
    #        _queue = _QUEUES[_idx + 1]
    #        args += ['--forced-queue {}'.format(_queue)]

    args += ['--submit']
    return ' '.join(args)
    

def _wsi_output_name(outdir, years=None, channels=None, regions=None):

    if years != None:
        if not isinstance(years, (list, tuple)):
            _years = [years]
        else:
            _years = years
        _years_str = ''.join(_years).replace('_', '').replace(' ', '')
    else:
        _years_str = '15161718'

    if channels != None:
        if not isinstance(channels, (list, tuple)):
            _channels = [channels]
        else:
            _channels = channels
        _channels_str = ''.join(_channels)
    else:
        _channels_str = 'All'

    if regions != None:
        if not isinstance(regions, (list, tuple)):
            _regions = [regions]
        else:
            _regions = regions
        _regions_str = ''.join(regions)
    else:
        _regions_str = 'Allregions'

    from datetime import date
    _date = '{}.{}.{}'.format(
        date.today().year,        
        date.today().month,
        date.today().day)

    _file_name = 'wsi_{}_{}decays_{}_{}.root'.format(_years_str, _channels_str, _regions_str, _date)
    _name = os.path.join(outdir, _file_name)
    return _name

def _build_hadd_list(jobs):
    _hadd_list = []
    for _job in jobs:
        if not _job.corrupted:
            _path = os.path.join(
                _job.output['path'], 
                _job.output['dir'], 
                _job.output['rfile'])
            _hadd_list.append(_path)
        else:
            print('BOOM: corupted job {} -- ignore!'.format(_job))
    return _hadd_list

def _hadd(out_name, input_list, dry_run=False):
    import subprocess
    _input_str = ' '.join(input_list)
    _cmd = './MergeWorkspaceOutputs {} {}'.format(out_name, _input_str)
    if not dry_run:
        subprocess.call(_cmd, shell=True)
    else:
        print(out_name)
        for _i_f, _file in enumerate(input_list):
            print('{} / {} - {}'.format(_i_f + 1, len(input_list), _file.split('/')[-1]))





if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('condor_input_dir')
    parser.add_argument('--channel', default=None)
    parser.add_argument('--year', default=None)
    parser.add_argument('--zcr', default=False, action='store_true')
    parser.add_argument('--hadd', default=False, action='store_true')
    parser.add_argument('--dry', default=False, action='store_true')
    parser.add_argument('--outdir', default='./')
    parser.add_argument('--construct-dict', default=False, action='store_true')
    parser.add_argument('--verbose', default=False, action='store_true')
    args = parser.parse_args()

    from boom.extern.tabulate import tabulate
    from boom.batch import filter_jobs, expected_jobs, build_jobs

    if args.zcr:
        _region = 'zcr'
    else:
        _region = 'sr'
        
    _dir = args.condor_input_dir
    _outdir = args.outdir

    _all_jobs = build_jobs(_dir)
    _all_jobs = filter_jobs(_all_jobs, channel=args.channel, year=args.year, region=_region)

    _corrupted_jobs = filter_jobs(_all_jobs, corrupted=True)
    lines = []
    for _job in _corrupted_jobs:
        lines += [[
            _job.channel,
            _job.year,
            _job.region,
            _job.process,
            _job.variation,
            _job.corrupted,
            _job.done
            ]]

    print()
    print('BOOM: corrupted jobs')
    print()
    print(tabulate(lines, headers=['chan', 'year', 'reg', 'proc', 'var', 'corrupted', 'finished']))
    print()  
    
    if len(_corrupted_jobs) != 0:
        print('BOOM: Corruption Reports:')
        print('BOOM: ------------------')
        for _job in _corrupted_jobs:
            print('BOOM:', _job)
            print('BOOM: \t dir - ', _job.output['dir'])
            print('BOOM: \t err - ', _job.output['msg']['err'])
            print('BOOM: \t out - ', _job.output['msg']['out'])
            print('BOOM: \t log - ', _job.output['msg']['log'])
            print()

        print('BOOM: Rerun commands:')
        print('BOOM: --------------')
        for _job in _corrupted_jobs:
            print(_rerun_cmd(_job))

    missings = []
    _expected = expected_jobs(channel=args.channel, year=args.year, z_cr=args.zcr)
    for  _c, _y, _p, _v in _expected:
        _job = filter_jobs(_all_jobs, year=_y, channel=_c, process=_p, variation=_v, region=_region)
        _job = [j for j in _job if j.output['rfile'] != None]
        if len(_job) == 0:
            missings += [[_c, _y, _p, _v]]
    print()
    print('BOOM: missing jobs:')
    print()
    print(tabulate(missings, headers=['chan', 'year', 'proc', 'var']))
    print() 

    if args.hadd:
        _name = _wsi_output_name(_outdir, years=args.year, channels=args.channel, regions=_region)
        _hadd_list = _build_hadd_list(_all_jobs)
        _hadd(_name, _hadd_list, dry_run=args.dry)



    if args.construct_dict:
        _queue_dict = {}
        for _job in _all_jobs:
            if not _job.corrupted:
                _key = '_'.join([
                    _job.year,
                    _job.channel,
                    _job.region,
                    _job.variation,
                    _job.process])
                _queue_dict[_key] = _job.best_queue

        #import json
        #with open('./data/condor_queue.json', 'w') as fout:
        #    json.dump(_queue_dict, fout, indent=2)
