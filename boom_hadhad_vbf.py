# python imports
import os
import ROOT
from multiprocessing import cpu_count

#local imports
from boom.core import boom_processor, close_stores
from boom.plotting import make_data_mc_plot
from boom.selection_utils import get_selections
from boom.variables import VARIABLES
from boom.database import get_processes

# Switch on debug output
#import logging
#logging.root.setLevel( logging.DEBUG )

# retrieve physics processes
physicsProcesses = get_processes(squash=True)
# filter out unused samples
physicsProcesses = [p for p in physicsProcesses if 'theta_1' not in p.name]
physicsProcesses = [p for p in physicsProcesses if 'theta_2' not in p.name]
physicsProcesses = [p for p in physicsProcesses if 'theta_3' not in p.name]
physicsProcesses = [p for p in physicsProcesses if 'theta_4' not in p.name]
physicsProcesses = [p for p in physicsProcesses if 'theta_5' not in p.name]
physicsProcesses = [p for p in physicsProcesses if 'theta_6' not in p.name]
physicsProcesses = [p for p in physicsProcesses if 'theta_7' not in p.name]
physicsProcesses = [p for p in physicsProcesses if 'theta_8' not in p.name]

categories =( #'preselection',
              ## signal regions
              'vbf_sr',
              #'vbf_0_sr',
              #'vbf_1_sr',
              ## control regions
              'vbf_zcr',
              #'vbf_zcr_0',
              # 'vbf_zcr_1',
              ## optimised signal regions
              #'vbf_1_high_sr',
              #'vbf_1_low_sr',
             #'vbf_0_high_sr',
             #'vbf_0_low_sr',
#              'vbf_high_sr',
#              'vbf_low_sr',
#              'vbf_high_zcr',
#              'vbf_low_zcr',
            )

### define your selection objects 
sels = get_selections(
    channels=('1p0n_1p0n', '1p0n_1p1n', '1p1n_1p0n', '1p1n_1p1n', '1p1n_1pXn', '1p1n_3p0n', '1pXn_1p1n', '3p0n_1p1n', '1p0n_1pXn', '1pXn_1p0n'), 
    categories= categories,
    regions='SR')


# define your list of variables
variables = [
     #VARIABLES['phi_star_7bins'],
     #VARIABLES['phi_star_5bins'],
     #VARIABLES['norm'],
     ## kinematics for categorisation in vbf
     VARIABLES['jet_1_pt'],
     VARIABLES['mjj'],
     VARIABLES['jets_deta'],
     VARIABLES['n_jets_30'],
     VARIABLES['vbf_tagger'],
]

# #### processor declaration, booking and running
processor = boom_processor(physicsProcesses, sels, variables)
processor.book()
processor.run(n_cores=cpu_count() - 1)


# plot making
import ROOT
stop_watch = ROOT.TStopwatch()

force_unblind = True

# multiple categories plots
for var in variables:
     if 'mmc' in var.name or 'phi_star' in var.name : chisquare = False
     else: chisquare = False
         
     if 'mmc' in var.name : force_unblind = False
     if 'vbf_tagger' in var.name : force_unblind = False

     make_data_mc_plot(processor, sels, var, categories=('vbf_zcr','vbf_sr'), force_unblind=force_unblind, print_chisquare=chisquare)

for var in variables:  
     # single category plots
     for category in categories:
         if 'preselection' in category: unblind = True
         elif '_zcr' in category: unblind = True
         else:  unblind = False

         if 'mmc' in var.name or 'phi_star' in var.name : chisquare = False
         else: chisquare = False

         if 'mmc' in var.name : unblind = False

#         make_data_mc_plot(processor, sels, var, categories=category, force_unblind=unblind, print_chisquare=chisquare)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p0n_1p0n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p0n_1p1n','1p1n_1p0n'), force_unblind=unblind)
#         make_data_mc_plot(processor, sels, var, categories=category, channels=('1p0n_1p1n'), force_unblind=unblind)
#         make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_1p0n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p0n_1pXn','1pXn_1p0n'), force_unblind=unblind)
#         make_data_mc_plot(processor, sels, var, categories=category, channels=('1p0n_1pXn'), force_unblind=unblind)
#         make_data_mc_plot(processor, sels, var, categories=category, channels=('1pXn_1p0n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_1p1n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_1pXn','1pXn_1p1n'), force_unblind=unblind) 
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_1pXn'), force_unblind=unblind) 
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1pXn_1p1n'), force_unblind=unblind)
         # make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_3p0n','3p0n_1p1n'), force_unblind=unblind)
#         make_data_mc_plot(processor, sels, var, categories=category, channels=('3p0n_1p1n'), force_unblind=unblind)
#         make_data_mc_plot(processor, sels, var, categories=category, channels=('1p1n_3p0n'), force_unblind=unblind)

     # multiple categories plots
#     continue

#     make_data_mc_plot(processor, sels, var, categories=('boost_tight_high_zcr','boost_loose_high_zcr','boost_tight_low_zcr','boost_loose_low_zcr', 'boost_tight_high_sr','boost_loose_high_sr','boost_tight_low_sr','boost_loose_low_sr'))
#     make_data_mc_plot(processor, sels, var, categories=('boost_tight_high_zcr','boost_tight_low_zcr', 'boost_tight_high_sr','boost_tight_low_sr'))
#     make_data_mc_plot(processor, sels, var, categories=('boost_loose_high_zcr','boost_loose_low_zcr', 'boost_loose_high_sr','boost_loose_low_sr'))
#     make_data_mc_plot(processor, sels, var, categories=('vbf_high_zcr','vbf_low_zcr','vbf_high_sr','vbf_low_sr'))

#     make_data_mc_plot(processor, sels, var, categories=('boost_tight_high_zcr','boost_tight_high_sr'))
#     make_data_mc_plot(processor, sels, var, categories=('boost_tight_low_zcr','boost_tight_low_sr'))
#     make_data_mc_plot(processor, sels, var, categories=('boost_loose_high_zcr','boost_loose_high_sr'))
#     make_data_mc_plot(processor, sels, var, categories=('boost_loose_low_zcr','boost_loose_low_sr'))
#     make_data_mc_plot(processor, sels, var, categories=('boost_tight_high_zcr','boost_loose_high_zcr', 'boost_tight_high_sr','boost_loose_high_sr' ))
#     make_data_mc_plot(processor, sels, var, categories=('boost_tight_low_zcr','boost_loose_low_zcr', 'boost_tight_low_sr','boost_loose_low_sr' ))
     
#     make_data_mc_plot(processor, sels, var, categories=('vbf_high_zcr','vbf_high_sr'))
#     make_data_mc_plot(processor, sels, var, categories=('vbf_low_zcr','vbf_low_sr'))


stop_watch.Stop()
stop_watch.Print()

print('closing stores...')
close_stores(physicsProcesses)
print('done')
